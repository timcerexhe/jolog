%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Written by: Timothy Cerexhe and Maurice Pagnucco
% REVISED: Januuary 23, 2009
% TESTED: ECLiPSe 6.0 under Mac OS X 10.5
%         SWI Prolog 5.6.57 under Mac OS X 10.5
%         SWI Prolog 5.6.57 under Windows XP
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% This is an AVIE/Golog Agent for the Scenario Iteration 5: Blockworld.
%
% This file defines the following predicates describing the interface
% to the outside world:
% -- prim_fluent(?Fluent): for each primitive fluent
% -- prim_action(?Action): for each primitive action
% -- exog_action(?Action): for each exogenous action
% -- senses(+Action, +Fluent): for each sensing action
% -- poss(+Action, +Condition): when Condition, Action is executable
% -- initially(+Fluent, +Value): Fluent has Value in S0, the initial situation
% -- causes_val(+Action, +Fluent, +Value, +Cond): when Cond holds, doing
%      Action causes Fluent to have Value
% -- proc(+Name, +Program): Golog complex action. It consists of a program
%      Name and a Program to be executed
% -- execute(+Action, +History, -SensingResult): do the Action, return
%      the SensingResult. The History is passed for diagnostic or other
%      use. The result is ignored unless action is a sensing action
% -- exog_occurs(-ActionList): return a list of exog_actions
%      that have occurred since the last time it was called
%      The predicate always succeeds returning [] when there are none
% -- initialize: run at start of programs
% -- finalize: run at end of programs
%
% The following predicates are assumed to be provided by the implementation
% independent Prolog/AVIE communication predicates in comm.pl:
% -- initializeAVIE: prepare the AVIE for reading
% -- finalizeAVIE: finished with AVIE for reading and writing
% -- sendAVIEActionNumber(+Num, -Result): send the action number Num
%      to AVIE, and return the value Result
% -- receiveAVIEActionNumber(-Actions): receive 0 or 1 action numbers in
%      list Actions from AVIE. Fail if no reply from AVIE
%
% The following predicates are assumed to be defined in the main
% program file (as they may contain Prolog specific code):
% -- initializeExog: perform any initialization of other sources of
%      exogenous actions that is required
% -- finalizeExog: things to do for other sources of exogenous actions
%      at end of program
% -- checkOtherExog(-ExogList): check whether a request has been
%      entered via keyboard
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DECLARATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- dynamic(charList/1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% senses(+Action, +Fluent): Action senses the value of Fluent
senses(sPos(Name), location(char, Name)).	% sPos(name)
senses(sPos(Id,Type), location(Type, Id)).	% sPos(id,type)

% prim_action(?Action): Action is a primitive action and it can be executed
% by AVIE
prim_action(goto(_,_,_)).	% goto(id, x, y)
prim_action(goto(_, _)).	% goto(id, name)
prim_action(turn(_,_,_)).  	% turn(id, x, z)
prim_action(turn(_,_)).  	% turn(id, name)
prim_action(look(_,_,_,_)).	% look(id, x, y, z)
prim_action(look(_,_)).		% look(id, name)

prim_action(sPos(_)).
prim_action(sPos(_,_)).

prim_action(play(id,generic_celebrate01)).
prim_action(play(id,generic_laughing01)).
prim_action(play(id,generic_beckon01)).
prim_action(play(id,generic_shoo01)).

% exog_action(?Action): Actions is an exogenous action and its occurrence
%     can be reported to Golog
exog_action(arrive(_,_,_)).	% arrive(id, x, z) - id arrives at x, z
exog_action(arrive(_,_)).	% arrive(id, name) - id arrives at name
exog_action(played(_,_)).	% played(id, name) - animation name completed
exog_action(enterReal(_,_,_)).	% enterReal(id, x, z)
exog_action(enterVirtual(_,_,_)).	% enterVirtual(id, x, z)
exog_action(enterBlock(_,_,_)).
exog_action(knockedOver(_)).	% knockedOver(id) - weapon strikes character
exog_action(gotUp(_)).
exog_action(charLostBlock(_)).	% charLostBlock(id) - when character's block falls into the correct hole
exog_action(destroyedBlock(_)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Fluents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prim_fluent(?Fluent): Fluent is a primitive fluent

prim_fluent(dummy).
% location(type, id)
prim_fluent(location(char, Id)) :- charList(L), member(Id, L). %objectType(Type), validId(Id).
%prim_fluent(location(char, _)).
prim_fluent(location(block, Id)) :- validId(Id).

% direction(type, id)? - NOT YET
%prim_fluent(direction(Type, Id)) :- objectType(Type), validId(Id).

%status(type, id) (kicking, helping, moving, down) when we get control when helping
%prim_fluent(status(Type, Id)) :- objectType(Type), validId(Id).
prim_fluent(status(char, Id)) :- charList(L), member(Id, L). 
%prim_fluent(status(char, _)). 

charList([3]).

% statusType(?T): T is a status type
statusType(T) :-
    T = kicking; T = helping; T = moving; T = down.

% objectType(?T): T is an object type
objectType(T) :-
    T = block; T = char; T = hole.
%    T = block; T = person; T = char; T = hole; T = weapon; T = cursor.

% validId(?I): I is a character id
validId(I) :-
    I = 0; I = 1; I = 2.

add(X, L, L) :- member(X, L), !.
add(X, L1, L2) :- append([X], L1, L2).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Causal laws
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% causes_val(+Action, +Fluent, +Value, +Cond): When Cond holds, doing
%     Action causes Fluent to have Value
causes_val(charLostBlock(Id), status(char, Id), helping, true).
causes_val(charLostBlock(Id), location(hole, Id), err, true).
causes_val(knockedOver(Id), status(char, Id), down, neg(status(char,Id)=kicking)).
causes_val(gotUp(Id), status(char, Id), helping, status(char,Id)=down).
causes_val(goto(Id, _, _), status(char, Id), moving, true).
causes_val(play(Id, _), status(char, Id), moving, true).
causes_val(played(Id, _), status(char, Id), helping, status(char,Id)=moving).
causes_val(arrive(Id, _, _), status(char, Id), helping, status(char,Id)=moving).
causes_val(arrive(Id, X, Z), location(char, Id), pos(X, Z), status(char,Id)=moving).

causes_val(enterVirtual(Id,_,_), dummy, Id, fiddleCharList(Id)).
causes_val(enterVirtual(Id,X,Z), location(char,Id), pos(X,Z), true).
causes_val(enterBlock(Id,X,Z), location(block,Id), pos(X,Z), true).
causes_val(destroyedBlock(Id), location(block,Id), err, true).

%causes_val(turnaround, direction, D, D is (-1) * direction).
%causes_val(arrive_at_station, location, N, N is location + direction).
%causes_val(request_delivery(N, M), delivery_requested(N), M, true).
%causes_val(push_go_button, holding_delivery_for(M), 1,
%    delivery_requested(location) = M).

fiddleCharList(Id) :- write('FIDDLE!'),nl, charList(L),retract(charList(_)), add(Id,L,L2), assert(charList(L2)). % :- write('FIDDLE!'),nl,get0(_),charList(L), retract(charList(_)), add(Id, L, L1), assert(charList(L1)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Preconditions of prim actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% poss(+Action, +Condition): When Condition is true, Action is possible
poss(goto(Id,_,_), status(char, Id) = helping).
poss(goto(Id,_), status(char, Id) = helping).
poss(turn(Id,_,_), status(char, Id) = helping).
poss(turn(Id,_), status(char, Id) = helping).
poss(look(Id,_,_,_), status(char, Id) = helping).
poss(look(Id,_), status(char, Id) = helping).

poss(sPos(_), true).
%poss(sPos(_,_), true).
poss(sPos(Id,Type),true) :- objectType(Type), validId(Id).

poss(play(Id,_), status(char, Id) = helping).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Initial state
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initially(+Fluent, +Value): Fluent has Value at S0, the initial situation
initially(dummy, na).
%initially(location(char, Id), err) :- charList(L), member(Id,L).
initially(location(block, Id), err) :- validId(Id).
initially(status(char, _), kicking). % :- charList(L), member(Id, L).
%initially(direction(Type, Id), 1) :- objectType(Type), validId(Id).
%initially(status(Type, Id), kicking) :- objectType(Type), validId(Id).
initially(location(char, _), err).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Golog PROGRAM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions of complex conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% proc(+Name, +Program): A user-defined procedure called Name with
%     Program body

proc(p1, while(true, if(status(char,0)=helping,goto(0,3,5),wait))).

proc(p2, while(true, if(status(char,X)=helping,goto(X,3,5),wait))).

proc(p3, [goto(0,3,5), pi(b, [?(location(block, b)=pos(0,0)), goto(0,0,0)])]).

proc(p4, [goto(0,3,5), pi(b, [?(direction(block, b)=1), goto(0,0,0)])]).

proc(p5, [goto(0,3,5), pi(b, goto(b,0,0))]). % WORKS

proc(p51, pi(b, goto(b,0,0))). % WORKS

proc(p6, [goto(0,3,5), pi(b, [?(some(x, some(y, (location(block, b)=pos(x,y))))), goto(1,0,0)])]).

proc(p7, [goto(0,3,5), pi(b, pi(x, pi(y, [?(location(block, b) = pos(x, y)), goto(b, x, y)])))]).

proc(p8, [goto(0,3,5), pi(b, [?(location(block, b) = pos(X, Y)), goto(b, X, Y)])]).

proc(p9, [?(location(block, b) = pos(X, Y)), goto(b, X, Y)]).

proc(p10, pi(b, if(location(block, b) = pos(X, Y), goto(b, X, Y), wait))).

proc(p11, pi(b, pi(x, pi(y, if(location(block, b) = pos(x, y), goto(b, x, y), wait))))).

proc(p12, ?(location(block, b) = pos(X, Y))). % WORKS??

proc(p13, [?(location(block, 0) = pos(X, Y)), goto(1, 3 ,5)]). %WORKS

proc(p14, [?(location(block, 0) = pos(X, Y)), goto(1, X ,Y)]). %WORKS

proc(p15, [?(location(block, 0) = pos(0, 0)), goto(1, 3 ,5)]). %WORKS

proc(p16, [?(true), goto(1, 3 ,5)]). %WORKS

proc(control, while(true, ndet(pi(b, [?(and(objectType(block, b),
                                        location(block, b) = pos(X, Y))),
                                      goto(b, X, Y)
                                     ]),
                               wait))).

proc(p17, while(true, ndet(
                         pi(c, [
                            ?(status(char,c) == helping),
%                            play(c, generic_celebrate01),
                            say('char'),say(c),
                            pi(b, 
                               pi(x,
                                  pi(y, [
                                     ?(location(block, b) = pos(_,_)),
                                     sPos(b,block),
                                     say('block'),say(b),
                                     ?(location(block, b) = pos(x,y)),
                                     say([x,y]),
                                     goto(c, x, y)
                                  ])
                               )
                            )
                         ]), wait)
                      )).

proc(p18, while(true, ndet(pi([c,b,x,y], [?(status(char,c)=helping), ?(location(block, b) = pos(x,y)), goto(c, x, y)]), wait))).

proc(p19, while(true, ndet(
                         [?(status(char,C) = helping),?(location(block,B) = pos(X,Y)),goto(C,X,Y)],
                         wait))).

proc(p20, while(true, ndet(check,wait))).
proc(check, pi(c, [?(status(char,c)=helping),kickAnyBlock(c)])).
proc(kickAnyBlock(Char), pi(b, pi(x, pi(y, [
                            ?(status(char,Char)=helping),
                            sPos(b, block),
                            ?(location(block,b)=pos(x,y)),
                            goto(Char,x,y)
                         ])))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

proc(p21, while(true, [say('START'),senseAll, say('DONE SENSING'), ndet(checkMate,[say('GONNA WAIT'), wait]), say('END')])).
proc(checkMate, pi(c, [?(status(char,c)=helping),?(location(char,c)=pos(_,_)),kickClosestBlock(c)])).
proc(kickClosestBlock(Char), [write('CHECKING CLOSEST BLOCK (char='),write(Char),say(') ...'),
                              ?(status(char,Char)=helping),
                              ?(location(char,Char)=pos(CX,CY)),
                              write('char '),write(Char),write(' at pos('),write(CX),write(', '),write(CY),say(')'),
                              findClosest(block, pos(CX,CY), pos(X,Y)),
                              goto(Char,X,Y)]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

proc(p22, while(true, [senseAll, ndet(moveSomeone, wait)])).
proc(moveSomeone, pi(c, [ ?(status(char,c)=helping),
                          ?(location(char,c)=pos(CX,CY)),
                          gotoBest(c, pos(CX,CY))])).

proc(gotoBest(Char, pos(CX,CY)),      [ if(some(i, location(block,i)=pos(_,_)), [
                                           findClosest(block,pos(CX,CY),pos(BX,BY)),
                                           distance(pos(CX,CY),pos(BX,BY),DB)
                                        ], %else
                                           ?(DB='infinity')
                                        ),
                                        if(some(i, location(hole,i)=pos(_,_)), [
                                           findClosest(hole,pos(CX,CY),pos(HX,HY)),
                                           distance(pos(CX,CY),pos(HX,HY),DH)
                                        ], %else
                                           ?(DH='infinity')
                                        ),
                                        moveChar(Char,DB,pos(BX,BY),DH,pos(HX,HY))]).

proc(moveChar(Char,DB,pos(BX,BY),DH,pos(HX,HY)), [ write('time to move char '),write(Char),write(' to box @ pos('),write(BX),write(', '),write(BY),write(') (dist='),write(DB),write(') or hole @ pos('),write(HX),write(', '),write(HY),write(') (dist='),write(DH),say(') ...'),
                                        if(and(neg(DH='infinity'), DH > 9), %goto hole if we are further than 3m away
                                           goto(Char, HX, HY),
                                        if(neg(DB='infinity'),
%                                        if(and(neg(DB='infinity'), DB < 9), %no holes/close enough, then try for a CLOSE box
                                           goto(Char, BX, BY),
                                        %else
                                           []))]). %nothing to do!


proc(p23, while(neg(dummy=5), wait)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%sense all blocks, characters and holes in a consecutive range from by validId (currently [0,8])
proc(senseAll, senseStuff(0)).
proc(senseStuff(I), [ if(location(block, I)=pos(_,_), %block exists (has location)
                         sPos(I,block), []),
                      if(neg(status(char,I)=kicking), %person under our control
                         sPos(I,char),
                      %else, char isn't free (kicking), so make sure we know where their hole is
                         if(and(location(char,I)=pos(_,_), neg(location(hole,I)=pos(_,_))), %char exists but hole doesn't
                            sPos(I,hole), []) %so we need to update!
                      ),
                      if(validId(I2), %recurse!
                         senseStuff(I2), [])
                    ]) :- I2 is I+1.

proc(distance(pos(X1,Y1),pos(X2,Y2),D), []) :- D is ((X1-X2)**2+(Y1-Y2)**2).

%find the closest object of Type to pos(CX,CY) and stores its position in pos(TX,TY)
%NB. you *should* check that objects of that type actually exist first to avoid problems with backtracking ...
proc(findClosest(Type,pos(CX,CY),pos(TX,TY)), pi(id, pi(x, pi(y, [
                                                 ?(location(Type,id)=pos(x,y)),
                                                 if(neg(somethingCloser(Type,pos(CX,CY),pos(x,y))),
                                                    ?(location(Type,id)=pos(TX,TY)),
                                                 %else
                                                    neg(true))
                                              ])))).

%complex proc: checks whether there is some object of Type closer to pos(CX,CY) than pos(X1,Y1)
proc(somethingCloser(Type,pos(CX,CY),pos(X1,Y1)), some(id, some(x2, some(y2,
                                                     and(location(Type,id)=pos(x2,y2),
                                                        (((CX-X1)**2+(CY-Y1)**2) > ((CX-x2)**2+(CY-y2)**2))
                                                     )
                                                  )))).

proc(closer(infinity, infinity), neg(true)).
proc(closer(infinity, _), neg(true)).
proc(closer(_, infinity), true).
proc(closer(D1,D2), (D1<D2)).

proc(write(X), []) :- write(X).
proc(say(X), []) :- write(X),nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions of complex actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Main Routine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HOOKS TO comm.pl CODE
%  This section provides predicates for executing actions from the IndiGolog
%  interpreter and returning exogenous actions detected by AVIE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialize: Perform any application dependent initialization
initialize :-
    initializeAVIE,
    initializeExog.

% finalize: Application dependent wrap-up
finalize :-
    finalizeAVIE,
    finalizeExog.

% execute(+Action, +History, -SensingResult): Execute Action on AVIE returning
%     SensingResult. Current action History is supplied for debugging
%     purposes in case something goes wrong
execute(Action, History, SensingResult) :-
    write('Executing action: '), write(Action), nl,
    sendAVIEAction(Action, Sr),
%    write('    Sensing result: '), write(Sr), nl, nl,
    (Sr =.. [_, SensingResult]; Sr = SensingResult),
    write('    Sensing result: '), write(SensingResult), nl, nl.
    
%errorRecoveryData(History).
    %debugAVIE(Action, History, SensingResult).

% If previous clause fails, call user-defined debug routine.
% The debug routine can reattempt the action returning SensingResult
%execute(Action, History, SensingResult) :-
    %debugAVIE(Action, History, SensingResult).

% exog_occurs(-ExogList): Check for occurrence of exogenous actions and
%     return them in list ExogList. Exogenous actions can occur at
%     various sources: here we check just the AVIE

exog_occurs(ExogList) :-
    write('Checking exogenous actions: '), nl,
    flush_output,
    sendAVIEAction('exog(0)', ExogList),
    write(ExogList), nl, write('Done'), nl.

%%exog_occurs(ExogList) :-
%%    checkAVIEExog(AVIEExogList),
%%    (AVIEExogList == [] -> true;
%%        (write('    AVIE exogenous action: '), write(AVIEExogList), nl, nl)),
%%    checkOtherExog(OtherExogList),
%%    (OtherExogList == [] -> true;
%%        (write('    Other exogenous action: '), write(OtherExogList), nl, nl)),
%%    append(AVIEExogList, OtherExogList, ExogList).

% checkAVIEExog(-AVIEExogList): Check for occurrence of exogenous actions
%     at AVIE. At present AVIE can only report one exogenous action at a time
%checkAVIEExog([Action]) :-
%    receiveAVIEActionNumber([N]),
%    actionNum(Action, N),
%    write('Exogenous action: *'), write(Action), write('* has occurred'), nl, !.
%
%checkAVIEExog([]).    % No exogenous action from AVIE

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use the following for testing independent of comm.pl.
% Uncomment to run.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%execute(Action, _, SensingResult) :-
%%    write('Executing action: '), write(Action), nl,
%%    (senses(Action, _) ->
%%        (write('[Enter Sensing value, terminate with "."]: '),
%%             read(SensingResult));
%%        nl),
%%    write('    Sensing result: '), write(SensingResult), nl, nl.
%%
%%% If previous clause fails, call user-defined debug routine
%%execute(Action, History, SensingResult) :-
%%    debugAVIE(Action, History, SensingResult).
%%
%%% It is probably better to do this by only checking for exogenous actions
%%%    from the keyboard but, if you want to go through the process slowly,
%%%    do it this way!
%%
%%exog_occurs(ExogList) :-
%%    write('Enter list (possibly empty) of exogenous actions: '),
%%    read(TempExogList),
%%    ((TempExogList = [_|_] ; TempExogList == []) ->
%%        (ExogList=TempExogList,
%%            write('    Exogenous actions: *'), write(ExogList),
%%            write('* have occurred'), nl, nl);
%%        (write('ERROR: You must enter a list (possibly empty) of actions'),nl,
%%            exog_occurs(ExogList))
%%    ).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG ROUTINES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% debugAVIE(+Action, +History, -SensingResult):
%     This predicate attempts to provide some basic debug and error recovery.
%     If you can't be bothered, uncomment the following clause and
%     the program will abort on failure to execute an action
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
debugAVIE(Action, History, SensingResult) :-
    write('** DEBUG ...'), nl,
    errorRecoveryData(History),
    errorRecoveryProc,
    execute(Action, History, SensingResult). % Try action again

% errorRecoveryData(+History): Extract values of primitive fluents at
%     the point where Hist actions are performed.
errorRecoveryData(History) :-
    write('    Actions performed so far: '),
    write(History), nl,
    bagof(U, prim_fluent(U), FluentList),
    printFluentValues(FluentList, History).

% printFluentValues(+FluentList, +History): Print value of primitive fluents
%     at the point where History actions have been performed
printFluentValues([], _) :-
    write('-----------------------------------------------'),
    nl.

printFluentValues([Hf | FluentList], History) :-
    write('    Primitive fluent '),
    write(Hf),
    write(' has value '),
    has_val(Hf, Hv, History),
    write(Hv), nl,
    printFluentValues(FluentList, History).

% errorRecoveryProc: What to do in case of error.
errorRecoveryProc:-
    write('If you wish to abort, enter "a".'), nl,
    get0(Val),
    get0(_),                     % Clear carriage return
    (Val == 65; Val == 97) ->    % 65 is ASCII 'A', 97 is ASCII 'a'
         abort;
         true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EOF: Agent/blockworld.pl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
