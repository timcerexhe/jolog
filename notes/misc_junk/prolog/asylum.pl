%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Written by: Timothy Cerexhe and Maurice Pagnucco
% REVISED: Januuary 23, 2009
% TESTED: ECLiPSe 6.0 under Mac OS X 10.5
%         SWI Prolog 5.6.57 under Mac OS X 10.5
%         SWI Prolog 5.6.57 under Windows XP
%         SWI Prolog 5.6.62 under Vista
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% This is an AVIE/Golog Agent for Scenario Iteration 6: Asylum.
%
% This file defines the following predicates describing the interface
% to the outside world:
% -- prim_fluent(?Fluent): for each primitive fluent
% -- prim_action(?Action): for each primitive action
% -- exog_action(?Action): for each exogenous action
% -- senses(+Action, +Fluent): for each sensing action
% -- poss(+Action, +Condition): when Condition, Action is executable
% -- initially(+Fluent, +Value): Fluent has Value in S0, the initial situation
% -- causes_val(+Action, +Fluent, +Value, +Cond): when Cond holds, doing
%      Action causes Fluent to have Value
% -- proc(+Name, +Program): Golog complex action. It consists of a program
%      Name and a Program to be executed
% -- execute(+Action, +History, -SensingResult): do the Action, return
%      the SensingResult. The History is passed for diagnostic or other
%      use. The result is ignored unless action is a sensing action
% -- exog_occurs(-ActionList): return a list of exog_actions
%      that have occurred since the last time it was called
%      The predicate always succeeds returning [] when there are none
% -- initialize: run at start of programs
% -- finalize: run at end of programs
%
% The following predicates are assumed to be provided by the implementation
% independent Prolog/AVIE communication predicates in comm.pl:
% -- initializeAVIE: prepare the AVIE for reading
% -- finalizeAVIE: finished with AVIE for reading and writing
% -- sendAVIEActionNumber(+Num, -Result): send the action number Num
%      to AVIE, and return the value Result
% -- receiveAVIEActionNumber(-Actions): receive 0 or 1 action numbers in
%      list Actions from AVIE. Fail if no reply from AVIE
%
% The following predicates are assumed to be defined in the main
% program file (as they may contain Prolog specific code):
% -- initializeExog: perform any initialization of other sources of
%      exogenous actions that is required
% -- finalizeExog: things to do for other sources of exogenous actions
%      at end of program
% -- checkOtherExog(-ExogList): check whether a request has been
%      entered via keyboard
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DECLARATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- dynamic(shadowList/1, antiShadowList/1, pieceList/1, antiPieceList/1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% senses(+Action, +Fluent): Action senses the value of Fluent
senses(sPos(_, Name), location(Name)) :- Name = door ; Name = antidoor.
%senses(sPos(Id,char), location(shadow, Id)) :- Id < 5.
senses(sPos(Id,char), location(antishadow, Id)) :- Id >= 5.
%senses(sPos(Id,piece), location(piece, Id)).
senses(sPos(Id,antipiece), location(antipiece, Id)).

% prim_action(?Action): Action is a primitive action and it can be executed
% by AVIE
prim_action(goto(_,_,_)).      % goto(id, x, y)
prim_action(goto(_, _)).       % goto(id, name)
prim_action(turn(_,_,_)).      % turn(id, x, z)
prim_action(turn(_,_)).  	    % turn(id, name)
prim_action(look(_,_,_,_)).	 % look(id, x, y, z)
prim_action(look(_,_)).		    % look(id, name)

prim_action(pickupAntiPiece(_,_)).
prim_action(placeAntiPiece(_,_)).

prim_action(sPos(_,_)).

prim_action(play(_Id,Animation)) :- animations(_Type, List), member(Animation, List).

animations(beckon, [generic_beckon01]).
animations(laugh, [generic_laughing01, generic_laughing02, generic_laughing03]).
animations(suspense, [generic_suspense01, generic_suspense02]).
animations(grieve, [generic_grief01, generic_grief02]).
animations(celebrate, [generic_celebrate01, generic_celebrate02, generic_celebrate03]).
animations(shoo, [generic_shoo01]).
animations(shout, [generic_shouting01, generic_shouting02]).
animations(talk, [generic_talking01, generic_talking02, generic_talking03]).
animations(shield, [generic_shieldLow01, generic_shieldLow02]).

emotionalResponses(happy, [laugh, talk, celebrate]).
emotionalResponses(sad, [grieve, shield]).
emotionalResponses(tired, []) :- printf("THIS SHOULD NEVER HAPPEN! Checking emotional responses for tired\n", []).
emotionalResponses(angry, [shout, shoo]).
emotionalResponses(playful, [beckon, suspense]).


pseudo_action(goRandom(_)). %set antishadow AS's status to random
pseudo_action(goBuild(_)).
pseudo_action(feel(_,_)).   %feel(id, emotion) - changes antishadow id's emotion

% exog_action(?Action): Action is an exogenous action and its occurrence can be reported to Golog
exog_action(overToYou).              % we are given control!
exog_action(arrive(_,_,_)).          % arrive(id, x, z) - id arrives at x, z
exog_action(arrive(_,_)).            % arrive(id, name) - id arrives at name
exog_action(played(_,_)).            % played(id, name) - animation name completed
exog_action(turned(_)).

%exog_action(enterShadow(_,_,_)).     % enterShadow(id, x, z)
exog_action(enterAntiShadow(_,_,_)). % enterAntiShadow(id, x, z)
%exog_action(enterPiece(_,_,_)).      % enterPiece(id, x, z)
exog_action(enterAntiPiece(_,_,_)).  % enterAntiPiece(id, x, z)

exog_action(pickedupAntiPiece(_,_)). % pickedupAntiPiece(antishadowId, antipieceId)
exog_action(placedAntiPiece(_,_)).   % placedAntiPiece(antishadowId, antipieceId)
%exog_action(placedPiece(_,_)).       % placedPiece(shadowId, pieceId)

exog_action(time(_)).                % time(percentRemaining)
exog_action(nop).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Fluents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prim_fluent(?Fluent): Fluent is a primitive fluent
prim_fluent(time).
prim_fluent(location(door)).
prim_fluent(location(antidoor)).
%prim_fluent(shadowScore).
prim_fluent(antiShadowScore).
%prim_fluent(shadowList).
prim_fluent(antiShadowList).
%prim_fluent(pieceList).
prim_fluent(antiPieceList).

%dynamic fluents can be created at runtime!
%dyn_fluent(location(shadow, _)).
dyn_fluent(location(antishadow, _)).
%dyn_fluent(location(piece, _)).
dyn_fluent(location(antipiece, _)).
dyn_fluent(status(_)).
dyn_fluent(emotion(_)).
dyn_fluent(holding(_)).

% statusType(?T): T is a status type
statusType(T) :-
    T = random; T = face_crowd; T = turning; T = preperformance; T = playing; T = build; T = deliver.

% objectType(?T): T is an object type
objectType(T) :-
    T = shadow; T = antishadow; T = piece; T = antipiece.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Causal laws
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% causes_val(+Action, +Fluent, +Value, +Cond): When Cond holds, doing
%     Action causes Fluent to have Value
causes_val(time(Time), time, Time, true). %update the global time
causes_val(overToYou, status(_), random, true). %overToYou no longer required, we keep it in for "extra functionality"!
causes_val(feel(Id,E), emotion(Id), E, member(Id, antiShadowList)). %feeling something changes your emotion
causes_val(goRandom(Id), status(Id), random, member(Id, antiShadowList)). %allow antishadow to change into random walk mode
causes_val(goBuild(Id), status(Id), build, member(Id, antiShadowList)). %allow antishadow to start building

causes_val(play(Id, _), status(Id), playing, member(Id, antiShadowList)). %when we play, our mode enters a blocking "playing" mode
causes_val(played(Id, _), status(Id), random, and(status(Id)=playing, member(Id, antiShadowList))). %when finished playing we go random again

%if we goto while building, then we have a mission, so enter blocking "deliver" mode
causes_val(goto(Id,_,_), status(Id), deliver, and(status(Id)=build, member(Id, antiShadowList))).
%if we arrive while delivering, then we have a mission, so go back into build mode
causes_val(arrive(Id,_,_), status(Id), build, and(status(Id)=deliver, member(Id, antiShadowList))).
%update location when you arrive somewhere
%causes_val(arrive(Id,X,Z), location(shadow, Id), pos(X, Z), member(Id, shadowList)).
causes_val(arrive(Id,X,Z), location(antishadow, Id), pos(X, Z), member(Id, antiShadowList)).

causes_val(turn(Id,_,_), status(Id), turning, true).
causes_val(turned(Id), status(Id), preperformance, status(Id) = turning).

%causes_val(enterShadow(Id,_,_), shadowList, New, New = [Id|shadowList]). %add shadow to shadowList
%causes_val(enterShadow(Id,X,Z), location(shadow, Id), pos(X,Z), true). %store position of new shadow

causes_val(enterAntiShadow(Id,_,_), antiShadowList, New, New = [Id|antiShadowList]). %add antishadow to antiShadowList
causes_val(enterAntiShadow(Id,X,Z), location(antishadow,Id), pos(X,Z), true). %and store various fluents for new antishadow
causes_val(enterAntiShadow(Id,_,_), status(Id), random, true).
causes_val(enterAntiShadow(Id,_,_), emotion(Id), E, and(emotionList(L), randomElement(L, E))).
causes_val(enterAntiShadow(Id,_,_), holding(Id), err, true).

%causes_val(enterPiece(Id,_,_), pieceList, New, New = [Id|pieceList]). %add piece to pieceList
%causes_val(enterPiece(Id,X,Z), location(piece, Id), pos(X,Z), true). %store position of new piece
causes_val(enterAntiPiece(Id,_,_), antiPieceList, New, New = [Id|antiPieceList]). %add antipiece to antiPieceList
causes_val(enterAntiPiece(Id,X,Z), location(antipiece, Id), pos(X,Z), true). %store position of new antipiece

%causes_val(pickupAntiPiece(_,Id), location(antipiece, Id), reserve, location(antipiece, Id) = pos(_,_)). %reserve piece!
causes_val(pickupAntiPiece(Id,_), status(Id), deliver, member(Id, antiShadowList)). %block (deliver) while trying to pickup

causes_val(pickedupAntiPiece(AntishadowId, AntipieceId), holding(AntishadowId), AntipieceId, true). %successfully holding antipiece
causes_val(pickedupAntiPiece(Id,_), status(Id), build, member(Id, antiShadowList)). %go back into build (from deliver) to continue

causes_val(placeAntiPiece(Id,_), status(Id), deliver, neg(holding(Id) = err)). %block (deliver) while trying to place

%causes_val(placedPiece(_,_), shadowScore, S2, and(shadowScore = S1, S2 is S1+1)). %increase shadow score
%causes_val(placedPiece(_,Id), location(piece, Id), err, true). %piece is gone

causes_val(placedAntiPiece(_,_), status(Id), face_crowd, member(Id, antiShadowList)). %start playing
causes_val(placedAntiPiece(_,Id), location(antipiece,Id), err, member(Id,antiPieceList)). %antipiece is gone
causes_val(placedAntiPiece(Id,_), holding(Id), err, member(Id,antiShadowList)). %we aren't holding anything anymore
causes_val(placedAntiPiece(_,_), antiShadowScore, S2, and(antiShadowScore = S1, S2 is S1+1)). %increase antishadow score

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Preconditions of prim actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% poss(+Action, +Condition): When Condition is true, Action is possible
%poss(_, true). %anything is possible!
poss(overToYou, true).
poss(goto(Id,_,_), or(status(Id) = random, status(Id) = build)).
poss(goto(Id,_), or(status(Id) = random, status(Id) = build)).
poss(turn(_Id,_,_), true).
poss(turn(_Id,_), true).
poss(look(_Id,_,_,_), true).
poss(look(_Id,_), true).

%poss(sPos(_,_), true).
poss(sPos(_, door), true).
poss(sPos(_, antidoor), true).
poss(sPos(Id,_), location(_, Id) = pos(_,_)).

%member(Id, shadowList)).
%poss(sPos(Id,char), member(Id, antiShadowList)).
%poss(sPos(Id,piece), member(Id, pieceList)).
%poss(sPos(Id,antipiece), member(Id, antiPieceList)).

poss(enterAntiShadow(_,_,_), true).
poss(pickupAntiPiece(_,_), true).
poss(pickedupAntiPiece(_,_), true).
poss(placeAntiPiece(_,_), true).
poss(placedAntiPiece(_,_), true).

poss(play(Id,_), neg(status(Id) = playing)).

poss(feel(_,_), true).
poss(goBuild(_), true).
poss(goRandom(_), true).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Initial state
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initially(+Fluent, +Value): Fluent has Value at S0, the initial situation
initially(time, 100).
initially(location(_), err).
initially(shadowScore, 0).
initially(antiShadowScore, 0).
%initially(shadowList, []).
initially(antiShadowList, []).
%initially(pieceList, []).
initially(antiPieceList, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Golog PROGRAM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions of complex conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% proc(+Name, +Program): A user-defined procedure called Name with Program body
getElement(Index, List, _Element) :- length(List, Length), ((Index >= Length) -> !), fail.
getElement(0, [H|_], H).
getElement(I1, [_|T], Element) :- I1 > 0, I2 is I1-1, getElement(I2, T, Element).

emotionList(L) :- L = [happy, sad, tired, angry, playful].

randomElement(List, Element) :-
   %printf("finding a random element in list %w\n", [List]),
   length(List, Length),
   Length > 0,
   Num is random(Length), !,
   getElement(Num, List, Element). %, !, printf("find random element = %w in list %w\n", [Element, List]).

proc(changeEmotion(Char), pi(emo, pi(list, pi(newEmo, [
                             ?(emo=emotion(Char)),
                             emotionTransList(emo, list),
                             randomElement(list, newEmo),
                             feel(Char, newEmo)
                          ])))).

proc(emotionTransList(happy, [happy, happy, playful, playful, tired]), []).
proc(emotionTransList(sad, [sad, angry]), []).
proc(emotionTransList(tired, [tired, happy, playful]), []).
proc(emotionTransList(angry, [angry, angry, sad, playful]), []).
proc(emotionTransList(playful, [playful, happy, tired]), []).

proc(randomElement(List, Element), []) :- randomElement(List, Element).
%proc(randomElement([H|T], Element), pro_rand_ele([H|T])).
%proc(randomElement(List, Element), some(x, and(x=List, pro_rand_ele(x)))).
%proc(pro_rand_ele(List), []) :- randomElement(List, Element).

proc(emotionList(L), []) :- emotionList(L).

proc(member(Id, L), []) :- member(Id, L).

%simple loop program
proc(loop, while(true, wait)).
proc(demo, [wait,wait,senseAll]).
proc(abc, while(true, if(time < 100, [pickupAntiPiece(5,1), while(true, wait)],
                                     wait))).

%=======================================================================
proc(control, while(true,
                 if(time < 100,
                    doStuff,
                 %else
                    wait
                 )
              )).

proc(doStuff, [
                 %senseAll,
                 checkBuildSchedule,
                 %if(some(id, some(x, some(y, location(antishadow,id) = pos(x,y)))), [
                    pi(as, pi(s, pi(list, [
                       ?(list = antiShadowList),
                       randomElement(list,as),
                       ?(s=status(as)),
                       %printf("lets try out antishadow = %w, (status = %w)\n", [as, s]),
                       move(as)
                    ])))
                 %], %else
                 %   [printf("WAIT INSTEAD!\n", []), wait]
                 %)
              ]).

proc(move(AS), 
   if(status(AS) = random,
      randomWalk(AS,5,8),
   %else
   if(status(AS) = build,
      build(AS),
   %else
   if(status(AS) = preperformance,
      play(AS),
   %else
   if(status(AS) = face_crowd,
      turn(AS,0,0),
   %else
      [] %printf("just wait\n", []),wait]
   ))))).

proc(play(AS), [
   ?(status(AS) = preperformance), %pi(x, [?(x=emotion(AS)), printf("antishadow %w has emotion %w\n", [AS, x])]),
   if(emotion(AS) = tired, [
      while(emotion(AS) = tired, [
         %printf("FEELING TIRED\n", []),
         changeEmotion(AS)
         %pi(emo, [?(emo=emotion(AS)), printf("%w has changed emotion to %w\n", [AS, emo])])
      ]),
      %printf("so now %w needs to go random ...\n", [AS]),
      goRandom(AS)
      %pi(stat, [ ?(status(AS) = stat), printf("seems to be ok ... %w has status %w. Done playing.\n", [AS, stat]) ])
   ], [ %else
      %printf("NOT TIRED, DO RANDOM!\n", []),
      pi(e, [?(emotion(AS)=e), playRandomAction(AS, e)])
   ])]).
 
proc(playRandomAction(Id, Emotion), play(Id, A)) :-
   %printf("playRandomAction %w! You feel %w?\n", [Id, Emotion]),
   emotionalResponses(Emotion, Responses), %printf("responses = %w\n", [Responses]),
   randomElement(Responses, R), %printf("lets try %w\n", [R]),
   animations(R, Animations), %printf("which has animations = %w\n", [Animations]),
   randomElement(Animations, A). %, printf("but we will just do %w\n", [A]).






%%%%%%%%%%%%%%%%%%%% SENSING %%%%%%%%%%%%%%%%%%%%%
proc(senseAll, pi(t, pi(r, [
        ?(t = time),
        ?(mod(t,10,r)),
        if(r = 0, [ %sense as every 10% time elapses
           %pi(l1, [?(l1 = shadowList),     senseList(shadow, char, l1)]),
           pi(l2, [?(l2 = antiShadowList), senseList(antishadow, char, l2)]),
           %pi(l3, [?(l3 = pieceList),      senseList(piece, piece, l3)]),
           pi(l4, [?(l4 = antiPieceList),  senseList(antipiece, antipiece, l4)])
        ], %else
        [])
    ]))).

proc(mod(X,Y,Z), []) :- Z is X mod Y.

%sense all objects of Type with Ids from List
proc(senseList(_Type, _AVIEType, []), []).
proc(senseList(Type, AVIEType, [Id|Rest]), [
       if(or(location(Type,Id) = err, and(Type = antipiece, location(antipiece,Id) = reserved)),
          [], %ignore object
       %else
          sPos(Id, AVIEType)
          %[printf("TRYING TO SENSE %w (%w)\n", [Id, AVIEType]), sPos(Id, AVIEType)]
       ),
       senseList(Type, AVIEType, Rest)
    ]).

%%%%%%%%%%%%%%%%%%%%% RANDOM WALK %%%%%%%%%%%%%%%%%%%%%%%

proc(randomWalk(AS, Min, Max), [ if(status(AS) = random, [
                                    pi(oldX, pi(oldY, pi(newX, pi(newY, [
                                       ?(location(antishadow, AS) = pos(oldX, oldY)),
                                       randomStepLocation(Min, Max, pos(oldX, oldY), pos(newX, newY)),
                                       %printf("RANDWALK - goto(%w, %w, %w)\n", [AS, newX, newY]),
                                       goto(AS, newX, newY)
                                    ]))))],
                                 %else
                                    [] %wait
                                 )]).

proc(randomStepLocation(Min, Max, pos(OldX,OldY), pos(NewX,NewY)), []) :-
     Step is 3.0,
     DoubleStep is 2.0*Step,
     random(0.0, DoubleStep, RandX),
     random(0.0, DoubleStep, RandY),
     TempX is OldX+RandX-Step,
     TempY is OldY+RandY-Step,
     Length is sqrt(TempX**2 + TempY**2), %printf("Length = sqrt(%w^2 + %w^2) = %w\n", [TempX,TempY,Length]),
     ((Length < Min, %!, %printf("SHORT\n",[]),%too short!
         NewX is TempX * Min / Length,
         NewY is TempY * Min / Length) ;
      (Length > Max, %!, %printf("LONG\n", []),%too long!
         NewX is TempX * Max / Length,
         NewY is TempY * Max / Length) ;
      (NewX is TempX, NewY is TempY)). %just right!


%%%%%%%%%%%%%%%%%%%%%%%%%%%%% BUILD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc(checkBuildSchedule, [
   %printf("checking build schedule with roll = %w ...\n", [Rand]),
   if(some(as, or(status(as) = build, status(as) = deliver)), %someone already building, we're ok!
      [], %[printf("someone already building ...\n", [])],
   %else, no one building, check if we need to
      pi(list, pi(length, pi(s, pi(t, [
         ?(list = antiShadowList),
         ?(s = antiShadowScore),
         ?(t is (((100 - time) / 20) - antiShadowScore)),
         ?(length(list, length)),
         %printf("score = %w, target = %w\n", [s, t]),
         %scaleNumber(Rand, t, Roll),
         %printf("scaling roll to %w\n", [Roll]),
         if(or(and(Rand < t, %score is low, still time, but `behind schedule'
               and(length > 0,
               and(antiShadowScore < 4,
                   time > 7))),
            %or we don't have much time left
               time =< 7), [
            %printf("good enough! now choose an antishadow builder ...\n", []),
            pi(id, [ %we need to build
               %printf("get random builder\n", []),
               randomElement(list, id),
               %printf("go build!\n", []),
               goBuild(id) %,
               %printf("send %w to its death\n", [id])
            ])
         ], %else, we are ok!
            [] %[printf("on schedule\n", [])]
         )]))))
   ) %, printf("END SCHEDULE\n", [])
   ]) :- random(0.0, 1.0, Rand).

proc(scaleNumber(Old, High, New), []) :- New is Old*High.

proc(build(AS), [ if(location(antidoor) = err, sPos(0, antidoor), []), %make sure we know where the antidoor is
                  pi(asx, pi(asz, [
                     ?(location(antishadow,AS) = pos(asx,asz)),
                     if(location(antidoor) = pos(ADX,ADZ), %and don't crash if it didn't work!
                        if(holding(AS) = err, %not holding anything
                           if(some(ap, some(x, some(y, location(antipiece, ap) = pos(x,y)))),
                              pi(ap, pi(apx, pi(apz, pi(d, [
                                 %printf("not holding anything ... find something to pickup\n", []),
                                 %printf("we are at pos(%w, %w)\n", [asx,asz]),
                                 findClosestId(antipiece,pos(asx,asz),ap),
                                 ?(location(antipiece, ap) = pos(apx,apz)), %so find closest piece
                                 %printf("try antipiece %w at pos(%w, %w)\n", [ap, apx, apz]),
                                 ?(distance(pos(asx,asz), pos(apx,apz), d)),
                                 %printf("distance = %w\n", [d]),
                                 if(d < 1,
                                    pickupAntiPiece(AS, ap),
                                 %else
                                    goto(AS, apx, apz) %and go to it ...
                                 )
                           ])))), [ %else
                              %printf("nothing to pickup! %w should go random!\n", [AS]),
                              goRandom(AS)
                           ]),
                        %else, we are holding something, so take it to the antidoor!
                           pi(h, pi(d, [
                              ?(h=holding(AS)),
                              distance(pos(asx,asz), pos(ADX,ADZ), d),
                              if(d < 1, [ %we are close enough to the door
                                 %printf("%w at pos(%w,%w) is holding %w and close to door at pos(%w, %w). SCORE!\n", [AS,asx,asz,h,ADX,ADZ]),
                                 placeAntiPiece(AS, h)
                              ], [ %else
                                 %printf("we are holding %w, so goto(%w, %w, %w)\n", [h,AS,ADX,ADZ]),
                                 goto(AS, ADX, ADZ)
                              ])
                           ]))),
                     %else
                        goRandom(AS)
                     )]))]).
                  




%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DISTANCE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

proc(findClosestId(Type,pos(CX,CY),Id), pi(id, pi(x, pi(y, [
                                           ?(location(Type,id)=pos(x,y)),
                                           if(neg(somethingCloser(Type,pos(CX,CY),pos(x,y))),
                                              ?(Id=id),
                                           %else
                                              neg(true))
                                        ])))).

%find the closest object of Type to pos(CX,CY) and stores its position in pos(TX,TY)
%NB. you *should* check that objects of that type actually exist first to avoid problems with backtracking ...
proc(findClosest(Type,pos(CX,CY),pos(TX,TY)), some(id, [
                                                 findClosestId(Type, pos(CX,CY), id),
                                                 ?(location(Type,id)=pos(TX,TY))
                                              ])).

%complex proc: checks whether there is some object of Type closer to pos(CX,CY) than pos(X1,Y1)
proc(somethingCloser(Type,pos(CX,CY),pos(X1,Y1)), some(id, some(x2, some(y2,
                                                     and(location(Type,id)=pos(x2,y2),
                                                        (((CX-X1)**2+(CY-Y1)**2) > ((CX-x2)**2+(CY-y2)**2))
                                                     )
                                                  )))).

proc(distance(pos(X1,Y1),pos(X2,Y2),D), []) :- D is ((X1-X2)**2+(Y1-Y2)**2).

proc(closer(infinity, infinity), neg(true)).
proc(closer(infinity, _), neg(true)).
proc(closer(_, infinity), true).
proc(closer(D1,D2), (D1<D2)).

%%%%%%%%%%%%%%%%%%%% UTILS %%%%%%%%%%%%%%%%%%%%%%
proc(printf(Format, Args), []) :- printf(Format, Args).
proc(write(X), []) :- write(X).
proc(say(X), []) :- write(X),nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions of complex actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Main Routine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%prolog :- indigolog(control). %the prolog goal is automatically run on startup

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HOOKS TO comm.pl CODE
%  This section provides predicates for executing actions from the IndiGolog
%  interpreter and returning exogenous actions detected by AVIE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialize: Perform any application dependent initialization
initialize :-
    initializeAVIE,
    initializeExog.

% finalize: Application dependent wrap-up
finalize :-
    finalizeAVIE,
    finalizeExog.

% execute(+Action, +History, -SensingResult): Execute Action on AVIE returning
%     SensingResult. Current action History is supplied for debugging
%     purposes in case something goes wrong
execute(Action, _History, SensingResult) :-
    %printf("Executing action: %w\n", [Action]),
    sendAVIEAction(Action, Sr),
    (Sr =.. [_, SensingResult]; Sr = SensingResult).
    %write('    Sensing result: '), write(SensingResult), nl, nl.
    
% If previous clause fails, call user-defined debug routine.
% The debug routine can reattempt the action returning SensingResult
execute(Action, History, SensingResult) :-
    debugAVIE(Action, History, SensingResult).

% exog_occurs(-ExogList): Check for occurrence of exogenous actions and
%     return them in list ExogList. Exogenous actions can occur at
%     various sources: here we check just the AVIE

exog_occurs(ExogList) :-
    %write('Checking exogenous actions: '), nl,
    flush_output,
    sendAVIEAction('exog(0)', ExogList),
    (((ExogList == []) -> (true, !)) ; (write(ExogList), nl)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG ROUTINES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% debugAVIE(+Action, +History, -SensingResult):
%     This predicate attempts to provide some basic debug and error recovery.
%     If you can't be bothered, uncomment the following clause and
%     the program will abort on failure to execute an action
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
debugAVIE(Action, History, SensingResult) :-
    write('** DEBUG ...'), nl,
    errorRecoveryData(History),
    errorRecoveryProc,
    execute(Action, History, SensingResult). % Try action again

% errorRecoveryData(+History): Extract values of primitive fluents at
%     the point where Hist actions are performed.
errorRecoveryData(History) :-
    write('    Actions performed so far: '),
    write(History), nl,
    bagof(U,
          (prim_fluent(U) ; (dyn_fluent(U), \+ prim_fluent(U))),
          FluentList),
    printFluentValues(FluentList, History).

% printFluentValues(+FluentList, +History): Print value of primitive fluents
%     at the point where History actions have been performed
printFluentValues([], _) :-
    write('-----------------------------------------------'),
    nl.

printFluentValues([Hf | FluentList], History) :-
    (has_val(Hf, Hv, History) -> (
       (dyn_fluent(Hf) -> printf("    Dynamic", []) ; printf("    Primitive", [])),
       printf(" fluent %w has value %w\n", [Hf,Hv])) ; true),
    printFluentValues(FluentList, History).

% errorRecoveryProc: What to do in case of error.
errorRecoveryProc:-
    write('If you wish to abort, enter "a".'), nl,
    get0(Val),
    get0(_),                     % Clear carriage return
    (Val == 65; Val == 97) ->    % 65 is ASCII 'A', 97 is ASCII 'a'
         break;
         true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EOF: Agent/asylum.pl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
