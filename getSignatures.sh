#! /bin/bash

JAVA=`which java`
JAVAP=`which javap` #consider javap -s for SIGNATURES
SED=`which sed`
UNZIP=`which unzip`
GREP=`which grep`
TR=`which tr`

#RUNTIME='/usr/lib/jvm/java-6-sun-1.6.0.10/jre/lib/rt.jar'
RUNTIME='/usr/lib/jvm/java-1.5.0-sun-1.5.0.16/jre/lib/rt.jar'
OUTPUT_DIR='/tmp'
JASMIN_OUTPUT='/dev/null'

if [ $# -lt 1 ]
then
   echo "usage: $0 fully-qualified-classname"
   exit
fi

if [ -f "$RUNTIME" -a -d "$OUTPUT_DIR" ]
then
   fqcn="$1"
   class=`echo "$fqcn" | "$SED" 's/.*\///g' -`
   output="$OUTPUT_DIR/$class"
   $UNZIP -pc "$RUNTIME" "$fqcn.class" > "$OUTPUT_DIR/$class.class"
   cd "$OUTPUT_DIR"

   "$JAVAP" "$class" | "$GREP" -v 'Compiled from' | "$GREP" -v '^}$' | "$GREP" -v '^$' | $TR '.' '/'
   rm -f "$class.class"
else
   echo "could not find java runtime at \"$RUNTIME\""
fi


