#! /usr/bin/python

import os, subprocess, sys
import difflib

BASE_DIR = os.path.split(sys.argv[0])[0]
JOLOG_DIR = os.path.join(BASE_DIR, "jolog_files") # % base_dir
JASMIN_DIR = os.path.join(BASE_DIR, "jasmin_files") # % base_dir
PROGRAM = os.path.join(BASE_DIR, "..", "jolog")

jolog_files = os.listdir(JOLOG_DIR)
jasmin_files = os.listdir(JASMIN_DIR)

tests = 0
passed = 0

prompt = False
remove = True

for i in range(len(jolog_files)):
   jolog = "test%d.jolog" % (i)
   jasmin = "test%d.j" % (i)
   if (jolog in jolog_files and jasmin in jasmin_files):
      print "test %d: " % (tests), #%s -> %s ..." % (tests, jolog, jasmin)
      tests = tests + 1
      #try:
      if 1:
         run = subprocess.Popen([PROGRAM, os.path.join(JOLOG_DIR, jolog)])
         run.wait()

         generated = open(os.path.join(JOLOG_DIR, jasmin)).readlines()
         original = open(os.path.join(JASMIN_DIR, jasmin)).readlines()

         #calculate the similarity (ratio) between generated and original, ignoring tabs and carriage returns
         if difflib.SequenceMatcher(lambda x : x in "\t\r", generated, original).ratio() == 1.0:
            passed = passed + 1
            print "PASSED"
            if (prompt != "never"):
               prompt = False
               remove = True
         else:
            print "FAILED"
            if (prompt != "never"):
               prompt = True

         retry = True
         userInput = ""
         while (prompt == True and retry):
            retry = False
            print "do you want to delete test output '%s'? [yes|no|always|never]" % os.path.join(JOLOG_DIR, jasmin)
            userInput = sys.stdin.readline().strip()
            if userInput == "yes":
               remove = True
            elif userInput == "always":
               remove = True
               prompt = "never"
            elif userInput == "no":
               remove = False
            elif userInput == "never":
               remove = False
               prompt = "never"
            else:
               print "unknown input"
               retry = True

         if remove:
            os.remove("%s" % os.path.join(JOLOG_DIR, jasmin)) #cleanup test output

      #except:
      #   print "EXCEPTION FAILED (can't find %s?)" % PROGRAM

print "Passed %d of %d tests" % (passed, tests)

if (tests == passed):
   print "YOU ARE AWESOME"


