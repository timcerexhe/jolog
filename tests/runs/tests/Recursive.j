.source Recursive.jolog
.class public Recursive
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label9:
.limit stack 3
.limit locals 2
label12:
.var 1 is r LRecursive; from label12 to label0
   new Recursive
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial Recursive/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual Recursive/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private test(Z)V
label13:
.limit stack 5
.limit locals 2
   iload_1
   ifne label3
   goto label16
label17:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"recursive\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label18:
   pop
   aload_0
   bipush 10
   iconst_1
   invokevirtual Recursive/recursive(IZ)V
   goto label19
.catch all from label18 to label19 using label17
label16:
   aload_0
   bipush 10
   iconst_0
   invokevirtual Recursive/recursive(IZ)V
label19:
   nop
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
 ; SAVE TRANSIENTS ...
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label3:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label19 to label2 using label18
.end method
.method private recursive(IZ)V
label20:
.limit stack 9
.limit locals 3
   iload_2
   ifne label8
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   new java/lang/Integer
   dup
   iload_1
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label25:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   iload_1
   iconst_0
   if_icmplt label30
   iconst_1
   goto label31
label30:
   iconst_0
label31:
   ifne label29
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label29:
   getstatic java/lang/System/out Ljava/io/PrintStream;
   iload_1
   invokevirtual java/io/PrintStream/println(I)V
   goto label32
label33:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"recursive\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label34:
   pop
   aload_0
   iload_1
   iconst_1
   isub
   iconst_1
   invokevirtual Recursive/recursive(IZ)V
   goto label35
.catch all from label34 to label35 using label33
label32:
   aload_0
   iload_1
   iconst_1
   isub
   iconst_0
   invokevirtual Recursive/recursive(IZ)V
label35:
   nop
label6:
.catch all from label35 to label6 using label34
 ; END USER PROGRAM =========================================================
.catch all from label25 to label24 using label24
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label23
label37:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label5:
label24:
 ; DO NDET 2 ======================================================
   pop
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "restoring initial ndet state"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore_1
 ; USER PROGRAM =========================================================
   iload_1
   iconst_0
   if_icmplt label40
   iconst_1
   goto label41
label40:
   iconst_0
label41:
   iconst_1
   ixor
   ifne label39
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label39:
 ; END USER PROGRAM =========================================================
.catch all from label24 to label26 using label27
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label23
label42:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label7:
label26:
 ; HANDLER =========================================================
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label37
   dup
   iconst_1
   if_icmpeq label42
   pop
label27:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label23:
 ; END OF NDET =========================================================
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label8:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield Recursive/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"recursive\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label4:
.catch all from label23 to label4 using label26
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield Recursive/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield Recursive/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield Recursive/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label43
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label43:
   aconst_null
   areturn
.end method
