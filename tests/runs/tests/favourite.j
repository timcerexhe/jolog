.source favourite.jolog
.class public favourite
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private jolog_action_0(Ljolog/Message;)Ljolog/Message;
label5:
.limit stack 6
.limit locals 3
   aconst_null
   astore_2
label8:
.var 2 is matcher Ljava/util/regex/Matcher; from label8 to label2
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_0_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/util/regex/Pattern
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   checkcast java/lang/CharSequence
   invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
   astore_2
   aload_2
   invokevirtual java/util/regex/Matcher/matches()Z
 ; THIS IS THE ELSE BRANCH
   ifeq label9
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   iconst_1
   invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
   invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "allFavourites"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "allFavourites"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   aload_2
   iconst_1
   invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
 ; DO NOT SAVE TRANSIENTS ...
   new jolog/Message
   dup
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "allFavourites"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   invokespecial jolog/Message/<init>(Ljava/lang/String;)V
   areturn
label3:
   goto label10
label9:
   new jolog/FailedPreconditionException
   dup
   ldc "precondition unsatisfied in action \"jolog_action_0\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label10:
 ; DO NOT SAVE TRANSIENTS ...
   return
label2:
.end method
.method public static main([Ljava/lang/String;)V
label11:
.limit stack 3
.limit locals 3
label14:
.var 1 is env Ljolog/HubEnvironment; from label14 to label0
   new jolog/HubEnvironment
   dup
   invokespecial jolog/HubEnvironment/<init>()V
   astore_1
label15:
.var 2 is f Lfavourite; from label15 to label0
   new favourite
   dup
   aload_1
   checkcast jolog/Environment
   invokespecial favourite/<init>(Ljolog/Environment;)V
   astore_2
   aload_1
   aload_2
   checkcast jolog/JologAgent
   invokevirtual jolog/HubEnvironment/addAgent(Ljolog/JologAgent;)V
   aload_1
   getstatic java/lang/System/in Ljava/io/InputStream;
   invokevirtual jolog/HubEnvironment/readFromStream(Ljava/io/InputStream;)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield favourite/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield favourite/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield favourite/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "allFavourites"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc ""
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_0
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield favourite/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_0_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc "(\\d+)"
   invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 4
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label16
label17:
   aload_0
   aload_1
   invokevirtual favourite/jolog_action_0(Ljolog/Message;)Ljolog/Message;
   areturn
label18:
.catch all from label17 to label18 using label18
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label16:
   aconst_null
   areturn
.end method
