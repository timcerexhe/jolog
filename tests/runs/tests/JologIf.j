.source JologIf.jolog
.class public JologIf
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label6:
.limit stack 3
.limit locals 2
label9:
.var 1 is jif LJologIf; from label9 to label0
   new JologIf
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial JologIf/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   invokevirtual JologIf/checkIf()V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method public checkIf()V
label10:
.limit stack 3
.limit locals 2
label13:
.var 1 is i I from label13 to label2
   iconst_0
   istore_1
   iload_1
   bipush 10
   if_icmpge label15
   iconst_1
   goto label16
label15:
   iconst_0
label16:
 ; THIS IS THE ELSE BRANCH
   ifeq label14
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "pass"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label3:
   goto label17
label14:
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "fail"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label4:
label17:
 ; DO NOT SAVE TRANSIENTS ...
   return
label2:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield JologIf/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield JologIf/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield JologIf/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label18
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label18:
   aconst_null
   areturn
.end method
