.source Primitive.jolog
.class public Primitive
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private update()V
label7:
.limit stack 5
.limit locals 1
   iconst_1
 ; THIS IS THE ELSE BRANCH
   ifeq label10
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "f"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   new java/lang/Integer
   dup
   iconst_1
   invokespecial java/lang/Integer/<init>(I)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "f"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   iconst_1
   if_icmpne label12
   iconst_1
   goto label13
label12:
   iconst_0
label13:
 ; THIS IS THE ELSE BRANCH
   ifeq label11
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "g"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc "updated"
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
label11:
label3:
label10:
 ; DO NOT SAVE TRANSIENTS ...
   return
 ; DO NOT SAVE TRANSIENTS ...
   return
label2:
.end method
.method public static main([Ljava/lang/String;)V
label14:
.limit stack 3
.limit locals 2
label17:
.var 1 is p LPrimitive; from label17 to label0
   new Primitive
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial Primitive/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   invokevirtual Primitive/go()V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method public go()V
label18:
.limit stack 6
.limit locals 1
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "f"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   invokevirtual java/io/PrintStream/print(I)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   aload_0
   invokevirtual Primitive/update()V
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "f"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   invokevirtual java/io/PrintStream/print(I)V
   dup
   ldc " "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "g"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label5:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield Primitive/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield Primitive/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "f"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   new java/lang/Integer
   dup
   iconst_0
   invokespecial java/lang/Integer/<init>(I)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield Primitive/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "g"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc "initial"
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label21
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label21:
   aconst_null
   areturn
.end method
