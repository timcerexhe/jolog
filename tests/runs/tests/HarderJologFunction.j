.source HarderJologFunction.jolog
.class public HarderJologFunction
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label21:
.limit stack 3
.limit locals 2
label24:
.var 1 is hjf LHarderJologFunction; from label24 to label0
   new HarderJologFunction
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial HarderJologFunction/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual HarderJologFunction/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private test(Z)V
label25:
.limit stack 6
.limit locals 2
   iload_1
   ifne label10
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label30:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label35:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   goto label38
label39:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"functionOne\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label40:
   pop
   aload_0
   iconst_1
   invokevirtual HarderJologFunction/functionOne(Z)V
   goto label41
.catch all from label40 to label41 using label39
label38:
   aload_0
   iconst_0
   invokevirtual HarderJologFunction/functionOne(Z)V
label41:
   nop
.catch all from label41 to label5 using label40
 ; END USER PROGRAM =========================================================
.catch all from label35 to label34 using label34
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label33
label43:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label5:
label34:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
   goto label44
label45:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"functionTwo\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label46:
   pop
   aload_0
   iconst_1
   invokevirtual HarderJologFunction/functionTwo(Z)V
   goto label47
.catch all from label46 to label47 using label45
label44:
   aload_0
   iconst_0
   invokevirtual HarderJologFunction/functionTwo(Z)V
label47:
   nop
.catch all from label47 to label6 using label46
 ; END USER PROGRAM =========================================================
.catch all from label34 to label42 using label42
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label33
label49:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label6:
label42:
 ; DO NDET 3 ======================================================
   pop
 ; USER PROGRAM =========================================================
   goto label50
label51:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"functionThree\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label52:
   pop
   aload_0
   iconst_1
   invokevirtual HarderJologFunction/functionThree(Z)V
   goto label53
.catch all from label52 to label53 using label51
label50:
   aload_0
   iconst_0
   invokevirtual HarderJologFunction/functionThree(Z)V
label53:
   nop
.catch all from label53 to label7 using label52
 ; END USER PROGRAM =========================================================
.catch all from label42 to label36 using label37
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_2
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label33
label54:
 ; REDO NDET 3 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 2)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label7:
label36:
 ; HANDLER =========================================================
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label43
   dup
   iconst_1
   if_icmpeq label49
   dup
   iconst_2
   if_icmpeq label54
   pop
label37:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label33:
 ; END OF NDET =========================================================
   nop
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label4:
.catch all from label33 to label4 using label36
 ; END USER PROGRAM =========================================================
.catch all from label30 to label29 using label29
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label28
label56:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label3:
label29:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
label9:
 ; END USER PROGRAM =========================================================
.catch all from label29 to label31 using label32
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label28
label57:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label8:
label31:
 ; HANDLER =========================================================
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label56
   dup
   iconst_1
   if_icmpeq label57
   pop
label32:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label28:
 ; END OF NDET =========================================================
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label10:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label28 to label2 using label31
.end method
.method private functionOne(Z)V
label58:
.limit stack 6
.limit locals 2
   iload_1
   ifne label14
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label63:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "function1a"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; END USER PROGRAM =========================================================
.catch all from label63 to label62 using label62
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label61
label67:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label12:
label62:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "function1b"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; END USER PROGRAM =========================================================
.catch all from label62 to label64 using label65
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label61
label68:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label13:
label64:
 ; HANDLER =========================================================
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label67
   dup
   iconst_1
   if_icmpeq label68
   pop
label65:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label61:
 ; END OF NDET =========================================================
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label14:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"functionOne\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label11:
.catch all from label61 to label11 using label64
.end method
.method private functionTwo(Z)V
label69:
.limit stack 5
.limit locals 2
   iload_1
   ifne label16
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "function2"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; SAVE TRANSIENTS ...
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label16:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"functionTwo\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label15:
.end method
.method private functionThree(Z)V
label72:
.limit stack 7
.limit locals 3
   iload_1
   ifne label20
 ; BEGIN PICK EMIT ...
label80:
.var 2 is i Ljava/lang/Integer; from label80 to label17
   aconst_null
   astore_2
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   iconst_0
   ldc "tests/HarderJologFunction.jolog: 26"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label78
 ; === PICK FAIL (stack = 0) ===
label75:
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label77:
   pop
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "tests/HarderJologFunction.jolog: 26"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   astore_2
label78:
 ; === DO PICK === stack = 0
   aload_0
   getfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.Integer"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/Integer"
   checkcast java/lang/Integer
   astore_2
.catch all from label78 to label79 using label75
 ; === USER PROGRAM === stack = 0
label79:
   aload_0
   getfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   ifne label82
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label82:
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_2
   invokevirtual java/io/PrintStream/print(Ljava/lang/Object;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label18:
label19:
   nop
 ; === FINISHED COMPILING PICK === stack = 0
 ; SAVE TRANSIENTS ...
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label20:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"functionThree\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label17:
.catch all from label79 to label17 using label77
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield HarderJologFunction/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield HarderJologFunction/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_0
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_0
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_1
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_2
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   bipush 7
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield HarderJologFunction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   bipush 8
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_0
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label83
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label83:
   aconst_null
   areturn
.end method
