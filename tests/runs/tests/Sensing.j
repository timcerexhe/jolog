.source Sensing.jolog
.class public Sensing
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private senseText()Ljolog/Message;
label7:
.limit stack 6
.limit locals 1
   iconst_1
 ; THIS IS THE ELSE BRANCH
   ifeq label10
   aload_0
   getfield Sensing/environment Ljolog/Environment;
   checkcast jolog/HubEnvironment
   ldc "hello world!"
   invokevirtual jolog/HubEnvironment/send(Ljava/lang/String;)V
   aload_0
   getfield Sensing/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "sensed"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   aload_0
   getfield Sensing/environment Ljolog/Environment;
   checkcast jolog/HubEnvironment
   invokevirtual jolog/HubEnvironment/sense()Ljava/lang/String;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
 ; DO NOT SAVE TRANSIENTS ...
   aconst_null
   areturn
label1:
   goto label11
label10:
   new jolog/FailedPreconditionException
   dup
   ldc "precondition unsatisfied in action \"senseText\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label11:
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method public static main([Ljava/lang/String;)V
label12:
.limit stack 3
.limit locals 3
label15:
.var 1 is env Ljolog/HubEnvironment; from label15 to label3
   new jolog/HubEnvironment
   dup
   invokespecial jolog/HubEnvironment/<init>()V
   astore_1
label16:
.var 2 is s LSensing; from label16 to label3
   new Sensing
   dup
   aload_1
   checkcast jolog/Environment
   invokespecial Sensing/<init>(Ljolog/Environment;)V
   astore_2
   aload_1
   aload_2
   checkcast jolog/JologAgent
   invokevirtual jolog/HubEnvironment/addAgent(Ljolog/JologAgent;)V
   aload_2
   iconst_0
   invokevirtual Sensing/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label3:
.end method
.method private test(Z)V
label17:
.limit stack 8
.limit locals 2
   iload_1
   ifne label6
   aload_0
   invokevirtual Sensing/senseText()Ljolog/Message;
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "Sensed: "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_0
   getfield Sensing/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "sensed"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   aload_0
   invokevirtual Sensing/senseText()Ljolog/Message;
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "Sensed: "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_0
   getfield Sensing/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "sensed"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; SAVE TRANSIENTS ...
   aload_0
   getfield Sensing/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label6:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield Sensing/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label5:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 5
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield Sensing/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield Sensing/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield Sensing/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield Sensing/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "sensed"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc ""
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label20
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label20:
   aconst_null
   areturn
.end method
