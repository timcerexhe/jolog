.source JologAction.jolog
.class public JologAction
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private myAction(Ljava/lang/String;)V
label14:
.limit stack 8
.limit locals 2
   aload_1
   invokevirtual java/lang/String/length()I
   iconst_0
   if_icmple label18
   iconst_1
   goto label19
label18:
   iconst_0
label19:
 ; THIS IS THE ELSE BRANCH
   ifeq label17
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "save"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_1
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   new java/lang/Integer
   dup
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "index"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   invokespecial java/lang/Integer/<init>(I)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "index"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   new java/lang/Integer
   dup
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "index"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   iconst_1
   iadd
   invokespecial java/lang/Integer/<init>(I)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
label12:
label17:
 ; DO NOT SAVE TRANSIENTS ...
   return
 ; DO NOT SAVE TRANSIENTS ...
   return
label11:
.end method
.method public static main([Ljava/lang/String;)V
label20:
.limit stack 3
.limit locals 2
label23:
.var 1 is ja LJologAction; from label23 to label0
   new JologAction
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial JologAction/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual JologAction/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private test(Z)V
label24:
.limit stack 5
.limit locals 2
   iload_1
   ifne label3
   aload_0
   ldc "my first argument"
   invokevirtual JologAction/myAction(Ljava/lang/String;)V
   goto label27
label28:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"showAll\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label29:
   pop
   aload_0
   iconst_1
   invokevirtual JologAction/showAll(Z)V
   goto label30
.catch all from label29 to label30 using label28
label27:
   aload_0
   iconst_0
   invokevirtual JologAction/showAll(Z)V
label30:
   nop
   aload_0
   ldc "hello!"
   invokevirtual JologAction/myAction(Ljava/lang/String;)V
   goto label31
label32:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"showAll\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label33:
   pop
   aload_0
   iconst_1
   invokevirtual JologAction/showAll(Z)V
   goto label34
.catch all from label33 to label34 using label32
label31:
   aload_0
   iconst_0
   invokevirtual JologAction/showAll(Z)V
label34:
   nop
   aload_0
   ldc "one more time ..."
   invokevirtual JologAction/myAction(Ljava/lang/String;)V
   goto label35
label36:
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failure in function \"showAll\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label37:
   pop
   aload_0
   iconst_1
   invokevirtual JologAction/showAll(Z)V
   goto label38
.catch all from label37 to label38 using label36
label35:
   aload_0
   iconst_0
   invokevirtual JologAction/showAll(Z)V
label38:
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label3:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label38 to label2 using label37
.catch all from label34 to label2 using label33
.catch all from label30 to label2 using label29
.end method
.method private showAll(Z)V
label39:
.limit stack 7
.limit locals 3
   iload_1
   ifne label10
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc " === SHOW ALL ==="
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label44:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
 ; BEGIN PICK EMIT ...
label52:
.var 2 is s Ljava/lang/String; from label52 to label6
   ldc ""
   astore_2
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   iconst_0
   ldc "./tests/JologAction.jolog: 22"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label50
 ; === PICK FAIL (stack = 0) ===
label47:
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label49:
   pop
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/JologAction.jolog: 22"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
label50:
 ; === DO PICK === stack = 0
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.String"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/String"
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
.catch all from label50 to label51 using label47
 ; === USER PROGRAM === stack = 0
label51:
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "save"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   iconst_0
   if_icmple label55
   iconst_1
   goto label56
label55:
   iconst_0
label56:
   ifne label54
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label54:
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "save"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   invokevirtual java/io/PrintStream/print(I)V
   dup
   ldc ": "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label7:
label8:
   nop
 ; === FINISHED COMPILING PICK === stack = 0
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label6:
.catch all from label51 to label6 using label49
 ; END USER PROGRAM =========================================================
.catch all from label44 to label43 using label43
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label42
label58:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label5:
label43:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/clear()V
   goto label59
label60:
   pop
   new jolog/ProgramFailureException
   dup
   ldc "cut failed at ./tests/JologAction.jolog: 28"
   invokespecial jolog/ProgramFailureException/<init>(Ljava/lang/String;)V
   athrow
label59:
   nop
.catch all from label59 to label9 using label60
 ; END USER PROGRAM =========================================================
.catch all from label43 to label45 using label46
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label42
label61:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label9:
label45:
 ; HANDLER =========================================================
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label58
   dup
   iconst_1
   if_icmpeq label61
   pop
label46:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label42:
 ; END OF NDET =========================================================
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label10:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield JologAction/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"showAll\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label4:
.catch all from label42 to label4 using label45
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield JologAction/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield JologAction/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "index"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   new java/lang/Integer
   dup
   iconst_1
   invokespecial java/lang/Integer/<init>(I)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield JologAction/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "save"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   ldc "built in"
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   new java/lang/Integer
   dup
   iconst_0
   invokespecial java/lang/Integer/<init>(I)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label62
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label62:
   aconst_null
   areturn
.end method
