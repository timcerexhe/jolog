.source JologRead.jolog
.class public JologRead
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label2:
.limit stack 3
.limit locals 3
label5:
.var 1 is i I from label5 to label0
   iconst_0
   istore_1
label6:
.var 2 is s Ljava/lang/String; from label6 to label0
   ldc ""
   astore_2
   getstatic java/lang/System/out Ljava/io/PrintStream;
   ldc "enter an int and a String: "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   new java/util/Scanner
   dup
   getstatic java/lang/System/in Ljava/io/InputStream;
   invokespecial java/util/Scanner/<init>(Ljava/io/InputStream;)V
   dup
   invokevirtual java/util/Scanner/nextInt()I
   istore_1
   ldc "\\w+"
   invokevirtual java/util/Scanner/next(Ljava/lang/String;)Ljava/lang/String;
   astore_2
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "Got "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   iload_1
   invokevirtual java/io/PrintStream/print(I)V
   dup
   ldc " and "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield JologRead/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield JologRead/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield JologRead/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label7
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label7:
   aconst_null
   areturn
.end method
