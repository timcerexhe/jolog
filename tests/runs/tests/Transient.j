.source Transient.jolog
.class public Transient
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label4:
.limit stack 3
.limit locals 2
label7:
.var 1 is t LTransient; from label7 to label0
   new Transient
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial Transient/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual Transient/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private test(Z)V
label8:
.limit stack 5
.limit locals 3
   aconst_null
   istore_2
   iload_1
   ifne label3
label11:
.var 2 is i I from label11 to label2
   iconst_0
   istore_2
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
 ; SAVE TRANSIENTS ...
   aload_0
   getfield Transient/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label3:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield Transient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/Transient.jolog: 7"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore_2
   aload_0
   getfield Transient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield Transient/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield Transient/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield Transient/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label12
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label12:
   aconst_null
   areturn
.end method
