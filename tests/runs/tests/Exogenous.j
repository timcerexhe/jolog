.source Exogenous.jolog
.class public Exogenous
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private jolog_action_0(Ljolog/Message;)Ljolog/Message;
label29:
.limit stack 5
.limit locals 3
   aconst_null
   astore_2
label32:
.var 2 is matcher Ljava/util/regex/Matcher; from label32 to label20
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_0_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/util/regex/Pattern
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   checkcast java/lang/CharSequence
   invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
   astore_2
   aload_2
   invokevirtual java/util/regex/Matcher/matches()Z
 ; THIS IS THE ELSE BRANCH
   ifeq label33
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "integers"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   iconst_1
   invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
   invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
 ; DO NOT SAVE TRANSIENTS ...
   new jolog/Message
   dup
   aload_0
   iconst_0
   invokevirtual Exogenous/getInts(Z)Ljava/lang/String;
   invokespecial jolog/Message/<init>(Ljava/lang/String;)V
   areturn
label21:
   goto label34
label33:
   new jolog/FailedPreconditionException
   dup
   ldc "precondition unsatisfied in action \"jolog_action_0\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label34:
 ; DO NOT SAVE TRANSIENTS ...
   return
label20:
.end method
.method private jolog_action_1(Ljolog/Message;)Ljolog/Message;
label35:
.limit stack 5
.limit locals 3
   aconst_null
   astore_2
label38:
.var 2 is matcher Ljava/util/regex/Matcher; from label38 to label23
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_1_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/util/regex/Pattern
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   checkcast java/lang/CharSequence
   invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
   astore_2
   aload_2
   invokevirtual java/util/regex/Matcher/matches()Z
 ; THIS IS THE ELSE BRANCH
   ifeq label39
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "doubles"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   iconst_1
   invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
   invokestatic java/lang/Double/parseDouble(Ljava/lang/String;)D
   invokevirtual jolog/Predicate/addArgument(D)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
 ; DO NOT SAVE TRANSIENTS ...
   new jolog/Message
   dup
   aload_0
   iconst_0
   invokevirtual Exogenous/getDoubles(Z)Ljava/lang/String;
   invokespecial jolog/Message/<init>(Ljava/lang/String;)V
   areturn
label24:
   goto label40
label39:
   new jolog/FailedPreconditionException
   dup
   ldc "precondition unsatisfied in action \"jolog_action_1\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label40:
 ; DO NOT SAVE TRANSIENTS ...
   return
label23:
.end method
.method private jolog_action_2(Ljolog/Message;)Ljolog/Message;
label41:
.limit stack 6
.limit locals 3
   aconst_null
   astore_2
label44:
.var 2 is matcher Ljava/util/regex/Matcher; from label44 to label26
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_2_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/util/regex/Pattern
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   checkcast java/lang/CharSequence
   invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
   astore_2
   aload_2
   invokevirtual java/util/regex/Matcher/matches()Z
 ; THIS IS THE ELSE BRANCH
   ifeq label45
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "log"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "log"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   aload_2
   iconst_1
   invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   ldc "\n"
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
 ; DO NOT SAVE TRANSIENTS ...
   aconst_null
   areturn
label27:
   goto label46
label45:
   new jolog/FailedPreconditionException
   dup
   ldc "precondition unsatisfied in action \"jolog_action_2\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label46:
 ; DO NOT SAVE TRANSIENTS ...
   return
label26:
.end method
.method private getInts(Z)Ljava/lang/String;
label47:
.limit stack 7
.limit locals 4
   aconst_null
   astore_2
   iload_1
   ifne label7
label50:
.var 2 is s Ljava/lang/String; from label50 to label0
   ldc ""
   astore_2
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label53:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
 ; BEGIN PICK EMIT ...
label61:
.var 3 is i Ljava/lang/Integer; from label61 to label2
   aconst_null
   astore_3
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_3
   aastore
   iconst_0
   ldc "./tests/Exogenous.jolog: 9"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label59
 ; === PICK FAIL (stack = 0) ===
label56:
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label58:
   pop
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/Exogenous.jolog: 9"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   astore_3
label59:
 ; === DO PICK === stack = 0
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.Integer"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/Integer"
   checkcast java/lang/Integer
   astore_3
.catch all from label59 to label60 using label56
 ; === USER PROGRAM === stack = 0
label60:
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "integers"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_3
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   ifne label63
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label63:
   aload_2
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   aload_3
   invokevirtual java/lang/Integer/intValue()I
   invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_2
label3:
label4:
   nop
 ; === FINISHED COMPILING PICK === stack = 0
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label60 to label2 using label58
 ; END USER PROGRAM =========================================================
.catch all from label53 to label52 using label52
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label51
label65:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label1:
label52:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
label6:
 ; END USER PROGRAM =========================================================
.catch all from label52 to label54 using label55
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label51
label66:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label5:
label54:
 ; HANDLER =========================================================
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label65
   dup
   iconst_1
   if_icmpeq label66
   pop
label55:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label51:
 ; END OF NDET =========================================================
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   aload_2
   areturn
label7:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/Exogenous.jolog: 7"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"getInts\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label0:
.catch all from label51 to label0 using label54
.end method
.method private getDoubles(Z)Ljava/lang/String;
label67:
.limit stack 8
.limit locals 4
   aconst_null
   astore_2
   iload_1
   ifne label15
label70:
.var 2 is s Ljava/lang/String; from label70 to label8
   ldc ""
   astore_2
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label73:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
 ; BEGIN PICK EMIT ...
label81:
.var 3 is d Ljava/lang/Double; from label81 to label10
   aconst_null
   astore_3
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_3
   aastore
   iconst_0
   ldc "./tests/Exogenous.jolog: 15"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label79
 ; === PICK FAIL (stack = 0) ===
label76:
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label78:
   pop
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/Exogenous.jolog: 15"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Double
   astore_3
label79:
 ; === DO PICK === stack = 0
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.Double"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/Double"
   checkcast java/lang/Double
   astore_3
.catch all from label79 to label80 using label76
 ; === USER PROGRAM === stack = 0
label80:
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "doubles"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_3
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   ifne label83
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label83:
   aload_2
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   aload_3
   invokevirtual java/lang/Double/doubleValue()D
   invokestatic java/lang/String/valueOf(D)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_2
label11:
label12:
   nop
 ; === FINISHED COMPILING PICK === stack = 1
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label10:
.catch all from label80 to label10 using label78
 ; END USER PROGRAM =========================================================
.catch all from label73 to label72 using label72
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label71
label85:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label9:
label72:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
label14:
 ; END USER PROGRAM =========================================================
.catch all from label72 to label74 using label75
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label71
label86:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label13:
label74:
 ; HANDLER =========================================================
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label85
   dup
   iconst_1
   if_icmpeq label86
   pop
label75:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label71:
 ; END OF NDET =========================================================
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   aload_2
   areturn
label15:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/Exogenous.jolog: 13"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 2
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"getDoubles\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label8:
.catch all from label71 to label8 using label74
.end method
.method private printHistory(Z)V
label87:
.limit stack 5
.limit locals 2
   iload_1
   ifne label17
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "UNHANDLED:"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "log"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; SAVE TRANSIENTS ...
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label17:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield Exogenous/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"printHistory\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label16:
.end method
.method public static main([Ljava/lang/String;)V
label90:
.limit stack 3
.limit locals 3
label93:
.var 1 is env Ljolog/HubEnvironment; from label93 to label18
   new jolog/HubEnvironment
   dup
   invokespecial jolog/HubEnvironment/<init>()V
   astore_1
label94:
.var 2 is e LExogenous; from label94 to label18
   new Exogenous
   dup
   aload_1
   checkcast jolog/Environment
   invokespecial Exogenous/<init>(Ljolog/Environment;)V
   astore_2
   aload_1
   aload_2
   checkcast jolog/JologAgent
   invokevirtual jolog/HubEnvironment/addAgent(Ljolog/JologAgent;)V
   aload_1
   getstatic java/lang/System/in Ljava/io/InputStream;
   invokevirtual jolog/HubEnvironment/readFromStream(Ljava/io/InputStream;)V
   aload_2
   iconst_0
   invokevirtual Exogenous/printHistory(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label18:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield Exogenous/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield Exogenous/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "log"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc ""
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "integers"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_0
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_0
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "doubles"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   dconst_0
   invokevirtual jolog/Predicate/addArgument(D)V
   new java/lang/Boolean
   dup
   iconst_0
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_0_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc "^(\\d+)$"
   invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_1_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc "^(\\d*.\\d*)$"
   invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield Exogenous/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_2_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc "^(.*)$"
   invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 6
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label95
label96:
   aload_0
   aload_1
   invokevirtual Exogenous/jolog_action_0(Ljolog/Message;)Ljolog/Message;
   areturn
label97:
.catch all from label96 to label97 using label97
label98:
   aload_0
   aload_1
   invokevirtual Exogenous/jolog_action_1(Ljolog/Message;)Ljolog/Message;
   areturn
label99:
.catch all from label98 to label99 using label99
label100:
   aload_0
   aload_1
   invokevirtual Exogenous/jolog_action_2(Ljolog/Message;)Ljolog/Message;
   areturn
label101:
.catch all from label100 to label101 using label101
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label95:
   aconst_null
   areturn
.end method
