.source MoreTransient.jolog
.class public MoreTransient
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private update(I)V
label13:
.limit stack 5
.limit locals 2
   iconst_1
 ; THIS IS THE ELSE BRANCH
   ifeq label16
   aload_0
   getfield MoreTransient/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iload_1
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
label9:
label16:
 ; DO NOT SAVE TRANSIENTS ...
   return
 ; DO NOT SAVE TRANSIENTS ...
   return
label8:
.end method
.method private test(Z)Ljava/lang/String;
label17:
.limit stack 7
.limit locals 4
   aconst_null
   astore_2
   iload_1
   ifne label7
label20:
.var 2 is s Ljava/lang/String; from label20 to label0
   ldc "favourites are:"
   astore_2
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label23:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
 ; BEGIN PICK EMIT ...
label31:
.var 3 is i Ljava/lang/Integer; from label31 to label2
   aconst_null
   astore_3
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_3
   aastore
   iconst_0
   ldc "./tests/MoreTransient.jolog: 14"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label29
 ; === PICK FAIL (stack = 0) ===
label26:
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label28:
   pop
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/MoreTransient.jolog: 14"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   astore_3
label29:
 ; === DO PICK === stack = 0
   aload_0
   getfield MoreTransient/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.Integer"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/Integer"
   checkcast java/lang/Integer
   astore_3
.catch all from label29 to label30 using label26
 ; === USER PROGRAM === stack = 0
label30:
   aload_0
   getfield MoreTransient/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_3
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   iconst_1
   if_icmpne label34
   iconst_1
   goto label35
label34:
   iconst_0
label35:
   ifne label33
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label33:
   aload_2
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   aload_3
   invokevirtual java/lang/Integer/intValue()I
   invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_2
label3:
label4:
   nop
 ; === FINISHED COMPILING PICK === stack = 0
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label30 to label2 using label28
 ; END USER PROGRAM =========================================================
.catch all from label23 to label22 using label22
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label21
label37:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label1:
label22:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
label6:
 ; END USER PROGRAM =========================================================
.catch all from label22 to label24 using label25
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label21
label38:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label5:
label24:
 ; HANDLER =========================================================
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label37
   dup
   iconst_1
   if_icmpeq label38
   pop
label25:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label21:
 ; END OF NDET =========================================================
   nop
 ; SAVE TRANSIENTS ...
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   aload_2
   areturn
label7:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/MoreTransient.jolog: 10"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
   aload_0
   getfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label0:
.catch all from label21 to label0 using label24
.end method
.method public static main([Ljava/lang/String;)V
label39:
.limit stack 4
.limit locals 2
label42:
.var 1 is f LMoreTransient; from label42 to label11
   new MoreTransient
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial MoreTransient/<init>(Ljolog/Environment;)V
   astore_1
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_1
   iconst_0
   invokevirtual MoreTransient/test(Z)Ljava/lang/String;
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   aload_1
   iconst_1
   invokevirtual MoreTransient/update(I)V
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_1
   iconst_0
   invokevirtual MoreTransient/test(Z)Ljava/lang/String;
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   aload_1
   bipush 37
   invokevirtual MoreTransient/update(I)V
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_1
   iconst_0
   invokevirtual MoreTransient/test(Z)Ljava/lang/String;
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label11:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield MoreTransient/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield MoreTransient/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield MoreTransient/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield MoreTransient/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "allFavourites"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc ""
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield MoreTransient/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_0
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label43
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label43:
   aconst_null
   areturn
.end method
