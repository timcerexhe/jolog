.source JologWhile.jolog
.class public JologWhile
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label5:
.limit stack 3
.limit locals 2
label8:
.var 1 is jw LJologWhile; from label8 to label0
   new JologWhile
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial JologWhile/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   invokevirtual JologWhile/checkWhile()V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method public checkWhile()V
label9:
.limit stack 3
.limit locals 2
label12:
.var 1 is i I from label12 to label2
   iconst_0
   istore_1
label13:
   iload_1
   bipush 10
   if_icmpge label15
   iconst_1
   goto label16
label15:
   iconst_0
label16:
   ifeq label14
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   iload_1
   invokevirtual java/io/PrintStream/print(I)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   iload_1
   iconst_1
   iadd
   istore_1
label3:
   goto label13
label14:
 ; DO NOT SAVE TRANSIENTS ...
   return
label2:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield JologWhile/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield JologWhile/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield JologWhile/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label17
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label17:
   aconst_null
   areturn
.end method
