.source FunBack.jolog
.class public FunBack
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private update(I)V
label17:
.limit stack 5
.limit locals 2
   iconst_1
 ; THIS IS THE ELSE BRANCH
   ifeq label20
   aload_0
   getfield FunBack/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iload_1
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
label1:
label20:
 ; DO NOT SAVE TRANSIENTS ...
   return
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private pickNext(Z)Ljava/lang/String;
label21:
.limit stack 7
.limit locals 3
   iload_1
   ifne label6
label29:
.var 2 is i Ljava/lang/Integer; from label29 to label3
   aconst_null
   astore_2
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   iconst_0
   ldc "tests/FunBack.jolog: 11"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label27
label24:
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
label26:
   pop
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "tests/FunBack.jolog: 11"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   astore_2
label27:
   aload_0
   getfield FunBack/jolog_fluent_store Ljolog/FluentStore;
   ldc "java.lang.Integer"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
   checkcast java/lang/Integer
   astore_2
.catch all from label27 to label28 using label24
label28:
   aload_0
   getfield FunBack/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   ifne label31
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label31:
 ; SAVE TRANSIENTS ...
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   aload_2
   invokevirtual java/lang/Integer/toString()Ljava/lang/String;
   areturn
label4:
label5:
   nop
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label6:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"pickNext\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label3:
.catch all from label28 to label3 using label26
.end method
.method private test(Z)V
label32:
.limit stack 6
.limit locals 3
   aconst_null
   astore_2
   iload_1
   ifne label14
label35:
.var 2 is s Ljava/lang/String; from label35 to label7
   ldc ""
   astore_2
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label38:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label43:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   aload_2
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   aload_0
   iconst_0
   invokevirtual FunBack/pickNext(Z)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_2
 ; END USER PROGRAM =========================================================
.catch all from label43 to label42 using label42
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label41
label47:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label10:
label42:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
   aload_2
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   aload_0
   iconst_1
   invokevirtual FunBack/pickNext(Z)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_2
 ; END USER PROGRAM =========================================================
.catch all from label42 to label44 using label45
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label41
label48:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label11:
label44:
 ; HANDLER =========================================================
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label47
   dup
   iconst_1
   if_icmpeq label48
   pop
label45:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label41:
 ; END OF NDET =========================================================
   nop
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label9:
.catch all from label41 to label9 using label44
 ; END USER PROGRAM =========================================================
.catch all from label38 to label37 using label37
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label36
label50:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label8:
label37:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
label13:
 ; END USER PROGRAM =========================================================
.catch all from label37 to label39 using label40
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label36
label51:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label12:
label39:
 ; HANDLER =========================================================
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label50
   dup
   iconst_1
   if_icmpeq label51
   pop
label40:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label36:
 ; END OF NDET =========================================================
   nop
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_2
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; SAVE TRANSIENTS ...
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label14:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "tests/FunBack.jolog: 20"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
   aload_0
   getfield FunBack/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label7:
.catch all from label36 to label7 using label39
.end method
.method public static main([Ljava/lang/String;)V
label52:
.limit stack 3
.limit locals 2
label55:
.var 1 is f LFunBack; from label55 to label15
   new FunBack
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial FunBack/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual FunBack/test(Z)V
   aload_1
   iconst_1
   invokevirtual FunBack/update(I)V
   aload_1
   iconst_0
   invokevirtual FunBack/test(Z)V
   aload_1
   bipush 37
   invokevirtual FunBack/update(I)V
   aload_1
   iconst_0
   invokevirtual FunBack/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label15:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield FunBack/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield FunBack/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield FunBack/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield FunBack/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "allFavourites"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   ldc ""
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield FunBack/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "fav"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_0
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label56
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label56:
   aconst_null
   areturn
.end method
