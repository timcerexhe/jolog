.source SomeAll.jolog
.class public SomeAll
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label11:
.limit stack 3
.limit locals 2
label14:
.var 1 is sa LSomeAll; from label14 to label0
   new SomeAll
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial SomeAll/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual SomeAll/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private test(Z)V
label15:
.limit stack 6
.limit locals 2
   iload_1
   ifne label10
   getstatic java/lang/System/out Ljava/io/PrintStream;
   ldc "Strings? "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label20:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   aload_0
   invokevirtual SomeAll/jolog_some0_evaluateXOR()Z
   ifne label24
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label24:
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "YES"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label5:
 ; END USER PROGRAM =========================================================
.catch all from label20 to label19 using label19
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label18
label26:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label3:
label19:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
   aload_0
   invokevirtual SomeAll/jolog_some1_evaluateXOR()Z
   iconst_1
   ixor
   ifne label28
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label28:
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "NO"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label8:
 ; END USER PROGRAM =========================================================
.catch all from label19 to label21 using label22
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label18
label29:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label6:
label21:
 ; HANDLER =========================================================
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label26
   dup
   iconst_1
   if_icmpeq label29
   pop
label22:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label18:
 ; END OF NDET =========================================================
   nop
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "Integers? "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_0
   invokevirtual SomeAll/jolog_some2_evaluateXOR()Z
   invokevirtual java/io/PrintStream/print(Z)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; SAVE TRANSIENTS ...
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label10:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label18 to label2 using label21
.end method
.method private jolog_some0_evaluateXOR()Z
.limit stack 7
.limit locals 2
label30:
 ; BEGIN PICK EMIT ...
label36:
.var 1 is s Ljava/lang/String; from label36 to end_some_success
   ldc ""
   astore_1
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   iconst_0
   ldc "./tests/SomeAll.jolog: 13"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label34
 ; === PICK FAIL (stack = 0) ===
label31:
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label33:
   pop
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/SomeAll.jolog: 13"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_1
label34:
 ; === DO PICK === stack = 0
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.String"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/String"
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_1
.catch all from label34 to label35 using label31
 ; === USER PROGRAM === stack = 0
label35:
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "strings"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_1
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   ifne label38
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label38:
label4:
   nop
 ; === FINISHED COMPILING PICK === stack = 0
   iconst_1
   ireturn
end_some_success:
   pop
   iconst_0
   ireturn
.catch all from label35 to end_some_success using label33
.catch all from label30 to end_some_success using end_some_success
.end method
.method private jolog_some1_evaluateXOR()Z
.limit stack 7
.limit locals 2
label39:
 ; BEGIN PICK EMIT ...
label45:
.var 1 is s Ljava/lang/String; from label45 to end_some_success
   ldc ""
   astore_1
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   iconst_0
   ldc "./tests/SomeAll.jolog: 13"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label43
 ; === PICK FAIL (stack = 0) ===
label40:
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label42:
   pop
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/SomeAll.jolog: 13"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_1
label43:
 ; === DO PICK === stack = 0
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.String"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/String"
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_1
.catch all from label43 to label44 using label40
 ; === USER PROGRAM === stack = 0
label44:
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "strings"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_1
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   ifne label47
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label47:
label7:
   nop
 ; === FINISHED COMPILING PICK === stack = 0
   iconst_1
   ireturn
end_some_success:
   pop
   iconst_0
   ireturn
.catch all from label44 to end_some_success using label42
.catch all from label39 to end_some_success using end_some_success
.end method
.method private jolog_some2_evaluateXOR()Z
.limit stack 7
.limit locals 2
label48:
 ; BEGIN PICK EMIT ...
label54:
.var 1 is i Ljava/lang/Integer; from label54 to end_some_success
   aconst_null
   astore_1
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   iconst_0
   ldc "./tests/SomeAll.jolog: 18"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label52
 ; === PICK FAIL (stack = 0) ===
label49:
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
 ; === REPICK === stack = 0
label51:
   pop
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/SomeAll.jolog: 18"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   astore_1
label52:
 ; === DO PICK === stack = 0
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
 ; === GET CLASS === stack = 1
   ldc "java.lang.Integer"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield SomeAll/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
 ; === GET BINDING === stack = 3
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
; === PICK CASTING Object to pick type "java/lang/Integer"
   checkcast java/lang/Integer
   astore_1
.catch all from label52 to label53 using label49
 ; === USER PROGRAM === stack = 0
label53:
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "ints"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_1
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   ifne label56
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label56:
label9:
   nop
 ; === FINISHED COMPILING PICK === stack = 0
   iconst_1
   ireturn
end_some_success:
   pop
   iconst_0
   ireturn
.catch all from label53 to end_some_success using label51
.catch all from label48 to end_some_success using end_some_success
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield SomeAll/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield SomeAll/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "strings"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   ldc "string"
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   new java/lang/Boolean
   dup
   iconst_0
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "ints"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_0
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield SomeAll/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "ints"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_1
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_0
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label57
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label57:
   aconst_null
   areturn
.end method
