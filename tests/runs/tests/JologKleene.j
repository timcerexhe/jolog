.source JologKleene.jolog
.class public JologKleene
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label6:
.limit stack 3
.limit locals 2
label9:
.var 1 is jk LJologKleene; from label9 to label0
   new JologKleene
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial JologKleene/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual JologKleene/checkKleene(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private checkKleene(Z)V
label10:
.limit stack 9
.limit locals 3
   iload_1
   ifne label5
label13:
.var 2 is i I from label13 to label2
   iconst_0
   istore_2
   aload_0
   getfield JologKleene/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   new java/lang/Integer
   dup
   iload_2
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   iconst_0
   ldc "./tests/JologKleene.jolog: 9"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label15
label14:
   pop
   aload_0
   getfield JologKleene/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "./tests/JologKleene.jolog: 9"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore_2
   aload_0
   getfield JologKleene/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   iload_2
   invokevirtual java/io/PrintStream/print(I)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   iload_2
   iconst_1
   iadd
   istore_2
   iload_2
   bipush 10
   if_icmpge label18
   iconst_1
   goto label19
label18:
   iconst_0
label19:
   ifne label17
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label17:
label4:
label3:
   aload_0
   getfield JologKleene/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   new java/lang/Integer
   dup
   iload_2
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   iconst_0
   ldc "./tests/JologKleene.jolog: 9"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label15:
   nop
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
 ; SAVE TRANSIENTS ...
   aload_0
   getfield JologKleene/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label5:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield JologKleene/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"checkKleene\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label15 to label2 using label14
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield JologKleene/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield JologKleene/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield JologKleene/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label20
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label20:
   aconst_null
   areturn
.end method
