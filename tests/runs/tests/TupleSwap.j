.source TupleSwap.jolog
.class public TupleSwap
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label2:
.limit stack 6
.limit locals 9
label5:
.var 1 is x I from label5 to label0
   iconst_0
   istore_1
label6:
.var 2 is y I from label6 to label0
   iconst_1
   istore_2
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "("
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   iload_1
   invokevirtual java/io/PrintStream/print(I)V
   dup
   ldc ", "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   iload_2
   invokevirtual java/io/PrintStream/print(I)V
   dup
   ldc ")"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   iconst_2
   anewarray java/lang/Object
   dup
   iconst_0
   new java/lang/Integer
   dup
   iload_2
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   dup
   iconst_1
   new java/lang/Integer
   dup
   iload_1
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   astore_3
   aload_3
   iconst_0
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore_1
   aload_3
   iconst_1
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore_2
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "("
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   iload_1
   invokevirtual java/io/PrintStream/print(I)V
   dup
   ldc ", "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   iload_2
   invokevirtual java/io/PrintStream/print(I)V
   dup
   ldc ")"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label7:
.var 5 is a Ljava/lang/String; from label7 to label0
   ldc "alpha"
   astore 5
label8:
.var 6 is b Ljava/lang/String; from label8 to label0
   ldc "beta"
   astore 6
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "("
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload 5
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc ", "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload 6
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc ")"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   iconst_2
   anewarray java/lang/Object
   dup
   iconst_0
   aload 6
   aastore
   dup
   iconst_1
   aload 5
   aastore
   astore 7
   aload 7
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore 5
   aload 7
   iconst_1
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore 6
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "("
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload 5
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc ", "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload 6
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc ")"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield TupleSwap/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield TupleSwap/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield TupleSwap/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label9
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label9:
   aconst_null
   areturn
.end method
