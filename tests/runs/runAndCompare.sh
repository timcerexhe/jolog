#! /bin/sh

ROOT=`echo $0 | sed -r 's/^(.*[/\\])[^/\\]+$/\1/'`

JOLOG="${ROOT}../../src/jolog"
JASMIN="java -jar ${ROOT}../../../jasmin-2.3/jasmin.jar"
JAVA='java'

TEST_DIR="${ROOT}tests"
OUTPUT_DIR="${ROOT}outputs"
INPUT_DIR="${ROOT}inputs"

tests=0
passed=0

#move into the jolog directory and check for files
#we cd as a hack so there is no prefix from ls ...
#there is undoubtedly a better way to do this :(
for f in `cd "$TEST_DIR" && ls *.jolog | sed 's/^\(.*\)\.jolog$/\1/'`
do
   if [ -e "$OUTPUT_DIR/$f.out" ]; then
      echo -n "TEST $tests ($f) ............. "
      tests=`expr "$tests" + 1`
      if `$JOLOG "$TEST_DIR/$f.jolog" > /dev/null && $JASMIN "$TEST_DIR/$f.j" > /dev/null`; then
         TEMP_FILE="/tmp/.junkFile$?"
         if [ -e "$INPUT_DIR/$f.in" ]; then
            if `cat "$INPUT_DIR/$f.in" | $JAVA "$f" 2> /dev/null | diff -y - "$OUTPUT_DIR/$f.out" 2> /dev/null > "$TEMP_FILE"`; then
               echo "PASSED"
               passed=`expr "$passed" + 1`
            else
               echo "FAILED"
               echo " === diff (output | expected) ==="
               cat "$TEMP_FILE"
            fi
         elif `$JAVA "$f" 2> /dev/null | diff -y - "$OUTPUT_DIR/$f.out" 2> /dev/null > "$TEMP_FILE"`; then
            echo "PASSED"
            passed=`expr "$passed" + 1`
         else
            echo "FAILED"
            echo " === diff (output | expected) ==="
            cat "$TEMP_FILE"
         fi
         rm "$TEMP_FILE"
      else
         echo "COMPILATION FAILURE"
      fi
   else
      echo "no output file for \"$OUTPUT_DIR/$f\""
   fi
done

#throw away the java class files in this dir
rm *.class

echo "PASSED $passed OF $tests TESTS"

if [ "$passed" -eq "$tests" -a "$tests" -gt 0 ]; then
   echo "YOU ARE AWESOME!"
fi

