.source test1.jolog
.class public my/package/emptyAgent
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.method public <init>(Ljolog/Environment;)V
.limit stack 1
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_1
   putfield my/package/emptyAgent/environment Ljolog/Environment;
   invokespecial jolog/FluentStore/<init>()V
   putfield my/package/emptyAgent/jolog_fluent_store Ljolog/FluentStore;
   return
.end method
.method public receive(Ljolog/Message;)V
.limit stack 5
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label0
   ;handle actions here!
label0:
   return
.end method
