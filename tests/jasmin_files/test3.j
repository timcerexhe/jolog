.source test3.jolog
.class public mypackage/innerpackage/helloAgent2
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.method public print(I)V
.limit stack 5
.limit locals 2
   iload_1
   iconst_0
   if_icmple label1
   iconst_0
   goto label2
label1:
   iconst_1
label2:
   ifne label0
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "Hello world!"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label0:
   return
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 1
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_1
   putfield mypackage/innerpackage/helloAgent2/environment Ljolog/Environment;
   invokespecial jolog/FluentStore/<init>()V
   putfield mypackage/innerpackage/helloAgent2/jolog_fluent_store Ljolog/FluentStore;
   return
.end method
.method public receive(Ljolog/Message;)V
.limit stack 5
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label3
   ;handle actions here!
label3:
   return
.end method
