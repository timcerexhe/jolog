//import mypackage.innerpackage.helloAgent2;
//import myTester;
import jolog.Message;
import jolog.FailedPreconditionException;
import jolog.JologMonitor;
import jolog.*;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import java.util.Vector;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.InetAddress;


class tester {

   /*public static void main(String[] args) throws jolog.FailedPreconditionException, InterruptedException {
      //helloAgent2 ea = new helloAgent2();
      //ea.print(1);

      myTester mt = new myTester(null);
      Message jm = new Message("hello!");
      jm = mt.receive(jm);
      System.out.println(jm.getContent());
   }*/

   public static void main(String[] args) {
      try {
         JologMonitor m = new JologMonitor(1);
         m.addChannel(System.in, JologChannel.STDIN);

         m.addAgent(new myTester(m));
         m.start();

         Socket sock = new Socket(InetAddress.getLocalHost(), 8080);
         m.addChannel(sock.getInputStream(), JologChannel.TCP);

         //a.receive(new Message("good luck from main!", JologChannel.FUNCTION));
      //} catch (FailedPreconditionException e) {
      //   System.out.println("huh, precondition failed? "+e.getMessage());
      //} catch (InterruptedException e) {
      //   System.out.println("Interrupted, even in main! "+e.getMessage());
      } catch (java.net.UnknownHostException e) {
         System.out.println("Cannot find localhost? "+e.getMessage());
      } catch (java.io.IOException e) {
         System.out.println("Error with IO: "+e.getMessage());
      }
   }

}

