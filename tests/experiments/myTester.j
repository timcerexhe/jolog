.source myTester.jolog
.class public myTester
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method private myAction(Z)V
label22:
.limit stack 4
.limit locals 2
   iload_1
   ifne label13
   iconst_1
   ifeq label25
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "i"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast [I
   iconst_0
   iaload
   iconst_1
   if_icmple label27
   iconst_1
   goto label28
label27:
   iconst_0
label28:
   ifeq label26
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "i"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast [I
   iconst_0
   iconst_0
   iastore
label26:
label12:
label25:
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/clear()V
   goto label29
label30:
   pop
   new jolog/ProgramFailureException
   dup
   ldc "cut failed at tests/myTester.jolog: 61"
   invokespecial jolog/ProgramFailureException/<init>(Ljava/lang/String;)V
   athrow
label29:
   nop
   return
   return
label13:
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"myAction\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label11:
.catch all from label29 to label11 using label30
.end method
.method private jolog_action_0(Ljolog/Message;Z)Ljolog/Message;
label31:
.limit stack 7
.limit locals 3
   iload 1734700589
   ifne label16
label34:
.var 2 is matcher Ljava/util/regex/Matcher; from label34 to label14
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_0_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/util/regex/Pattern
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   checkcast java/lang/CharSequence
   invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
   astore_2
   aload_2
   invokevirtual java/util/regex/Matcher/matches()Z
   ifeq label35
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   new java/lang/Integer
   dup
   aload_0
   aload_2
   iconst_1
   invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
   iconst_0
   invokevirtual myTester/process(Ljava/lang/String;Z)I
   invokespecial java/lang/Integer/<init>(I)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/clear()V
   goto label36
label37:
   pop
   new jolog/ProgramFailureException
   dup
   ldc "cut failed at tests/myTester.jolog: 65"
   invokespecial jolog/ProgramFailureException/<init>(Ljava/lang/String;)V
   athrow
label36:
   nop
   new jolog/Message
   dup
   aload_2
   iconst_1
   invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
   invokespecial jolog/Message/<init>(Ljava/lang/String;)V
   areturn
label15:
   goto label38
label35:
   new jolog/FailedPreconditionException
   dup
   ldc "precondition unsatisfied in action \"jolog_action_0\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label38:
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/clear()V
   goto label39
label40:
   pop
   new jolog/ProgramFailureException
   dup
   ldc "cut failed at tests/myTester.jolog: 65"
   invokespecial jolog/ProgramFailureException/<init>(Ljava/lang/String;)V
   athrow
label39:
   nop
label16:
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"jolog_action_0\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label14:
.catch all from label39 to label14 using label40
.catch all from label36 to label14 using label37
.end method
.method private jolog_action_1(Ljolog/Message;Z)Ljolog/Message;
label41:
.limit stack 7
.limit locals 3
   iload 1734700589
   ifne label19
label44:
.var 2 is matcher Ljava/util/regex/Matcher; from label44 to label17
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "jolog_action_1_pattern"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/util/regex/Pattern
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   checkcast java/lang/CharSequence
   invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
   astore_2
   aload_2
   invokevirtual java/util/regex/Matcher/matches()Z
   ifeq label45
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "save"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "s"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   invokevirtual java/lang/String/length()I
   iconst_4
   if_icmple label47
   iconst_1
   goto label48
label47:
   iconst_0
label48:
   ifeq label46
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "i"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast [I
   iconst_0
   aload_0
   iconst_0
   invokevirtual myTester/hello(Z)I
   iastore
label46:
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "s"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   aload_1
   invokevirtual jolog/Message/getContent()Ljava/lang/String;
   ldc " : "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "i"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast [I
   iconst_0
   iaload
   invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/clear()V
   goto label49
label50:
   pop
   new jolog/ProgramFailureException
   dup
   ldc "cut failed at tests/myTester.jolog: 70"
   invokespecial jolog/ProgramFailureException/<init>(Ljava/lang/String;)V
   athrow
label49:
   nop
   aload_0
   aload_0
   getfield myTester/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "save"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   iconst_0
   invokevirtual myTester/response(Ljava/lang/String;Z)Ljolog/Message;
   areturn
label18:
   goto label51
label45:
   new jolog/FailedPreconditionException
   dup
   ldc "precondition unsatisfied in action \"jolog_action_1\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label51:
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/clear()V
   goto label52
label53:
   pop
   new jolog/ProgramFailureException
   dup
   ldc "cut failed at tests/myTester.jolog: 70"
   invokespecial jolog/ProgramFailureException/<init>(Ljava/lang/String;)V
   athrow
label52:
   nop
label19:
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"jolog_action_1\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label17:
.catch all from label52 to label17 using label53
.catch all from label49 to label17 using label50
.end method
.method private hello(Z)I
label54:
.limit stack 6
.limit locals 13
   iload_1
   ifne label21
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "Agent generating response ..."
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label57:
.var 2 is x Ljava/lang/String; from label57 to label20
   ldc "x"
   astore_2
label58:
.var 3 is y Ljava/lang/String; from label58 to label20
   ldc "y"
   astore_3
label59:
.var 5 is tuple [Ljava/lang/Object; from label59 to label20
   iconst_2
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   dup
   iconst_1
   aload_3
   aastore
   astore 5
label60:
.var 6 is a I from label60 to label20
   iconst_1
   istore 6
label61:
.var 7 is b I from label61 to label20
   iconst_2
   istore 7
   iconst_2
   anewarray java/lang/Object
   dup
   iconst_0
   new java/lang/Integer
   dup
   iload 7
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   dup
   iconst_1
   new java/lang/Integer
   dup
   iload 6
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   astore 8
   aload 8
   iconst_0
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore 6
   aload 8
   iconst_1
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore 7
   iconst_2
   anewarray java/lang/Object
   dup
   iconst_0
   aload_3
   aastore
   dup
   iconst_1
   aload_2
   aastore
   astore 10
   aload 10
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
   aload 10
   iconst_1
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_3
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "x = "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc ", y = "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_3
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc ", tuple = (x, y) = ("
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload 5
   iconst_0
   aaload
   invokevirtual java/io/PrintStream/print(Ljava/lang/Object;)V
   dup
   ldc ", "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload 5
   iconst_1
   aaload
   invokevirtual java/io/PrintStream/print(Ljava/lang/Object;)V
   dup
   ldc ") after swap?"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label62:
.var 12 is r Ljava/util/Random; from label62 to label20
   new java/util/Random
   dup
   invokespecial java/util/Random/<init>()V
   astore 12
   aload 12
   invokevirtual java/util/Random/nextInt()I
   ireturn
label21:
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"hello\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label20:
.end method
.method private response(Ljava/lang/String;Z)Ljolog/Message;
label63:
.limit stack 9
.limit locals 5
   iload_2
   ifne label8
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label68:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   aload_1
   ldc "password"
   checkcast java/lang/Object
   invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
   ifne label72
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label72:
label73:
.var 3 is exclamations Ljava/lang/String; from label73 to label2
   ldc ""
   astore_3
label74:
.var 4 is i I from label74 to label2
   iconst_0
   istore 4
   iconst_0
   istore 4
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_3
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   dup
   iconst_1
   aload_3
   aastore
   dup
   iconst_2
   new java/lang/Integer
   dup
   iload 4
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   iconst_0
   ldc "tests/myTester.jolog: 36"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label76
label75:
   pop
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "tests/myTester.jolog: 36"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   dup
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_1
 ; === variable # 1 : stack = 1
   dup
   iconst_1
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_3
 ; === variable # 2 : stack = 1
   iconst_2
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore 4
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   iload 4
   iconst_2
   if_icmpge label79
   iconst_1
   goto label80
label79:
   iconst_0
label80:
   ifne label78
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label78:
   aload_3
   ldc "!"
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_3
label4:
label3:
   iload 4
   iconst_1
   iadd
   istore 4
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_3
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   dup
   iconst_1
   aload_3
   aastore
   dup
   iconst_2
   new java/lang/Integer
   dup
   iload 4
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   iconst_0
   ldc "tests/myTester.jolog: 36"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label76:
   nop
   iload 4
   iconst_2
   if_icmpge label83
   iconst_1
   goto label84
label83:
   iconst_0
label84:
   iconst_1
   ixor
   ifne label82
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label82:
   iconst_0
   istore 4
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_3
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   dup
   iconst_1
   aload_3
   aastore
   dup
   iconst_2
   new java/lang/Integer
   dup
   iload 4
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   iconst_0
   ldc "tests/myTester.jolog: 43"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label86
label85:
   pop
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "tests/myTester.jolog: 43"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   dup
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_1
 ; === variable # 1 : stack = 1
   dup
   iconst_1
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_3
 ; === variable # 2 : stack = 1
   iconst_2
   aaload
   checkcast java/lang/Integer
   invokevirtual java/lang/Integer/intValue()I
   istore 4
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   iload 4
   iconst_2
   if_icmpge label89
   iconst_1
   goto label90
label89:
   iconst_0
label90:
   ifne label88
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label88:
   aload_3
   ldc "!"
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_3
   iload 4
   iconst_1
   iadd
   istore 4
label5:
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_3
   anewarray java/lang/Object
   dup
   iconst_0
   aload_1
   aastore
   dup
   iconst_1
   aload_3
   aastore
   dup
   iconst_2
   new java/lang/Integer
   dup
   iload 4
   invokespecial java/lang/Integer/<init>(I)V
   aastore
   iconst_0
   ldc "tests/myTester.jolog: 43"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label86:
   nop
   iload 4
   iconst_2
   if_icmpge label93
   iconst_1
   goto label94
label93:
   iconst_0
label94:
   iconst_1
   ixor
   ifne label92
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label92:
   new jolog/Message
   dup
   ldc "HAX012'd"
   aload_3
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   invokespecial jolog/Message/<init>(Ljava/lang/String;)V
   areturn
label2:
.catch all from label86 to label2 using label85
.catch all from label76 to label2 using label75
 ; END USER PROGRAM =========================================================
.catch all from label68 to label67 using label67
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label66
label96:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label1:
label67:
 ; DO NDET 2 ======================================================
   pop
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "restoring initial ndet state"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_1
 ; USER PROGRAM =========================================================
   aload_1
   ldc "password"
   checkcast java/lang/Object
   invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
   iconst_1
   ixor
   ifne label98
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label98:
   new jolog/Message
   dup
   ldc "PREVIOUS: "
   aload_1
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   invokespecial jolog/Message/<init>(Ljava/lang/String;)V
   areturn
label7:
 ; END USER PROGRAM =========================================================
.catch all from label67 to label69 using label70
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label66
label99:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label6:
label69:
 ; HANDLER =========================================================
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label96
   dup
   iconst_1
   if_icmpeq label99
   pop
label70:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield myTester/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label66:
 ; END OF NDET =========================================================
   nop
label8:
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"response\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label0:
.catch all from label66 to label0 using label69
.end method
.method private process(Ljava/lang/String;Z)I
label100:
.limit stack 3
.limit locals 4
   iload_2
   ifne label10
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "STORE "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   aload_1
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   ldc "?"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label103:
.var 3 is parsed I from label103 to label9
   aload_1
   invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
   istore_3
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   ldc "parsed = "
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   dup
   iload_3
   invokevirtual java/io/PrintStream/print(I)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   iload_3
   ireturn
label10:
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"process\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label9:
.end method
