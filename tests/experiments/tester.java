//import mypackage.innerpackage.helloAgent2;
//import myTester;
import jolog.Message;
import jolog.FailedPreconditionException;
import jolog.JologMonitor;
import jolog.*;

import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import java.util.Vector;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.Socket;
import java.net.InetAddress;


class tester {

   public static void main(String[] args) throws InterruptedException, IOException {
      //helloAgent2 ea = new helloAgent2();
      //ea.print(1);

      myTester mt = new myTester(null);

      String in;
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      while ((in = br.readLine()) != null) {
         try {
            Message jm = new Message(in);
            jm = mt.receive(jm);
            if (jm != null) {
               System.out.println(jm.getContent());
            } else {
               System.out.println("agent gave us null");
            }
            mt.myAction();
         } catch (jolog.FailedPreconditionException e) {
            System.out.println("agent threw an exception!");
         }
      }
   }

   /*public static void main(String[] args) {
      try {
         JologMonitor m = new JologMonitor(1);
         m.addChannel(System.in, JologChannel.STDIN);

         m.addAgent(new myTester(m));
         m.start();

         Socket sock = new Socket(InetAddress.getLocalHost(), 8080);
         m.addChannel(sock.getInputStream(), JologChannel.TCP);

         //a.receive(new Message("good luck from main!", JologChannel.FUNCTION));
      //} catch (FailedPreconditionException e) {
      //   System.out.println("huh, precondition failed? "+e.getMessage());
      //} catch (InterruptedException e) {
      //   System.out.println("Interrupted, even in main! "+e.getMessage());
      } catch (java.net.UnknownHostException e) {
         System.out.println("Cannot find localhost? "+e.getMessage());
      } catch (java.io.IOException e) {
         System.out.println("Error with IO: "+e.getMessage());
      }
   }*/

}

