.source junk.jolog
.class public junk
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label10:
.limit stack 3
.limit locals 2
label13:
.var 1 is j Ljunk; from label13 to label0
   new junk
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial junk/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual junk/test(Z)V
 ; DO NOT SAVE TRANSIENTS ...
   return
label0:
.end method
.method private test(Z)V
label14:
.limit stack 6
.limit locals 3
   aconst_null
   astore_2
   iload_1
   ifne label5
label17:
.var 2 is s Ljava/lang/String; from label17 to label2
   ldc ""
   astore_2
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_0
   anewarray java/lang/Object
   iconst_0
   ldc "saving initial ndet state"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
label20:
 ; DO NDET 1 ======================================================
 ; USER PROGRAM =========================================================
   ldc "i will"
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   invokestatic junk/go()Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_2
 ; END USER PROGRAM =========================================================
.catch all from label20 to label19 using label19
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_0
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label18
label24:
 ; REDO NDET 1 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 0)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label3:
label19:
 ; DO NDET 2 ======================================================
   pop
 ; USER PROGRAM =========================================================
   ldc "i will"
   ldc " "
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   invokestatic junk/stop()Ljava/lang/String;
   invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
   astore_2
 ; END USER PROGRAM =========================================================
.catch all from label19 to label21 using label22
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   aconst_null
   iconst_1
   ldc "saving ndet backtracking index"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label18
label25:
 ; REDO NDET 2 =========================================================
   pop
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "failed ndet (index = 1)"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label4:
label21:
 ; HANDLER =========================================================
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   dup
   iconst_0
   if_icmpeq label24
   dup
   iconst_1
   if_icmpeq label25
   pop
label22:
 ; FAIL POINT =========================================================
   pop
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   new jolog/FailedPreconditionException
   dup
   ldc "ndet failure"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label18:
 ; END OF NDET =========================================================
   nop
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_2
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
 ; SAVE TRANSIENTS ...
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   new jolog/TransientState
   dup
   iconst_0
   anewarray java/lang/Object
   ldc ""
   invokespecial jolog/TransientState/<init>([Ljava/lang/Object;Ljava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   return
label5:
 ; RESTORING TRANSIENT STATE
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "junk.jolog: 7"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   invokevirtual java/lang/Object/toString()Ljava/lang/String;
   astore_2
   aload_0
   getfield junk/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
 ; DONE RESTORING TRANSIENT STATE
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label18 to label2 using label21
.end method
.method private static go()Ljava/lang/String;
label26:
.limit stack 2
.limit locals 0
 ; DO NOT SAVE TRANSIENTS ...
   ldc "GO"
   areturn
label6:
.end method
.method private static stop()Ljava/lang/String;
label29:
.limit stack 2
.limit locals 0
 ; DO NOT SAVE TRANSIENTS ...
   ldc "STOP"
   areturn
label8:
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 4
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield junk/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield junk/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield junk/jolog_back_stack Ljava/util/Stack;
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label32
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label32:
   aconst_null
   areturn
.end method
