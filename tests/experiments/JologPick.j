.source JologPick.jolog
.class public JologPick
.super java/lang/Object
.implements jolog/JologAgent
.field private final environment Ljolog/Environment;
.field private final jolog_fluent_store Ljolog/FluentStore;
.field private final jolog_back_stack Ljava/util/Stack;
.method public static main([Ljava/lang/String;)V
label6:
.limit stack 3
.limit locals 2
label9:
.var 1 is p LJologPick; from label9 to label0
   new JologPick
   dup
   aconst_null
   checkcast jolog/Environment
   invokespecial JologPick/<init>(Ljolog/Environment;)V
   astore_1
   aload_1
   iconst_0
   invokevirtual JologPick/test(Z)V
   return
label0:
.end method
.method private test(Z)V
label10:
.limit stack 7
.limit locals 3
   iload_1
   ifne label5
label18:
.var 2 is i Ljava/lang/Integer; from label18 to label4
   aconst_null
   astore_2
   aload_0
   getfield JologPick/jolog_back_stack Ljava/util/Stack;
   new jolog/ChoicePointState
   dup
   iconst_1
   anewarray java/lang/Object
   dup
   iconst_0
   aload_2
   aastore
   iconst_0
   ldc "JologPick.jolog: 13"
   invokespecial jolog/ChoicePointState/<init>([Ljava/lang/Object;ILjava/lang/String;)V
   invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
   pop
   goto label16
label13:
   aload_0
   getfield JologPick/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/pop()Ljava/lang/Object;
   pop
   athrow
label15:
   pop
   aload_0
   getfield JologPick/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   ldc "JologPick.jolog: 13"
   invokevirtual jolog/ChoicePointState/restoreState(Ljava/lang/String;)[Ljava/lang/Object;
 ; === variable # 0 : stack = 1
   iconst_0
   aaload
   checkcast java/lang/Integer
   astore_2
label16:
   aload_0
   getfield JologPick/jolog_fluent_store Ljolog/FluentStore;
   ldc "java.lang.Integer"
   invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
   aload_0
   getfield JologPick/jolog_back_stack Ljava/util/Stack;
   invokevirtual java/util/Stack/peek()Ljava/lang/Object;
   checkcast jolog/ChoicePointState
   dup
   invokevirtual jolog/ChoicePointState/getBacktrackId()I
   swap
   invokevirtual jolog/ChoicePointState/increaseBacktrackId()V
   invokevirtual jolog/FluentStore/getNext(Ljava/lang/Class;I)Ljava/lang/Object;
   checkcast java/lang/Integer
   astore_2
.catch all from label16 to label17 using label13
label17:
   aload_0
   getfield JologPick/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   aload_2
   invokevirtual jolog/Predicate/addArgument(Ljava/lang/Object;)V
   invokevirtual jolog/FluentStore/load(Ljolog/Predicate;)Ljava/lang/Object;
   checkcast java/lang/Boolean
   invokevirtual java/lang/Boolean/booleanValue()Z
   iconst_1
   if_icmpne label21
   iconst_1
   goto label22
label21:
   iconst_0
label22:
   ifne label20
   new jolog/FailedPreconditionException
   dup
   ldc "holds condition"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label20:
   getstatic java/lang/System/out Ljava/io/PrintStream;
   dup
   aload_2
   invokevirtual java/io/PrintStream/print(Ljava/lang/Object;)V
   ldc "\n"
   invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
label3:
label4:
   nop
   new jolog/FailedPreconditionException
   dup
   ldc "fail"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
   return
label5:
   new jolog/FailedPreconditionException
   dup
   ldc "backtrack to function \"test\""
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label2:
.catch all from label17 to label2 using label15
.end method
.method public <init>(Ljolog/Environment;)V
.limit stack 6
.limit locals 2
   aload_0
   invokespecial java/lang/Object/<init>()V
   aload_0
   aload_1
   putfield JologPick/environment Ljolog/Environment;
   aload_0
   new jolog/FluentStore
   dup
   invokespecial jolog/FluentStore/<init>()V
   putfield JologPick/jolog_fluent_store Ljolog/FluentStore;
   aload_0
   new java/util/Stack
   dup
   invokespecial java/util/Stack/<init>()V
   putfield JologPick/jolog_back_stack Ljava/util/Stack;
   aload_0
   getfield JologPick/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_1
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield JologPick/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   iconst_2
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield JologPick/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   bipush 7
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_1
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   aload_0
   getfield JologPick/jolog_fluent_store Ljolog/FluentStore;
   new jolog/Predicate
   dup
   ldc "num"
   invokespecial jolog/Predicate/<init>(Ljava/lang/String;)V
   dup
   bipush 8
   invokevirtual jolog/Predicate/addArgument(I)V
   new java/lang/Boolean
   dup
   iconst_0
   invokespecial java/lang/Boolean/<init>(Z)V
   invokevirtual jolog/FluentStore/store(Ljolog/Predicate;Ljava/lang/Object;)V
   return
.end method
.method public receive(Ljolog/Message;)Ljolog/Message;
.limit stack 3
.limit locals 3
.throws jolog/FailedPreconditionException
.throws java/lang/InterruptedException
   aload_1
   ifnull label23
   new jolog/FailedPreconditionException
   dup
   ldc "unhandled action"
   invokespecial jolog/FailedPreconditionException/<init>(Ljava/lang/String;)V
   athrow
label23:
   aconst_null
   areturn
.end method
