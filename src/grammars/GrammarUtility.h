/*
 * GrammarUtility.h
 *
 *  Created on: 2/08/2009
 *      Author: timothyc
 */

#ifndef GRAMMAR_UTILITY
#define GRAMMAR_UTILITY

#include <iostream>
#include <sstream>
#include <string>

#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_grammar.hpp>
#include <boost/spirit/include/classic_ast.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>
#include <boost/spirit/include/classic_lists.hpp>
#include <boost/spirit/include/classic_iterator.hpp>

#define TRUNCATE_SIZE 20

using std::string;

enum ParserTags {file_tag, package_tag, import_tag, agent_tag, implements_tag, extends_tag,
                 agent_contents_tag, type_tag, action_definition_tag, fluent_definition_tag,
                 function_definition_tag, parameter_definition_tag, privacy_tag, prefix_tag,
                 initialiser_tag, scope_tag, jump_tag, expression_tag, assignment_expression_tag,
                 assignment_operator_tag, conditional_expression_tag, logical_or_expression_tag,
                 logical_and_expression_tag, relational_expression_tag, additive_expression_tag,
                 multiplicative_expression_tag, cast_expression_tag, unary_expression_tag,
                 postfix_expression_tag, array_tag, function_tag, member_tag, atomic_expression_tag,
                 expression_statement_tag, statement_tag, if_tag, else_tag, while_tag, for_tag,
                 identifier_tag, local_tag, tuple_tag, ndet_tag, kleene_tag, pick_tag, holds_tag,
                 literal_tag, string_tag, char_tag, integer_tag, double_tag, boolean_tag,
                 skip_tag, error_tag};

class ErrorMessage {
   public:
      ErrorMessage(ParserTags tag, std::ostream& errorStream, bool& errorFlag) :
         tag(tag), stderr(errorStream), errorFlag(errorFlag) {}

      ErrorMessage report(ParserTags tag) const {
         return ErrorMessage(tag, stderr, errorFlag);
      }

      /**
       * return a pretty string representation of the provided token's
       * file and line number
       */
      template <typename iter_t>
      static string location(const iter_t& i) {
         string s("unknown");
         const iter_t end;
         if (i != end) { // && (!(*i).is_root())) {
            std::stringstream ss;
            ss << i.get_position().file
            << ": "
            << i.get_position().line;
            s = ss.str();
         }
         return s;
      }

      template <typename iter_t>
      static void error(std::ostream& output, const iter_t& source, string message) {
         output << "(" << location(source) << ") ERROR: " << message << std::endl;
      }

      static string truncate(string s) {
         size_t pos = s.find_first_not_of(" \r\n\t");
         if (pos != string::npos) {
            s = s.substr(pos, string::npos);
         }
         pos = s.find('\n');
         s = s.substr(0, pos);
         if (s.size() > TRUNCATE_SIZE) {
            s = s.substr(0, TRUNCATE_SIZE) + " ...";
         }
         return s;
      }

      template <typename iter_t>
      void operator()(const iter_t& begin, const iter_t& end) const {
         //std::ostream& stderr = std::cout;
         errorFlag = true;
         string s(begin, end);
         s = truncate(s);

         //int line = begin.get_position().line; //lineNum(input, begin);

         //std::stringstream ss;
         //ss << begin.get_position().file << ": " << begin.get_position().line;
         //string line(ss.str());

         if (tag == file_tag) {
            error(stderr, begin, "stray \"" + s + "\" at file scope");
         } else if (tag == agent_tag) {
            error(stderr, begin, "unrecognised token \"" + s + "\" in agent body");
         } else if (tag == import_tag) {
            if (s.size() > 0) {
               error(stderr, begin, "invalid import \"" + s + "\" (is it quoted?)");
            } else {
               error(stderr, begin, "empty import statement");
            }
         } else if (tag == parameter_definition_tag) {
            error(stderr, begin, "expected parameter list, not \"" + s + "\"");
         } else if (tag == for_tag) {
            error(stderr, begin, "invalid for loop specifier \"" + s + "\"");
         } else if (tag == expression_tag) {
            error(stderr, begin, "expected expression but found \"" + s + "\"");
         } else if (tag == expression_statement_tag) {
            error(stderr, begin, "expected ';' after expression but found \"" + s + "\"");
         } else if (tag == statement_tag) {
            error(stderr, begin, "expected statement but found \"" + s + "\"");
         } else if (tag == fluent_definition_tag) {
            error(stderr, begin, "expected variable name after type but found \"" + s + "\"");
         } else if (tag == initialiser_tag) {
            error(stderr, begin, "expected expression or '}' but found \"" + s + "\"");
         } else if (tag == char_tag) {
            error(stderr, begin, "invalid char suffix '" + s + "'");
         } else if (tag == agent_contents_tag) {
            error(stderr, begin, "unexpected \""+s+"\" in agent body (expected action, fluent or function)");
         } else if (tag == implements_tag) {
            error(stderr, begin, "cannot implement \""+s+"\" -- invalid type declaration");
         } else if (tag == extends_tag) {
            error(stderr, begin, "cannot extend \""+s+"\" -- invalid type declaration");
         } else if (tag == action_definition_tag) {
            error(stderr, begin, "unexpected \""+s+"\" in action definition");
         } else if (tag == type_tag) {
            error(stderr, begin, "invalid type name \""+s+"\"");
         } else {
            error(stderr, begin, "unknown token \"" + s + "\" (ID: " + tagToName(tag) + ")");
         }
      }

      static std::string tagToName(int tag) {
         std::string s;
         if (tag == file_tag) { s = "file";
         } else if (tag == package_tag) { s = "package";
         } else if (tag == import_tag) { s = "import";
         } else if (tag == agent_tag) { s = "agent";
         } else if (tag == implements_tag) { s = "implements";
         } else if (tag == extends_tag) { s = "extends";
         } else if (tag == agent_contents_tag) { s = "agent contents";
         } else if (tag == type_tag) { s = "type";
         } else if (tag == privacy_tag) { s = "privacy";
         } else if (tag == action_definition_tag) { s = "action";
         } else if (tag == fluent_definition_tag) { s = "fluent";
         } else if (tag == function_definition_tag) { s = "function";
         } else if (tag == parameter_definition_tag) { s = "parameter";
         } else if (tag == prefix_tag) { s = "prefix";
         } else if (tag == initialiser_tag) { s = "initialiser";
         } else if (tag == scope_tag) { s = "scope";
         } else if (tag == jump_tag) { s = "jump";
         } else if (tag == expression_tag) { s = "expression";
         } else if (tag == assignment_expression_tag) { s = "assignment";
         } else if (tag == assignment_operator_tag) { s = "assignment operator";
         } else if (tag == conditional_expression_tag) { s = "conditional";
         } else if (tag == logical_or_expression_tag) { s = "logical or";
         } else if (tag == logical_and_expression_tag) { s = "logical and";
         } else if (tag == relational_expression_tag) { s = "relational";
         } else if (tag == additive_expression_tag) { s = "addition";
         } else if (tag == multiplicative_expression_tag) { s = "multiplication";
         } else if (tag == cast_expression_tag) { s = "cast";
         } else if (tag == unary_expression_tag) { s = "unary";
         } else if (tag == postfix_expression_tag) { s = "postfix";
         } else if (tag == array_tag) { s = "array";
         } else if (tag == function_tag) { s = "function";
         } else if (tag == member_tag) { s = "member";
         } else if (tag == atomic_expression_tag) { s = "atomic";
         } else if (tag == tuple_tag) { s = "tuple";
         } else if (tag == ndet_tag) { s = "nondeterministic branch (ndet)";
         } else if (tag == kleene_tag) { s = "nondeterministic iteration (kleene)";
         } else if (tag == pick_tag) { s = "nondeterministic arguments (pick)";
         } else if (tag == holds_tag) { s = "condition holds check";
         } else if (tag == expression_statement_tag) { s = "expression statement";
         } else if (tag == statement_tag) { s = "statement";
         } else if (tag == if_tag) { s = "if statement";
         } else if (tag == else_tag) { s = "else statement";
         } else if (tag == while_tag) { s = "while statement";
         } else if (tag == for_tag) { s = "for statement";
         } else if (tag == identifier_tag) { s = "identifier";
         } else if (tag == local_tag) { s = "local variable";
         } else if (tag == literal_tag) { s = "literal";
         } else if (tag == string_tag) { s = "string literal";
         } else if (tag == char_tag) { s = "char literal";
         } else if (tag == integer_tag) { s = "integer literal";
         } else if (tag == double_tag) { s = "double literal";
         } else if (tag == boolean_tag) { s = "boolean literal";
         } else if (tag == skip_tag) { s = "skip";
         } else if (tag == error_tag) { s = "error";
         } else { std::stringstream ss; ss << "<unknown/" << tag << ">"; s = ss.str();
         }
         return s;
      }

   private:
      ParserTags tag;
      std::ostream& stderr;
      bool& errorFlag;
};

#endif
