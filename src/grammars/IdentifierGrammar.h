/*
 * IdentifierGrammar.h
 *
 *  Created on: 2/08/2009
 *      Author: timothyc
 */

#ifndef IDENTIFIER_GRAMMAR
#define IDENTIFIER_GRAMMAR

#include <string>
#include <sstream>

#include "GrammarUtility.h"

using std::string;
using namespace boost::spirit::classic;

class IdentifierGrammar : public grammar<IdentifierGrammar> {
   public:
      typedef position_iterator<const char*> iterator_t;
      typedef node_iter_data_factory<int> factory_t;

      template <typename ScannerT>
      class definition {
         public:
            typedef ScannerT scanner_t;
            definition(IdentifierGrammar const& self) {
               identifier
               = no_node_d[*space_p] >> token_node_d[ lexeme_d[ ((alpha_p | '_') >> *(alnum_p | '_')) ] ]
                                                      ;

            }

            rule<scanner_t, parser_tag<identifier_tag> > const& start() const {
               return identifier;
            }

         private:
            rule<scanner_t, parser_tag<identifier_tag> > identifier;
      };

};


#endif
