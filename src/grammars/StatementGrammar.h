/*
 * StatementGrammar.h
 *
 *  Created on: 2/08/2009
 *      Author: timothyc
 */

#ifndef STATEMENT_GRAMMAR
#define STATEMENT_GRAMMAR

#include <string>
#include <sstream>

#include "GrammarUtility.h"
#include "ExpressionGrammar.h"
#include "IdentifierGrammar.h"

using std::string;
using namespace boost::spirit::classic;

class StatementGrammar : public grammar<StatementGrammar> {
   private:
      ErrorMessage errorReporter;

   public:
      typedef position_iterator<const char*> iterator_t;
      typedef node_iter_data_factory<int> factory_t;

      StatementGrammar(ErrorMessage e) : errorReporter(e) {}

      ErrorMessage report(ParserTags tag) const {
         return errorReporter.report(tag);
      }

      template <typename ScannerT>
      class definition {
         public:
            typedef ScannerT scanner_t;
            definition(StatementGrammar const& self) : expression(self.errorReporter) {

               scope //may infinite loop ... remove nothing_p
                  = root_node_d[ ch_p('{') ]
                  >> *(statement) // /*| ((error - '}')[self.statement_error] *//*>> nothing_p*/))
                  >> (no_node_d[ ch_p('}') ]); // | ((error - '}')[self.statement_error] /*>> nothing_p*/));

               statement
                  = jump_statement
                  | local_variable_definition
                  | if_statement
                  | while_statement
                  | for_statement
                  | holds
                  | ndet
                  | kleene
                  | pick
                  | scope
                  | !expression >> (no_node_d[ ch_p(';') ]) // | (+alnum_p)[self.report(expression_statement_tag)])
                  | ((+(alnum_p - '}'))[self.report(expression_statement_tag)])
                  ;

               if_statement
                  = root_node_d[ str_p("if") ] >> no_node_d[ ch_p('(') ] >> expression >> no_node_d[ ch_p(')') ]
                  >> scope
                  >> !else_statement;

               else_statement
                  = no_node_d[ str_p("else") ] >> (if_statement | scope);

               while_statement
                  = root_node_d[ str_p("while") ] >> no_node_d[ ch_p('(') ] >> expression >> no_node_d[ ch_p(')') ]
                  >> scope
                  ;

               for_statement
                  = no_node_d[ str_p("for") >> '(' ]
                  >> ((!expression >> ';' >> !expression >> ';' >> !expression)) //| error[self.report(for_tag)])
                  >> ch_p(')')
                  >> scope
                  ;

               jump_statement
                  = root_node_d[ str_p("return") ]
                  >> !expression
                  >> (no_node_d[ ch_p(';') ] | ((+alnum_p)[self.report(expression_statement_tag)] >> nothing_p))
                  ;

               local_variable_definition
                  = *(str_p("transient"))
                  >> typeName
                  >> identifier
                  >> !( ('=' >> expression)
                        | *(',' >> identifier)
                  )
                  >> no_node_d[ ch_p(';') ]
                  ;

               ndet
                  = root_node_d[ str_p("ndet") ]
                  >> no_node_d[ ch_p('(') ]
                  >> list_p(statement, no_node_d[ch_p('|')])
                  >> no_node_d[ ch_p(')') ]
                  ;

               pick
                  = root_node_d[str_p("pick") | "some" | "all"]
                  >> no_node_d[ch_p('(')] >> typeName >> identifier >> no_node_d[ch_p(')')]
                  >> scope
                  ;

               holds
                  = root_node_d[ch_p('?')]
                  >> no_node_d[ch_p('(')]
                  >> expression
                  >> no_node_d[ ch_p(')') >> ch_p(';') ]
                  ;

               kleene
                  = (scope | expression)
                  >> root_node_d[ch_p('*')]
                  >> no_node_d[ch_p(';')]
                  ;

            }

            rule<scanner_t, parser_tag<statement_tag> > const& start() const {
               return statement;
            }

         private:
            IdentifierGrammar identifier;
            TypeNameGrammar typeName;
            ExpressionGrammar expression;

            rule<scanner_t, parser_tag<scope_tag> > scope;
            rule<scanner_t, parser_tag<jump_tag> > jump_statement;
            rule<scanner_t, parser_tag<statement_tag> > statement;
            rule<scanner_t, parser_tag<if_tag> > if_statement;
            rule<scanner_t, parser_tag<else_tag> > else_statement;
            rule<scanner_t, parser_tag<while_tag> > while_statement;
            rule<scanner_t, parser_tag<for_tag> > for_statement;
            rule<scanner_t, parser_tag<local_tag> > local_variable_definition;
            rule<scanner_t, parser_tag<ndet_tag> > ndet;
            rule<scanner_t, parser_tag<kleene_tag> > kleene;
            rule<scanner_t, parser_tag<pick_tag> > pick;
            rule<scanner_t, parser_tag<holds_tag> > holds;

      };

};

#endif
