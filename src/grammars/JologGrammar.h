/*
 * JologGrammar.h
 *
 *  Created on: 2/08/2009
 *      Author: timothyc
 */

#ifndef JOLOG_GRAMMAR
#define JOLOG_GRAMMAR

#include <string>
#include <sstream>
#include <cstring>

#include "GrammarUtility.h"
#include "LiteralGrammar.h"
#include "IdentifierGrammar.h"
#include "TypeNameGrammar.h"
#include "ExpressionGrammar.h"
#include "StatementGrammar.h"
#include "SkipGrammar.h"

using std::string;
using namespace boost::spirit::classic;

class JologGrammar : public grammar<JologGrammar> {
   public:
      typedef position_iterator<const char*> iterator_t;
      typedef node_iter_data_factory<int> factory_t;

      template <typename ScannerT>
      class definition {
         public:
            typedef ScannerT scanner_t;
            definition(JologGrammar const& self) :
               expression(self.getErrorReporter()), statement(self.getErrorReporter()) {

               jolog_file
               = !jolog_package
               >> *jolog_import
               >> !jolog_agent
               >> (end_p | (lexeme_d[ +anychar_p ][self.report(file_tag)] >> nothing_p))
               ;

               jolog_package
               = root_node_d[ str_p("package") ]
                              >> (lexeme_d[ list_p(
                                    token_node_d[ +(alnum_p | '_') ],
                                    no_node_d[ ch_p('.') ]
                              )]
                              | error[self.report(package_tag)]
                              )
                              >> (no_node_d[ ch_p(';') ] | error[self.report(expression_statement_tag)])
                              ;

               jolog_import
               = root_node_d[ str_p("import") ]
                              >> (lexeme_d[ list_p(
                                    token_node_d[ +(alnum_p | '_' | '*') ],
                                    no_node_d[ ch_p('.') ] )
                                    ]
                                    | error[self.report(import_tag)]
                              )
                              >> (no_node_d[ ch_p(';') ] | error[self.report(expression_statement_tag)])
                              ;

               jolog_agent
               = *prefix >> str_p("agent")
               >> (typeName | error[self.report(type_tag)])
               >> ( *( implements | extends )
                     >> no_node_d[ ch_p('{') ] //| (error[self.report(class_tag)] /*>> nothing_p*/))
                                   >> *(jolog_agent_contents) // | ((+(error - '}'))[self.report(class_tag)]))
                                   >> no_node_d[ ch_p('}') ])
                                   | error[self.report(agent_tag)]
                                           ;

               implements
               = root_node_d[ str_p("implements") ] >> (typeName | error[self.report(implements_tag)]);

               extends
               = root_node_d[ str_p("extends") ] >> (typeName | error[self.report(extends_tag)]);

               jolog_agent_contents
               = action_definition
               | function_definition
               | fluent_definition
               | lexeme_d[ +(anychar_p - '}' - eol_p) ][self.report(agent_contents_tag)]
                                                        ;

               action_definition
               = *prefix
               >> (str_p("prim_action") | "exog_action" | "sensing_action")
               //we either have a pattern (for exog_actions) or a function: name(args) (for prim_actions)
               >> (((regex_pattern
                     | (identifier
                           >> no_node_d[ ch_p('(') ]
                                         >> !list_p(parameter_definition, no_node_d[ ch_p(',') ])
                                         >> no_node_d[ ch_p(')') ])
               )
               //                       >> ((no_node_d[ ch_p('(') ]  >> !list_p(parameter_definition, no_node_d[ ch_p(',') ]) >> no_node_d[ ch_p(')') ])) // | (error[self.parameter_error] >> nothing_p))
               >> no_node_d[ ch_p('{') ]
                             >> *( ("possible"
                                   >> (expression | error[self.report(action_definition_tag)])
                                   >> (no_node_d[ ch_p(';') ] | error[self.report(expression_statement_tag)])
                             )

                             | ("set"
                                   >> (expression | error[self.report(action_definition_tag)])
                                   //>> (no_node_d[ ch_p('=') ] | error[self.report(action_definition_tag)])
                                   //>> (expression | error[self.report(action_definition_tag)])
                                   >> !(str_p("if") >> expression)
                                   >> (no_node_d[ ch_p(';') ] | error[self.report(expression_statement_tag)])
                             )

                             | ("send"
                                   >> (statement | error[self.report(action_definition_tag)])
                             )

                             | ("return"
                                   >> (expression | error[self.report(action_definition_tag)])
                                   >> (no_node_d[ ch_p(';') ] | error[self.report(expression_statement_tag)])
                             )
                             )
                             >> (no_node_d[ ch_p('}') ] | error[self.report(action_definition_tag)])
               )
               | error[self.report(action_definition_tag)]
               )
               ;

               fluent_definition
               = *prefix >> typeName
               >> identifier
               >> !(no_node_d[ ch_p('(') ]
                               >> !list_p(expression, no_node_d[ ch_p(',')])
                               >> no_node_d[ ch_p(')') ]
               ) //| (error[self.variable_error] /*>> nothing_p*/))
               >> !('=' >> (initialiser | (error[self.report(expression_tag)] /*>> nothing_p*/)))
               >> (no_node_d[ ch_p(';') ] | eps_p[self.report(expression_statement_tag)])
               ;

               function_definition
               = !privacy >> *prefix >> typeName >> identifier
               >> ((no_node_d[ ch_p('(') ]  >> !list_p(parameter_definition, no_node_d[ ch_p(',') ]) >> no_node_d[ ch_p(')') ])) // | error[self.report(parameter_definition_tag)])
               >> no_node_d[ ch_p('{') ]
               >> *(statement)
               >> (no_node_d[ ch_p('}') ] | error[self.report(scope_tag)])
               ;

               parameter_definition
               = *(str_p("transient")) >> typeName >> identifier;

               prefix
               = str_p("final") | "static" | "abstract";

               initialiser
               = (root_node_d[ ch_p('{') ] >> infix_node_d[ !list_p(initialiser, ',') ] >> (no_node_d[ ch_p('}') ] | (error[self.report(initialiser_tag)] /*>> nothing_p*/)))
               | expression; //zero allowed?

               privacy
               = str_p("public") | "private" | "protected";

               regex_pattern //TODO do we want to use escapes? to allow " for example!
               = no_node_d[ ch_p('"') ] >> token_node_d[ lexeme_d[ *(anychar_p - '"')]] >> no_node_d[ ch_p('"') ];

               error
               //= lexeme_d[ +anychar_p ]
               //= (anychar_p - space_p)
               //= token_node_d[ ( +(alnum_p | '_') | ((ch_p('[') | '{' | '(') /*>> *(ch_p(']') | '}' | ')')*/) | anychar_p) ]
               = token_node_d[ +(alnum_p | space_p | '_')
                               | ((!(ch_p('[') | '{' | '(')) >> *(anychar_p - (ch_p(']') | '}' | ')')) >> (ch_p(']') | '}' | ')'))
                             ]
               ;
            }

            rule<scanner_t, parser_tag<file_tag> > const& start() const {
               return jolog_file;
            }

         private:
            LiteralGrammar literal;
            IdentifierGrammar identifier;
            ExpressionGrammar expression;
            StatementGrammar statement;
            TypeNameGrammar typeName;

            rule<scanner_t, parser_tag<file_tag> > jolog_file;
            rule<scanner_t, parser_tag<package_tag> > jolog_package;
            rule<scanner_t, parser_tag<import_tag> > jolog_import;
            rule<scanner_t, parser_tag<agent_tag> > jolog_agent;
            rule<scanner_t, parser_tag<implements_tag> > implements;
            rule<scanner_t, parser_tag<extends_tag> > extends;
            rule<scanner_t, parser_tag<agent_contents_tag> > jolog_agent_contents;
            rule<scanner_t, parser_tag<action_definition_tag> > action_definition;
            rule<scanner_t, parser_tag<fluent_definition_tag> > fluent_definition;
            rule<scanner_t, parser_tag<function_definition_tag> > function_definition;
            rule<scanner_t, parser_tag<parameter_definition_tag> > parameter_definition;
            rule<scanner_t, parser_tag<privacy_tag> > privacy;
            rule<scanner_t, parser_tag<prefix_tag> > prefix;
            rule<scanner_t, parser_tag<string_tag> > regex_pattern;
            rule<scanner_t, parser_tag<initialiser_tag> > initialiser;

            rule<scanner_t, parser_tag<error_tag> > error;
      };

      JologGrammar(std::ostream& stderr, bool& errorFlag) :
         stderr(stderr), errorFlag(errorFlag) {}

      ErrorMessage getErrorReporter() const {
         return ErrorMessage(file_tag, stderr, errorFlag);
      }

      ErrorMessage report(ParserTags tag) const {
         return ErrorMessage(tag, stderr, errorFlag);
      }

      tree_parse_info<iterator_t, factory_t> parseString(const char* s, string filename) {
         iterator_t begin(s, s+strlen(s), filename);
         iterator_t end;
         SkipGrammar skip;
         return ast_parse<factory_t>(begin, end, *this, skip);
      }

   private:
      std::ostream& stderr;
      bool& errorFlag;
};

#endif
