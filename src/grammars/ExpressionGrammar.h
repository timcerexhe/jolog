/*
 * ExpressionGrammar.h
 *
 *  Created on: 2/08/2009
 *      Author: timothyc
 */

#ifndef EXPRESSION_GRAMMAR
#define EXPRESSION_GRAMMAR

#include <string>
#include <sstream>

#include "GrammarUtility.h"
#include "LiteralGrammar.h"
#include "IdentifierGrammar.h"
#include "TypeNameGrammar.h"

using std::string;
using namespace boost::spirit::classic;

class ExpressionGrammar : public grammar<ExpressionGrammar> {
   private:
      ErrorMessage errorReporter;

   public:
      typedef position_iterator<const char*> iterator_t;
      typedef node_iter_data_factory<int> factory_t;

      ExpressionGrammar(ErrorMessage e) : errorReporter(e) {}

      ErrorMessage report(ParserTags tag) const {
         return errorReporter.report(tag);
      }

      template <typename ScannerT>
      class definition {
         public:
            typedef ScannerT scanner_t;
            definition(ExpressionGrammar const& self) {

               expression
               = list_p(assignment_expression, longest_d[ str_p("<<") | ">>" ])
               | (token_node_d[ +(anychar_p - ';' - '}') ][self.report(expression_tag)]) //TODO assuming expression statement ... do error messages seem to work with this?
               // | eps_p[self.report(expression_tag)]
               ;

               assignment_expression
               = conditional_expression >> !(root_node_d[ assignment_operator ] >> conditional_expression);

               assignment_operator
               = longest_d[ (str_p("*=") | "/=" | "%=" | "+=" | "-=" | '=') ];

               conditional_expression
               = logical_or_expression >> !('?' >> expression >> ':' >> expression);

               logical_or_expression
               = list_p(logical_and_expression, root_node_d[ str_p("||") ]);

               logical_and_expression
               //= list_p(inclusive_or_expression, root_node_d[ str_p("&&") ]);
               = list_p(relational_expression, root_node_d[ str_p("&&") ]);

               relational_expression
               = list_p(additive_expression, longest_d[ (str_p("==") | "!=" | '>' | '<' | ">=" | "<=") ]);

               additive_expression
               = list_p(multiplicative_expression, root_node_d[ (ch_p('+') | '-') ]);

               multiplicative_expression
               = list_p(cast_expression, root_node_d[ (ch_p('*') | '/' | '%') ]);

               cast_expression
               = *(inner_node_d[ '(' >> typeName >> ')' ]) >> unary_expression;

               unary_expression
               = (root_node_d[ str_p("++") | "--" ] >> unary_expression)
               | (root_node_d[ (ch_p('+') | '-' | '!') ] >> cast_expression)
               | postfix_expression
               ;

               /*postfix_expression
						= atomic_expression
						>> *( array_reference
						    | function_call
						    | member_reference
						    | (str_p("++") | "--")
						    )
						;*/

               postfix_expression
               = atomic_expression
               >> *( (root_node_d[ ch_p('[') ] >> expression >> no_node_d[ ch_p(']') ])
                     | (root_node_d[ ch_p('(') ] >> !list_p(assignment_expression, no_node_d[ ch_p(',') ]) >> no_node_d[ ch_p(')') ])
                     | (root_node_d[ ch_p('.') ] >> postfix_expression)
                     | (root_node_d[ str_p("++") | "--" ])
               )
               ;

               /*					array_reference
						= root_node_d[ ch_p('[') ]
						>> expression
						>> no_node_d[ ch_p(']') ]
						;

					function_call
						= root_node_d[ ch_p('(') ]
						>> !list_p(assignment_expression, no_node_d[ ch_p(',') ])
						>> no_node_d[ ch_p(')') ]
						;

					member_reference
						= root_node_d[ ch_p('.') ] >> postfix_expression;*/

               atomic_expression
               = literal
               | (root_node_d[ str_p("new") ] >> typeName >> ch_p('(') >> !list_p(conditional_expression, no_node_d[ ch_p(',') ]) >> no_node_d[ ch_p(')') ])
               | (root_node_d[ str_p("new") ] >> typeName >> ch_p('[') >> conditional_expression >> no_node_d[ ch_p(']') ] >> *(str_p("[]")))
               | someAll
               | identifier
               | tuple
               | (no_node_d[ ch_p('(') ] >> root_node_d[ expression ] >> no_node_d[ ch_p(')') ])
               ;

               tuple
               = root_node_d[ ch_p('(') ]
                              >> expression >> +(no_node_d[ ch_p(',') ] >> expression) //tuples need arity > 1
                              >> no_node_d[ ch_p(')') ]
                                            ;

               someAll
               = root_node_d[ str_p("some") | "all" ]
               >> no_node_d[ch_p('(')]
               >> typeName
               >> identifier
               >> no_node_d[ch_p('|')]
               >> expression
               >> no_node_d[ch_p(')')]
               ;

            }

            rule<scanner_t, parser_tag<expression_tag> > const& start() const {
               return expression;
            }

         private:
            LiteralGrammar literal;
            IdentifierGrammar identifier;
            TypeNameGrammar typeName;

            rule<scanner_t, parser_tag<expression_tag> > expression;
            rule<scanner_t, parser_tag<assignment_expression_tag> > assignment_expression;
            rule<scanner_t, parser_tag<assignment_operator_tag> > assignment_operator;
            rule<scanner_t, parser_tag<conditional_expression_tag> > conditional_expression;
            rule<scanner_t, parser_tag<logical_or_expression_tag> > logical_or_expression;
            rule<scanner_t, parser_tag<logical_and_expression_tag> > logical_and_expression;
            rule<scanner_t, parser_tag<relational_expression_tag> > relational_expression;
            rule<scanner_t, parser_tag<additive_expression_tag> > additive_expression;
            rule<scanner_t, parser_tag<multiplicative_expression_tag> > multiplicative_expression;
            rule<scanner_t, parser_tag<cast_expression_tag> > cast_expression;
            rule<scanner_t, parser_tag<unary_expression_tag> > unary_expression;
            rule<scanner_t, parser_tag<postfix_expression_tag> > postfix_expression;
            rule<scanner_t, parser_tag<array_tag> > array_reference;
            rule<scanner_t, parser_tag<function_tag> > function_call;
            rule<scanner_t, parser_tag<member_tag> > member_reference;
            rule<scanner_t, parser_tag<atomic_expression_tag> > atomic_expression;
            rule<scanner_t, parser_tag<tuple_tag> > tuple;
            rule<scanner_t, parser_tag<pick_tag> > someAll;
      };

};

#endif
