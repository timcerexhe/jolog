/*
 * TypeNameGrammar.h
 *
 *  Created on: 4/08/2009
 *      Author: timothyc
 */

#ifndef TYPENAME_GRAMMAR
#define TYPENAME_GRAMMAR

#include <string>
#include <sstream>

#include "GrammarUtility.h"

using std::string;
using namespace boost::spirit::classic;

class TypeNameGrammar : public grammar<TypeNameGrammar> {
   public:
      typedef position_iterator<const char*> iterator_t;
      typedef node_iter_data_factory<int> factory_t;

      template <typename ScannerT>
      class definition {
         public:
            typedef ScannerT scanner_t;
            definition(TypeNameGrammar const& self) {
               typeName
               = lexeme_d[ token_node_d[ ((alpha_p | '_') >> *(alnum_p | '_')) ] ]
               //>> !(no_node_d[ ch_p('<') ] >> list_p(typeName, no_node_d[ ch_p(',') ]) >> no_node_d[ ch_p('>') ])
               >> *str_p("[]")
               ;

            }

            rule<scanner_t, parser_tag<type_tag> > const& start() const {
               return typeName;
            }

         private:
            rule<scanner_t, parser_tag<type_tag> > typeName;
      };

};

#endif
