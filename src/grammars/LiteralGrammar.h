/*
 * LiteralGrammar.h
 *
 *  Created on: 2/08/2009
 *      Author: timothyc
 */

#ifndef LITERAL_GRAMMAR
#define LITERAL_GRAMMAR

#include <string>
#include <sstream>

#include "GrammarUtility.h"

using std::string;
using namespace boost::spirit::classic;

class LiteralGrammar : public grammar<LiteralGrammar> {
   public:
      typedef position_iterator<const char*> iterator_t;
      typedef node_iter_data_factory<int> factory_t;

      template <typename ScannerT>
      class definition {
         public:
            typedef ScannerT scanner_t;
            definition(LiteralGrammar const& self) {
               literal
               = longest_d[( integer_literal
                           | hex_literal
                           | double_literal
                           | boolean_literal
                           | string_literal
                           | char_literal)]
               ;

               integer_literal
               = int_p
               ;

               hex_literal
               = token_node_d[ lexeme_d[ "0x" >> uint_parser<unsigned, 16, 1, 8>() ] ]
                               ;

               double_literal
               = strict_real_p
               ;

               boolean_literal
               = as_lower_d[ (str_p("true") | "false") ];

               string_literal
               = lexeme_d[ no_node_d[ ch_p('"') ] >> token_node_d[ *(c_escape_ch_p - '"') ] >> no_node_d[ ch_p('"') ] ];

               char_literal
               = lexeme_d[ no_node_d[ ch_p('\'') ]
                             >> (c_escape_ch_p - '\'')
                             >> no_node_d[ ch_p('\'') ]
                         ];

            }

            rule<scanner_t, parser_tag<literal_tag> > const& start() const {
               return literal;
            }

         private:
            rule<scanner_t, parser_tag<literal_tag> > literal;
            rule<scanner_t, parser_tag<integer_tag> > integer_literal;
            rule<scanner_t, parser_tag<integer_tag> > hex_literal;
            rule<scanner_t, parser_tag<double_tag> > double_literal;
            rule<scanner_t, parser_tag<boolean_tag> > boolean_literal;
            rule<scanner_t, parser_tag<string_tag> > string_literal;
            rule<scanner_t, parser_tag<char_tag> > char_literal;
      };

};

#endif
