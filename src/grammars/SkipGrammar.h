/*
 * SkipGrammar.h
 *
 *  Created on: 2/08/2009
 *      Author: timothyc
 */

#ifndef SKIP_GRAMMAR
#define SKIP_GRAMMAR

#include <string>
#include <sstream>

#include "GrammarUtility.h"

class SkipGrammar : public grammar<SkipGrammar> {
   public:
      template <typename ScannerT>
      class definition {
         public:
            typedef ScannerT scanner_t;
            definition(SkipGrammar const& self) {
               skip
               = +space_p
               | comment_p("//")
               | comment_p("/*", "*/")
               ;

            }

            rule<scanner_t, parser_tag<skip_tag> > const& start() const {
               return skip;
            }

         private:
            rule<scanner_t, parser_tag<skip_tag> > skip;
      };

};

#endif
