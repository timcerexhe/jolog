/*
 * jolog.cpp
 *
 *  Created on: Aug 13, 2009
 *      Author: timothyc
 */


#include <iostream>
#include <fstream>
#include <string>

#include "compiler/JologCompiler.h"
#include "compiler/ComponentCompiler.h"

using std::string;

string getSignatureScript(string program);

int main(int argc, char** argv) {
	int status = EXIT_FAILURE;
	string filename, outputFile;

   if (argc <= 1) {
      std::cerr << "usage: jolog file.jolog [ more files ... ]" << std::endl;
   }

	for (int i = 1; i < argc; ++i) {
		filename = string(argv[i]);

		size_t dot = filename.rfind(".");
		outputFile = filename.substr(0, dot)+".j";

		std::cout << "converting " << filename << " -> " << outputFile << std::endl;


		std::ifstream input(filename.c_str());
		std::ofstream output(outputFile.c_str());

		if (input.good() && output.good()) { //our files are open ... let's go!
			string inputText, line;
			while (getline(input, line)) {
				inputText += line + "\n"; //make sure the lines are split, so we get error line numbers
			}

			if (JologCompiler::compile(inputText, filename.c_str(), output, std::cerr, getSignatureScript(argv[0]))) {
				status = EXIT_SUCCESS;
			}
		}
	}
	return status;
}

string getSignatureScript(string program) {
   size_t index = program.find_last_of("/\\");
   string script;
   if (index != string::npos) {
      script = program.substr(0, index);
      index = script.find_last_of("/\\");
      if (index != string::npos) {
         script = script.substr(0, index+1) + "getSignatures.sh"; //+1 so we include native delimiter ;)
      } else {
         script = ""; //reset if we screwed up
      }
   }
   return script;
}


