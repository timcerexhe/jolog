/*
 * TestCompiler.cpp
 *
 *  Created on: 23/06/2009
 *      Author: timothyc
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "compiler/JologCompiler.h"
#include "compiler/ComponentCompiler.h"

using std::string;

#define LINE_WIDTH 50

void testParser(string message, string input, bool expected, int& tests, int& passed);

int main(int argc, char** argv) {
	int tests = 0;
	int passed = 0;

	testParser("empty program", "", true, tests, passed);
	testParser("empty import", "import", false, tests, passed);
	testParser("unquoted import", "import file", false, tests, passed);
	testParser("import", "import file.gpp\r\n ", false, tests, passed);
	testParser("imports", "import file1.gpp\nimport file2.gpp\n", false, tests, passed);
	testParser("empty import", "import;", false, tests, passed);
	testParser("import", "import file;", true, tests, passed);
	testParser("import", "import file.gpp;\r\n ", true, tests, passed);
	testParser("imports", "import file1.gpp;\nimport file2.gpp;\n", true, tests, passed);
	testParser("imports", "import file1.gpp\nimport file2.gpp;\n", false, tests, passed);
	testParser("import", "import package.file.*;", true, tests, passed);

	testParser("empty package", "package", false, tests, passed);
	testParser("unquoted package", "package file", false, tests, passed);
	testParser("package", "package file.gpp\r\n ", false, tests, passed);
	testParser("packages", "package file1.gpp\npackage file2.gpp\n", false, tests, passed);
	testParser("empty package", "package;", false, tests, passed);
	testParser("package", "package file;", true, tests, passed);
	testParser("package", "package file.gpp;\r\n ", true, tests, passed);
	testParser("packages", "package file1.gpp;\npackage file2.gpp;\n", false, tests, passed);
	testParser("packages", "package file1.gpp\npackage file2.gpp;\n", false, tests, passed);
	testParser("package *", "package package.file.*;", false, tests, passed);
	testParser("package", "package package.file.name;", true, tests, passed);
	testParser("package+import", "package package.file; import package.file.*;", true, tests, passed);
	testParser("package+imports", "package package.file; import package.file.*; import package.file.more.*;", true, tests, passed);

	testParser("agent", "agent myAgent {}", true, tests, passed);
	testParser("if", "agent myAgent{if(true){} }", false, tests, passed);
	testParser("if (spaces)", "agentmyAgent {\n   if (true) {\n   }\n}", false, tests, passed);
	testParser("if-else", "agent myAgent {\nif(true) {} else {} }", false, tests, passed);
	testParser("if-else if-else", "agent myAgent {\nif(true) {\n} else if (false) {} else {} }", false, tests, passed);

	testParser("agent", "agent myAgent { }", true, tests, passed);
	testParser("parameters", "agent myAgent {\n void _my_3rd_func_(int meaningl3ss, real fake) {\n \n}\n}", true, tests, passed);
	testParser("parameters", "agent myAgent {\n public void _my_3rd_func_(int meaningl3ss, real fake) {\n \n}\n}", true, tests, passed);
	testParser("if", "agent myAgent{\nvoid myFunc() { if(true){} } }", true, tests, passed);
	testParser("if (spaces)", "agentmyAgent {\nint myFunc() {   if (true) {\n   }\n}}", true, tests, passed);
	testParser("if-else", "agent myAgent {\nvoid myFunc() {\nif(true) {} else {} }}", true, tests, passed);
	testParser("if-else if-else", "agent myAgent {\nvoid myFunc() {\nif(true) {} else if (false) {} else {}\n}\n}", true, tests, passed);
	testParser("if(if-else)-else", "agent myAgent {void myFunc() { if(true){if (true) {i++;} else {i--;} } else { --k; } } }", true, tests, passed);

	testParser("generic type", "agent untemplatedAgent<> {}", false, tests, passed);
	testParser("generic type", "agent untemplatedAgent<>[] {}", false, tests, passed);
	testParser("generic type", "agent myTemplatedAgent<T> {}", true, tests, passed);
	testParser("generic type", "agent myTemplatedAgent<myTemplateAgent<T>> {}", true, tests, passed);
	testParser("generic type", "agent myTemplatedAgent<T>[] {}", true, tests, passed);
	testParser("generic type", "agent myTemplatedAgent<myTemplateAgent<T>>[] {}", true, tests, passed);

	testParser("implements", "agent myAgent implements myInterface1 {}", true, tests, passed);
	testParser("implements2", "agent myAgent implements myInterface1 implements myInterface2 {}", true, tests, passed);
	testParser("extends", "agent myAgent extends myAgent1 {}", true, tests, passed);
	testParser("extends2", "agent myAgent extends myAgent1 extends myAgent2 {}", true, tests, passed);
	testParser("implement+extend", "agent myAgent implements myInterface1 extends myAgent1 {}", true, tests, passed);
	testParser("extend+implement", "agent myAgent extends myAgent1 implements myInterface1 {}", true, tests, passed);

	testParser("simple arithmetic", "agent myAgent { public int myFunc() { return 1+2*3/4%5; } }", true, tests, passed);
	testParser("spaced simple arithmetic", "agent myAgent { public int myFunc() { return (1 + 2 * 3 / 4 % 5); } }", true, tests, passed);
	testParser("empty expression", "agent myAgent { public void myFunc() { return; } }", true, tests, passed);

	testParser("bitwise or", "agent myAgent { public int myFunc() { return (1 | 2); } }", false, tests, passed);
	testParser("bitwise and", "agent myAgent { public int myFunc() { return (2 & 3); } }", false, tests, passed);
	testParser("bitwise xor", "agent myAgent { public int myFunc() { return (3 ^ 4); } }", false, tests, passed);
	testParser("bitwise not", "agent myAgent { public int myFunc() { return ~0; } }", false, tests, passed);

	testParser("logical or + and", "agent myAgent { public int myFunc() { return (1 || 2 && 3); } }", true, tests, passed);
	testParser("bitwise or, xor + and", "agent myAgent { public int myFunc() { return (1 | 2 & 3 ^ 4); } }", false, tests, passed);
	testParser("ors", "agent myAgent { public int myFunc() { return (1 || 2 || 3); } }", true, tests, passed);
	testParser("ands", "agent myAgent { public int myFunc() { return (1 && 2 && 3); } }", true, tests, passed);
	testParser("nots", "agent myAgent { public int myFunc() { return (!1 || !0); } }", true, tests, passed);

	testParser("maths/python relations", "agent myAgent { public int myFunc() { return 9 < 10 <= 13 >= 12 > 11; } }", true, tests, passed);
	testParser("ugly expression", "agent myAgent { public int myFunc() { return out << variable << (2 * (3+4%5) || i++ && --j || 6||7&&8 || 9 < 10 <= 13 >= 12 > 11); } }", true, tests, passed);
	testParser("ugly expression", "agent myAgent { public int myFunc() { return out << variable << 2 * (3+4%5) || i++ && --j || 6||7&&8 || 9 < 10 <= 13 >= 12 > 11; } }", true, tests, passed);
	testParser("ugly expression", "agent myAgent { public int myFunc() { return !0x1Fe * (3+4%5) || 9 < 10 <= 13 >= 12 > 11; } }", true, tests, passed);

	testParser("postfix", "agent myAgent { public int myFunc() { return --i++; } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i++++; } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i++; } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i--[0]; } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i[0]; } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i.j(i, 3); } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i[0].j(i, 3); } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i++[0].j(i, 3); } }", true, tests, passed);
	testParser("postfix", "agent myAgent { public int myFunc() { return i++--[0].j(i, 3); } }", true, tests, passed);

	testParser("unary", "agent myAgent { public int myFunc() { return !-i; } }", true, tests, passed);
	testParser("real unary", "agent myAgent { public int myFunc() { return !-3.14; } }", true, tests, passed);

	testParser("single assignment", "agent myAgent { public void myFunc() { i = j; j = 1+2; }}", true, tests, passed);
	testParser("multi assignment", "agent myAgent { public void myFunc() { i = 1+2 = j; }}", false, tests, passed);
	testParser("multi assignment", "agent myAgent { public void myFunc() { i = j = 1+2; }}", false, tests, passed);
	testParser("multi assignment", "agent myAgent { public void myFunc() { i = j = 1+2 = k; }}", false, tests, passed);

	testParser("variable", "agent myAgent { int myVar; }", true, tests, passed);
	testParser("variable", "agent myAgent { int myVar = 3; }", true, tests, passed);
	testParser("variable", "agent myAgent { string myVar = \"hello\"; }", true, tests, passed);
	testParser("variable", "agent myAgent { string myVar = 'hello'; }", false, tests, passed);
	testParser("variable", "agent myAgent { boolean myVar = true; }", true, tests, passed);
	testParser("variable", "agent myAgent { real myVar = 3; }", true, tests, passed);
	testParser("variable", "agent myAgent { real myVar = 3.14; }", true, tests, passed);
	testParser("variable", "agent myAgent { int[] myArray = {0, 1, 2, 3}; }", true, tests, passed);
	testParser("variable", "agent myAgent { string[] myArray = {\"a\", \"b\", \"seedy\"}; }", true, tests, passed);
	testParser("variable", "agent myAgent { int[][] my2dArray = {{0}, {1,2}, {2,3,4}}; }", true, tests, passed);

	testParser("variable", "agent myAgent { public int myVar; }", false, tests, passed);
	testParser("variable", "agent myAgent { private int myVar = 3; }", false, tests, passed);
	testParser("variable", "agent myAgent { protected string myVar = \"hello\"; }", false, tests, passed);
	testParser("variable", "agent myAgent { public string myVar = 'hello'; }", false, tests, passed);
	testParser("variable", "agent myAgent { public boolean myVar = true; }", false, tests, passed);
	testParser("variable", "agent myAgent { public real myVar = 3; }", false, tests, passed);
	testParser("variable", "agent myAgent { public real myVar = 3.14; }", false, tests, passed);
	testParser("variable", "agent myAgent { public int[] myArray = {0, 1, 2, 3}; }", false, tests, passed);
	testParser("variable", "agent myAgent { public string[] myArray = {\"a\", \"b\", \"seedy\"}; }", false, tests, passed);
	testParser("variable", "agent myAgent { public int[][] my2dArray = {{0}, {1,2}, {2,3,4}}; }", false, tests, passed);

	testParser("while", "agent myAgent { public void myFunc() { while(true) {}}}", true, tests, passed);
	testParser("while", "agent myAgent { public void myFunc() { i = 0; while(i < 1) {++i;}}}", true, tests, passed);
	testParser("for", "agent myAgent { public void myFunc() { for(;;) {}}}", true, tests, passed);
	testParser("for", "agent myAgent { public void myFunc() { for(i=1;i<2;++i) {}}}", true, tests, passed);
	testParser("for", "agent myAgent { public void myFunc() { for(i=1;i<2;++i) {++i;}}}", true, tests, passed);

	testParser("ternary", "agent myAgent { public int myFunc() { return 3<4<=5 ? 0 : 1; }}", true, tests, passed);
	testParser("ternary", "agent myAgent { public int myFunc() { return (3<4<=5 ? 0 : 1); }}", true, tests, passed);
	testParser("ternary", "agent myAgent { public int myFunc() { return ((3<4<=5) ? 0 : 1); }}", true, tests, passed);
	testParser("ternary", "agent myAgent { public void myFunc() { true?i=0:i=1;}}", true, tests, passed);
	testParser("ternary", "agent myAgent { public void myFunc() { (true)?i=0:i=1;}}", false, tests, passed);
	testParser("ternary", "agent myAgent { public void myFunc() { (true?i=0:i=1);}}", true, tests, passed);

	testParser("nested agent", "agent outer { public agent inner {} }", false, tests, passed);
	testParser("nested agent", "agent outer { public enum inner {} }", false, tests, passed);
	testParser("nested agent", "agent outer { public agent inner {} }", false, tests, passed);
	testParser("nested agent", "enum outer { public enum inner {} }", false, tests, passed);
	testParser("nested agent", "agent outer { public agent middle { public agent inner {} } }", false, tests, passed);

	testParser("variable", "agent myAgent { public void myFunc() { int i = 1; } }", true, tests, passed);
	testParser("typecast", "agent myAgent { public void myFunc() { int i = 1; real r = (real) i; } }", true, tests, passed);
	testParser("typecast", "agent myAgent { public void myFunc() { if((real) i > (bool) (0+1/2) ? 0 : 10) {} } }", true, tests, passed);

	testParser("action", "agent myAgent { action myAction() {} }", false, tests, passed);
	testParser("action", "agent myAgent { public exog_action myAction() {} }", false, tests, passed);
	testParser("action", "agent myAgent { sensing_action myAction)( {} }", false, tests, passed);
	testParser("action", "agent myAgent { private prim_action myAction() {} }", false, tests, passed);
	testParser("action", "agent myAgent { action myAction() { possible true; } }", false, tests, passed);
	testParser("action", "agent myAgent { action myAction() { possible true; possible i>3; } }", false, tests, passed);
	testParser("action", "agent myAgent { action myAction() { sets name = \"jones\"; } }", false, tests, passed);
	testParser("action", "agent myAgent { action myAction() { possible true; sets age = 3.5; } }", false, tests, passed);
	testParser("action", "agent myAgent { action myAction() { possible true; sets age = 3.5; sets heart = false; } }", false, tests, passed);
	testParser("action", "agent myAgent { action myAction() { possible askMe(age, 3, myAgent.hello); } }", false, tests, passed);

	testParser("action", "agent myAgent { action myAction {} }", true, tests, passed);
	testParser("action", "agent myAgent { public exog_action myAction {} }", true, tests, passed);
	testParser("action", "agent myAgent { sensing_action myAction {} }", true, tests, passed);
	testParser("action", "agent myAgent { private prim_action myAction {} }", true, tests, passed);
	testParser("action", "agent myAgent { action myAction { possible true; } }", true, tests, passed);
	testParser("action", "agent myAgent { action myAction { possible true; possible i>3; } }", true, tests, passed);
	testParser("action", "agent myAgent { action myAction { sets name = \"jones\"; } }", true, tests, passed);
	testParser("action", "agent myAgent { action myAction { possible true; sets age = 3.5; } }", true, tests, passed);
	testParser("action", "agent myAgent { action myAction { possible true; sets age = 3.5; sets heart = false; } }", true, tests, passed);
	testParser("action", "agent myAgent { action myAction { possible askMe(age, 3, myAgent.hello); } }", true, tests, passed);

	testParser("static agent", "static agent myAgent { }", false, tests, passed);
	testParser("abstract agent", "abstract agent myAgent { }", true, tests, passed);
	testParser("final agent", "final agent myAgent { }", true, tests, passed);

	testParser("static final agent", "static final agent myAgent { }", false, tests, passed);
	testParser("abstract final agent", "abstract final agent myAgent { }", true, tests, passed);
	testParser("final static agent", "final static agent myAgent { }", false, tests, passed);
	testParser("final abstract agent", "final abstract agent myAgent { }", true, tests, passed);
	testParser("static final agent", "static final agent myAgent { }", false, tests, passed);
	testParser("abstract final agent", "abstract final agent myAgent { }", true, tests, passed);
	testParser("final abstract static agent", "final abstract static agent myAgent { }", false, tests, passed);
	testParser("final static abstract agent", "final static abstract agent myAgent { }", false, tests, passed);
	testParser("abstract final static agent", "abstract final static agent myAgent { }", false, tests, passed);

	testParser("static function", "agent myAgent { public static void myFunc() {}}", false, tests, passed);
	testParser("abstract function", "agent myAgent { public abstract void myFunc() {}}", true, tests, passed);
	testParser("final function", "agent myAgent { public final void myFunc() {}}", true, tests, passed);

	testParser("static final function", "agent myAgent { public static final void myFunc() {}}", false, tests, passed);
	testParser("abstract function", "agent myAgent { public abstract void myFunc() {}}", true, tests, passed);
	testParser("final static function", "agent myAgent { public final static void myFunc() {}}", false, tests, passed);

	testParser("static variable", "agent myAgent { static int variable; }", false, tests, passed);
	testParser("final variable", "agent myAgent { final int variable; }", true, tests, passed);
	testParser("static final variable", "agent myAgent { static final int variable; }", false, tests, passed);
	testParser("final static variable", "agent myAgent { final static int variable; }", false, tests, passed);

	testParser("static inner agent", "agent outer { static agent myAgent { } }", false, tests, passed);
	testParser("final inner agent", "agent outer { final agent myAgent { } }", false, tests, passed);
	testParser("static final inner agent", "agent outer { static final agent myAgent { } }", false, tests, passed);
	testParser("final static inner agent", "agent outer { final static agent myAgent { } }", false, tests, passed);

	testParser("static action", "agent myAgent { public static action myAction {}}", false, tests, passed);
	testParser("abstract action", "agent myAgent { public abstract action myAction() {}}", true, tests, passed);
	testParser("final action", "agent myAgent { public final action myAction() {}}", true, tests, passed);
	testParser("abstract final action", "agent myAgent { public abstract final action myAction() {}}", true, tests, passed);
	testParser("final abstract action", "agent myAgent { public final abstract action myAction() {}}", true, tests, passed);
	testParser("final x2 abstract", "final final abstract agent myAgent { }", true, tests, passed);
	testParser("final x2 abstract x2", "abstract final final abstract agent myAgent { }", true, tests, passed);

	testParser("output", "agent printAgent { void print() { out << \"hello \" << name << endl; } }", true, tests, passed);
	testParser("output", "agent printAgent { void print() { System.out << \"hello \" << name << endl; } }", true, tests, passed);
	testParser("output", "agent printAgent { void print() { System.out << \"hello \" << this.name << endl; } }", true, tests, passed);
	testParser("output", "agent printAgent { public void myFunction(int i) { System.out << \"Hello world!\" << endl; } }", true, tests, passed);
	testParser("output", "package my.package; agent printAgent { public void myFunction(int i) { System.out << \"Hello world!\" << endl; } }", true, tests, passed);
	testParser("output", "package my.package;\r\nagent printAgent {\r\n   public void myFunction(int i) {\r\n      System.out << \"Hello world!\" << endl;\r\n   }\r\n}\r\n", true, tests, passed);

	//TODO: exceptions, function parameters?

	std::cout << "Passed " << passed << " of " << tests << " tests" << std::endl;
	if (tests == passed) {
		std::cout << "YOU ARE AWESOME!" << std::endl;
	}

	return 0;
}

void testParser(string message, string input, bool expected, int& tests, int& passed) {
	std::stringstream console;
	std::stringstream ss;

	ss << "Test " << tests << ": " << message << " ";
	++tests;

	int length = ss.str().size();
	size_t size = std::max(LINE_WIDTH-length, 0);
	string dots(size, '.');
	ss << dots;

	std::stringstream filename;
	filename << "test " << (tests-1);

	if (JologCompiler::parse(input, filename.str(), console) == expected) {
		++passed;
		ss << " passed";
	} else {
		ss << " failed";
	}
	//std::cout << ss.str() << std::endl;
	std::cout << console.str() << ss.str() << std::endl;
}

