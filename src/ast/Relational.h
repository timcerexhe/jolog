/*
 * Relational.h
 *
 *  Created on: Aug 22, 2009
 *      Author: timothyc
 */

#ifndef RELATIONAL
#define RELATIONAL

#include <string>
#include <vector>

#include "JologNode.h"

using std::string;
using std::vector;

class Relational : public JologNode {
	public:
		Relational(JologNode* p1, string op, JologNode* p2, string location) :
		   JologNode("relational expression", location),
		   p1(JologCast::convertToPrimitive(p1, location)),
		   p2(JologCast::convertToPrimitive(p2, location)), op(op) {

		   ensure(op == "==" || op == "!=" || op == "<" || op == ">" || op == "<=" || op == ">=",
		          "unknown relational or equality operator '"+op+"'");
		}

		virtual ~Relational() {
			delete p1;
			delete p2;
		}

		virtual JologType getType() const {
			JologType p1Type = p1->getType();
			JologType p2Type = p2->getType();
			//TODO: allow implicit conversions
			ensure(p1Type == p2Type, "type mismatch with "+op+" operator ("+p1Type.prettyPrint()+" vs "+p2Type.prettyPrint()+")");

			//all primitive types (except Booleans) work with all operators
			//references and Booleans only work with == and !=
			ensure(((JologType::isPrimitiveType(p1Type) && p1Type != JologType::Boolean()) ||
			       op == "==" || op == "!="), "relational operator "+op+" does not apply to "+p1Type.prettyPrint());
			return JologType::Boolean();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			p1->emit(out, stack);
			p2->emit(out, stack);
			string failLabel = newLabel();
			string endLabel = newLabel();
			out << opcodes::branchOnRelationalFailure(op, p1->getType(), failLabel, stack);
			stack.push(); //load result

			StackDepth leftStack, rightStack; //only one can occur ... we ignore both
			//fall through on success
			out << opcodes::loadBooleanLiteral(true, leftStack);
			out << opcodes::gotoLabel(endLabel);

			out << opcodes::label(failLabel);
			out << opcodes::loadBooleanLiteral(false, rightStack);

			//end!
			out << opcodes::label(endLabel);
		}

	private:
		JologNode* p1;
		JologNode* p2;
		string op;
};

#endif
