/*
 * Holds.h
 *
 *  Created on: Oct 5, 2009
 *      Author: timothyc
 */

#ifndef HOLDS
#define HOLDS

#include "JologNode.h"
#include "opcodes.h"
#include "JologType.h"

class Holds : public JologNode {
   public:
      Holds(JologNode* condition, string location) :
            JologNode("holds", location), condition(condition) {}

      virtual ~Holds() {
         delete condition;
      }

      virtual JologType getType() const {
         return JologType::Boolean();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         JologType type = condition->getType();
         ensure(type == JologType::Boolean(),
                "holds check requires boolean condition, got \""+condition->getType().prettyPrint()+"\"");

         string failPoint = newLabel();
         string endPoint = newLabel();
         condition->emit(out, stack);
         out << opcodes::branchIfTrue(endPoint, false, stack);

         //fail point
         out << opcodes::throwFailedPreconditionException("holds condition", stack);

         //the end!
         out << opcodes::label(endPoint);
         checkStack(stack.final(), initial, "holds");
      }

   private:
      JologNode* condition;
};

#endif
