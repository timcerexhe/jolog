/*
 * ShortLiteral.h
 *
 *  Created on: Oct 6, 2009
 *      Author: timothyc
 */

#ifndef SHORT_LITERAL
#define SHORT_LITERAL

#include <string>
#include "JologNode.h"

using std::string;

class ShortLiteral : public JologNode {
	public:
		ShortLiteral(short s) :
         JologNode("short literal"), value(s) {}

		virtual ~ShortLiteral() {}

		virtual JologType getType() const {
			return JologType::Short();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadIntegerLiteral(value, stack);
		}

	private:
		short value;
};

#endif
