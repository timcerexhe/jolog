/*
 * StackDepth.h
 *
 *  Created on: Sep 10, 2009
 *      Author: timothyc
 */

#ifndef STACK_DEPTH
#define STACK_DEPTH

#include "../compiler/JologCompileException.h"

class StackDepth {
   public:
      StackDepth() : stack(0), highest(0) {}

      void push() {
         ++stack;
         highest = std::max(highest, stack);
      }

      void pop(string location="<internal opcode>") {
         if (stack <= 0) {
            throw JologCompileException(location, "pop from an empty stack");
         }
         --stack;
      }

      int max() {
         return highest;
      }

      int final() {
         return stack;
      }

   private:
      int stack, highest;
};

#endif
