/*
 * If.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef IF
#define IF

#include "JologNode.h"
#include "Ndet.h"
#include "opcodes.h"

class If : public JologNode {
	public:
		If(string location) :
         JologNode("if statement", location), condition(0), p1(0), p2(0) {}

		void addCondition(JologNode* c) {
			ensure(c != 0, "if statement without a condition");
			condition = c;
		}

		virtual JologType getType() const {
			return JologType::Void();
		}

		void addThen(JologNode* e) {
			ensure(e != 0, "if statement without a body");
			p1 = e;
		}

		void addElse(JologNode* e) {
			p2 = e;
		}

		virtual ~If() {
         delete condition;
         delete p1;
         if (p2 != 0) { delete p2; }
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   ensure(condition != 0, "if statement requires a condition");
		   ensure(p1 != 0, "if statement requires a then body");

		   int initial = stack.final();
			string elsePoint = newLabel();

			//condition
			condition->emit(out, stack);
			out << " ; THIS IS THE ELSE BRANCH" << std::endl;
			out << opcodes::branchIfTrue(elsePoint, true, stack);

			//then statement
			StackDepth leftStack, rightStack;
			p1->emit(out, leftStack);

			if (p2 != 0) { //there is an else
				string endPoint = newLabel();

				out << opcodes::gotoLabel(endPoint); //if jumps over else

				out << opcodes::label(elsePoint); //else point
				p2->emit(out, rightStack);

				out << opcodes::label(endPoint); //the end is here
			} else { //no else
				out << opcodes::label(elsePoint); //no else, so we just end
			}

			checkStack(leftStack.final(), 0, "if body");
         checkStack(rightStack.final(), 0, "else body");

         int max = std::max(leftStack.max(), rightStack.max());
         for (int i = 0; i < max; ++i) { //increase the max
            stack.push();
         }
         for (int i = 0; i < max; ++i) { //take it back down
            stack.pop("<internal -- if> ("+getLocation()+")");
         }
         checkStack(stack.final(), initial, "if statement");
		}

	private:
		JologNode* condition;
		JologNode* p1;
		JologNode* p2;
};

#endif
