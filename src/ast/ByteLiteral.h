/*
 * ByteLiteral.h
 *
 *  Created on: Oct 6, 2009
 *      Author: timothyc
 */

#ifndef BYTE_LITERAL
#define BYTE_LITERAL

#include <string>
#include "JologNode.h"

using std::string;

class ByteLiteral : public JologNode {
	public:
		ByteLiteral(int b) :
         JologNode("byte literal"), value(b) {
		   ensure(b >= -128 && b <= 127, "byte literal '"+opcodes::intToString(b)+"' out of range -128 to 127");
		}

		virtual ~ByteLiteral() {}

		virtual JologType getType() const {
			return JologType::Byte();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadIntegerLiteral(value, stack);
		}

	private:
		int value;
};

#endif
