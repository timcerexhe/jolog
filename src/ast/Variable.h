/*
 * Variable.h
 *
 *  Created on: Aug 17, 2009
 *      Author: timothyc
 */

#ifndef VARIABLE
#define VARIABLE

#include "JologType.h"
#include "opcodes.h"
#include "Identifier.h"
#include "IntegerLiteral.h"
#include "BooleanLiteral.h"
#include "DoubleLiteral.h"
#include "LongLiteral.h"
#include "CharLiteral.h"
#include "FloatLiteral.h"
#include "ShortLiteral.h"
#include "ByteLiteral.h"
#include "StringLiteral.h"
#include "NamespaceResolver.h"

class Variable : public JologNode {
   public:
      Variable(string location, JologType* type, string name, JologNode* initialiser,
               string endLabel, NamespaceResolver* resolver, bool transient) :

               JologNode("variable", location), varType(type), varName(name),
               init(initialiser), endLabel(endLabel), resolver(resolver) {

         if (init != 0) {
            ensure(init->getType() == *type, "cannot convert \""+initialiser->getType().getClassName()+"\" to \""+
                   varType->getClassName()+"\" when initialising \""+varName+"\"");

         } else if (varType->getType() == JologType::Int()) {
            init = new IntegerLiteral(0);
         } else if (varType->getType() == JologType::Boolean()) {
            init = new BooleanLiteral(false);
         } else if (varType->getType() == JologType::Double()) {
            init = new DoubleLiteral(0);
         } else if (varType->getType() == JologType::Long()) {
            init = new LongLiteral(0);
         } else if (varType->getType() == JologType::Character()) {
            init = new CharLiteral(0);
         } else if (varType->getType() == JologType::Float()) {
            init = new FloatLiteral(0);
         } else if (varType->getType() == JologType::Short()) {
            init = new ShortLiteral(0);
         } else if (varType->getType() == JologType::Byte()) {
            init = new ByteLiteral(0);
         } else if (varType->getType() == JologType::String()) {
            init = new StringLiteral("");
         } else { //reference
            std::cout << "providing null" << std::endl;
            init = new Identifier(0, "null", resolver, "");
         }
         slot = resolver->addLocal(varName, *varType, getLocation(), transient);
      }

      virtual JologType getType() const {
         return *varType;
      }

      string getName() const {
         return varName;
      }

      JologNode* getInitialiser() const {
         return init;
      }

      int getSlot() const {
         return slot;
      }

      bool operator==(const Variable& other) const {
         return ((varName == other.varName) && (*varType == *(other.varType)));
      }

      virtual ~Variable() {
         if (varType != 0) { delete varType; }
         if (init != 0) { delete init; }
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         string startLabel = newLabel();
         out << opcodes::label(startLabel);
         out << ".var " << slot << " is " << varName << " " << varType->getTypeName() << " from " << startLabel << " to " << endLabel << opcodes::endl();
         init->emit(out, stack);
         out << opcodes::store(slot, varType->getType(), stack);
      }

      virtual void assign(ostream& out, const JologNode* initialiser) const {
         ensure(false, "cannot assign to internal class \"Variable\"");
      }

   private:
      JologType* varType;
      string varName;
      JologNode* init;
      int slot;
      string endLabel;
      NamespaceResolver* resolver;
};

#endif
