/*
 * StringLiteral.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef STRING_LITERAL
#define STRING_LITERAL

#include <string>
#include "JologNode.h"

using std::string;

class StringLiteral : public JologNode {
	public:
		StringLiteral(string s) :
         JologNode("string literal"), value(s) {}

		virtual ~StringLiteral() {}

		virtual JologType getType() const {
			return JologType::String();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadStringLiteral(value, stack);
		}

	private:
		string value;
};

#endif
