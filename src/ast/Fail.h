/*
 * Fail.h
 *
 *  Created on: Oct 5, 2009
 *      Author: timothyc
 */

#ifndef FAIL
#define FAIL

#include "JologNode.h"
#include "opcodes.h"
#include "JologType.h"

class Fail : public JologNode {
   public:
      Fail(string location) :
          JologNode("fail", location) {}

      virtual ~Fail() {}

      virtual JologType getType() const {
         return JologType::Void();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         out << opcodes::throwFailedPreconditionException("fail", stack);
         checkStack(stack.final(), initial, "fail");
      }

};

#endif
