/*
 * Kleene.h
 *
 *  Created on: Sep 29, 2009
 *      Author: timothyc
 *
 *  IMPORTANT NOTE!
 *  Kleene star relies on catching arbitrary exceptions after the star is performed
 *  and retrying the starred program to see if that fixes the problem.
 *  This means that ALL TRAILING CODE must be emitted within the Kleene exception
 *  handler, hence the *endScope* field.
 *
 *  USE IT! :)
 *
 *  Note also that if the kleene program fails, then kleene will NOT catch it
 *  that would not be useful, this allows us to potentially backtrack from a
 *  kleene construct (from a while loop, for example).
 */

#ifndef KLEENE
#define KLEENE

#include "JologNode.h"
#include "NamespaceResolver.h"

class Kleene : public JologNode {
   public:
      /**
       * @param endScope: indicates what region of code the kleene star should catch
       * exceptions from. This should ordinarily be the end of current scope.
       */
      Kleene(string location, JologNode* program, string endScope, NamespaceResolver* resolver) :
         JologNode("kleene", location), program(program), endScope(endScope), resolver(resolver) {
         ensure(program != 0, "kleene operator requires a program");
      }

      virtual ~Kleene() {
         delete program;
      }

      virtual JologType getType() const {
         return JologType::Void();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         string startKleene = newLabel();
         string endKleene = newLabel();

         //out << "KLEENE (stack = " << stack.final() << ")" << std::endl;
         //save state
         std::vector<NamespaceResolver::variableType> variables;
         resolver->getStateVariables(variables);
         resolver->saveState(out, stack, variables, true, getLocation());

         //try zero executions first ...
         out << opcodes::gotoLabel(endKleene);

         //do the program once more ...
         out << opcodes::label(startKleene);

         stack.push(); //exception handler catches exception
         out << opcodes::pop(stack); //and we promptly throw it away ...

         //restore state + pop it off stack
         resolver->restoreState(out, stack, variables, getLocation());
         resolver->popState(out, stack, "kleene @ "+getLocation());

         //run the user program
         program->emit(out, stack);

         //save new state
         resolver->saveState(out, stack, variables, true, getLocation());

         out << opcodes::label(endKleene);

         //catch subsequent exceptions and try the program again to see if that fixes things
         resolver->addTrailingExceptionHandler(endKleene, endScope, startKleene);
         //out << opcodes::catchException(JologType::FailedPreconditionException().getClassName(), endKleene, endScope, startKleene);

         //make sure that there is always an instruction between
         //postCall and endScope (otherwise the JVM gets upset about
         //catching an exception over an empty range)
         out << opcodes::nop();

         checkStack(stack.final(), initial, "kleene");
      }

   private:
      JologNode* program;
      string endScope;
      NamespaceResolver* resolver;
};

#endif
