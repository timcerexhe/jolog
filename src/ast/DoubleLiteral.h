/*
 * DoubleLiteral.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef DOUBLE_LITERAL
#define DOUBLE_LITERAL

#include <string>
#include "JologNode.h"

using std::string;

class DoubleLiteral : public JologNode {
	public:
		DoubleLiteral(double r) :
         JologNode("real literal"), value(r) {}

		virtual ~DoubleLiteral() {}

		virtual JologType getType() const {
			return JologType::Double();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadDoubleLiteral(value, stack);
		}

	private:
		double value;
};

#endif
