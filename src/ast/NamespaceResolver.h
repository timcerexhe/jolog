/*
 * NamespaceResolver.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef NAMESPACE_RESOLVER
#define NAMESPACE_RESOLVER

#include <map>
#include <iostream>
#include <sstream>
#include "JologNode.h"
#include "../compiler/JologCompileException.h"

using std::ostream;
using std::pair;
using std::make_pair;
using std::map;

class NamespaceResolver {
   private:
      struct Resolution {
         public:
            Resolution() : name(), type("void"), isStatic(false), slot(NO_SLOT), transient(false) {}

            Resolution(string name, JologType type, bool isStatic, int slot, bool transient) :
               name(name), type(type), isStatic(isStatic), slot(slot), transient(transient) {}

            string name;
            JologType type;
            bool isStatic;
            int slot;
            bool transient;
      };

   public:
      typedef Resolution variableType;
      static const int NO_SLOT = -1; //needs to be negative: slots >= 0 exist

      NamespaceResolver(const NamespaceResolver* parent, int* slots = 0) :
         types(), resolutions(), parent(parent), slots(slots) {
         if (parent == 0) {
            throw JologCompileException("", "child namespace resolver requires a non-null parent");
         }
         if (slots == 0) { //if we weren't explicitly given one then we use our parent's count
            this->slots = parent->slots;
         }
      }

      NamespaceResolver() : types(), resolutions(), parent(0), slots(0) {
         //TODO: add inheritance hierarchies, to improve resolution

         //we are root! add global definitions
         //primitive types map to themselves (just in case)
         addType("I", "I");
         addType("D", "D");
         addType("B", "B");
         addType("L", "L");
         addType("F", "F");
         addType("C", "C");
         addType("Z", "Z");
         addType("S", "S");
         addType("V", "V");

         //this is how mortals (should) declare types
         addType("int", "I");
         addType("double", "D");
         addType("byte", "B");
         addType("long", "L");
         addType("float", "F");
         addType("char", "C");
         addType("boolean", "Z");
         addType("short", "S");
         addType("void", "V");

         //corresponding object types for all primitives
         addType("Integer", "java/lang/Integer");
         addType("Double", "java/lang/Double");
         addType("Byte", "java/lang/Byte");
         addType("Long", "java/lang/Long");
         addType("Float", "java/lang/Float");
         addType("Character", "java/lang/Character");
         addType("Boolean", "java/lang/Boolean");
         addType("Short", "java/lang/Short");

         //some useful definitions that we can't really do without
         addType("System", "java/lang/System");
         addType("String", "java/lang/String");
         addType("Object", "java/lang/Object");
         addMember("endl", "java/lang/String", true, true);
         addMember("null", "java/lang/Object", true, true);

         //streamy goodness ...
         addType("PrintStream", "java/io/PrintStream");
         addType("InputStream", "java/io/InputStream");
         addType("DataInputStream", "java/io/DataInputStream");
//TODO constructor?
         addMember("java/io/PrintStream/print(Ljava/lang/Object;)V", "void", false, true);
         addMember("java/io/PrintStream/print(Ljava/lang/String;)V", "void", false, true);
         addMember("java/io/PrintStream/print(I)V", "void", false, true);
         addMember("java/io/PrintStream/print(D)V", "void", false, true);
         addMember("java/io/PrintStream/print(Z)V", "void", false, true);
         addMember("java/io/PrintStream/print()V", "void", false, true);

         addMember("java/util/Scanner/nextBoolean()Z", "Z", false, true);
         addMember("java/util/Scanner/nextByte()B", "B", false, true);
         addMember("java/util/Scanner/nextDouble()D", "D", false, true);
         addMember("java/util/Scanner/nextFloat()F", "F", false, true);
         addMember("java/util/Scanner/nextInt()I", "I", false, true);
         addMember("java/util/Scanner/nextLong()L", "L", false, true);
         addMember("java/util/Scanner/nextShort()S", "S", false, true);
         addMember("java/util/Scanner/next(Ljava/lang/String;)Ljava/lang/String;", "String", false, true);

         // ... now with newlines
         addMember("java/io/PrintStream/println(Ljava/lang/Object;)V", "void", false, true);
         addMember("java/io/PrintStream/println(Ljava/lang/String;)V", "void", false, true);
         addMember("java/io/PrintStream/println(I)V", "void", false, true);
         addMember("java/io/PrintStream/println(D)V", "void", false, true);
         addMember("java/io/PrintStream/println(Z)V", "void", false, true);
         addMember("java/io/PrintStream/println()V", "void", false, true);

         //some more useful bits (now that we have streams declared!)
         addMember("java/lang/System/in", "java/io/InputStream", true, true);
         addMember("java/lang/System/out", "java/io/PrintStream", true, true);
         addMember("java/lang/System/err", "java/io/PrintStream", true, true);
         addType("InterruptedException", "java/lang/InterruptedException");
         addType("CharSequence", "java/lang/CharSequence");
         addMember("java/lang/String/equals(Ljava/lang/Object;)Z", "boolean", false, true);
         addMember("java/lang/String/length()I", "int", false, true);
         addType("Stack", "java/util/Stack");
         addMember("java/util/Stack/size()I", "int", false, true);

         //regex stuff
         addType("Matcher", "java/util/regex/Matcher");
         addType("Pattern", "java/util/regex/Pattern");
         addMember("java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;", "Pattern", true, true);
         addMember("java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;", "Matcher", false, true);
         addMember("java/util/regex/Matcher/matches()Z", "boolean", false, true);
         addMember("java/util/regex/Matcher/group(I)Ljava/lang/String;", "String", false, true);

         //important Jolog stuff
         addType("FailedPreconditionException", "jolog/FailedPreconditionException");
         addType("JologAgent", "jolog/JologAgent");
         addType("Message", "jolog/Message");
         addType("JologMonitor", "jolog/JologMonitor");
         addType("FluentStore", "jolog/FluentStore");
         addType("Environment", "jolog/Environment");
         addMember("jolog/Environment/addAgent(Ljolog/JologAgent;)V", "V", false, true);
         addMember("jolog/Message/getContent()Ljava/lang/String;", "String", false, true);

         addType("HubEnvironment", "jolog/HubEnvironment");
         addMember("jolog/HubEnvironment/addAgent(Ljolog/JologAgent;)V", "V", false, true);
         addMember("jolog/HubEnvironment/readFromStream(Ljava/io/InputStream;)V", "V", false, true);

         //string -> primitive parsers
         addMember("java/lang/Integer/parseInt(Ljava/lang/String;)I", "int", true, true);
         addMember("java/lang/Double/parseDouble(Ljava/lang/String;)D", "double", true, true);
         addMember("java/lang/Long/parseLong(Ljava/lang/String;)L", "long", true, true);
         addMember("java/lang/Float/parseFloat(Ljava/lang/String;)F", "float", true, true);
         addMember("java/lang/Character/parseCharacter(Ljava/lang/String;)C", "char", true, true);
         addMember("java/lang/Short/parseShort(Ljava/lang/String;)S", "short", true, true);
         addMember("java/lang/Boolean/parseBoolean(Ljava/lang/String;)Z", "boolean", true, true);

         //special objects' toStrings() ...
         addMember("java/lang/Object/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Integer/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Double/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Long/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Short/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Character/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Float/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Byte/toString()Ljava/lang/String;", "String", false, true);
         addMember("java/lang/Boolean/toString()Ljava/lang/String;", "String", false, true);

         //useful while we don't have a proper resolver :(
         addMember("java/util/Random/nextInt()I", "I", false, true);
      }

      JologType lookupType(string type, string location) const {
         JologType jt(JologType::Void());

         map<string, string>::const_iterator t = types.find(type);
         if (t != types.end()) {
            jt = JologType((*t).second);
         } else {
            bool found = false;
            t = types.begin();
            while (t != types.end() && !found) {
               if ((*t).second == type) {
                  found = true;
                  jt = JologType((*t).second);
               }
               ++t;
            }

            if (!found) {
               map<string, Resolution>::const_iterator r = resolutions.find(type);
               if (r != resolutions.end()) {
                  jt = (*r).second.type;

               } else if (parent != 0) {
                  jt = parent->lookupType(type, location);
               } else {
                  throw JologCompileException(location, "\""+type+"\" could not be resolved to a type");
               }
            }
         }

         return jt;
      }

      int getFreeSlot(string location, JologType type) {
         int size = 1;
         if (type == JologType::Long() || type == JologType::Double()) {
            size = 2;
         }

         if (slots == 0) {
            throw JologCompileException(location, "cannot add local variables to agent scope");
         }
         int free = *slots;
         (*slots) += size;
         std::cout << "GETTING NEXT FREE SLOT (" << free << ") --> FUNCTION SLOTS = " << *slots << "                                                                                       *" << std::endl;
         return free;
      }

      void addType(string name, string fqcn) {
         if (types.find(name) == types.end()) {
            types[name] = fqcn;
            std::cout << " === ADD TYPE : " << name << " <=> " << fqcn << std::endl;
         }
      }

      //NB all functions are transient: it doesn't make sense to save them!
      void addMember(string fqcn, JologType type, bool isStatic, bool transient) {
         if (resolutions.find(fqcn) == resolutions.end()) {
            resolutions[fqcn] = Resolution(fqcn, type, isStatic, NO_SLOT, transient);
            std::cout << " === ADD MEMBER : " << fqcn << " <=> " << type.prettyPrint() << " (static = " << isStatic << ", transient = " << transient << ")" << std::endl;
         }
      }

      //NB all functions are transient: it doesn't make sense to save them!
      void addMember(string fqcn, string type, bool isStatic, bool transient) {
         if (resolutions.find(fqcn) == resolutions.end()) {
            resolutions[fqcn] = Resolution(fqcn, lookupType(type, "namespace internal"), isStatic, NO_SLOT, transient);
            std::cout << " === ADD MEMBER : " << fqcn << " <=> " << type << " (static = " << isStatic << ", transient = " << transient << ")" << std::endl;
         }
      }

      int addLocal(string fqcn, JologType type, string location, bool transient) {
         int slot;
         if (resolutions.find(fqcn) == resolutions.end()) {
            std::cout << " === ADD LOCAL : " << fqcn << " <=> " << type.prettyPrint() << " (" << location << " / transient = " << transient << ")" << std::endl;
            slot = getFreeSlot(location, type);
            resolutions[fqcn] = Resolution(fqcn, type, false, slot, transient);
         } else {
            throw JologCompileException(location, "variable \""+fqcn+"\" already defined in this scope");
            //slot = resolutions.find(fqcn)->second.slot;
         }
         return slot;
      }

      int getSlot(string fqcn) const {
         int slot = NO_SLOT;
         map<string, Resolution>::const_iterator r = resolutions.find(fqcn);
         if (r != resolutions.end()) {
            slot = (*r).second.slot;
         } else if (parent != 0) {
            slot = parent->getSlot(fqcn);
         }
         return slot;
      }

      int nextSlot() const {
         std::cout << "NEXT SLOT = " << *slots << std::endl;
         return *slots;
      }

      bool isStatic(string name) const {
         bool s = true;
         map<string, Resolution>::const_iterator r = resolutions.find(name);
         if (r != resolutions.end()) {
            s = (*r).second.isStatic;
            std::cout << " === IS STATIC? : " << name << " => " << (*r).second.type.prettyPrint() << " (static = " << (*r).second.isStatic << ", transient = " << (*r).second.transient << ")" << std::endl;
         } else if (parent != 0) {
            s = parent->isStatic(name);
         }
         return s;
      }

      JologType resolveFunctionReturnType(string location, string definition) const {
         JologType ret(JologType::Void());
         bool found = false;
         for (map<string, Resolution>::const_iterator it = resolutions.begin(); !found && it != resolutions.end(); ++it) {
            string fqcn = (*it).second.name;
            if (fqcn.size() > definition.size()) { //it is long enough to be right
               if (fqcn.substr(0, definition.size()) == definition) { //so check substrings
                  ret = (*it).second.type;
                  found = true;
               }
            }
         }
         if (!found) {
            if (parent != 0) {
               ret = parent->resolveFunctionReturnType(location, definition);
            } else {
               //ret = JologType::Void();
               throw JologCompileException(location, "cannot resolve return type of unknown function \""+definition+"\"");
            }
         }

         std::cout << "resolving return type of \"" << definition << "\" to \"" << ret.getClassName() << "\"" << std::endl;
         return ret;
      }

      void addTrailingExceptionHandler(string from, string to, string with) {
         handlers.push_back(opcodes::catchException(JologType::allExceptions(), from, to, with));
      }

      void printTrailingHandlers(std::ostream& out) const {
         //print out backwards (that is: first in, last out)
         //earlier code catches AFTER later code is finished
         for (std::vector<string>::const_reverse_iterator it = handlers.rbegin(); it != handlers.rend(); ++it) {
            out << *it;
         }
      }

      void initialiseBacktrackStack(std::ostream& out, StackDepth& stack) const {
         out << opcodes::loadReference(0, stack);
         out << opcodes::newObject(JologType::BacktrackStack(), stack);
         out << opcodes::duplicate(stack);
         stack.pop("<internal initialise backtrack stack>"); //stack object
         out << opcodes::invokespecial(JologType::BacktrackStack().getClassName(),
                                       "<init>()"+JologType::Void().getTypeName());
         out << opcodes::putField(lookupType("this", "constructor").getClassName()+"/"+JologType::backtrack_stack_fieldName(),
                                  JologType::BacktrackStack(), stack);
      }

      void saveDummyState(std::ostream& out, StackDepth& stack, int backtrackId, string location) const {
         std::vector<Resolution> variables;
         emitSaveState(out, stack, true, true, backtrackId, variables, location);
      }

      void saveState(std::ostream& out, StackDepth& stack, std::vector<Resolution>& variables,
                     bool serialise, string location) const {
         emitSaveState(out, stack, false, serialise, 0, variables, location);
      }

      //DO NOT CALL THIS METHOD ON A DUMMY STATE!
      void restoreState(std::ostream& out, StackDepth& stack,
                        std::vector<Resolution>& variables, string location) const {
         emitRestoreState(out, stack, variables, location);
      }

      void clearBacktrackHistory(std::ostream& out, StackDepth& stack) const {
         int initial = stack.final();

         JologType thisClass = lookupType("this", "clear backtrack history");
         out << opcodes::load(0, thisClass, stack);
         out << opcodes::getField(thisClass.getClassName()+"/"+JologType::backtrack_stack_fieldName(),
                                  JologType::BacktrackStack(), stack);
         stack.pop("<internal clear backtrack history>"); //stack
         out << opcodes::invokevirtual(JologType::BacktrackStack().getClassName(), "clear()"+JologType::Void().getTypeName());

         if (initial != stack.final()) { throw JologCompileException("INTERNAL COMPILER ERROR", "clearing backtrack history should not affect final stack depth"); }
      }

      //TODO Jolog needs an error message too!
      void popState(std::ostream& out, StackDepth& stack, string location) const {
         int initial = stack.final();

         JologType thisClass = lookupType("this", "pop backtrack state");
         out << opcodes::load(0, thisClass, stack);
         out << opcodes::getField(thisClass.getClassName()+"/"+JologType::backtrack_stack_fieldName(),
                                  JologType::BacktrackStack(), stack);
         stack.pop("pop choice point in "+location); //stack
         stack.push(); //last choice point
         out << opcodes::invokevirtual(JologType::BacktrackStack().getClassName(), "pop()"+JologType::Object().getTypeName());
         out << opcodes::pop(stack); //through choice point away

         if (initial != stack.final()) { throw JologCompileException("INTERNAL COMPILER ERROR", "popping backtrack state should not affect final stack depth"); }
      }

      void getBacktrackIndex(std::ostream& out, StackDepth& stack, string location, bool increase) const {
         int initial = stack.final();
         JologType thisClass = lookupType("this", "pop backtrack state");
         out << opcodes::load(0, thisClass, stack);
         out << opcodes::getField(thisClass.getClassName()+"/"+JologType::backtrack_stack_fieldName(),
                                  JologType::BacktrackStack(), stack);
         stack.pop("pop choice point in "+location); //stack
         stack.push(); //last choice point
         out << opcodes::invokevirtual(JologType::BacktrackStack().getClassName(),
                                       "peek()"+JologType::Object().getTypeName());

         //make sure we really have a choice point (not just some object!)
         out << opcodes::typecast(JologType::ChoicePointState(), stack);

         if (increase) {
            out << opcodes::duplicate(stack); //save a copy for later
         }

         stack.pop("pop choice point in "+location); //choice point
         stack.push(); //index
         out << opcodes::invokevirtual(JologType::ChoicePointState().getClassName(), "getBacktrackId()"+JologType::Int().getTypeName());

         if (increase) {
            out << opcodes::swap(stack); //we need the choice point back! swap it with the backtrack id
            stack.pop("pop choice point in "+location);
            out << opcodes::invokevirtual(JologType::ChoicePointState().getClassName(), "increaseBacktrackId()"+JologType::Void().getTypeName());
         }


         if (initial + 1 != stack.final()) { throw JologCompileException("INTERNAL COMPILER ERROR", "loading backtrack index should not affect final stack depth"); }
      }

      void getStateVariables(std::vector<Resolution>& variables, bool transient=false) const {
         //ask parent scope for any variables it may be holding
         if (parent != 0) {
            parent->getStateVariables(variables, transient);
         }
         //then collect all of our own
         for (map<string, Resolution>::const_iterator it = resolutions.begin(); it != resolutions.end(); ++it) {
            //only accept variables (ie they have a slot), but don't add the this pointer ...
            if (((*it).second.slot != NO_SLOT) &&
                ((*it).second.transient == transient) &&
                ((*it).second.name != "this")) {
               variables.push_back((*it).second);
            }
         }
      }

      void getAllLocals(std::vector<Resolution>& locals) const {
         if (parent != 0) {
            parent->getAllLocals(locals);
         }
         for (map<string, Resolution>::const_iterator it = resolutions.begin(); it != resolutions.end(); ++it) {
            if ((*it).second.slot != NO_SLOT && (*it).second.name != "this") {
               locals.push_back((*it).second);
            }
         }
      }

      void printAllFunctions() const {
         for(map<string,Resolution>::const_iterator i = resolutions.begin(); i != resolutions.end(); ++i) {
            if ((*i).second.slot == NO_SLOT) {
               std::cout << " - " << (*i).second.name << std::endl;
            }
         }
      }

   private:
      std::map<string, string> types;
      std::map<string, Resolution> resolutions;
      const NamespaceResolver* parent;
      int* slots;
      std::vector<string> handlers;

      void emitSaveState(std::ostream& out, StackDepth& stack, bool dummy, bool serialise, int backtrackId,
                         std::vector<Resolution>& variables, string location) const {
         int initial = stack.final();

         //get the backtrack stack
         JologType thisClass = lookupType("this", "pop backtrack state");
         out << opcodes::load(0, thisClass, stack);
         out << opcodes::getField(thisClass.getClassName()+"/"+JologType::backtrack_stack_fieldName(),
                                  JologType::BacktrackStack(), stack);

         if (serialise) {
            //create a new choice point
            out << opcodes::newObject(JologType::ChoicePointState(), stack);
            out << opcodes::duplicate(stack);
         } else {
            out << opcodes::newObject(JologType::TransientState(), stack);
            out << opcodes::duplicate(stack);
         }

         if (!dummy) {
            emitLocals(out, stack, variables);
         } else {
            out << opcodes::loadNull(stack);
         }

         if (serialise) {
            out << opcodes::loadIntegerLiteral(backtrackId, stack);
            out << opcodes::loadStringLiteral(location, stack);
            stack.pop("save state ("+location+")"); //string
            stack.pop("save state ("+location+")"); //backtrack id
            stack.pop("save state ("+location+")"); //data
            stack.pop("save state ("+location+")"); //new choice point object
            string args = JologType(JologType::Object().getClassName(), 1).getTypeName() +
                          JologType::Int().getTypeName() +
                          JologType::String().getTypeName();
            out << opcodes::invokespecial(JologType::ChoicePointState().getClassName(),
                                          "<init>("+args+")"+JologType::Void().getTypeName());
         } else {
            out << opcodes::loadStringLiteral(location, stack);
            stack.pop("save transient state ("+location+")"); //string
            stack.pop("save transient state ("+location+")"); //data
            stack.pop("save transient state ("+location+")"); //new transient state object
            string args = JologType(JologType::Object().getClassName(), 1).getTypeName() +
                          JologType::String().getTypeName();
            out << opcodes::invokespecial(JologType::TransientState().getClassName(),
                                          "<init>("+args+")"+JologType::Void().getTypeName());
         }

         //push the choice point onto the stack
         stack.pop("save state ("+location+")"); //stack
         stack.pop("save state ("+location+")"); //choice point
         stack.push(); //return
         out << opcodes::invokevirtual(JologType::BacktrackStack().getClassName(),
                                       "push("+JologType::Object().getTypeName()+")"+JologType::Object().getTypeName());
         out << opcodes::pop(stack); //throw the return value away

         if (initial != stack.final()) { throw JologCompileException("INTERNAL COMPILER ERROR", "storing backtrack state should not affect final stack depth"); }
      }

      void emitLocals(std::ostream& out, StackDepth& stack, std::vector<Resolution>& variables) const {
         out << opcodes::loadIntegerLiteral(variables.size(), stack);
         out << opcodes::newArray(JologType::Object(), stack);

         int index = 0;
         for (std::vector<Resolution>::const_iterator i = variables.begin(); i != variables.end(); ++i) {
            out << opcodes::duplicate(stack);
            out << opcodes::loadIntegerLiteral(index, stack);
            JologCast::emitLocalAsReference(out, (*i).slot, (*i).type, stack);
            out << opcodes::storeArray(JologType::Object(), stack);
            index++;
            //TODO warn/ensure that each variable is serializable/externalizable
         }
      }

      void emitRestoreState(std::ostream& out, StackDepth& stack,
                            std::vector<Resolution>& variables, string location) const {
         int initial = stack.final();

         if (variables.size() > 0) {
            //get the backtrack stack
            JologType thisClass = lookupType("this", "pop backtrack state");
            out << opcodes::load(0, thisClass, stack);
            out << opcodes::getField(thisClass.getClassName()+"/"+JologType::backtrack_stack_fieldName(),
                                     JologType::BacktrackStack(), stack);

            //get the top choice point (DO NOT REMOVE IT!)
            stack.pop("restore state ("+location+")"); //stack
            stack.push(); //last choice point
            out << opcodes::invokevirtual(JologType::BacktrackStack().getClassName(), "peek()"+JologType::Object().getTypeName());

            //make sure we really have a choice point (not just some object!)
            out << opcodes::typecast(JologType::ChoicePointState(), stack);

            out << opcodes::loadStringLiteral(location, stack);

            stack.pop("restore state ("+location+")"); //location string
            stack.pop("restore state ("+location+")"); //choice point object
            stack.push(); //state array
            out << opcodes::invokevirtual(JologType::ChoicePointState().getClassName(),
                                          "restoreState("+JologType::String().getTypeName()+")"+JologType(JologType::Object().getClassName(), 1).getTypeName());
            int arrayIndex = 0;
            for (std::vector<Resolution>::const_iterator it = variables.begin(); it != variables.end(); ++it) {
               out << " ; === variable # " << arrayIndex << " : stack = " << stack.final() << std::endl; //TODO remove me!
               if (it != variables.end() - 1) {
                  out << opcodes::duplicate(stack);
               }
               out << opcodes::loadIntegerLiteral(arrayIndex, stack);
               arrayIndex++;
               out << opcodes::loadArray(JologType::Object(), stack);

               std::cout << "RESOLVER CASTING Object to variable type \"" << (*it).type.prettyPrint() << "\"" << std::endl;
               JologCast::castAtoB(out, JologType::Object(), (*it).type, stack, "restore state @ "+location);
               out << opcodes::store((*it).slot, (*it).type, stack);
            }
         }

         if (initial != stack.final()) { } //throw JologCompileException("INTERNAL COMPILER ERROR", "restoring backtrack state should not affect final stack depth ("+opcodes::intToString(initial)+" != "+opcodes::intToString(stack.final())+")"); }
      }

};

#endif
