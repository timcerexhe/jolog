/*
 * Fluent.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef FLUENT
#define FLUENT

#include <vector>
#include "JologType.h"
#include "JologCast.h"
#include "NamespaceResolver.h"
#include "opcodes.h"

using std::vector;

class Fluent : public JologNode {
   public:
      Fluent(JologType* type, string name, JologNode* initialiser,
             NamespaceResolver* resolver, string location, bool allowAssignment) :
         JologNode("fluent", location), type(type), name(name),
         initialiser(initialiser), resolver(resolver), allowAssignment(allowAssignment) {

         resolver->addMember(name, *type, false, true); //assert this doesn't exist?
         //NB make transient since we should not be saving/restoring fluents ...
      }

      /**
       * Fluent "call" constructor -- resolver should already know what type it is
       * and will get angry (intentionally) if it doesn't
       */
      Fluent(string name, NamespaceResolver* resolver, string location, bool allowAssignment) :
         JologNode(location), name(name), initialiser(0),
         resolver(resolver), allowAssignment(allowAssignment) {

         type = new JologType(resolver->lookupType(name, getLocation()));
      }

      void addArgument(JologNode* a) {
         args.push_back(a);
      }

      const JologNode* getInitialiser() const {
         return initialiser;
      }

      virtual string getName() const {
         return name;
      }

      virtual JologType getType() const {
         return *type;
      }

      virtual void assign(ostream& out, const JologNode* initialiser, StackDepth& stack) const {
         ensure(allowAssignment, "only actions may assign to fluents");
         int initial = stack.final();
         loadPredicate(out, stack);
         JologCast::emitAsReference(out, initialiser, stack);

         stack.pop("fluent initialiser ("+getLocation()+")"); //initialiser
         stack.pop("fluent name ("+getLocation()+")"); //predicate
         stack.pop("fluent value ("+getLocation()+")"); //store
         out << opcodes::invokevirtual(JologType::FluentStore().getClassName(),
               "store(" + JologType::Predicate().getTypeName() + JologType::Object().getTypeName() + ")" +
               JologType::Void().getTypeName());
         checkStack(stack.final(), initial, "storing fluent");
      }

      virtual ~Fluent() {
         delete type;
         for (vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
            delete *it;
         }
         delete initialiser;
         //NB do not delete resolver ... it does not belong to us!
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         loadPredicate(out, stack);

         stack.pop("fluent name ("+getLocation()+")"); //predicate
         stack.pop("fluent store ("+getLocation()+")"); //fluent store
         stack.push(); //fluent
         out << opcodes::invokevirtual(JologType::FluentStore().getClassName(), "load("+JologType::Predicate().getTypeName()+")"+JologType::Object().getTypeName());

         std::cout << "FLUENT " << name << " casting OBJECT to " << getType().prettyPrint() << std::endl;
         JologCast::castAtoB(out, JologType::Object(), getType(), stack, getLocation());

         checkStack(stack.final(), initial + 1, "loading fluent");
      }

   private:
      JologType* type;
      string name;
      vector<JologNode*> args;
      JologNode* initialiser;
      NamespaceResolver* resolver;
      bool allowAssignment;

      void loadPredicate(ostream& out, StackDepth& stack) const {
         out << opcodes::loadReference(0, stack); //load this.jolog_fluent_store
         string fluentStore(resolver->lookupType("this", getLocation()).getClassName() + "/" + JologType::fluent_store_fieldName());
         out << opcodes::getField(fluentStore, JologType(JologType::FluentStore()), stack);

         out << opcodes::newObject(JologType::Predicate(), stack);
         out << opcodes::duplicate(stack);
         out << opcodes::loadStringLiteral(name, stack);

         stack.pop("fluent predicate name ("+getLocation()+")"); //string
         stack.pop("fluent predicate object ("+getLocation()+")"); //predicate
         out << opcodes::invokespecial(JologType::Predicate().getClassName(), "<init>("+JologType::String().getTypeName()+")"+JologType::Void().getTypeName());
         for (vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
            out << opcodes::duplicate(stack);

            (*it)->emit(out, stack);

            stack.pop("fluent argument "+opcodes::intToString(it - args.begin())+" ("+getLocation()+")"); //argument
            stack.pop("fluent argument "+opcodes::intToString(it - args.begin())+" ("+getLocation()+")"); //predicate

            if ((*it)->getType() == JologType::Long() || (*it)->getType() == JologType::Double()) {
               stack.pop("long fluent argument "+opcodes::intToString(it - args.begin())+" ("+getLocation()+")"); //remember the REST of these longer datatypes
            }

            //make sure we only use a primitive type or java/lang/Object when calling addArgument()
            string argType = (*it)->getType().getTypeName();
            if (!JologType::isPrimitiveType((*it)->getType())) {
               argType = JologType::Object().getTypeName();
            }
            out << opcodes::invokevirtual(JologType::Predicate().getClassName(), "addArgument("+argType+")"+JologType::Void().getTypeName());
         }
      }

};

#endif
