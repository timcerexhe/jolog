/*
 * Function.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef FUNCTION
#define FUNCTION

#include <vector>

#include "JologNode.h"
#include "Scope.h"
#include "Label.h"
#include "Return.h"
#include "FailedPreconditionException.h"
#include "opcodes.h"
#include "Variable.h"
#include "NamespaceResolver.h"
#include "RestoreState.h"

using std::vector;

class Function : public JologNode {
   public:
      Function(JologType* returnType, string privacy,
               bool staticModifier, bool abstractModifier, bool finalModifier, string name,
               const NamespaceResolver* parent, string location) :

               JologNode("function definition", location), returnType(returnType), privacy(privacy),
               staticModifier(staticModifier), abstractModifier(abstractModifier), finalModifier(finalModifier),
               name(name), arguments(), body(0), slots(0) {

         ensure(parent != 0, "functions must belong to an agent scope");
         resolver = new NamespaceResolver(parent, &slots);
         if (!staticModifier) {
            ensure(resolver->addLocal("this", parent->lookupType("this", location), location, true) == 0, "could not insert \"this\" pointer as 0th parameter");
         }

         //slot = 1 for this pointer is ok (for now) since static functions are not permitted
         ensure(returnType != 0, "functions require a return type");
         std::cout << "CREATED FUNCTION \"" << name << "\" (slots currently @ " << slots << ")" << std::endl;
         ensure(privacy == "public" || privacy == "private" || privacy == "protected",
                "unknown privacy level \""+privacy+"\"");
      }

      virtual void addArgument(Variable* v) {
         arguments.push_back(v);

         //string type = resolver->lookupType(v->getType()).getType();
         //std::cout << "ADD FUNCTION ARGUMENT: type = " << type << std::endl;
         //resolver->addMember(v->getName(), type, false, v->getSlot());
         //TODO: ensure no duplications in resolver (or use for detecting backtracking!)
      }

      virtual void importPrototype(NamespaceResolver* parent) {
         //grab the next slot after the other args
         //resolver doesn't know this is actually a function argument
         //nor does it need to: no one else is allowed to use it
         if (willBacktrack()) {
            backtrackSlot = resolver->getFreeSlot(getLocation(), *returnType);
         }
         parent->addMember(getFQCN(), *returnType, staticModifier, true);
      }

      virtual void addBody(Scope* b) {
         //prototype should be fully defined by now, but we need to add it to PARENT scope
         body = b;
         redoPoint = newLabel();

         //save transient state (if backtracking is allowed) before inserting a default return
//         vector<NamespaceResolver::variableType> transientVariables;
//         resolver->getStateVariables(transientVariables, true);

//         if (willBacktrack()) {
////            std::cout << "TRANSIENT VARIABLES IN FUNCTION " << name << ":" << std::endl;
////            for (vector<NamespaceResolver::variableType>::const_iterator it = transientVariables.begin(); it != transientVariables.end(); ++it) {
////               std::cout << " - " << (*it).name << std::endl;
////            }
//            //save transients
//            //bodyStream << " ; DEFAULT RETURN : SAVE TRANSIENTS!" << opcodes::endl();
//            //resolver->saveState(bodyStream, stack, transientVariables, false, getLocation());
//            body->addStatement(new SaveState(getLocation(), resolver));
//         }

         if (returnType != 0 && *returnType != JologType::Void()) {
            //TODO provide default args
            //bodyStream << opcodes::returnType(*returnType, stack);
         } else {
            body->addStatement(new Return(0, willBacktrack() ? resolver : 0));
            //bodyStream << opcodes::returnVoid();
         }

         if (willBacktrack()) {
            body->addStatement(new Label(redoPoint));
            //bodyStream << opcodes::label(redoPoint);

            body->addStatement(new RestoreState(getLocation(), body->getResolver()));
            //resolver->restoreState(bodyStream, stack, transientVariables, getLocation());
            //resolver->popState(bodyStream, stack, getLocation());

            body->addStatement(new FailedPreconditionException("backtrack to function \""+name+"\""));
            //bodyStream << opcodes::throwFailedPreconditionException("backtrack to function \""+name+"\"", stack);
         }
      }

      virtual ~Function() {
         if (returnType != 0) { delete returnType; }
         if (body) { delete body; }
         for (vector<Variable*>::const_iterator it = arguments.begin(); it != arguments.end(); ++it) {
            if (*it != 0) { delete *it; }
         }
         delete resolver;
      }

      virtual JologType getType() const {
         return returnType->getType();
      }

      virtual string getFQCN() const {
         string fqcn = resolver->lookupType("this", getLocation()).getClassName() + "/" + name + "(";

         //arguments
         for (vector<Variable*>::const_iterator it = arguments.begin(); it != arguments.end(); ++it) {
            //fqcn += resolver->lookupType((*it)->getType().getClassName(), getLocation()).getTypeName();
            fqcn += (*it)->getType().getTypeName();
         }

         if (willBacktrack()) {
            fqcn += JologType::Boolean().getTypeName();
         }

         fqcn += ")" + returnType->getTypeName();
         return fqcn;
      }

      virtual bool willBacktrack() const {
         return isPrivate() && !isStatic();
      }

      virtual bool isStatic() const {
         return staticModifier;
      }

      virtual bool isPrivate() const {
         return privacy == "private";
      }

      virtual void emit(ostream& out, StackDepth& ignore) const {
         out << ".method " << privacy << " ";
         if (staticModifier) { out << "static "; }
         if (finalModifier) { out << "final "; }
         if (abstractModifier) { out << "abstract "; }

         out << name + "(";

         //arguments
         for (vector<Variable*>::const_iterator it = arguments.begin(); it != arguments.end(); ++it) {
            //out << resolver->lookupType((*it)->getType().getClassName(), getLocation()).getTypeName();
            out << (*it)->getType().getTypeName();
         }

         if (willBacktrack()) {
            out << JologType::Boolean().getTypeName();
         }

         out << ")" + returnType->getTypeName() << opcodes::endl();

         string start = newLabel();
         string end = newLabel();
         string fail = newLabel();
         //out << opcodes::catchException(JologType::allExceptions(), start, end, end);
         out << opcodes::label(start);

         StackDepth stack;
         std::ostringstream bodyStream;

         if (willBacktrack()) {
            bodyStream << opcodes::load(backtrackSlot, JologType::Boolean(), stack);
            bodyStream << opcodes::branchIfTrue(redoPoint, false, stack);
         }
         body->emit(bodyStream, stack); //emit body into a temporary stream

         /*if (!staticModifier) {
            //safety first! also, this is after we we *should* have returned
            //so this nop should never get called ...
            bodyStream << opcodes::nop();
            bodyStream << opcodes::label(end);
            bodyStream << opcodes::throwFailedPreconditionException("backtrack into function", stack); //throw to emulate backtracking into function
         }*/

         //ha! the body has now grabbed all the local variables it needs and worked out the stack size
         out << ".limit stack " << std::max(2, stack.max()) << opcodes::endl();
         out << ".limit locals " << slots << opcodes::endl(); //'slots' stores the variable number we are up to!

         //little hack to initialise all of the function's transient variables at the start:
         //the JVM doesn't seem happy with us doing it at the end, though it appears to work regardless ...
         vector<NamespaceResolver::variableType> transients;
         body->getResolver()->getStateVariables(transients, true);
         for (vector<NamespaceResolver::variableType>::const_iterator i = transients.begin(); i != transients.end(); ++i) {
            out << opcodes::loadNull(stack);
            out << opcodes::store((*i).slot, (*i).type, stack);
         }

         out << bodyStream.str(); //now spit out the body to the actual file

         out << ".end method" << opcodes::endl();
      }

      virtual NamespaceResolver* getResolver() const {
         return resolver;
      }

   private:
      JologType* returnType;
      string privacy;
      bool staticModifier, abstractModifier, finalModifier;
      string name;
      vector<Variable*> arguments;
      Scope* body;
      NamespaceResolver* resolver;
      int slots;
      int backtrackSlot;
      string redoPoint;

};

#endif
