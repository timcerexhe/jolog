/*
 * Ndet.h
 *
 *  Created on: Sep 29, 2009
 *      Author: timothyc
 */

#ifndef NDET
#define NDET

#include <vector>
#include "JologNode.h"
#include "NamespaceResolver.h"

class Ndet : public JologNode {
	public:
	   Ndet(string location, string endScope, NamespaceResolver* resolver) :
	      JologNode("Ndet", location), endScope(endScope), resolver(resolver) {}

		void addProgram(Scope* p, string endLabel) {
			ensure(p != 0, "cannot add null statement to scope");
			programs.push_back(std::make_pair(p, endLabel));
		}

		virtual ~Ndet() {
			std::vector< pair<JologNode*,string> >::const_iterator it = programs.begin();
			while (it != programs.end()) {
				delete (*it).first;
				++it;
			}
		}

		virtual JologType getType() const {
			return JologType::Void();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
	      StackDepth junkStack;
	      ensure(programs.size() > 1, "ndet requires at least two subprograms");

		   int initial = stack.final();
			std::vector< std::pair<JologNode*,string> >::const_iterator it = programs.begin();
			string endLabel = newLabel(); //"END_LABEL";
			string nextSubProgram = newLabel(); //"subProgram2";
			string lastSubProgram = newLabel(); //"subProgram1";
         string handler = newLabel(); //"HANDLER";
         string failPoint = newLabel(); //"NDET_FAILURE";

         std::vector<string> retries;
         std::vector<NamespaceResolver::variableType> variables;
         resolver->getStateVariables(variables);

         //save initial state
         resolver->saveState(out, stack, variables, true, "saving initial ndet state");

         int ndetIndex = 0;
			while (it != programs.end()) {
	         //start a subprogram
	         out << opcodes::label(lastSubProgram);
	         out << " ; DO NDET " << ndetIndex+1 << " ======================================================" << std::endl;

            if (it != programs.begin()) { //restore initial state and try an alternative ...
               stack.push(); //exception handler catches exception
               out << opcodes::pop(stack); //and we promptly throw it away ...

               resolver->restoreState(out, stack, variables, "restoring initial ndet state");
            }

            out << " ; USER PROGRAM =========================================================" << std::endl;
            int preProgram = stack.final();
		   	(*it).first->emit(out, stack);
		   	checkStack(stack.final(), preProgram, "ndet subprogram "+opcodes::intToString(ndetIndex));
		   	out << " ; END USER PROGRAM =========================================================" << std::endl;

            if (it != programs.end() - 1) {
               out << opcodes::catchException(JologType::allExceptions(), lastSubProgram, nextSubProgram, nextSubProgram);
               lastSubProgram = nextSubProgram;
               nextSubProgram = newLabel(); //"subProgram"+opcodes::intToString(ndetIndex+3);
            } else {
               out << opcodes::catchException(JologType::allExceptions(), lastSubProgram, handler, failPoint);
            }

            //save handler index (we don't care about actual state: we have the initial state
		   	//there is potentially a user program on top, but that means it may be able to
		   	//catch a future exception if we backtrack here again, we just need to remember
		   	//which subprogram needs to be retried
		   	resolver->saveDummyState(out, stack, ndetIndex, "saving ndet backtracking index");

				out << opcodes::gotoLabel(endLabel); //we made it! jump to the end!

				//we have backtracked and want to retry this subprogram
				//throw exception and see who wants to claim it!
            string redo = newLabel(); //"REDO_"+opcodes::intToString(ndetIndex+1);
            retries.push_back(redo);
            out << opcodes::label(redo);
            out << " ; REDO NDET " << ndetIndex+1 << " =========================================================" << std::endl;

            stack.push(); //exception handler catches exception
            out << opcodes::pop(stack); //and we promptly throw it away ...

            junkStack.push(); //hack so it doesn't complain next step ...
            out << opcodes::pop(junkStack); //throw away duplicated index too (see handler)
            out << opcodes::throwFailedPreconditionException("failed ndet (index = "+opcodes::intToString(ndetIndex)+")", stack);

            //give the scope its end point
            out << opcodes::label((*it).second);

            ndetIndex++;
				++it;
			}

			//backtracking exception handler
			out << opcodes::label(handler);
			out << " ; HANDLER =========================================================" << std::endl;

			//load dummy handler's backtrack id (java stack should be +1 after this)
			resolver->getBacktrackIndex(out, stack, "ndet @ "+getLocation(), false);

         //throw the dummy state away
			resolver->popState(out, stack, "ndet @ "+getLocation());

         //check against handler vector
			ndetIndex = 0;
         for (vector<string>::const_iterator retry = retries.begin(); retry != retries.end(); ++retry) {
            //if index == i, goto redoNdet_i
            out << opcodes::duplicate(stack); //duplicate the backtrack id
            out << opcodes::loadIntegerLiteral(ndetIndex, stack);
            out << opcodes::branchIfEqualIntegers(*retry, false, stack);
            ndetIndex++;
         }

         //else, fall through to fail
         //NB first make sure we throw out that bloody backtrack id!
         //we do this BEFORE fail so we don't try to pop the
         //handler in cases where it won't be there
         //specifically when we jump into fail after failing the last subprogram
         out << opcodes::pop(stack);

         out << opcodes::label(failPoint);
         out << " ; FAIL POINT =========================================================" << std::endl;

         stack.push(); //exception handler catches exception
         out << opcodes::pop(stack); //and we promptly throw it away ...

         //we have failed, throw away the initial state so we can backtrack further
         resolver->popState(out, stack, "ndet @ "+getLocation());

			out << opcodes::throwFailedPreconditionException("ndet failure", stack);

			//exit point
		   out << opcodes::label(endLabel);
		   out << " ; END OF NDET =========================================================" << std::endl;

		   resolver->addTrailingExceptionHandler(endLabel, endScope, handler);
         //out << opcodes::catchException(JologType::FailedPreconditionException().getClassName(), endLabel, endScope, handler);

         //make sure that there is always an instruction between
         //postCall and endScope (otherwise the JVM gets upset about
         //catching an exception over an empty range)
         out << opcodes::nop();

		   checkStack(stack.final(), initial, "Ndet");
		}

	private:
	   string endScope;
	   NamespaceResolver* resolver;
		std::vector< std::pair<JologNode*,string> > programs;
};

#endif
