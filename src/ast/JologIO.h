/*
 * JologIO.h
 *
 *  Created on: Aug 22, 2009
 *      Author: timothyc
 */

#ifndef JOLOG_IO
#define JOLOG_IO

#include <vector>
#include "JologNode.h"
#include "JologType.h"
#include "NamespaceResolver.h"

using std::vector;

class JologIO : public JologNode {
	public:
		JologIO(JologNode* stream, NamespaceResolver* resolver, string location) :
		   JologNode("I/O expression", location), stream(stream), type(UNSET), resolver(resolver) {}

		virtual ~JologIO() {
			delete stream;
			//NB. do NOT delete resolver ... it doesn't belong to us!
		}

		void addInput(JologNode* i) {
			if (type == UNSET) {
				type = INPUT;
			} else {
				ensure(type == INPUT, "invalid mixed IO on stream");
			}
			ensure(i != 0, "cannot read into null");
			data.push_back(i);
		}

		void addOutput(JologNode* i) {
			if (type == UNSET) {
				type = OUTPUT;
			} else {
				ensure(type == OUTPUT, "invalid mixed IO on stream");
			}
			ensure(i != 0, "cannot send null data");
			data.push_back(i);
		}

		virtual JologType getType() const {
			return JologType::Void();
		}

      virtual void emit(ostream& out, StackDepth& stack) const {
         JologType streamType(stream->getType());
         if (type == OUTPUT && streamType == JologType("java/io/PrintStream")) {
            emitPrintStream(out, stack);
         } else if (type == INPUT &&
                    (streamType == JologType::InputStream() ||
                     streamType == JologType::Scanner())) {
            emitInputStream(out, stack);
         } else {
            emitAgent(out, stack);
         }
      }

	private:
		enum StreamType {INPUT, OUTPUT, UNSET};

		JologNode* stream;
		vector<JologNode*> data;
		StreamType type;
		NamespaceResolver* resolver;

      void emitPrintStream(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         JologType streamType = stream->getType();
         stream->emit(out, stack);
         for (vector<JologNode*>::const_iterator it = data.begin(); it != data.end(); ++it) {
            if (it != (data.end() - 1)) {
               out << opcodes::duplicate(stack);
            }

            (*it)->emit(out, stack);

            JologType t((*it)->getType());
            if (!JologType::isPrimitiveType(t) && t != JologType::String()) {
               t = JologType::Object();
            }

            string signature("print("+t.getTypeName()+")");
            JologType returnType(resolver->resolveFunctionReturnType(getLocation(), streamType.getClassName()+"/"+signature));

            stack.pop("IO argument "+opcodes::intToString(it - data.begin())+" ("+getLocation()+")"); //print arg
            stack.pop("IO argument "+opcodes::intToString(it - data.begin())+" ("+getLocation()+")"); //stream
            out << opcodes::invokevirtual(streamType.getClassName(), signature+JologType(returnType).getTypeName());
            if (returnType != JologType::Void()) {
               out << opcodes::pop(stack); //throw away the junk if the print function returns something
            }
         }
         checkStack(stack.final(), initial, "I/O");
      }

      void emitInputStream(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         JologType streamType = stream->getType();

         if (streamType == JologType::InputStream()) {
            out << opcodes::newObject(JologType::Scanner(), stack);
            out << opcodes::duplicate(stack);
            stream->emit(out, stack);
            stack.pop(); //input stream
            stack.pop(); //scanner
            out << opcodes::invokespecial(JologType::Scanner().getClassName(), "<init>("+JologType::InputStream().getTypeName()+")V");
         } else {
            ensure(streamType == JologType::Scanner(), "input operation requires a Scanner or an InputStream");
            stream->emit(out, stack);
         }
         for (vector<JologNode*>::const_iterator it = data.begin(); it != data.end(); ++it) {
            if (it != (data.end() - 1)) {
               out << opcodes::duplicate(stack); //the stream
            }

            Identifier* in = dynamic_cast<Identifier*>(*it);
            ensure(in != 0, "input operation must read into a local variable"); //we have a variable ... this may work!

            int slot = in->getSlot();
            ensure(slot != NamespaceResolver::NO_SLOT, "input operation reading into a non-existent local");

            JologType input = in->getType();
            if (input == JologType::Int()) {
               stack.pop("I/O @ "+getLocation()); //scanner
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "nextInt()I");
            } else if (input == JologType::Boolean()) {
               stack.pop("I/O @ "+getLocation()); //scanner
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "nextBoolean()Z");
            } else if (input == JologType::Double()) {
               stack.pop("I/O @ "+getLocation()); //scanner
               stack.push();
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "nextDouble()D");
            } else if (input == JologType::Long()) {
               stack.pop("I/O @ "+getLocation()); //scanner
               stack.push();
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "nextLong()L");
            } else if (input == JologType::Float()) {
               stack.pop("I/O @ "+getLocation()); //scanner
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "nextFloat()F");
            } else if (input == JologType::Short()) {
               stack.pop("I/O @ "+getLocation()); //scanner
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "nextShort()S");
            } else if (input == JologType::Byte()) {
               stack.pop("I/O @ "+getLocation()); //scanner
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "nextByte()B");

//            characters are not supported!?
//            } else if (input == JologType::Character()) {
//               stack.pop("I/O @ "+getLocation()); //scanner
//               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "readChar()C");

            } else if (input == JologType::String()) {
               out << opcodes::loadStringLiteral("\\w+", stack); //w for word: scanner gives us a "string" of word characters with leading whitespace stripped!

               stack.pop(); //pattern
               stack.pop("I/O @ "+getLocation()); //data input stream
               out << opcodes::invokevirtual(JologType::Scanner().getClassName(), "next("+JologType::String().getTypeName()+")"+JologType::String().getTypeName());
            } else {
               ensure(false, "cannot read into type \""+input.prettyPrint()+"\"");
            }
            stack.push(); //read in result

            out << opcodes::store(slot, input, stack);
         }
         checkStack(stack.final(), initial, "I/O");
      }

      void emitAgent(ostream& out, StackDepth& stack) const {
//         ensure(false, "TODO: send/receive to/from agent");
         int initial = stack.final();
         //JologType streamType = stream->getType();

         if (type == OUTPUT) {
            stream->emit(out, stack);
            out << opcodes::typecast(JologType("jolog/HubEnvironment"), stack);
            for (vector<JologNode*>::const_iterator it = data.begin(); it != data.end(); ++it) {
               //if (it != (data.end() - 1)) {
               //   out << opcodes::duplicate(stack);
               //}

               (*it)->emit(out, stack);
               JologCast::castAtoB(out, (*it)->getType(), JologType::String(), stack, getLocation());

               if (it != data.begin()) {
                  string signature("concat("+JologType::String().getTypeName()+")"+JologType::String().getTypeName());

                  stack.pop("IO argument "+opcodes::intToString(it - data.begin())+" ("+getLocation()+")"); //print arg
                  stack.pop("IO argument "+opcodes::intToString(it - data.begin())+" ("+getLocation()+")"); //stream
                  stack.push();
                  out << opcodes::invokevirtual(JologType::String().getClassName(), signature);
               }
            }
            out << opcodes::invokevirtual(JologType("jolog/HubEnvironment").getClassName(), "send("+JologType::String().getTypeName()+")"+JologType::Void().getTypeName());
         } else if (type == INPUT) {
            ensure(data.size() == 1, "sensing IO can only handle one argument");
            ensure(dynamic_cast<Fluent*>(data[0]) != 0, "sensing IO must read into a fluent");

            MiniSense ms(stream);
            data[0]->assign(out, &ms, stack);
            initial++;

            //data.first->emit(out, stack);
            
            //stream->emit(out, stack);
            //out << opcodes::invokevirtual(JologType::Environment().getClassName(), "sense()"+JologType::String().getTypeName());
            //JologCast::castAtoB(out, JologType::String(), data.first->getType(), getLocation());

            

         }
         checkStack(stack.final(), initial, "I/O");
      }

      class MiniSense : public JologNode {
         public:
            MiniSense(JologNode* stream) : JologNode("mini sense"), stream(stream) {}
            JologType getType() const { return JologType::String(); }
            void emit(ostream& out, StackDepth& stack) const {
               stream->emit(out, stack);
               out << opcodes::typecast(JologType("jolog/HubEnvironment"), stack);
               out << opcodes::invokevirtual("jolog/HubEnvironment/sense()"+JologType::String().getTypeName());
               stack.pop(); //environment
               stack.push(); //sensed data
            }
   
         private:
            JologNode* stream;
      };
};

#endif
