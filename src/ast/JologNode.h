/*
 * JologNode.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef JOLOG_NODE
#define JOLOG_NODE

#include <iostream>
#include <sstream>
#include <string>

#include "StackDepth.h"
#include "../compiler/JologCompileException.h"

using std::ostream;
using std::string;

class JologType;

class JologNode {
	public:
	   JologNode(string nodeType, string location = "") :
         nodeType(nodeType), location(location) {}

		void ensure(bool assertion, string errorMessage) const {
			if (!assertion) {
				throw JologCompileException(location, errorMessage);
			}
		}

		void warn(bool assertion, string errorMessage) const { //TODO: scrap this all together?
			if (!assertion) {
				std::cerr << "WARNING: " << errorMessage << std::endl;
			}
		}

      void checkStack(int final, int expected, string location) const {
         std::stringstream ss;
         ss << "incorrect stack depth after " << location << " (expected " << expected << ", found " << final << ")";
         //ensure(final == expected, ss.str());
         if (final != expected) {
            std::cout << this->location << ": CHECK STACK: " << ss.str() << std::endl;
         }
      }

		virtual JologType getType() const = 0;

		virtual void emit(ostream& out, StackDepth& stack) const {}

      virtual void assign(ostream& out, const JologNode* initialiser, StackDepth& stack) const {
         string error = "cannot assign to ";
         string suffix = "expression";
         if (nodeType.size() > 0) {
            string firstLetter(1, tolower(nodeType.at(0))); //string containing first letter
            if (firstLetter.find_first_of("aeiou") != string::npos) { //starts with a vowel
               suffix = "an "+nodeType;
            } else {
               suffix = "a "+nodeType;
            }
         }
         ensure(false, error+suffix);
      }

      virtual string getLocation() const {
         return location;
      }

		virtual ~JologNode() {}

		static string newLabel() {
			static size_t labelId = 0;

			std::stringstream ss;
			ss << "label" << labelId;
			++labelId;
			return ss.str();
		}

	private:
      string nodeType;
      string location;
};

#endif

