/*
 * For.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef FOR
#define FOR

#include "JologNode.h"
#include "opcodes.h"

class For : public JologNode {
	public:
		For(string location) :
         JologNode("for loop"), initialiser(0), condition(0), update(0), body(0) {}

		void addInitialiser(JologNode* i) {
			initialiser = i;
		}

		void addCondition(JologNode* c) {
			condition = c;
		}

		void addUpdate(JologNode* u) {
			update = u;
		}

		void addBody(JologNode* b) {
			body = b;
		}

		virtual JologType getType() const {
			return JologType::Void();
		}

		virtual ~For() {
			if (initialiser != 0) { delete initialiser; }
			if (condition != 0) { delete condition; }
			if (update != 0) { delete update; }
			if (body != 0) { delete body; }
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
	      if (initialiser != 0) { initialiser->emit(out, stack); }

			string startLoop = newLabel();
			string endLoop = newLabel();
			out << opcodes::label(startLoop);

			//condition
			if (condition != 0) {
				condition->emit(out, stack);
				out << opcodes::branchIfTrue(endLoop, true, stack);
			} else {
				warn(false, "for loop with no condition will run forever");
			}

			//body
			if (body != 0) { body->emit(out, stack); }

			//update and jump back to start of loop
			if (update != 0) { update->emit(out, stack); }
			out << opcodes::gotoLabel(startLoop);

			//done!
			out << opcodes::label(endLoop);
			checkStack(stack.final(), initial, "for loop");
		}

	private:
		JologNode* initialiser;
		JologNode* condition;
		JologNode* update;
		JologNode* body;
};

#endif
