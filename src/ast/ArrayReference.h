/*
 * ArrayReference.h
 *
 *  Created on: Aug 27, 2009
 *      Author: timothyc
 */

#ifndef ARRAY_REFERENCE
#define ARRAY_REFERENCE

#include <ostream>
#include "JologNode.h"

using std::ostream;

class ArrayReference : public JologNode {
   public:
      ArrayReference(string location, JologNode* array, JologNode* index) :
         JologNode("array reference", location), array(array), index(index) {}

      ArrayReference(string location, int variableSlot, int index) :
         JologNode("array reference", location), slot(variableSlot), array(0), index(new IntegerLiteral(index)) {}

      virtual JologType getType() const {
         JologType t = (array != 0) ? array->getType() : JologType("java/lang/Object", 1);
         ensure(t.arrayDimensions() > 0, "invalid array type \""+t.prettyPrint()+"\"");

         //but this is an array reference: so we want the type of the internal stuff
         return JologType(t.baseType().getClassName(), t.arrayDimensions()-1);
      }

      virtual ~ArrayReference() {
         if (array != 0) { delete array; } //we may not have an array!
         delete index;
         //NB. do NOT delete resolver ... it doesn't belong to us!
      }

      virtual void assign(ostream& out, const JologNode* initialiser, StackDepth& stack) const {
         int initial = stack.final();
         if (array != 0) {
            array->emit(out, stack);
         } else {
            out << opcodes::loadReference(slot, stack);
         }
         index->emit(out, stack);
         initialiser->emit(out, stack);
         out << opcodes::storeArray(getType(), stack);
         checkStack(stack.final(), initial, "assignment to array reference");
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         ensure(index->getType() == JologType::Int(), "array index must be an integer");
         if (array != 0) {
            array->emit(out, stack);
         } else {
            out << opcodes::loadReference(slot, stack);
         }
         index->emit(out, stack);
         out << opcodes::loadArray(getType(), stack);
         checkStack(stack.final(), initial + 1, "array reference");
      }

   private:
      int slot;
      JologNode* array;
      JologNode* index;

};

#endif
