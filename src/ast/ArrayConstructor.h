/*
 * ArrayConstructor.h
 *
 *  Created on: Sep 10, 2009
 *      Author: timothyc
 */

#ifndef ARRAY_CONSTRUCTOR
#define ARRAY_CONSTRUCTOR

#include "JologNode.h"
#include "Identifier.h"
#include "NamespaceResolver.h"
#include "../compiler/JologCompileException.h"

class ArrayConstructor : public JologNode {
	public:
		ArrayConstructor(JologType classType, JologNode* size, NamespaceResolver* resolver, string location) :
		   JologNode("array constructor", location), size(size),
		   type(resolver->lookupType(classType.getClassName(), getLocation())) {}

		virtual JologType getType() const {
			return JologType(type.baseType().getClassName(), type.arrayDimensions()+1);
		}

		virtual ~ArrayConstructor() {
			delete size;
			//NB. do NOT delete resolver ... it doesn't belong to us!
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
		   size->emit(out, stack);
		   out << opcodes::newArray(type, stack);
		   checkStack(stack.final(), initial + 1, "array constructor");
		}

	private:
      JologNode* size;
      JologType type;

};

#endif
