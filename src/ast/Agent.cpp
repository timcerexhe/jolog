/*
 * Agent.cpp
 *
 *  Created on: Aug 25, 2009
 *      Author: timothyc
 */

#include "Agent.h"

void Agent::emit(ostream& out, StackDepth& stack) const {
   out << ".source " << source << opcodes::endl();
   out << ".class public ";

   if (abstract) { out << "abstract "; }
   if (final) { out << "final "; }

   out << name->getClassName() << opcodes::endl();
   out << ".super " << super->getClassName() << opcodes::endl();
   out << ".implements " << resolver->lookupType("JologAgent", getLocation()).getClassName() << opcodes::endl(); //required interface
   for (vector<JologType*>::const_iterator it = implements.begin(); it != implements.end(); ++it) {
      out << ".implements " << (*it)->getClassName() << opcodes::endl();
   }
   out << ".field private final " << JologType::environment_fieldName() << " " << JologType::Environment().getTypeName() << opcodes::endl();
   out << ".field private final " << JologType::fluent_store_fieldName() << " " << JologType::FluentStore().getTypeName() << opcodes::endl();
   out << ".field private final " << JologType::backtrack_stack_fieldName() << " " << JologType::BacktrackStack().getTypeName() << opcodes::endl();

   for (vector<Fluent*>::const_iterator it = fluents.begin(); it != fluents.end(); ++it) {
      resolver->addMember((*it)->getName(), (*it)->getType(), false, true);
   }
   for (vector<Action*>::const_iterator it = actions.begin(); it != actions.end(); ++it) {
      (*it)->emit(out, stack);
   }
   for (vector<JologNode*>::const_iterator it = functions.begin(); it != functions.end(); ++it) {
      (*it)->emit(out, stack);
   }

   { //COMPILE CONSTRUCTOR
      StackDepth stack;
      std::stringstream ss;
      compileConstructor(ss, stack);

      out << ".method public <init>(" << JologType::Environment().getTypeName() << ")V" << opcodes::endl();
      out << ".limit stack " << stack.max() << opcodes::endl();
      out << ".limit locals 2" << opcodes::endl();

      out << ss.str();

      out << ".end method" << opcodes::endl();
   }

   { //COMPILE EXOGENOUS ACTION RECEIVER
      StackDepth stack;
      std::stringstream ss;
      compileActionReceive(ss, stack);

      out << ".method public receive("
      << JologType::Message().getTypeName()
      << ")"
      << JologType::Message().getTypeName()
      << opcodes::endl();

      out << ".limit stack " << stack.max() << opcodes::endl();
      out << ".limit locals 3" << opcodes::endl();

      out << ss.str();

      out << ".end method" << opcodes::endl();
   }
   //compileRun(out);
}

void Agent::compileConstructor(ostream& out, StackDepth& stack) const {
   //initialise the super class
   out << opcodes::loadReference(0, stack);
   out << opcodes::invokespecial(super->getClassName(), "<init>()"+JologType::Void().getTypeName());

   //initialise the environment
   out << opcodes::loadReference(0, stack); //this
   out << opcodes::loadReference(1, stack); //environment (argument)
   out << opcodes::putField(name->getType().getClassName()+"/"+JologType::environment_fieldName(), JologType::Environment(), stack);

   //initialise the fluent store
   out << opcodes::loadReference(0, stack);
   out << opcodes::newObject(JologType::FluentStore(), stack);
   out << opcodes::duplicate(stack);
   stack.pop(); //fluent store object
   out << opcodes::invokespecial(JologType::FluentStore().getClassName(), "<init>()"+JologType::Void().getTypeName());
   out << opcodes::putField(name->getType().getClassName()+"/"+JologType::fluent_store_fieldName(), JologType(JologType::FluentStore()), stack);

   //initialise the backtrack stack
   resolver->initialiseBacktrackStack(out, stack);

   for (vector<Fluent*>::const_iterator it = fluents.begin(); it != fluents.end(); ++it) {
      bool defaultInitialiser = false;
      const JologNode* initialiser = (*it)->getInitialiser(); //comes with its own initialiser!
      if (initialiser == 0) { //no initialiser provided, supply a default
         defaultInitialiser = true;
         JologType fluentType = (*it)->getType();
         std::cout << "constructor providing default argument for " + (*it)->getName() << std::endl;
         if (fluentType == JologType::Int()) {
            initialiser = new IntegerLiteral(0);
         } else if (fluentType == JologType::Byte()) {
            initialiser = new ByteLiteral(0);
         } else if (fluentType == JologType::Short()) {
            initialiser = new ShortLiteral(0);
         } else if (fluentType == JologType::Character()) {
            initialiser = new CharLiteral(0);
         } else if (fluentType == JologType::Double()) {
            initialiser = new DoubleLiteral(0);
         } else if (fluentType == JologType::Long()) {
            initialiser = new LongLiteral(0);
         } else if (fluentType == JologType::Boolean()) {
            initialiser = new BooleanLiteral(false);
         } else if (fluentType == JologType::Float()) {
            initialiser = new FloatLiteral(0);
         } else if (fluentType == JologType::String()) {
            initialiser = new StringLiteral("");
         } else {
            warn(false, "providing null as default argument for fluent \"" + (*it)->getName() + "\"");
            initialiser = new Identifier(0, "null", resolver, getLocation());
         }
      }
      std::cout << "CONSTRUCTOR COMPILING A FLUENT ..." << std::endl;
      (*it)->assign(out, initialiser, stack);
      std::cout << "DONE!" << std::endl;

      if (defaultInitialiser) { //we created it, now we throw it away
         delete initialiser;
      }
   }

   out << opcodes::returnVoid();
}

void Agent::compileActionReceive(ostream& out, StackDepth& stack) const {
   //variables:
   // 0 = this
   // 1 = message (first argument)
   // 2 = Matcher object (overwritten for each action)

   string endPoint = newLabel();

   out << opcodes::throws(resolver->lookupType("FailedPreconditionException", getLocation()).getClassName());
   out << opcodes::throws(resolver->lookupType("InterruptedException", getLocation()).getClassName());
   out << opcodes::loadReference(1, stack);
   out << opcodes::branchIfNull(endPoint, false, stack); //abort if message is null

   for (vector<Action*>::const_iterator it = actions.begin(); it != actions.end(); ++it) {
      if ((*it)->getActionType() == Action::EXOGENOUS_ACTION) {
         string startAction = newLabel();
         string endAction = newLabel();
         out << opcodes::label(startAction);

         out << opcodes::loadReference(0, stack); //call this.action(message)
         out << opcodes::loadReference(1, stack);
         out << opcodes::invokevirtual((*it)->getFQCN());
         out << opcodes::returnType(JologType::Message(), stack);

         out << opcodes::label(endAction);

         //catch an exception (if the action didn't match or preconditions weren't true)
         out << opcodes::catchException(JologType::allExceptions(), startAction, endAction, endAction);
      }
   }

   //we got to the end, so NOTHING WORKED! Throw a new exception to let the caller know
   //TODO: nicer message? store message in exception?
   out << opcodes::throwFailedPreconditionException("unhandled action", stack);

   out << opcodes::label(endPoint); //end of actions!
   out << opcodes::loadNull(stack);
   out << opcodes::returnType(JologType::Message(), stack);
}

//void Agent::compileRun(ostream& out) const {
//   StackDepth stack;
//   out << ".method public run()V" << opcodes::endl();
//   out << opcodes::returnVoid();
//   out << ".end method" << opcodes::endl();
//}

Agent::~Agent() {
   for (vector<JologType*>::const_iterator it = implements.begin(); it != implements.end(); ++it) {
      delete *it;
   }
   for (vector<Fluent*>::const_iterator it = fluents.begin(); it != fluents.end(); ++it) {
      delete *it;
   }
   for (vector<Action*>::const_iterator it = actions.begin(); it != actions.end(); ++it) {
      delete *it;
   }
   for (vector<JologNode*>::const_iterator it = functions.begin(); it != functions.end(); ++it) {
      delete *it;
   }
   delete resolver; //woo! resolver DOES belong to us!
}


