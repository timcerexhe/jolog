/*
 * RestoreState.h
 *
 *  Created on: Oct 19, 2009
 *      Author: timothyc
 */

#ifndef RESTORE_STATE
#define RESTORE_STATE

#include "JologNode.h"
#include "opcodes.h"
#include "JologType.h"

class RestoreState : public JologNode {
   public:
      RestoreState(string location, NamespaceResolver* resolver) :
          JologNode("restore state", location), resolver(resolver) {}

      virtual ~RestoreState() {}

      virtual JologType getType() const {
         return JologType::Void();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         vector<NamespaceResolver::variableType> variables;
         resolver->getStateVariables(variables, true);
         std::cout << "RESTORE TRANSIENT STATE HAS VARIABLES:" << std::endl;
         for (vector<NamespaceResolver::variableType>::const_iterator i = variables.begin(); i != variables.end(); ++i) {
            std::cout << " - " << (*i).name << " (" << (*i).slot << ")" << std::endl;
         }

         out << " ; RESTORING TRANSIENT STATE" << std::endl;
         resolver->restoreState(out, stack, variables, getLocation());
         resolver->popState(out, stack, getLocation()); //throw away the transient state data
         out << " ; DONE RESTORING TRANSIENT STATE" << std::endl;
      }

   private:
      NamespaceResolver* resolver;
};

#endif
