/*
 * Label.h
 *
 *  Created on: Oct 8, 2009
 *      Author: timothyc
 */

#ifndef LABEL
#define LABEL

#include <string>
#include "JologNode.h"

using std::string;

class Label : public JologNode {
	public:
		Label(string label) : JologNode("label"), label(label) {}

		virtual ~Label() {}

		virtual JologType getType() const {
			return JologType::Void();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::label(label);
		}

	private:
		string label;
};

#endif
