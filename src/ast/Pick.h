/*
 * Pick.h
 *
 *  Created on: Oct 10, 2009
 *      Author: timothyc
 *
 */

#ifndef PICK
#define PICK

#include "JologNode.h"
#include "NamespaceResolver.h"

class Pick : public JologNode {
   public:
      /**
       * @param endScope: indicates what region of code the pick should catch
       * exceptions from. This should ordinarily be the end of current scope.
       */
      //Pick(string location, JologType pickType, string endScope,
      //     string pickName, JologNode* program, NamespaceResolver* resolver) :

      Pick(string location, Variable* variable, string endScope,
           JologNode* program, NamespaceResolver* resolver) :

               JologNode("pick", location), type(variable->getType()), program(program),
               endScope(endScope), resolver(resolver), variable(variable), endPick(newLabel()) {

         /*type = pickType;
         if (pickType == JologType::Int()) {
            type = JologType("java/lang/Integer");
         } else if (pickType == JologType::Float()) {
            type == JologType("java/lang/Float");
         } else if (pickType == JologType::Double()) {
            type == JologType("java/lang/Double");
         } else if (pickType == JologType::Character()) {
            type == JologType("java/lang/Character");
         } else if (pickType == JologType::Long()) {
            type == JologType("java/lang/Long");
         } else if (pickType == JologType::Boolean()) {
            type == JologType("java/lang/Boolean");
         } else if (pickType == JologType::Short()) {
            type == JologType("java/lang/Short");
         } else if (pickType == JologType::Byte()) {
            type == JologType("java/lang/Byte");
         }*/
         ensure(!JologType::isPrimitiveType(type), "pick requires a reference type (got \""+type.getClassName()+"\")");
         //variable = new Variable(new JologType(type), pickName, 0, endPick, resolver, false);
         //std::cout << " === CREATE PICK VARIABLE \"" << variable->getName() << "\" @ SLOT " << variable->getSlot() << " ===" << std::endl;

//         variableSlot(resolver->getFreeSlot(getLocation(), JologType::PickVariable()));
         ensure(program != 0, "pick requires a program");
      }

      virtual ~Pick() {
         delete program;
         delete variable;
      }

      static bool isSlash(char c) {
         return c == '/';
      }

      virtual JologType getType() const {
         return JologType::Void();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         out << " ; BEGIN PICK EMIT ..." << std::endl;
         int initial = stack.final();

         string failPick = newLabel();
         string startPick = newLabel();
         string repick = newLabel();
         string doPick = newLabel();
         string doProgram = newLabel();

         //create a new variable
         //std::cout << "PICK VARIABLE @ " << variable << std::endl;
         //std::cout << "type = " << variable->getType().prettyPrint() << std::endl;
         //if (dynamic_cast<Variable*>(variable) != 0) { //we have a variable, so make sure it gets declared
         variable->emit(out, stack);
         
         //std::cout << "PICK VARIABLE DONE" << std::endl;

         //save state
         std::vector<NamespaceResolver::variableType> variables;
         resolver->getStateVariables(variables);
         resolver->saveState(out, stack, variables, true, getLocation());
         out << opcodes::gotoLabel(doPick);

         out << " ; === PICK FAIL (stack = " << stack.final() << ") ===" << std::endl;

         //pick fails
         out << opcodes::label(failPick);
         stack.push(); //exception handler catches exception
         resolver->popState(out, stack, "pick @ "+getLocation()); //throw away the pick choice point
         out << opcodes::throwReference(stack); //rethrow the exception (pick fails)

         out << " ; === REPICK === stack = " << stack.final() << std::endl;
         //repick
         out << opcodes::label(repick);

         stack.push(); //this is our exception handler
         out << opcodes::pop(stack); //but we don't want the exception ...

         //restore state and increase backtrack id
         resolver->restoreState(out, stack, variables, getLocation());

         //get next binding (DO PICK)
         out << opcodes::label(doPick);

         out << " ; === DO PICK === stack = " << stack.final() << std::endl;

         //get the fluent store
         out << opcodes::loadReference(0, stack);
         string fluentStore(resolver->lookupType("this", getLocation()).getClassName() + "/" + JologType::fluent_store_fieldName());
         out << opcodes::getField(fluentStore, JologType::FluentStore(), stack);

         out << " ; === GET CLASS === stack = " << stack.final() << std::endl;

         //get the pick type
         string pickClassType(type.getClassName());
         replace_if(pickClassType.begin(), pickClassType.end(), isSlash, '.');

         out << opcodes::loadStringLiteral(pickClassType, stack);

         //and turn it into a Java "Class" object
         stack.pop(); //class name
         stack.push(); //java Class object
         out << opcodes::invokestatic(JologType::Class().getClassName(), "forName("+JologType::String().getTypeName()+")"+JologType::Class().getTypeName());
         resolver->getBacktrackIndex(out, stack, "pick @ "+getLocation(), true);

         out << " ; === GET BINDING === stack = " << stack.final() << std::endl;

         stack.pop(); //backtrack index
         stack.pop(); //class object
         stack.pop(); //fluent store
         stack.push(); //variable object
         out << opcodes::invokevirtual(JologType::FluentStore().getClassName(),
                                       "getNext(" + JologType::Class().getTypeName() +
                                                    JologType::Int().getTypeName() +
                                       ")" + JologType::Object().getTypeName());

         out << "; === PICK CASTING Object to pick type \"" << type.prettyPrint() << "\"" << std::endl;
         JologCast::castAtoB(out, JologType::Object(), type, stack, "pick @ "+getLocation());

         //int slot;
         //if (dynamic_cast<Variable*>(variable) != 0) {
         int slot = variable->getSlot();
         //} else {
         //   slot = ((Identifier*) variable)->getSlot();
         //}
         out << opcodes::store(slot, type, stack);
         out << opcodes::catchException(JologType::allExceptions(), doPick, doProgram, failPick);

         //run the user program
         out << " ; === USER PROGRAM === stack = " << stack.final() << std::endl;
         out << opcodes::label(doProgram);
         program->emit(out, stack);

         std::cout << " ; === END PICK/USER PROGRAM === stack = " << stack.final() << std::endl;

         out << opcodes::label(endPick);

         //catch subsequent exceptions and try the program again to see if that fixes things
         resolver->addTrailingExceptionHandler(doProgram, endScope, repick);

         //make sure that there is always an instruction between
         //endPick and endScope (otherwise the JVM gets upset about
         //catching an exception over an empty range)
         out << opcodes::nop();

         checkStack(stack.final(), initial, "pick");
         out << " ; === FINISHED COMPILING PICK === stack = " << stack.final() << std::endl;
      }

   private:
      JologType type;
      //string identifier;
      JologNode* program;
      string endScope;
      NamespaceResolver* resolver;
      Variable* variable;
      string endPick;
};

#endif
