/*
 * Agent.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef AGENT
#define AGENT

#include <vector>

#include "JologNode.h"
#include "JologType.h"
#include "opcodes.h"
#include "Fluent.h"
#include "Action.h"
#include "Function.h"
#include "Scope.h"
#include "Assignment.h"
#include "Constructor.h"
#include "FunctionCall.h"
#include "Identifier.h"
#include "SomeAll.h"
#include "IntegerLiteral.h"
#include "DoubleLiteral.h"
#include "BooleanLiteral.h"
#include "StringLiteral.h"
#include "LongLiteral.h"
#include "FloatLiteral.h"
#include "ShortLiteral.h"
#include "ByteLiteral.h"
#include "NamespaceResolver.h"

using std::vector;

class Agent : public JologNode {
   public:
      Agent(string sourceFile, JologType* name, JologType* super,
            bool abstract, bool final, NamespaceResolver* resolver, string location) :

               JologNode("agent definition", location), name(name), super(super),
               abstract(abstract), final(final), resolver(resolver) {

         //get source file (without package or directory prefixes)
         size_t fileStart = sourceFile.rfind('/');
         if (fileStart == string::npos) { //no slashes? use whole string
            fileStart = 0;
         } else {
            ++fileStart; //move past the slash character
         }
         source = sourceFile.substr(fileStart, string::npos);

         ensure(name->arrayDimensions() == 0, "class type cannot be an array");
         resolver->addType("this", name->getClassName()); // == 0, "error inserting \"this\" pointer at variable slot 0");
         ensure(super->arrayDimensions() == 0, "class super type cannot be an array");
         resolver->addType("super", super->getClassName());

         string environmentField = name->getClassName() + "/" + JologType::environment_fieldName();
         resolver->addMember(environmentField, JologType(JologType::Environment()), false, true);

         string fluentStoreField = name->getClassName() + "/" + JologType::fluent_store_fieldName();
         resolver->addMember(fluentStoreField, JologType(JologType::FluentStore()), false, true);
      }

      string getName() const {
         return name->getClassName();
      }

      void addInterface(JologType* i) {
         implements.push_back(i);
      }

      void addFluent(Fluent* f) {
         fluents.push_back(f);
      }

      void addSomeAll(SomeAll* sa) {
         functions.push_back(sa);
      }

      void addAction(Action* a) {
         std::cout << "ADDING ACTION" << std::endl;
         actions.push_back(a);
      }

      void addFunction(Function* f) {
         functions.push_back(f);
      }

      virtual ~Agent();

      virtual void emit(ostream& out, StackDepth& ignore) const;

      virtual JologType getType() const {
         return JologType(name->getClassName());
      }

   private:
      string source;
      const JologType* name;
      const JologType* super;
      const bool abstract, final;
      vector<JologType*> implements;
      NamespaceResolver* resolver;

      vector<Fluent*> fluents;
      vector<JologNode*> functions;
      vector<Action*> actions;
      //vector<Function*> functions;

      void compileConstructor(ostream& out, StackDepth& stack) const;
      void compileActionReceive(ostream& out, StackDepth& stack) const;
      void compileRun(ostream& out) const;
};

#endif
