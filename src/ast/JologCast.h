/*
 * JologCast.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#include "JologNode.h"
#include "opcodes.h"

#ifndef JOLOG_CAST
#define JOLOG_CAST

class JologCast : public JologNode {
	public:
	   JologCast(JologType conversion, JologNode* expr, string location) :
	      JologNode("typecast", location), conversion(conversion), expr(expr) {}

		JologCast(JologType* conversion, JologNode* expr, string location) :
		   JologNode("typecast", location), conversion(*conversion), expr(expr) {

		   std::cout << "ALERT: using dodgy cast. Here be pirates ..." << std::endl;

		   //we've got a copy, now throw it away!
		   //note that we can do this: we were given the object, we just don't need it anymore
		   //this version should only really be called by ComponentCompiler (because it is already too busy)
		   delete conversion;
		}

		virtual ~JologCast() {
			delete expr;
		}

		virtual JologType getType() const {
			return conversion;
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();

			//TODO: add conversions and maybe some type checking :)
		   ensure(expr != 0, "cannot cast null object");
		   std::cout << "STACK @ " << stack.final() << std::endl;
         expr->emit(out, stack);
         std::cout << "STACK @ " << stack.final() << std::endl;
         std::cout << "CAST FROM " << expr->getType().prettyPrint() << " TO " << conversion.prettyPrint() << std::endl;
         castAtoB(out, expr->getType(), conversion, stack, getLocation());
         std::cout << "STACK @ " << stack.final() << std::endl;
         checkStack(stack.final(), initial+1, "typecast");

         /*if (expr->getType() == JologType::Object()) {
            castObjectToType(out, getType(), stack, getLocation());
         } else if (expr->getType() == JologType::Int() && conversion->getType() == JologType::Double()) {
            out << opcodes::convertIntToDouble(stack);
         } else if (expr->getType() == JologType::Double() && conversion->getType() == JologType::Int()) {
            out << opcodes::convertDoubleToInt(stack);

         } else if (expr->getType() == JologType("java/lang/Integer", 0) && conversion->getType() == JologType::Int()) {
            stack.pop("cast ("+getLocation()+")");
            stack.push();
            out << opcodes::invokevirtual("java/lang/Integer", "intValue()"+JologType::Int().getTypeName());
         } else if (expr->getType() == JologType("java/lang/Boolean", 0) && conversion->getType() == JologType::Boolean()) {
            stack.pop("cast ("+getLocation()+")");
            stack.push();
            out << opcodes::invokevirtual("java/lang/Boolean", "booleanValue()"+JologType::Boolean().getTypeName());
         } else if (expr->getType() == JologType("java/lang/Double", 0) && conversion->getType() == JologType::Double()) {
            stack.pop("cast ("+getLocation()+")");
            stack.push();
            stack.push();
            out << opcodes::invokevirtual("java/lang/Double", "doubleValue()"+JologType::Double().getTypeName());


         } else if (expr->getType() == JologType("java/lang/Object", 0) && conversion->getType() == JologType::Int()) {
            out << opcodes::typecast(JologType("java/lang/Integer", 0), stack);
            stack.pop("cast ("+getLocation()+")");
            stack.push();
            out << opcodes::invokevirtual("java/lang/Integer", "intValue()"+JologType::Int().getTypeName());
         } else if (expr->getType() == JologType("java/lang/Object", 0) && conversion->getType() == JologType::Double()) {
            out << opcodes::typecast(JologType("java/lang/Double", 0), stack);
            stack.pop("cast ("+getLocation()+")");
            stack.push();
            stack.push();
            out << opcodes::invokevirtual("java/lang/Double", "doubleValue()"+JologType::Int().getTypeName());

         } else if (expr->getType() == JologType("java/lang/Object", 0) && conversion->getType() == JologType::Boolean()) {
            out << opcodes::typecast(JologType("java/lang/Boolean", 0), stack);
            stack.pop("cast ("+getLocation()+")");
            stack.push();
            out << opcodes::invokevirtual("java/lang/Boolean", "booleanValue()"+JologType::Int().getTypeName());

         } else if (conversion->getType() == JologType::String()) {

            JologType original(expr->getType());
            if (original == JologType::Int() || original == JologType::Double() ||
                original == JologType::Boolean() || original == JologType::Character()) {

               stack.pop("cast ("+getLocation()+")"); //arg
               stack.push(); //return
               out << opcodes::invokestatic(JologType::String().getClassName(), "valueOf("+original.getTypeName()+")"+JologType::String().getTypeName()); //convenient static string method
            } else {
               stack.pop("cast ("+getLocation()+")"); //arg
               stack.push(); //return
               out << opcodes::invokevirtual(original.getClassName(), "toString()"+JologType::String().getTypeName());
            }

         } else {
            std::cout << "ALERT! DOING HARDCORE TYPECAST FROM " << expr->getType().getClassName() << " to " << conversion->getClassName() << std::endl;
            out << opcodes::typecast(conversion->getType(), stack);
         }
         checkStack(stack.final(), initial+1, "typecast");*/
		}

      virtual void assign(ostream& out, const JologNode* initialiser) const {
         ensure(false, "cannot assign to a typecast expression");
      }

      static JologType* primitiveToReference(JologType* type) {
         if (*type == JologType::Int()) {
            delete type;
            return new JologType("java/lang/Integer");
         } else if (*type == JologType::Float()) {
            delete type;
            return new JologType("java/lang/Float");
         } else if (*type == JologType::Double()) {
            delete type;
            return new JologType("java/lang/Double");
         } else if (*type == JologType::Character()) {
            delete type;
            return new JologType("java/lang/Character");
         } else if (*type == JologType::Long()) {
            delete type;
            return new JologType("java/lang/Long");
         } else if (*type == JologType::Boolean()) {
            delete type;
            return new JologType("java/lang/Boolean");
         } else if (*type == JologType::Short()) {
            delete type;
            return new JologType("java/lang/Short");
         } else if (*type == JologType::Byte()) {
            delete type;
            return new JologType("java/lang/Byte");
         } else {
            return type;
         }
      }

      static void castAtoB(std::ostream& out, JologType a, JologType b, StackDepth& stack, string location) {
         //special case for converting to a string: everything works!
         if (a == JologType::Void() || b == JologType::Void()) {
            throw JologCompileException(location, "cannot typecast to or from void ("+a.prettyPrint()+" -> "+b.prettyPrint()+")");
         }

         if (b == JologType::String()) {
            if (JologType::isPrimitiveType(a)) {
               stack.pop("cast ("+location+")");
               stack.push();
               out << opcodes::invokestatic(JologType::String().getClassName(), "valueOf("+a.getTypeName()+")"+JologType::String().getTypeName()); //convenient static string method
            } else if (a != JologType::String()) {
               stack.pop("cast ("+location+")");
               stack.push();
               out << opcodes::invokevirtual(a.getClassName(), "toString()"+JologType::String().getTypeName());
            }

         //primitive types need two steps
         } else if (JologType::isPrimitiveType(a) && JologType::isPrimitiveType(b)) {
            if (a == JologType::Boolean() || b == JologType::Boolean()) {
               if (a != b) {
                  throw JologCompileException(location, "cannot cast to or from boolean type");
               } //else we do nothing ... they are both already boolean!

            } else if ((a == JologType::Int() || a == JologType::Short() || a == JologType::Byte() || a == JologType::Character() || a == JologType::Short()) &&
                       (a == JologType::Int() || a == JologType::Short() || a == JologType::Byte() || a == JologType::Character() || a == JologType::Short())) {
               //do nothing: both values can be treated as INTS (responsibility of caller)

            } else if (a == JologType::Short() || a == JologType::Byte() || a == JologType::Character() || a == JologType::Short()) {
               //when converting from one of these to something NOT in this set (which would be handled above)
               //then we *actually* want to convert from an int
               castPrimitives(out, JologType::Int(), b, stack, location);

            } else { //no idea, delegate to someone who knows the available opcodes!
               castPrimitives(out, a, b, stack, location);
            }

         //primitives cannot be cast to any reference type (use corresponding reference type)
         } else if (JologType::isPrimitiveType(a) && !JologType::isPrimitiveType(b)) {
            throw JologCompileException(location, "primitive type \""+a.prettyPrint()+"\" cannot be cast to reference type \""+b.prettyPrint()+"\" (use corresponding Java reference type)");

         //primitive array can be cast to itself, to an object or an object array
         } else if (a.arrayDimensions() > 0 && JologType::isPrimitiveType(a.baseType())) {
            if (b.baseType() == JologType::Object()) {
               out << opcodes::typecast(b, stack);
            } else if (a != b) {
               throw JologCompileException(location, "array of primitive type \""+a.prettyPrint()+"\" can be cast to itself or to an Object or Object array. Casting to \""+b.prettyPrint()+"\" is not permitted.");
            } //else we cast to our own type?! do nothing!

         } else if (!JologType::isPrimitiveType(a) && JologType::isPrimitiveType(b)) {
            if (a.arrayDimensions() > 0) {
               throw JologCompileException(location, "cannot convert array of references to primitive type");
            } else {
               std::cout << location << ": CAST attempting conversion from \""+a.prettyPrint()+"\" to \""+b.prettyPrint()+"\"" << std::endl;
               emitReferenceAsPrimitive(out, a, b, stack, location);
            }

         } else if (!JologType::isPrimitiveType(a) && !JologType::isPrimitiveType(b)) {
            if (a != b) { //don't bother typecasting to ourself
               std::cout << "DOING HARDCORE TYPECAST FROM \""+a.prettyPrint()+"\" to \""+b.prettyPrint()+"\"" << std::endl;
               out << opcodes::typecast(b, stack);
            }
         } else {
            throw JologCompileException(location, "INTERNAL COMPILER ERROR: unhandled case when casting \""+a.prettyPrint()+"\" to \""+b.prettyPrint()+"\"");
         }
      }

      static JologNode* convertToPrimitive(JologNode* node, string location) {
         JologType type = node->getType();
         if (type == JologType("java/lang/Integer")) {
            return new JologCast(JologType::Int(), node, location);
         } else if (type == JologType("java/lang/Double")) {
            return new JologCast(JologType::Double(), node, location);
         } else if (type == JologType("java/lang/Long")) {
            return new JologCast(JologType::Long(), node, location);
         } else if (type == JologType("java/lang/Float")) {
            return new JologCast(JologType::Float(), node, location);
         } else if (type == JologType("java/lang/Byte")) {
            return new JologCast(JologType::Byte(), node, location);
         } else if (type == JologType("java/lang/Short")) {
            return new JologCast(JologType::Short(), node, location);
         } else if (type == JologType("java/lang/Boolean")) {
            return new JologCast(JologType::Boolean(), node, location);
         } else if (type == JologType("java/lang/Character")) {
            return new JologCast(JologType::Character(), node, location);
         } else {
            return node;
         }
      }

      static void castPrimitives(std::ostream& out, JologType a, JologType b, StackDepth& stack, string location) {
         if (a != b) { //don't bother if they are already the same!
            if (a == JologType::Int()) {
               if (b == JologType::Byte()) {
                  out << opcodes::convertIntToByte(stack);
               } else if (b == JologType::Character()) {
                  out << opcodes::convertIntToChar(stack);
               } else if (b == JologType::Double()) {
                  out << opcodes::convertIntToDouble(stack);
               } else if (b == JologType::Float()) {
                  out << opcodes::convertIntToFloat(stack);
               } else if (b == JologType::Long()) {
                  out << opcodes::convertIntToLong(stack);
               } else if (b == JologType::Short()) {
                  out << opcodes::convertIntToShort(stack);
               } else {
                  throw JologCompileException(location, "no primitive conversion between \""+a.prettyPrint()+"\" and \""+b.prettyPrint()+"\"");
               }
            } else if (a == JologType::Float()) {
               if (b == JologType::Int()) {
                  out << opcodes::convertFloatToInt(stack);
               } else if (b == JologType::Double()) {
                  out << opcodes::convertFloatToDouble(stack);
               } else if (b == JologType::Long()) {
                  out << opcodes::convertFloatToLong(stack);
               } else {
                  throw JologCompileException(location, "no primitive conversion between \""+a.prettyPrint()+"\" and \""+b.prettyPrint()+"\"");
               }
            } else if (a == JologType::Double()) {
               if (b == JologType::Int()) {
                  out << opcodes::convertDoubleToInt(stack);
               } else if (b == JologType::Float()) {
                  out << opcodes::convertDoubleToFloat(stack);
               } else if (b == JologType::Long()) {
                  out << opcodes::convertDoubleToLong(stack);
               } else {
                  throw JologCompileException(location, "no primitive conversion between \""+a.prettyPrint()+"\" and \""+b.prettyPrint()+"\"");
               }
            } else if (a == JologType::Long()) {
               if (b == JologType::Int()) {
                  out << opcodes::convertLongToInt(stack);
               } else if (b == JologType::Float()) {
                  out << opcodes::convertLongToFloat(stack);
               } else if (b == JologType::Double()) {
                  out << opcodes::convertLongToDouble(stack);
               } else {
                  throw JologCompileException(location, "no primitive conversion between \""+a.prettyPrint()+"\" and \""+b.prettyPrint()+"\"");
               }
            } else {
               throw JologCompileException(location, "no primitive conversion between \""+a.prettyPrint()+"\" and \""+b.prettyPrint()+"\"");
            }
         }
      }


/*          JologType objectType;
            if (type == JologType::Boolean()) {
               objectType = "java/lang/Boolean";
               out << opcodes::typecast(objectType, stack);
            } else if (type == JologType::Byte()) {
               objectType = "java/lang/Byte";
               out << opcodes::typecast(objectType, stack);
            } else if (type == JologType::Short()) {
               objectType = "java/lang/Short";
               out << opcodes::typecast(objectType, stack);
            } else if (type == JologType::Int()) {
               objectType = "java/lang/Integer";
               out << opcodes::typecast(objectType, stack);
            } else if (type == JologType::Long()) {
               objectType = "java/lang/Long";
               out << opcodes::typecast(objectType, stack);
            } else if (type == JologType::Character()) {
               objectType = "java/lang/Character";
               out << opcodes::typecast(objectType, stack);
            } else if (type == JologType::Float()) {
               objectType = "java/lang/Float";
               out << opcodes::typecast(objectType, stack);
            } else if (type == JologType::Double()) {
               objectType = "java/lang/Double";
               out << opcodes::typecast(objectType, stack);
            } else { //reference types can go directly
               throw JologCompileException(location, "cannot cast Object to \""+type+"\"");
            }
            emitReferenceAsPrimitive(out, objectType, stack, location);
         } else if (type.arrayDimensions() > 0 && JologType::isPrimitiveType(type.baseType())) {
            if (1) {
               throw JologCompileException(location, "");
            }
         } else if (type != objectType) { //primitive arrays can be converted
            throw JologCompileException(location, "arrays of primitive type cannot be typecast");
            out << opcodes::typecast(type, stack);
         }
      }*/

      static void emitAsReference(std::ostream& out, const JologNode* value, StackDepth& stack) {
         JologType type = value->getType();
         emitAsReferencePrefix(out, type, stack);
         value->emit(out, stack);
         emitAsReferenceSuffix(out, type, stack);
      }

      static void emitLocalAsReference(std::ostream& out, int slot, JologType type, StackDepth& stack) {
         emitAsReferencePrefix(out, type, stack);
         out << opcodes::load(slot, type, stack);
         emitAsReferenceSuffix(out, type, stack);
      }

      static JologType emitReferenceAsPrimitive(std::ostream& out, JologType objectType, StackDepth& stack, string location) {
         stack.pop("cast ("+location+")");
         stack.push();
         JologType primitive(objectType);
         if (objectType == JologType("java/lang/Integer")) {
            out << opcodes::invokevirtual(objectType.getClassName(), "intValue()I");
            primitive = JologType::Int();
         } else if (objectType == JologType("java/lang/Double")) {
            stack.push();
            out << opcodes::invokevirtual(objectType.getClassName(), "doubleValue()D");
            primitive = JologType::Double();
         } else if (objectType == JologType("java/lang/Long")) {
            stack.push();
            out << opcodes::invokevirtual(objectType.getClassName(), "LongValue()L");
            primitive = JologType::Long();
         } else if (objectType == JologType("java/lang/Float")) {
            out << opcodes::invokevirtual(objectType.getClassName(), "floatValue()F");
            primitive = JologType::Float();
         } else if (objectType == JologType("java/lang/Short")) {
            out << opcodes::invokevirtual(objectType.getClassName(), "shortValue()S");
            primitive = JologType::Short();
         } else if (objectType == JologType("java/lang/Character")) {
            out << opcodes::invokevirtual(objectType.getClassName(), "charValue()C");
            primitive = JologType::Character();
         } else if (objectType == JologType("java/lang/Boolean")) {
            out << opcodes::invokevirtual(objectType.getClassName(), "booleanValue()Z");
            primitive = JologType::Boolean();
         } else if (objectType == JologType("java/lang/Byte")) {
            out << opcodes::invokevirtual(objectType.getClassName(), "byteValue()B");
            primitive = JologType::Byte();
         } else {
            throw JologCompileException(location, "INTERNAL COMPILER ERROR: \""+objectType.prettyPrint()+"\" cannot be converted to a primitive");
         }
         return primitive;
      }

      static void emitReferenceAsPrimitive(std::ostream& out, JologType objectType, JologType primitiveType, StackDepth& stack, string location) {
         if (objectType == JologType::Object()) { //this should ONLY be used for conversion from fluentStore/pick --> Object --> Integer (et al) --> int (et al)
            if (primitiveType == JologType::Int()) {
               objectType = JologType("java/lang/Integer");
            } else if (primitiveType == JologType::Double()) {
               objectType = JologType("java/lang/Double");
            } else if (primitiveType == JologType::Long()) {
               objectType = JologType("java/lang/Long");
            } else if (primitiveType == JologType::Float()) {
               objectType = JologType("java/lang/Float");
            } else if (primitiveType == JologType::Short()) {
               objectType = JologType("java/lang/Short");
            } else if (primitiveType == JologType::Byte()) {
               objectType = JologType("java/lang/Byte");
            } else if (primitiveType == JologType::Boolean()) {
               objectType = JologType("java/lang/Boolean");
            } else if (primitiveType == JologType::Character()) {
               objectType = JologType("java/lang/Character");
            } else {
               throw JologCompileException(location, "INTERNAL COMPILER ERROR: unknown conversion from Object type to primitive \""+primitiveType.prettyPrint()+"\"");
            }
            //we need to convert the Object to this reference type first
            out << opcodes::typecast(objectType, stack);
         }
         std::cout << location << ": CAST converted to type \"" << objectType.prettyPrint() << "\" --> primitive = " << primitiveType.prettyPrint() << std::endl;
         JologType objectPrimitive = emitReferenceAsPrimitive(out, objectType, stack, location);
         castPrimitives(out, objectPrimitive, primitiveType, stack, location);
      }

	private:
		JologType conversion;
		JologNode* expr;

      static void emitAsReferencePrefix(std::ostream& out, JologType type, StackDepth& stack) {
         if (type.arrayDimensions() == 0) { //arrays are already of reference type
            if (type == JologType::Void()) {
               throw JologCompileException("", "INTERNAL COMPILER ERROR: cannot emit a reference to a void type");
            } else if (type == JologType::Boolean()) {
               out << opcodes::newObject(JologType("java/lang/Boolean"), stack);
               out << opcodes::duplicate(stack);
            } else if (type == JologType::Byte()) {
               out << opcodes::newObject(JologType("java/lang/Byte"), stack);
               out << opcodes::duplicate(stack);
            } else if (type == JologType::Short()) {
               out << opcodes::newObject(JologType("java/lang/Short"), stack);
               out << opcodes::duplicate(stack);
            } else if (type == JologType::Int()) {
               out << opcodes::newObject(JologType("java/lang/Integer"), stack);
               out << opcodes::duplicate(stack);
            } else if (type == JologType::Long()) {
               out << opcodes::newObject(JologType("java/lang/Long"), stack);
               out << opcodes::duplicate(stack);
            } else if (type == JologType::Character()) {
               out << opcodes::newObject(JologType("java/lang/Character"), stack);
               out << opcodes::duplicate(stack);
            } else if (type == JologType::Float()) {
               out << opcodes::newObject(JologType("java/lang/Float"), stack);
               out << opcodes::duplicate(stack);
            } else if (type == JologType::Double()) {
               out << opcodes::newObject(JologType("java/lang/Double"), stack);
               out << opcodes::duplicate(stack);
            } //else it is already a reference
         }
      }

      static void emitAsReferenceSuffix(std::ostream& out, JologType type, StackDepth& stack) {
         if (type.arrayDimensions() == 0) {
            if (type == JologType::Boolean()) {
               stack.pop("cast to reference"); //boolean
               stack.pop("cast to reference"); //Boolean object
               out << opcodes::invokespecial("java/lang/Boolean", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } else if (type == JologType::Byte()) {
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               out << opcodes::invokespecial("java/lang/Byte", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } else if (type == JologType::Short()) {
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               out << opcodes::invokespecial("java/lang/Short", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } else if (type == JologType::Int()) {
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               out << opcodes::invokespecial("java/lang/Integer", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } else if (type == JologType::Long()) {
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               out << opcodes::invokespecial("java/lang/Long", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } else if (type == JologType::Character()) {
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               out << opcodes::invokespecial("java/lang/Character", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } else if (type == JologType::Float()) {
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               out << opcodes::invokespecial("java/lang/Float", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } else if (type == JologType::Double()) {
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               stack.pop("cast to reference");
               out << opcodes::invokespecial("java/lang/Double", "<init>("+type.getTypeName()+")"+JologType::Void().getTypeName());
            } //else it is already a reference
         }
      }
};

#endif
