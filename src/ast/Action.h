/*
 * Action.h
 *
 *  Created on: Aug 25, 2009
 *      Author: timothyc
 */

#ifndef ACTION
#define ACTION

#include <string>

#include "JologNode.h"
#include "JologType.h"
#include "Function.h"
#include "NamespaceResolver.h"

using std::string;

class Action : public Function {
   public:
      enum ActionType {PRIMITIVE_ACTION, EXOGENOUS_ACTION, SENSING_ACTION};

      Action(ActionType actionType, bool abstractModifier, bool finalModifier,
             string name, NamespaceResolver* resolver, string location) :

               Function(new JologType(actionType == PRIMITIVE_ACTION ? JologType::Void() : JologType::Message()),
                        "private", false, abstractModifier, finalModifier, name, resolver, location),
                        name(name), type(actionType) {}

      virtual string getName() const {
         return name;
      }

      virtual ActionType getActionType() const {
         return type;
      }

      virtual bool willBacktrack() const {
         return false;
      }

   private:
      string name;
      ActionType type;
};

#endif
