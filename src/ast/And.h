/*
 * And.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef AND
#define AND

#include "JologNode.h"
#include "opcodes.h"
#include "JologType.h"

class And : public JologNode {
   public:
      And(JologNode* a, JologNode* b, string location) :
         JologNode("logical and", location), p1(a), p2(b) {}

      virtual ~And() {
         delete p1;
         delete p2;
      }

      virtual JologType getType() const {
         return JologType::Boolean();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         JologType p1Type = p1->getType();
         JologType p2Type = p2->getType();
         ensure(p1Type == JologType::Boolean() && p2Type == JologType::Boolean(),
                "and operator requires binary operands, got \""+p1Type.getClassName()+"\" and \""+p2Type.getClassName()+"\"");

         string failPoint = newLabel();
         string endPoint = newLabel();
         p1->emit(out, stack);
         out << opcodes::branchIfTrue(failPoint, true, stack);
         p2->emit(out, stack);
         out << opcodes::branchIfTrue(failPoint, true, stack);

         stack.push(); //push the result on
         StackDepth junk; //disregard the other guy!
         //fall through to success point
         out << opcodes::loadBooleanLiteral(true, junk);
         out << opcodes::gotoLabel(endPoint);

         //fail point
         out << opcodes::label(failPoint);
         out << opcodes::loadBooleanLiteral(false, junk);

         //the end!
         out << opcodes::label(endPoint);
         checkStack(stack.final(), initial+1, "logical and");
      }

   private:
      JologNode* p1;
      JologNode* p2;
};

#endif
