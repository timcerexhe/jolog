/*
 * FunctionCall.h
 *
 *  Created on: Aug 21, 2009
 *      Author: timothyc
 */

#ifndef FUNCTION_CALL
#define FUNCTION_CALL

#include <vector>

#include "JologNode.h"
#include "Identifier.h"
#include "NamespaceResolver.h"

using std::vector;

class FunctionCall : public JologNode {
   public:
      FunctionCall(JologNode* parent, string name, NamespaceResolver* resolver,
                   string location, string endScope, bool canBacktrack) :

               JologNode("function", location), name(name), returnType(JologType::Void()),
               knownReturnType(false), resolver(resolver),
               endScope(endScope), canBacktrack(canBacktrack) {

         if (parent == 0) {
            this->parent = new Identifier(0, "this", resolver, location);
         } else {
            this->parent = parent;
         }
         calculateReturnType();
      }

      void addArgument(JologNode* a) {
         args.push_back(a);

         //recalculate return type based on new signature
         calculateReturnType();
      }

      virtual JologType getType() const {
         string prettyArgs;
         for (vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
            if (it != args.begin()) {
               prettyArgs += ", ";
            }
            prettyArgs += (*it)->getType().prettyPrint();
         }
         if (!knownReturnType) {
            std::cout << "AH! Function call not found? definition = " << getDefinition() << std::endl;
            std::cout << "so we are looking for: " << parent->getType().getClassName() + "/" + getDefinition() << std::endl;
            std::cout << "looking in resolver " << resolver << ", its functions are:" << std::endl;
            resolver->printAllFunctions();
            //((FunctionCall*) this)->calculateReturnType();
         }
         ensure(knownReturnType, "function \"" + name + "(" + prettyArgs + ")\" not a member of \"" + this->parent->getType().prettyPrint() + "\"");
         return returnType;
      }

      virtual ~FunctionCall() {
         for(vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
            if (*it != 0) { delete *it; }
         }
         //NB. do NOT delete resolver ... it doesn't belong to us!
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         std::cout << "FUNCTION CALL ... stack @ " << stack.final() << std::endl;
         ensure(!isBacktrackingFunction || canBacktrack, "call to backtracking function \""+name+"\" not permitted in this context");
         if (isBacktrackingFunction && canBacktrack) {
            const JologNode* succeed = new functionFunctor(this, false);
            const JologNode* fail = new functionFunctor(this, true);
            emitBacktrackingFunctionCall(out, stack, succeed, fail);
            delete succeed;
            delete fail;
         } else {
            emitFunction(out, stack);
         }

         if (returnType != JologType::Void()) { //one extra item on stack if the function returns something
            stack.push();
            initial++; //ignore the return value, so initial should equal final
            if (returnType == JologType::Double() || returnType == JologType::Long()) {
               stack.push();
               initial++; //actually +2 if we return a particularly large thing!
               //out << opcodes::pop(stack);
            }
            //out << opcodes::pop(stack);
         }

         std::cout << "FUNCTION IS OVER ... stack @ " << stack.final() << std::endl;
         checkStack(stack.final(), initial, "function call ("+name+")");
      }

      void assignTo(ostream& out, JologNode* destination, StackDepth& stack) const {
         ensure(!isBacktrackingFunction || canBacktrack, "call to backtracking function \""+name+"\" not permitted in this context");
         std::cout << "ASSIGNMENT TO FUNCTION ... stack @ " << stack.final() << std::endl;
         if (isBacktrackingFunction && canBacktrack) {
            const JologNode* succeed = new MiniAssignment(destination, new functionFunctor(this, false));
            const JologNode* fail = new MiniAssignment(destination, new functionFunctor(this, true));
            emitBacktrackingFunctionCall(out, stack, succeed, fail);
            delete succeed;
            delete fail;
         } else {
            const JologNode* call = new functionFunctor(this, false);
            destination->assign(out, call, stack);
            delete call;
         }
         std::cout << "ASSIGNMENT TO FUNCTION IS OVER ... stack @ " << stack.final() << std::endl;
      }

   private:
      JologNode* parent;
      string name;
      vector<JologNode*> args;
      JologType returnType;
      bool knownReturnType;
      NamespaceResolver* resolver;
      string endScope;
      bool isBacktrackingFunction;
      bool canBacktrack; //are we allowed to backtrack

      class functionFunctor : public JologNode {
         public:
            functionFunctor(const FunctionCall* call, bool backtrack) :
               JologNode("function call functor"), call(call), backtrack(backtrack) {}

            void emit(ostream& out, StackDepth& stack) const {
               call->emitFunction(out, stack, backtrack);
            }

            JologType getType() const { return call->getType(); }
            ~functionFunctor() {}

         private:
            const FunctionCall* call;
            bool backtrack;
      };

      class MiniAssignment : public JologNode {
         public:
            MiniAssignment(JologNode* left, JologNode* right) :
               JologNode("mini assignment"), left(left), right(right) {}
            void emit(ostream& out, StackDepth& stack) const {
               left->assign(out, right, stack);
            }
            JologType getType() const { return JologType::Void(); }
            ~MiniAssignment() {}
         private:
            JologNode* left;
            JologNode* right;
      };

      string getDefinition() const {
         string definition = name + "(";

         //arguments
         for(vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
            definition += (JologType((*it)->getType()).getTypeName());
         }

         if (isBacktrackingFunction) {
            definition += JologType::Boolean().getTypeName();
         }

         definition += ")";
         return definition;
      }

      void emitFunction(std::ostream& out, StackDepth& stack, bool backtrackStatus = false) const {
         //call getType() so we know that this function exists (and get a nice error message if it doesn't!)
         string definition = getDefinition() + getType().getTypeName();
         string type = parent->getType().getClassName() + "/" + definition;

         ensure(name != "<init>", "implementation error -- calls to class constructors should be done via Constructor.h");

         //load parent?
         if (!resolver->isStatic(type)) {
            parent->emit(out, stack);
         }

         //load args
         for(vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
            (*it)->emit(out, stack);
         }
         if (isBacktrackingFunction) {
            out << opcodes::loadBooleanLiteral(backtrackStatus, stack); //do we backtrack this time?
         }

         if (!resolver->isStatic(type)) {
            std::cout << "TYPE " << type << " IS VIRTUAL" << std::endl;
            out << opcodes::invokevirtual(parent->getType().getClassName(), definition);
            stack.pop("virtual function call object ("+getLocation()+")"); //parent
         } else {
            std::cout << "TYPE " << type << " IS STATIC" << std::endl;
            out << opcodes::invokestatic(parent->getType().getClassName(), definition);
         }

         //beware! if you get an error saying could not pop argument n then remember we cheated and actually popped the PARENT FIRST ...
         //now get rid of the arguments
         for(vector<JologNode*>::const_reverse_iterator it = args.rbegin(); it != args.rend(); ++it) {
            stack.pop("function call argument " + opcodes::intToString(it - args.rbegin())+ " ("+getLocation()+")");
         }
         if (isBacktrackingFunction) {
            stack.pop("function call backtrack flag ("+getLocation()+")"); //we sent the backtrack flag too
         }
      }

      void emitBacktrackingFunctionCall(ostream& out, StackDepth& stack, const JologNode* succeed, const JologNode* fail) const {
         string doFunction = newLabel();
         string failPoint = newLabel();
         string redo = newLabel();
         string postCall = newLabel();

         //start function call
         //resolver->saveState(out, stack, getLocation());
         out << opcodes::gotoLabel(doFunction);

         //fail point
         out << opcodes::label(failPoint);

         stack.push(); //exception handler catches exception
         out << opcodes::pop(stack); //and we promptly throw it away ...

         out << opcodes::throwFailedPreconditionException("failure in function \""+name+"\"", stack);

         //redo
         out << opcodes::label(redo);

         stack.push(); //exception handler catches exception
         out << opcodes::pop(stack); //and we promptly throw it away ...

         fail->emit(out, stack); //emitFunction(out, stack, true);
         out << opcodes::gotoLabel(postCall);
         out << opcodes::catchException(JologType::allExceptions(), redo, postCall, failPoint);

         //do the function
         out << opcodes::label(doFunction);
         succeed->emit(out, stack); //emitFunction(out, stack, false);

         //we're done!
         out << opcodes::label(postCall);

         //make sure that there is always an instruction between
         //postCall and endScope (otherwise the JVM gets upset about
         //catching an exception over an empty range)
         out << opcodes::nop();

         //out << opcodes::catchException(JologType::FailedPreconditionException().getClassName(), postCall, endScope, redo);
         resolver->addTrailingExceptionHandler(postCall, endScope, redo);
      }


      void calculateReturnType() {
         try {
            isBacktrackingFunction = false; //pretend this is just any function ...
            returnType = resolver->resolveFunctionReturnType(getLocation(), parent->getType().getClassName() + "/" + getDefinition());
            knownReturnType = true;
         } catch (JologCompileException e) {
            try {
               isBacktrackingFunction = true; //that didn't work, maybe it is a backtracking function?
               returnType = resolver->resolveFunctionReturnType(getLocation(), parent->getType().getClassName() + "/" + getDefinition());
               knownReturnType = true;
            } catch (JologCompileException e) {
               isBacktrackingFunction = false; //still no love, reset everything and try again later (after more arguments are supplied)
               knownReturnType = false;
            }
         }
         std::cout << "FUNCTION CALL " << name << " returns " << returnType.prettyPrint() << " (known = " << std::boolalpha << knownReturnType << ")" << std::endl;
         std::cout << "PARENT = " << parent->getType().prettyPrint() << std::endl;
      }

};

#endif
