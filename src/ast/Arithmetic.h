/*
 * Arithmetic.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef ARITHMETIC
#define ARITHMETIC

#include "JologNode.h"
#include "opcodes.h"

class Arithmetic : public JologNode {
   public:
      Arithmetic(string op, JologNode* lhs, JologNode* rhs, string location) :
         JologNode("arithmetic expression", location), op(op),
         left(JologCast::convertToPrimitive(lhs, location)),
         right(JologCast::convertToPrimitive(rhs, location)),
         returnType(JologType::Void()) {

         JologType leftType = left->getType();
         JologType rightType = right->getType();
         JologType type = JologType::Void();

         if (JologType::isPrimitiveType(leftType) && JologType::isPrimitiveType(rightType)) {
            if (leftType == JologType::Void() || rightType == JologType::Void()) {
               ensure(false, "arithmetic on void types is not permitted");
            } else if (leftType == JologType::Boolean() || rightType == JologType::Boolean()) {
               ensure(false, "boolean arithmetic is not permitted");
            } else if (leftType == JologType::Double() || rightType == JologType::Double()) {
               type = JologType::Double();
            } else if (leftType == JologType::Float() || rightType == JologType::Float()) {
               type = JologType::Float();
            } else if (leftType == JologType::Long() || rightType == JologType::Long()) {
               type = JologType::Long();
            } else { //int, byte, short, character
               type = JologType::Int();
            }
         } else if (leftType == JologType::String() || rightType == JologType::String()) {
            type = JologType::String();
            ensure(op == "+", "arithmetic operator \""+op+"\" not defined on Strings");
         } else {
            ensure(false, "invalid operands \""+leftType.getClassName()+"\" and \""+rightType.getClassName()+"\" to arithmetic operator '"+op+"'");
         }

         std::cout << "ARITHMETIC RETURN TYPE = " << type.prettyPrint() << " (" << leftType.prettyPrint() << " " << op << " " << rightType.prettyPrint() << ")" << std::endl;
         returnType = type;
      }

      virtual ~Arithmetic() {
         delete left;
         delete right;
      }

      virtual JologType getType() const {
         //std::cout << "ARITHMETIC TYPE = " << returnType.getClassName() << std::endl;
         return returnType;
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();

         std::cout << "ARITHMETIC OP " << op << " CASTING " << left->getType().prettyPrint() << " and " << right->getType().prettyPrint() << " to " << getType().prettyPrint() << std::endl;
         left->emit(out, stack);
         JologCast::castAtoB(out, left->getType(), getType(), stack, "arithmetic @ "+getLocation());
         right->emit(out, stack);
         JologCast::castAtoB(out, right->getType(), getType(), stack, "arithmetic @ "+getLocation());

         if (getType() == JologType::String()) {
            stack.pop("arithmetic ("+getLocation()+")"); //arg
            stack.pop("arithmetic ("+getLocation()+")"); //parent
            stack.push(); //result
            out << opcodes::invokevirtual(JologType::String().getClassName(), "concat("+JologType::String().getTypeName()+")"+JologType::String().getTypeName());
         } else if (getType() == JologType::Int()) {
            out << opcodes::integerArithmeticOperation(op, stack);
         } else if (getType() == JologType::Double()) {
            out << opcodes::doubleArithmeticOperation(op, stack);
         } else if (getType() == JologType::Long()) {
            out << opcodes::longArithmeticOperation(op, stack);
         } else if (getType() == JologType::Float()) {
            out << opcodes::floatArithmeticOperation(op, stack);
         } else {
            ensure(false, "invalid operands \""+left->getType().prettyPrint()+"\" and \""+right->getType().prettyPrint()+"\" to arithmetic operator '"+op+"'");
         }
         checkStack(stack.final(), initial+1, "arithmetic operation");
      }

   private:
      string op;
      JologNode* left;
      JologNode* right;
      JologType returnType;
};

#endif
