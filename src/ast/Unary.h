/*
 * Unary.h
 *
 *  Created on: Aug 20, 2009
 *      Author: timothyc
 */

#ifndef UNARY
#define UNARY

#include <string>
#include "JologNode.h"

using std::string;

class Unary : public JologNode {
	public:
		Unary(string op, JologNode* expr) :
         JologNode("unary expression"), op(op), expr(expr) {}

		virtual ~Unary() {}

		virtual JologType getType() const {
			if (op == "-" || op == "!" || op == "+") {
			   return expr->getType();
			} else if (op == "++" || op == "--") {
			   return JologType::Void();
			} else {
			   throw JologCompileException(getLocation(), "invalid unary operator \""+op+"\"");
			}
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
			expr->emit(out, stack);

			if (op == "-") {
		      out << opcodes::negate(getType(), getLocation(), stack);
            initial++;
            if (getType() == JologType::Double() || getType() == JologType::Long()) { initial++; }

			} else if (op == "!") {
            ensure(getType() == JologType::Boolean(), "not operator requires boolean type (got \""+getType().getClassName()+"\")");
            out << opcodes::loadBooleanLiteral(true, stack);
            out << opcodes::exclusiveOr(stack); //true XOR true = false, false XOR true = true ... logical not!
            initial++;
            if (getType() == JologType::Double() || getType() == JologType::Long()) { initial++; }

			} else if (op == "++") {
			   JologType t = expr->getType();
			   if (t == JologType::Int() || t == JologType::Short() ||
			       t == JologType::Byte() || t == JologType::Character()) {
			      out << opcodes::loadIntegerLiteral(1, stack);
			   } else if (t == JologType::Double()) {
			      out << opcodes::loadDoubleLiteral(1, stack);
			   } else if (t == JologType::Long()) {
			      out << opcodes::loadLongLiteral(1, stack);
			   } else if (t == JologType::Float()) {
			      out << opcodes::loadFloatLiteral(1, stack);
			   } else {
			      ensure(false, "prefix increment ++ requires numeric type (got \""+t.prettyPrint()+"\")");
			   }
				out << opcodes::add(t, getLocation(), stack); //TODO should this give a value back too?
            int slot = NamespaceResolver::NO_SLOT;
				if (dynamic_cast<Variable*>(expr)) {
				   slot = ((Variable*) expr)->getSlot();
            } else if (dynamic_cast<Identifier*>(expr)) {
               slot = ((Identifier*) expr)->getSlot();
				}
            ensure(slot != NamespaceResolver::NO_SLOT, "prefix increment ++ requires a variable");
            out << opcodes::store(slot, t, stack);

			} else if (op == "--") {
			   JologType t = expr->getType();
            if (t == JologType::Int() || t == JologType::Short() ||
                t == JologType::Byte() || t == JologType::Character()) {
               out << opcodes::loadIntegerLiteral(1, stack);
            } else if (t == JologType::Double()) {
               out << opcodes::loadDoubleLiteral(1, stack);
            } else if (t == JologType::Long()) {
               out << opcodes::loadLongLiteral(1, stack);
            } else if (t == JologType::Float()) {
               out << opcodes::loadFloatLiteral(1, stack);
            } else {
               ensure(false, "prefix decrement ++ requires numeric type (got \""+t.prettyPrint()+"\")");
            }
            out << opcodes::sub(t, getLocation(), stack); //TODO should this give a value back too?
            int slot = NamespaceResolver::NO_SLOT;
				if (dynamic_cast<Variable*>(expr)) {
				   slot = ((Variable*) expr)->getSlot();
            } else if (dynamic_cast<Identifier*>(expr)) {
               slot = ((Identifier*) expr)->getSlot();
				}
            ensure(slot != NamespaceResolver::NO_SLOT, "prefix decrement -- requires a variable");
            out << opcodes::store(slot, t, stack);

			} else { //else, *should* be unary + which appears pretty useless!
            ensure(op == "+", "invalid unary operator \""+op+"\"");
            ensure(JologType::isPrimitiveType(getType()) && getType() != JologType::Boolean(), "unary + requires a numeric type");
            initial++;
            if (getType() == JologType::Double() || getType() == JologType::Long()) { initial++; }
         }

			checkStack(stack.final(), initial, "unary '"+op+"' operation");
		}

	private:
		string op;
		JologNode* expr;
};

#endif
