/*
 * Constructor.h
 *
 *  Created on: Aug 26, 2009
 *      Author: timothyc
 */

#ifndef CONSTRUCTOR
#define CONSTRUCTOR

#include <vector>

#include "JologNode.h"
#include "Identifier.h"
#include "NamespaceResolver.h"
#include "../compiler/JologCompileException.h"

using std::vector;

class Constructor : public JologNode {
	public:
		Constructor(JologType classType, NamespaceResolver* resolver, string location) :
		   JologNode("constructor", location), resolver(resolver),
		   type(resolver->lookupType(classType.getClassName(), getLocation())) {

		   ensure(type != JologType::Void(), "cannot call a method on a void type");
		   ensure(!JologType::isPrimitiveType(type), "cannot call a method on primitive type \""+type.getClassName()+"\"");
		}

		void addArgument(JologNode* a) {
			args.push_back(a);
		}

		virtual JologType getType() const {
			return type;
		}

		virtual ~Constructor() {
			for(vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
				if (*it != 0) { delete *it; }
			}
			//NB. do NOT delete resolver ... it doesn't belong to us!
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
         out << opcodes::newObject(type, stack);
         out << opcodes::duplicate(stack);

			string definition = "<init>(";
         //push args
			for(vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
				(*it)->emit(out, stack);
            definition += resolver->lookupType((*it)->getType().getClassName(), getLocation()).getTypeName();
			}
         definition += ")" + JologType::Void().getTypeName();

         for (vector<JologNode*>::const_reverse_iterator it = args.rbegin(); it != args.rend(); ++it) {
            stack.pop("contructor argument "+opcodes::intToString(it - args.rbegin())+" ("+getLocation()+")"); //remove all arguments (in REVERSE order) ... although it is much of a muchness here!
         }
         stack.pop("constructor \"this\" argument ("+getLocation()+")"); //this/object
         out << opcodes::invokespecial(type.getClassName(), definition);

         //should be one more entry now than when we started (we duplicated newObject so one copy is not consumed)
		   checkStack(stack.final(), initial + 1, "constructor");
		}

	private:
		vector<JologNode*> args;
      NamespaceResolver* resolver;
      JologType type;

};

#endif
