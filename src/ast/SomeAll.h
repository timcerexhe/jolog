/*
 * SomeAll.h
 *
 *  Created on: Oct 21, 2009
 *      Author: timothyc
 *
 */

#ifndef SOME_ALL
#define SOME_ALL

#include "Pick.h"
#include "Unary.h"
#include "Holds.h"
#include "NamespaceResolver.h"

#define END_SOME_LABEL "end_some_success"

class SomeAll : public Pick {
   public:
      //this code was inherited from PICK, changes there should probably be reflected here too
      /**
       * @param endScope: indicates what region of code the pick should catch
       * exceptions from. This should ordinarily be the end of current scope.
       */
      //Pick(string location, JologType pickType, string endScope,
      //     string pickName, JologNode* program, NamespaceResolver* resolver) :

      SomeAll(string location, Variable* variable, JologNode* condition,
              NamespaceResolver* callingResolver, NamespaceResolver* newResolver, string someOrAll, int someId) :

               Pick(location, variable, END_SOME_LABEL, modifyProgram(condition, location, someOrAll == "some"), newResolver),
               callingResolver(callingResolver), newResolver(newResolver), someId(someId), some(someOrAll == "some") {

         initialiseCall();
         std::cout << "SOME/ALL " << someId << " USING VARIABLE IN SLOT " << variable->getSlot() << std::endl;
      }

//         ensure(!JologType::isPrimitiveType(type), (some ? "some" : "all") + " requires a reference type (got \""+type.getClassName()+"\")");
         //variable = new Variable(new JologType(type), pickName, 0, endPick, resolver, false);
//         std::cout << " === CREATE SOME/ALL VARIABLE \"" << variable->getName() << "\" @ SLOT " << variable->getSlot() << " ===" << std::endl;

//         variableSlot(resolver->getFreeSlot(getLocation(), JologType::PickVariable()));
//         ensure(program != 0, (some ? "some" : "all") + " requires a program");

      JologNode* getCall() const {
         return call;
      }

      ~SomeAll() {
         delete newResolver; //this belongs to us (pick), but the caller is another function so it looks after the other one
         delete call;
      }

      virtual void emit(std::ostream& out, StackDepth& stack) const {
         //TODO: tidy up the backtrack stack: we aren't coming back, so we shouldn't leave our crap
         out << ".method private " << definition << std::endl;

         std::stringstream ss;
         int initial = stack.final();

         string start = newLabel();

         ss << opcodes::label(start);
         //initialise our variable because the JVM is being lame
         //out << opcodes::loadNull(stack);
         //out << opcodes::storeReference(slot, stack);

         Pick::emit(ss, stack);
         ss << opcodes::loadBooleanLiteral(some, stack);
         ss << opcodes::returnType(JologType::Boolean(), stack);
         ss << opcodes::label(END_SOME_LABEL);

         //our final exception handler indicates failure of the some/all
         stack.push(); //exception
         ss << opcodes::pop(stack); //throw away the exception
         ss << opcodes::loadBooleanLiteral(!some, stack);
         ss << opcodes::returnType(JologType::Boolean(), stack);

//         ss << " ; =================================== SOME/ALL ADDING TRAILING HANDLER (" << start << " - " << END_SOME_LABEL << " w/ " << END_SOME_LABEL << ")" << std::endl;
//         ss << opcodes::catchException(JologType::allExceptions(), start, END_SOME_LABEL, END_SOME_LABEL);
//         newResolver->addTrailingExceptionHandler(start, END_SOME_LABEL, END_SOME_LABEL);

         out << ".limit stack " << stack.max() << std::endl;
         out << ".limit locals " << localCount << std::endl;
         out << ss.str();

         newResolver->printTrailingHandlers(out);
         out << opcodes::catchException(JologType::allExceptions(), start, END_SOME_LABEL, END_SOME_LABEL);

         out << ".end method" << std::endl;
         checkStack(stack.final(), initial, "some");
      }


      virtual JologType getType() const {
         return JologType::Boolean();
      }

   private:
      NamespaceResolver* callingResolver;
      NamespaceResolver* newResolver;
      string definition;
      FunctionCall* call;
      int someId;
      bool some;
      int localCount;

      static JologNode* modifyProgram(JologNode* condition, string location, bool some) {
         if (condition->getType() != JologType::Boolean()) {
            throw JologCompileException(location, string(some ? "some" : "all") + " requires a boolean condition");
         }
         if (!some) { //all!
            condition = new Unary("!", condition);
         }
         condition = new Holds(condition, location);
         return condition;
      }

      string getName() const {
         return "jolog_some"+opcodes::intToString(someId)+"_evaluateXOR";
      }

      static bool localComparison(const NamespaceResolver::variableType& a, const NamespaceResolver::variableType& b) {
         return (a.slot < b.slot);
      }

      void initialiseCall() {
         JologType thisType = callingResolver->lookupType("this", getLocation());
         definition = getName() + "(";

         vector< NamespaceResolver::variableType > locals;
         callingResolver->getAllLocals(locals);
         std::sort(locals.begin(), locals.end(), localComparison);
         localCount = locals.size() + 2; //1 for this pointer, 1 for pick variable

         for (vector<NamespaceResolver::variableType>::const_iterator i = locals.begin(); i != locals.end(); ++i) {
            definition += (*i).type.getTypeName();
         }

         definition += ")" + JologType::Boolean().getTypeName();
         callingResolver->addMember(thisType.getClassName() + "/" + definition, JologType::Boolean(), false, true);

         //make sure the prototype is imported (above) BEFORE trying to create the call (below)
         call = new FunctionCall(0, getName(), callingResolver, getLocation(), "", false);

         //do this after the prototype has been registered so the function call will link correctly
         for (vector<NamespaceResolver::variableType>::const_iterator i = locals.begin(); i != locals.end(); ++i) {
            call->addArgument(new Identifier(0, (*i).name, callingResolver, getLocation())); //the location is a bit of a hack!
         }
         //std::cout << "CALL TO FUNCTION \"" << prototype << " should be finalised with resolver " << callingResolver << std::endl;
         //callingResolver->printAllFunctions();

      }

};

#endif
