/*
 * While.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef WHILE
#define WHILE

#include "JologNode.h"
#include "opcodes.h"

class While : public JologNode {
	public:
		While(JologNode* condition, JologNode* body) :
			JologNode("while statement"), condition(condition), body(body) {}

		virtual ~While() {
			delete condition;
			delete body;
		}

		virtual JologType getType() const {
			return JologType::Void();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
			string startPoint = newLabel();
			string endPoint = newLabel();

			//start
			out << opcodes::label(startPoint);

			//condition
			condition->emit(out, stack);
			out << opcodes::branchIfTrue(endPoint, true, stack);

			//body
			body->emit(out, stack);
			out << opcodes::gotoLabel(startPoint);

			out << opcodes::label(endPoint);
         checkStack(stack.final(), initial, "while loop");
		}

	private:
		JologNode* condition;
		JologNode* body;
};

#endif
