/*
 * Identifier.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef IDENTIFIER
#define IDENTIFIER

#include "JologNode.h"
#include "opcodes.h"
#include "NamespaceResolver.h"

class Identifier : public JologNode {
	public:
		Identifier(JologNode* parent, string id, NamespaceResolver* resolver, string location) :
		   JologNode("variable", location), parent(parent), id(id), resolver(resolver) {

         if (parent == 0) {
   			std::cout << "creating identifier : " << id << " with parent <null>" << std::endl;
         } else if (resolver->isStatic(parent->getType().getClassName())) {
   			std::cout << "creating identifier : " << id << " with static parent <" << parent->getType().getClassName() << ">" << std::endl;
         } else {
   			std::cout << "creating identifier : " << id << " with parent <" << parent << ">" << std::endl;
         }

         //what about class members? this.id? someone needs to take responsibility for this!
		}

		virtual ~Identifier() {
         if (parent != 0) { delete parent; }
         //NB. do NOT delete resolver ... it doesn't belong to us!
      }

		virtual JologType getType() const { //our name is our type, need to resolve that to get actual type!
			JologType type(id);
			if (parent != 0) {
				//type = resolver->lookupType(parent).getType() + "/" + id;
				//type = parent->getType() + "/" + id;

            type = resolver->lookupType(parent->getType().getClassName() + "/" + id, getLocation());
			} else {
				type = resolver->lookupType(id, getLocation());
			}
			//type = resolver->lookupType(type).getType();
			//std::cout << "GET TYPE OF IDENTIFIER " << id << " = " << type << std::endl;
			return type;
		}

      virtual void assign(ostream& out, const JologNode* initialiser, StackDepth& stack) const {
         int initial = stack.final();
         if ((parent != 0) && (!resolver->isStatic(getType().getClassName()))) {
            parent->emit(out, stack);
         }

         ensure(initialiser != 0, "cannot assign null expression");
         initialiser->emit(out, stack);

         if (parent != 0) {
            string field = parent->getType().getClassName() + "/" + id;
            if (resolver->isStatic(field)) {
               out << opcodes::putStaticField(field, resolver->lookupType(field, getLocation()), stack);
            } else {
               out << opcodes::putField(field, resolver->lookupType(field, getLocation()), stack);
            }

         } else {
            int slot = resolver->getSlot(id);
            ensure(slot != NamespaceResolver::NO_SLOT, "cannot assign; variable \""+id+"\" not in scope");

            //resolver ...getType(field)
            out << opcodes::store(slot, getType(), stack);
         }
         checkStack(stack.final(), initial, "storing identifier");
      }

      static bool isBuiltin(string id) {
         bool builtin = false;
         if ((id == "endl") ||
             (id == "null") ||
             (id == "environment") ||
             (id == JologType::environment_fieldName()) ||
             (id == JologType::fluent_store_fieldName())) {

            builtin = true;
         }
         return builtin;
      }

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
			if (id == "endl") { //special language addition
				out << opcodes::loadEscapedStringLiteral("\\n", stack);

			} else if (id == JologType::environment_fieldName()) {
			   out << opcodes::loadReference(0, stack); //load this first
				out << opcodes::getField(resolver->lookupType("this", getLocation()).getClassName()+"/"+JologType::environment_fieldName(),
                                     JologType(JologType::Environment()), stack);

			} else if (id == "null") {
				out << opcodes::loadNull(stack);

			} else if (id == JologType::fluent_store_fieldName()) {
				ensure(false, "\""+JologType::fluent_store_fieldName()+"\" is a protected resource; direct access is not allowed");

			} else if (id == "length" && parent != 0 && parent->getType().arrayDimensions() > 0) {
			   parent->emit(out, stack);
			   out << opcodes::getArrayLength(stack);

			} else if (parent != 0) { //class field
            string field = parent->getType().getClassName() + "/" + id;
            if (resolver->isStatic(field)) {
               out << opcodes::getStaticField(field, resolver->lookupType(field, getLocation()), stack);
            } else {
               parent->emit(out, stack);
               out << opcodes::getField(field, resolver->lookupType(field, getLocation()), stack);
            }

         } else { //local variable
				int slot = resolver->getSlot(id);
				if (slot != NamespaceResolver::NO_SLOT) {
					JologType type = resolver->lookupType(id, getLocation()).getType();
					out << opcodes::load(slot, type, stack);
				} else {
					ensure(false, "unknown identifier \"" + id + "\"");
				}
			}
			checkStack(stack.final(), initial + 1, "loading identifier");
		}

		int getSlot() const {
		   return resolver->getSlot(id);
		}

	private:
      JologNode* parent;
		string id;
		NamespaceResolver* resolver;
};

#endif
