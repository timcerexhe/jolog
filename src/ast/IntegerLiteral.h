/*
 * IntegerLiteral.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef INTEGER_LITERAL
#define INTEGER_LITERAL

#include <string>
#include "JologNode.h"

using std::string;

class IntegerLiteral : public JologNode {
	public:
		IntegerLiteral(int i) : JologNode("integer literal"), value(i) {}

		virtual ~IntegerLiteral() {}

		virtual JologType getType() const {
			return JologType::Int();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadIntegerLiteral(value, stack);
		}

	private:
		int value;
};

#endif
