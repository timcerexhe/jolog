/*
 * Scope.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef SCOPE
#define SCOPE

#include <vector>
#include "JologNode.h"
#include "NamespaceResolver.h"

class Scope : public JologNode {
	public:
      //for internal use by other constructs that
      //nest several scopes to make things work nicely
      //DO NOT USE THIS CONSTRUCTOR FOR USER-DEFINED SCOPE
      Scope(string location) :
         JologNode("scope", location), endLabel(""), resolver(0) {}

	   Scope(string location, NamespaceResolver* parent) :
	      JologNode("scope", location), endLabel(""),
         resolver(new NamespaceResolver(parent)) {}

	   Scope(string location, string endLabel, NamespaceResolver* parent) :
	      JologNode("scope", location), endLabel(endLabel),
         resolver(new NamespaceResolver(parent)) {}

	   NamespaceResolver* getResolver() const {
         ensure(resolver != 0, "scope does not hold a namespace resolver");
	      return resolver;
	   }

		void addStatement(JologNode* s) {
			ensure(s != 0, "cannot add null statement to scope");
			statements.push_back(s);
		}

		virtual ~Scope() {
			std::vector<JologNode*>::const_iterator it = statements.begin();
			while (it != statements.end()) {
				delete *it;
				++it;
			}
			delete resolver; //woo! scope owns its own little resolver!
		}

		virtual JologType getType() const {
			return JologType::Void();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
			std::vector<JologNode*>::const_iterator it = statements.begin();
			while (it != statements.end()) {
				(*it)->emit(out, stack);
				++it;
			}

			if (endLabel.size() > 0) { //don't emit a label if we weren't given one
			   out << opcodes::label(endLabel);
			}
			resolver->printTrailingHandlers(out);
			checkStack(stack.final(), initial, "scope");
		}

	private:
	   string endLabel;
		std::vector<JologNode*> statements;
		NamespaceResolver* resolver;
};

#endif
