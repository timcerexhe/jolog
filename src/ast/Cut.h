/*
 * Cut.h
 *
 *  Created on: Oct 5, 2009
 *      Author: timothyc
 */

#ifndef CUT
#define CUT

#include "JologNode.h"
#include "opcodes.h"
#include "JologType.h"

class Cut : public JologNode {
   public:
      Cut(string location, string endScope, NamespaceResolver* resolver) :
          JologNode("cut", location), endScope(endScope), resolver(resolver) {}

      virtual ~Cut() {}

      virtual JologType getType() const {
         return JologType::Void();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();

         resolver->clearBacktrackHistory(out, stack);

         string cut = newLabel();
         string fail = newLabel();

         out << opcodes::gotoLabel(cut);

         out << opcodes::label(fail);

         stack.push(); //exception handler catches exception
         out << opcodes::pop(stack); //and we promptly throw it away ...

         out << opcodes::throwProgramFailureException("cut failed at "+getLocation(), stack);

         //the end!
         out << opcodes::label(cut);

         resolver->addTrailingExceptionHandler(cut, endScope, fail);
         //out << opcodes::catchException(JologType::FailedPreconditionException().getClassName(), cut, endScope, fail);

         //make sure that there is always an instruction between
         //postCall and endScope (otherwise the JVM gets upset about
         //catching an exception over an empty range)
         out << opcodes::nop();

         checkStack(stack.final(), initial, "cut");
      }

   private:
      string endScope;
      NamespaceResolver* resolver;
};

#endif
