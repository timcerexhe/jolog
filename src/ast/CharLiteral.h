/*
 * CharLiteral.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef CHAR_LITERAL
#define CHAR_LITERAL

#include <string>
#include "JologNode.h"
#include "JologType.h"

using std::string;

class CharLiteral : public JologNode {
	public:
		CharLiteral(char c) : JologNode("character literal"), value(c) {}

		virtual ~CharLiteral() {}

		virtual JologType getType() const {
			return JologType::Character();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadCharacterLiteral(value, stack);
		}

	private:
		char value;
};

#endif
