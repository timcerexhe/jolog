/*
 * JologType.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef JOLOG_TYPE
#define JOLOG_TYPE

#include <iostream>
#include "JologNode.h"

using std::ostream;

class JologType : public JologNode {
	public:
		JologType(string fqcn, int arrayDimensions = 0) :
         JologNode("JologType"), fqcn(fqcn), dimensions(arrayDimensions) {

         ensure(arrayDimensions >= 0, "array dimensionality must be non-negative");
      }

		JologType(const JologType& other) :
         JologNode("JologType"), fqcn(other.fqcn), dimensions(other.dimensions) {}

		string getClassName() const {
         string s(fqcn);

         if (dimensions > 0) { //arrays always print as typename
            s = getTypeName();
         }
			return s;
		}

		string getTypeName() const {
			return type(fqcn, dimensions);
		}

		virtual JologType getType() const {
			return *this;
		}

		bool operator!=(const JologType& other) const {
		   return !operator==(other);
		}

		bool operator==(const JologType& other) const {
			return ((getClassName() == other.getClassName()) &&
                 (dimensions == other.dimensions));
		}

		static JologType commonBase(const JologType a, const JologType b) {
			//JologType base;

			if (a == b) {
				return a;
			} else if ((a.dimensions == b.dimensions) &&
                    ((a.fqcn == "I" && b.fqcn == "D") ||
					      (a.fqcn == "D" && b.fqcn == "I"))) {
				return JologType("D", a.dimensions);
			} else if ((a.dimensions == b.dimensions) && //same dimension
                    ((a.fqcn != "I" && b.fqcn != "I") && //and both of reference type
                     (a.fqcn != "D" && b.fqcn != "D") &&
                     (a.fqcn != "Z" && b.fqcn != "Z") &&
                     (a.fqcn != "C" && b.fqcn != "C"))) {

			   return JologType("java/lang/Object", a.dimensions);
			} else {
			   throw JologCompileException("JologType common base", "cannot get common base class between incompatible types \""+a.prettyPrint()+"\" and \""+b.prettyPrint()+"\"");
			}
			//TODO: inheritance -> common ancestor

			//return base;
		}

		virtual JologType baseType() const { return JologType(fqcn); }
		virtual size_t arrayDimensions() const { return dimensions; }

		virtual ~JologType() {}

		static string environment_fieldName() {
		   return "environment";
      }

      static string fluent_store_fieldName() {
         return "jolog_fluent_store";
      }

      static string backtrack_stack_fieldName() {
         return "jolog_back_stack";
      }

      static bool isPrimitiveType(JologType type) {
         bool primitive = false;
         if (type == JologType::Boolean() ||
             type == JologType::Byte() ||
             type == JologType::Short() ||
             type == JologType::Int() ||
             type == JologType::Long() ||
             type == JologType::Character() ||
             type == JologType::Float() ||
             type == JologType::Double()) {
            primitive = true;
         }
         return primitive;
      }

      //builtin types
		static JologType Void() { return JologType("V"); }
		static JologType Boolean() { return JologType("Z"); }
		static JologType Byte() { return JologType("B"); }
		static JologType Short() { return JologType("S"); }
		static JologType Int() { return JologType("I"); }
		static JologType Long() { return JologType("L"); }
		static JologType Character() { return JologType("C"); }
		static JologType Float() { return JologType("F"); }
		static JologType Double() { return JologType("D"); }

		//common class types
      static JologType Object() { return JologType("java/lang/Object"); }
		static JologType String() { return JologType("java/lang/String"); }
		static JologType System() { return JologType("java/lang/System"); }
		static JologType Message() { return JologType("jolog/Message"); }
		static JologType Predicate() { return JologType("jolog/Predicate"); }
		static JologType FluentStore() { return JologType("jolog/FluentStore"); }
		static JologType FailedPreconditionException() { return JologType("jolog/FailedPreconditionException"); }
		static JologType ProgramFailureException() { return JologType("jolog/ProgramFailureException"); }
		static JologType Environment() { return JologType("jolog/Environment"); }
		static JologType Matcher() { return JologType("java/util/regex/Matcher"); }
		static JologType Pattern() { return JologType("java/util/regex/Pattern"); }
		static JologType BacktrackStack() { return JologType("java/util/Stack"); }
		static JologType ChoicePointState() { return JologType("jolog/ChoicePointState"); }
		static JologType TransientState() { return JologType("jolog/TransientState"); }
      static JologType Class() { return JologType("java/lang/Class"); }
      static JologType PrintStream() { return JologType("java/io/PrintStream"); }
      static JologType InputStream() { return JologType("java/io/InputStream"); }
      static JologType Scanner() { return JologType("java/util/Scanner"); }

      static string allExceptions() {
         return "all";
      }

		virtual void emit(ostream& out, StackDepth& stack) const {
		   out << getClassName() << "\n"; //TODO: hack! endl() is in opcodes, but it needs JologType!
			//ensure(false, "JologType cannot emit: ambiguous decision between ClassName and TypeName for \""+getClassName()+"\"");
		}

		static string type(string classname, size_t dimensions) {
			string s;
			if (classname == "int" || classname == "I") {
				s = "I";
			} else if (classname == "double" || classname == "D") {
				s = "D";
			} else if (classname == "long" || classname == "L") {
			   s = "L";
			} else if (classname == "float" || classname == "F") {
				s = "F";
			} else if (classname == "boolean" || classname == "Z") {
				s = "Z";
			} else if (classname == "char" || classname == "C") {
			   s = "C";
			} else if (classname == "short" || classname == "S") {
			   s = "S";
			} else if (classname == "byte" || classname == "B") {
			   s = "B";
			} else if (classname == "void" || classname == "V") {
			   s = "V";
			} else {
				s = "L"+classname+";";
			}

         for (size_t d = 0; d < dimensions; ++d) {
            s = "[" + s;
         }

			return s;
		}

      string prettyPrint() const {
         string pretty = fqcn;
         if (fqcn == "I") { pretty = "int"; }
         if (fqcn == "D") { pretty = "double"; }
         if (fqcn == "Z") { pretty = "boolean"; }
         if (fqcn == "V") { pretty = "void"; }
         if (fqcn == "S") { pretty = "short"; }
         if (fqcn == "B") { pretty = "byte"; }
         if (fqcn == "L") { pretty = "long"; }
         if (fqcn == "F") { pretty = "float"; }
         if (fqcn == "C") { pretty = "char"; }

         for (size_t i=0; i < dimensions; ++i) {
            pretty += "[]";
         }
         return pretty;
      }

      virtual void assign(ostream& out, const JologNode* initialiser, StackDepth& stack) const {
         ensure(false, "cannot assign to internal class \"JologType\"");
      }

	private:
		string fqcn;
      size_t dimensions;

};

#endif
