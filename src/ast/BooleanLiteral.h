/*
 * BooleanLiteral.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef BOOLEAN_LITERAL
#define BOOLEAN_LITERAL

#include <string>
#include <algorithm>
#include "JologNode.h"
#include "JologType.h"

using std::string;

class BooleanLiteral : public JologNode {
	public:
	   BooleanLiteral(bool boolean) :
         JologNode("boolean literal"), value(boolean) {}

		BooleanLiteral(string boolean) : JologNode("boolean literal") {

			std::transform(boolean.begin(), boolean.end(), boolean.begin(), tolower);
			ensure(boolean == "true" || boolean == "false", "invalid boolean value \""+boolean+"\"");
			value = (boolean == "true");
		}

		virtual ~BooleanLiteral() {}

		virtual JologType getType() const {
			return JologType::Boolean();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadBooleanLiteral(value, stack);
		}

	private:
		bool value;
};

#endif
