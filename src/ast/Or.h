/*
 * Or.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef OR
#define OR

#include "JologNode.h"
#include "opcodes.h"

class Or : public JologNode {
   public:
      Or(JologNode* a, JologNode* b, string location) :
         JologNode("logical or", location), p1(a), p2(b) {}

      virtual ~Or() {
         if (p1 != 0) { delete p1; }
         if (p2 != 0) { delete p2; }
      }

      virtual JologType getType() const {
         return JologType::Boolean();
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         JologType p1Type = p1->getType();
         JologType p2Type = p2->getType();
         ensure(p1Type == JologType::Boolean() && p2Type == JologType::Boolean(),
               "or operator requires binary operands, got \""+p1Type.getClassName()+"\" to \""+p2Type.getClassName()+"\"");

         string successPoint = newLabel();
         string endPoint = newLabel();

         p1->emit(out, stack);
         out << opcodes::branchIfTrue(successPoint, false, stack);
         p2->emit(out, stack);
         out << opcodes::branchIfTrue(successPoint, false, stack);

         //fall through to fail point
         out << opcodes::loadBooleanLiteral(false, stack);
         out << opcodes::gotoLabel(endPoint);

         //success point
         out << opcodes::label(successPoint);
         out << opcodes::loadBooleanLiteral(true, stack);

         //the end!
         out << opcodes::label(endPoint);
         checkStack(stack.final(), initial+1, "logical or");
      }

   private:
      JologNode* p1;
      JologNode* p2;
};

#endif
