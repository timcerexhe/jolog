/*
 * FailedPreconditionException.h
 *
 *  Created on: Sep 18, 2009
 *      Author: timothyc
 */

#ifndef FAILED_PRECONDITION_EXCEPTION
#define FAILED_PRECONDITION_EXCEPTION

#include <string>
#include "JologNode.h"

using std::string;

class FailedPreconditionException : public JologNode {
	public:
		FailedPreconditionException() :
         JologNode("failed precondition exception") {}

      FailedPreconditionException(string message) :
         JologNode("failed precondition exception"), message(message) {}

		virtual ~FailedPreconditionException() {}

		virtual JologType getType() const {
			return JologType::FailedPreconditionException();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::throwFailedPreconditionException(message, stack);
		}

	private:
		string message;
};

#endif
