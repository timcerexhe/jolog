/*
 * LongLiteral.h
 *
 *  Created on: Oct 6, 2009
 *      Author: timothyc
 */

#ifndef LONG_LITERAL
#define LONG_LITERAL

#include <string>
#include "JologNode.h"

using std::string;

class LongLiteral : public JologNode {
	public:
		LongLiteral(long l) :
         JologNode("long literal"), value(l) {}

		virtual ~LongLiteral() {}

		virtual JologType getType() const {
			return JologType::Long();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadLongLiteral(value, stack);
		}

	private:
		long value;
};

#endif
