/*
 * Ternary.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef TERNARY
#define TERNARY

class Ternary : public JologNode {
	public:
		Ternary(JologNode* condition, JologNode* p1, JologNode* p2, string location) :
		   JologNode("ternary expression", location), condition(condition), p1(p1), p2(p2) {}

		virtual ~Ternary() {
			if (condition != 0) { delete condition; }
			if (p1 != 0) { delete p1; }
			if (p2 != 0) { delete p2; }
		}

		virtual JologType getType() const {
			return p1->getType(); //TODO: check p2 as well!
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			string falseLabel = newLabel();
			string endLabel = newLabel();

			condition->emit(out, stack);
			out << opcodes::branchIfTrue(falseLabel, true, stack);

			StackDepth leftStack, rightStack;

			p1->emit(out, leftStack);
			out << opcodes::gotoLabel(endLabel);

			out << opcodes::label(falseLabel);
			p2->emit(out, rightStack);

			out << opcodes::label(endLabel);

			std::stringstream errorMessage;
         errorMessage << "unbalanced stack at end of ternary (" << leftStack.final() << " vs " << rightStack.final() << ")";
         ensure(leftStack.final() == rightStack.final(), errorMessage.str());

         int max = std::max(leftStack.max(), rightStack.max());
         for (int i = 0; i < max; ++i) { //increase the max
            stack.push();
         }
         //TODO: is this right? if doesn't do this!
         for (int i = 0; i < (max - leftStack.final()); ++i) { //take it back down to ternary body's final value
            stack.pop("tuple element "+opcodes::intToString(i)+" ("+getLocation()+")");
         }
		}

	private:
		JologNode* condition;
		JologNode* p1;
		JologNode* p2;
};

#endif
