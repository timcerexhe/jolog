/*
 * Assignment.h
 *
 *  Created on: Aug 25, 2009
 *      Author: timothyc
 */

#ifndef ASSIGNMENT
#define ASSIGNMENT

#include "JologNode.h"
#include "JologCast.h"
#include "NamespaceResolver.h"
#include "opcodes.h"
#include "FunctionCall.h"

class Assignment : public JologNode {
	public:
		Assignment(JologNode* lhs, string op, JologNode* rhs, NamespaceResolver* resolver, string location) :
		   JologNode("assignment", location), cloned(false), left(lhs), right(rhs), resolver(resolver) {

		   ensure(lhs != 0 && rhs != 0, "assignment requires non-null operands");
         string base(JologType::commonBase(left->getType(), right->getType()).getClassName());

         JologType leftType = left->getType();
         JologType rightType = right->getType();
         ensure(base.size() != 0, "invalid conversion in assignment from \"" + rightType.getClassName() + "\" to \"" + leftType.getClassName() + "\"");


         //TODO appropriate type rules from Arithmetic.h
         if ((left->getType() == JologType::Int()) && (right->getType() == JologType::Double())) {
            right = new JologCast(JologType::Int(), right, location);
         } else if ((left->getType() == JologType::Double()) && (right->getType() == JologType::Int())) {
            right = new JologCast(JologType::Double(), right, location);
         } // else if (right->getType() != commonBase) { typecast? //TODO check this

			if (op != "=") { //TODO: other operators: += -= *= /= %=
				//ensure(op.size() >= 1, "invalid assignment operator \""+op+"\"");
				//right = new Arithmetic(string(op[0]), right, lhs);
				ensure(false, "assignment supports only the '=' operator");
			}
		}

		virtual ~Assignment() {
		   if (!cloned) {
            delete left;
            delete right;
		   }
			//NB. do NOT delete resolver ... it doesn't belong to us!
		}

		virtual JologType getType() const {
			//ensure(false, "assignment does not return a value");
			return JologType::Void();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();
		   if (dynamic_cast<FunctionCall*>(right) != 0) {
		      ((FunctionCall*) right)->assignTo(out, left, stack);
		   } else {
		      left->assign(out, right, stack);
		   }
         checkStack(stack.final(), initial, "assignment");
		}

	private:
	   bool cloned;
		JologNode* left;
		JologNode* right;
		NamespaceResolver* resolver;
};

#endif
