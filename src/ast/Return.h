/*
 * Return.h
 *
 *  Created on: Aug 19, 2009
 *      Author: timothyc
 */

#ifndef RETURN
#define RETURN

#include "JologNode.h"
#include "opcodes.h"

class Return : public JologNode {
	public:
	   /**
	    * the resolver is for backtracking functions that need to save
	    * transient variables before the function is gone
	    * provide null when this is not required
	    */
		Return(JologNode* expr, NamespaceResolver* resolver) :
         JologNode("return statement"), expr(expr), resolver(resolver) {}

		virtual ~Return() {
			if (expr != 0) { delete expr; } //we may not have an expression
			//don't delete the resolver, it doesn't belong to us
		}

		virtual JologType getType() const {
			if (expr == 0) {
				return JologType::Void();
			} else {
				return expr->getType();
			}
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
		   int initial = stack.final();

		   if (resolver != 0) {
		      out << " ; SAVE TRANSIENTS ..." << opcodes::endl();
		      std::vector<NamespaceResolver::variableType> variables;
		      resolver->getStateVariables(variables, true);
		      resolver->saveState(out, stack, variables, false, getLocation());
		   } else {
		      out << " ; DO NOT SAVE TRANSIENTS ..." << opcodes::endl();
		   }

			if (expr == 0) { //return void
				out << opcodes::returnVoid();
			} else {
				expr->emit(out, stack);
				//JologCast::castAtoB(out, expr->getType(), WHAT TYPE SHOULD I RETURN?, stack, getLocation()); //TODO
				out << opcodes::returnType(expr->getType(), stack);
			}
			checkStack(stack.final(), initial, "return statement");
		}

	private:
		JologNode* expr;
		NamespaceResolver* resolver;
};

#endif
