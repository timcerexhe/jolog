/*
 * Tuple.h
 *
 *  Created on: Aug 27, 2009
 *      Author: timothyc
 */

#ifndef TUPLE
#define TUPLE

#include <ostream>
#include <vector>
#include "JologNode.h"
#include "Function.h"
#include "JologCast.h"

using std::ostream;
using std::vector;

class Tuple : public JologNode {
   public:
      Tuple(string location, NamespaceResolver* resolver) :
         JologNode("tuple", location), elements(),
         slot(resolver->getFreeSlot(getLocation(), JologType::Object())), resolver(resolver) {}

      void addElement(JologNode* e) {
         elements.push_back(e);
      }

      virtual JologType getType() const {
         return JologType(JologType::Object().getClassName(), 1);
      }

      virtual ~Tuple() {
         vector<JologNode*>::const_iterator i = elements.begin();
         while (i != elements.end()) {
            delete *i;
            ++i;
         }
         //NB. do NOT delete resolver ... it doesn't belong to us!
      }

      virtual void emit(ostream& out, StackDepth& stack) const {
         int initial = stack.final();
         //std::cout << "EMITTING TUPLE (" << initial << ")" << std::endl;
         int index = 0;
         out << opcodes::loadIntegerLiteral(elements.size(), stack);

         out << opcodes::newArray(JologType::Object(), stack);
         for (vector<JologNode*>::const_iterator i = elements.begin(); i != elements.end(); ++i) {
            //if (i != (elements.end() - 1)) {
            out << opcodes::duplicate(stack);
            //}
            out << opcodes::loadIntegerLiteral(index, stack);
            ++index;

            ensure((*i)->getType() != JologType::Void(), "void is not a type, cannot convert to reference");
            JologCast::emitAsReference(out, *i, stack);

            out << opcodes::storeArray(getType(), stack);
         }
         checkStack(stack.final(), initial+1, "tuple"); //need a copy of the tuple left over!
         //std::cout << "END EMITTING TUPLE (" << stack.final() << ")" << std::endl;
      }

      virtual void assign(ostream& out, const JologNode* initialiser, StackDepth& stack) const {
         int initial = stack.final();
         //std::cout << "ASSIGNING TO TUPLE (" << initial << ")" << std::endl;
         ensure(initialiser->getType().arrayDimensions() > 0, "cannot assign a scalar value to a tuple");
         //doing assignment without a variable slot, get one now

         //if (slot == NamespaceResolver::NO_SLOT) {
         //   slot = resolver->getFreeSlot(getLocation(), JologType::Object()); //technically an array of objects, but it is still only one slot
         //   std::cout << "TUPLE ASSIGNMENT GRABBING SLOT " << slot << " (fn = " << slot + 1<< ")" << std::endl;
         //}

         initialiser->emit(out, stack);
         out << opcodes::store(slot, initialiser->getType(), stack);

         int index = 0;
         for (vector<JologNode*>::const_iterator i = elements.begin(); i != elements.end(); ++i) {
            /*JologNode* arrayType = new ArrayReference(getLocation(), slot, index);
            if ((*i)->getType() == JologType::Int()) {
               arrayType = new JologCast(new JologType("java/lang/Integer", 0), arrayType, getLocation());
            } else if ((*i)->getType() == JologType::Boolean()) {
               arrayType = new JologCast(new JologType("java/lang/Boolean", 0), arrayType, getLocation());
            } else if ((*i)->getType() == JologType::Double()) {
               arrayType = new JologCast(new JologType("java/lang/Double", 0), arrayType, getLocation());
            }
            JologCast ref(new JologType((*i)->getType()), arrayType, getLocation());*/
            //(*i)->assign(out, &ref, stack);

            JologNode* arrayReference = new JologCast((*i)->getType(),
                                                      new ArrayReference(getLocation(), slot, index),
                                                      getLocation());
            (*i)->assign(out, arrayReference, stack);
            delete arrayReference;
            ++index;
         }
         checkStack(stack.final(), initial, "tuple assignment");
         //std::cout << "END ASSIGNING TO TUPLE (" << stack.final() << ")" << std::endl;
      }

   private:
      vector<JologNode*> elements;
      int slot;
      NamespaceResolver* resolver;

};

#endif
