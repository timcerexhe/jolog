/*
 * FloatLiteral.h
 *
 *  Created on: Oct 6, 2009
 *      Author: timothyc
 */

#ifndef FLOAT_LITERAL
#define FLOAT_LITERAL

#include <string>
#include "JologNode.h"

using std::string;

class FloatLiteral : public JologNode {
	public:
		FloatLiteral(float f) :
         JologNode("float literal"), value(f) {}

		virtual ~FloatLiteral() {}

		virtual JologType getType() const {
			return JologType::Float();
		}

		virtual void emit(ostream& out, StackDepth& stack) const {
			out << opcodes::loadFloatLiteral(value, stack);
		}

	private:
		float value;
};

#endif
