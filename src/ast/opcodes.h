/*
 * opcodes.h
 *
 *  Created on: Aug 12, 2009
 *      Author: timothyc
 */

#ifndef OPCODES
#define OPCODES

#include <string>

using std::string;

class opcodes {
   public:
      static string load(int index, const JologType type, StackDepth& stack) {
         string s;
         if (type == JologType::Int() || type == JologType::Boolean() ||
             type == JologType::Character() || type == JologType::Byte() ||
             type == JologType::Short()) {
            s = loadInteger(index, stack);
         } else if (type == JologType::Float()) {
            s = loadFloat(index, stack);
         } else if (type == JologType::Double()) {
            s = loadDouble(index, stack);
         } else if (type == JologType::Long()) {
            s = loadLong(index, stack);
         } else {
            s = loadReference(index, stack);
         }
         return s;
      }

      static string loadArray(const JologType type, StackDepth& stack) {
         string s;
         stack.pop(); //array
         stack.pop(); //index
         stack.push(); //result

         if (type == JologType::Int()) {
            s = "   iaload" + endl();
         } else if (type == JologType::Float()) {
            s = "   faload" + endl();
         } else if (type == JologType::Double()) {
            stack.push();
            s = "   daload" + endl();
         } else if (type == JologType::Long()) {
            stack.push();
            s = "   laload" + endl();
         } else if (type == JologType::Boolean() || type == JologType::Byte()) {
            s = "   baload" + endl();
         } else if (type == JologType::Character()) {
            s = "   caload" + endl();
         } else if (type == JologType::Short()) {
            s = "   saload" + endl();
         } else {
            s = "   aaload" + endl();
         }
         return s;
      }

      static string loadReference(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.push(); //value
         if (index >= 0 && index <= 3) {
            ss << "   aload_" << index << endl();
         } else {
            ss << "   aload " << index << endl();
         }
         return ss.str();
      }

      static string loadInteger(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.push(); //value
         if (index >= 0 && index <= 3) {
            ss << "   iload_" << index << endl();
         } else {
            ss << "   iload " << index << endl();
         }
         return ss.str();
      }

      static string loadFloat(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.push(); //value
         if (index >= 0 && index <= 3) {
            ss << "   fload_" << index << endl();
         } else {
            ss << "   fload " << index << endl();
         }
         return ss.str();
      }

      static string loadDouble(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.push(); //value
         stack.push(); //DOUBLES TAKE UP TWO SLOTS TODO CHECK ME
         if (index >= 0 && index <= 3) {
            ss << "   dload_" << index << endl();
         } else {
            ss << "   dload " << index << endl();
         }
         return ss.str();
      }

      static string loadLong(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.push(); //value
         stack.push(); //LONGS TAKE UP TWO SLOTS TODO CHECK ME
         if (index >= 0 && index <= 3) {
            ss << "   lload_" << index << endl();
         } else {
            ss << "   lload " << index << endl();
         }
         return ss.str();
      }

      static string store(int index, const JologType type, StackDepth& stack) {
         string s;
         if (type == JologType::Int() || type == JologType::Boolean() ||
             type == JologType::Character() || type == JologType::Byte() ||
             type == JologType::Short()) {
            s = storeInteger(index, stack);
         } else if (type == JologType::Float()) {
            s = storeFloat(index, stack);
         } else if (type == JologType::Double()) {
            s = storeDouble(index, stack);
         } else if (type == JologType::Long()) {
            s = storeLong(index, stack);
         } else {
            s = storeReference(index, stack);
         }
         std::cout << "STORE \"" << type.getClassName() << "\" --> " << s;
         return s;
      }

      static string storeArray(const JologType type, StackDepth& stack) {
         string s;
         if (type == JologType::Double() || type == JologType::Long()) {
            stack.pop(); //pop twice for doubles
         }
         stack.pop(); //value
         stack.pop(); //index
         stack.pop(); //array
         if (type == JologType::Int()) {
            s = "   iastore" + endl();
         } else if (type == JologType::Double()) {
            s = "   dastore" + endl();
         } else if (type == JologType::Float()) {
            s = "   fastore" + endl();
         } else if (type == JologType::Long()) {
            s = "   lastore" + endl();
         } else if (type == JologType::Boolean() || type == JologType::Byte()) {
            s = "   bastore" + endl();
         } else if (type == JologType::Character()) {
            s = "   castore" + endl();
         } else if (type == JologType::Short()) {
            s = "   saload" + endl();
         } else {
            s = "   aastore" + endl();
         }
         return s;
      }

      static string storeInteger(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.pop();
         if (index >= 0 && index <= 3) {
            ss << "   istore_" << index << endl();
         } else {
            ss << "   istore " << index << endl();
         }
         return ss.str();
      }

      static string storeFloat(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.pop();
         if (index >= 0 && index <= 3) {
            ss << "   fstore_" << index << endl();
         } else {
            ss << "   fstore " << index << endl();
         }
         return ss.str();
      }

      static string storeDouble(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.pop(); //pop twice for doubles
         stack.pop();
         if (index >= 0 && index <= 3) {
            ss << "   dstore_" << index << endl();
         } else {
            ss << "   dstore " << index << endl();
         }
         return ss.str();
      }

      static string storeLong(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.pop(); //pop twice for longs
         stack.pop();
         if (index >= 0 && index <= 3) {
            ss << "   lstore_" << index << endl();
         } else {
            ss << "   lstore " << index << endl();
         }
         return ss.str();
      }

      static string storeReference(int index, StackDepth& stack) {
         std::stringstream ss;
         stack.pop();
         if (index >= 0 && index <= 3) {
            ss << "   astore_" << index << endl();
         } else {
            ss << "   astore " << index << endl();
         }
         return ss.str();
      }

      static string invokevirtual(string classname, string definition) {
         return "   invokevirtual " + classname + "/" + definition + endl();
      }

      static string invokevirtual(string fqcn) {
         return "   invokevirtual " + fqcn + endl();
      }

      static string invokestatic(string classname, string definition) {
         return "   invokestatic " + classname + "/" + definition + endl();
      }

      static string invokespecial(string classname, string definition) {
         return "   invokespecial " + classname + "/" + definition + endl();
      }

      static string invokeinterface(string classname, string definition, int variableSlot) {
         return "   invokeinterface " + classname + "/" + definition + " " + intToString(variableSlot) + endl();
      }

      static string returnVoid() {
         return "   return" + endl();
      }

      static string returnType(JologType typeName, StackDepth& stack) {
         string s;
         stack.pop();
         if (typeName == JologType::Int() || typeName == JologType::Boolean() ||
             typeName == JologType::Character() || typeName == JologType::Byte() ||
             typeName == JologType::Short()) {
            s = "   ireturn" + endl();
         } else if (typeName == JologType::Float()) {
            s = "   freturn" + endl();
         } else if (typeName == JologType::Double()) {
            stack.pop(); //doubles take up two slots
            s = "   dreturn" + endl();
         } else if (typeName == JologType::Long()) {
            stack.pop();
            s = "   lreturn" + endl();
         } else {
            s = "   areturn" + endl();
         }
         return s;
      }

      static string branchIfTrue(string target, bool invert, StackDepth& stack) {
         return branchIfZero(target, !invert, stack);
      }

      static string branchIfNull(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         if (invert) {
            s = "   ifnonnull" + target;
         } else {
            s = "   ifnull " + target;
         }
         return s + endl();
      }

      static string branchIfEqualReferences(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         stack.pop();
         if (invert) {
            s = "   if_acmpne " + target;
         } else {
            s = "   if_acmpeq " + target;
         }
         return s + endl();
      }

      static string branchIfEqualIntegers(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         stack.pop();
         if (invert) {
            s = "   if_icmpne " + target;
         } else {
            s = "   if_icmpeq " + target;
         }
         return s + endl();
      }

      static string branchIfGreaterThanIntegers(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         stack.pop();
         if (invert) {
            s = "   if_icmple " + target;
         } else {
            s = "   if_icmpgt " + target;
         }
         return s + endl();
      }

      static string branchIfLessThanIntegers(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         stack.pop();
         if (invert) {
            s = "   if_icmpge " + target;
         } else {
            s = "   if_icmplt " + target;
         }
         return s + endl();
      }

      static string branchIfZero(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         if (invert) {
            s = "   ifne " + target;
         } else {
            s = "   ifeq " + target;
         }
         return s + endl();
      }

      static string branchIfGreaterThanZero(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         if (invert) {
            s = "   ifle " + target;
         } else {
            s = "   ifgt " + target;
         }
         return s + endl();
      }

      static string branchIfLessThanZero(string target, bool invert, StackDepth& stack) {
         string s;
         stack.pop();
         if (invert) {
            s = "   ifge " + target;
         } else {
            s = "   iflt " + target;
         }
         return s + endl();
      }

      //cannot compare booleans or references
      static string branchOnRelationalFailure(string op, JologType type, string failureLabel, StackDepth& stack) {
         if (type == JologType::Int() || type == JologType::Byte() ||
             type == JologType::Short() || type == JologType::Character() ||
             (type == JologType::Boolean() && (op == "==" || op == "!="))) {
            return branchOnIntegerRelationalFailure(op, failureLabel, stack);
         } else if (type == JologType::Float()) {
            return branchOnFloatRelationalFailure(op, failureLabel, stack);
         } else if (type == JologType::Long()) {
            return branchOnLongRelationalFailure(op, failureLabel, stack);
         } else if (type == JologType::Double()) {
            return branchOnDoubleRelationalFailure(op, failureLabel, stack);
         } else if (!JologType::isPrimitiveType(type) && (op == "==" || op == "!=")) {
            return branchIfEqualReferences(failureLabel, op == "==", stack); //TODO CHECK INVERT!
         } else {
            throw JologCompileException("", "INTERNAL COMPILER ERROR: invalid type '"+type.prettyPrint()+"' for relational operator '"+op+"'");
         }
      }

      static string branchOnIntegerRelationalFailure(string op, string failureLabel, StackDepth& stack) {
         string operand;
         stack.pop();
         stack.pop();
         if (op == "<") { operand = "if_icmpge";
         } else if (op == "<=") { operand = "if_icmpgt";
         } else if (op == ">") { operand = "if_icmple";
         } else if (op == ">=") { operand = "if_icmplt";
         } else if (op == "==") { operand = "if_icmpne";
         } else if (op == "!=") { operand = "if_icmpeq";
         } else {
            throw JologCompileException("", "INTERNAL COMPILER ERROR: invalid relational operator (" + op + ")");
         }
         return ("   " + operand + " " + failureLabel + endl());
      }

      static string branchOnFloatRelationalFailure(string op, string failureLabel, StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   fcmpl" + endl() +
                loadIntegerLiteral(0, stack) +
                branchOnIntegerRelationalFailure("<", failureLabel, stack);
      }

      static string branchOnLongRelationalFailure(string op, string failureLabel, StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.pop();
         stack.pop();
         stack.push();
         return "   lcmpl" + endl() +
                loadIntegerLiteral(0, stack) +
                branchOnIntegerRelationalFailure("<", failureLabel, stack);
      }

      static string branchOnDoubleRelationalFailure(string op, string failureLabel, StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.pop();
         stack.pop();
         stack.push();
         return "   dcmpl" + endl() +
                loadIntegerLiteral(0, stack) +
                branchOnIntegerRelationalFailure("<", failureLabel, stack);
      }

      static bool isRelationalOperator(string op) {
         return (op == "<" || op == "<=" || op == ">" || op == ">=" || op == "==" || op == "!=");
      }

      static string integerArithmeticOperation(string op, StackDepth& stack) {
         string operand;
         stack.pop();
         stack.pop();
         stack.push();

         if (op == "+") { operand = "iadd";
         } else if (op == "-") { operand = "isub";
         } else if (op == "*") { operand = "imul";
         } else if (op == "/") { operand = "idiv";
         } else if (op == "%") { operand = "irem";
         } else {
            throw JologCompileException("", "INTERNAL COMPILER ERROR: invalid arithmetic operator (" + op + ")");
         }
         return "   " + operand + endl();
      }

      static string floatArithmeticOperation(string op, StackDepth& stack) {
         string operand;
         stack.pop();
         stack.pop();
         stack.push();

         if (op == "+") { operand = "fadd";
         } else if (op == "-") { operand = "fsub";
         } else if (op == "*") { operand = "fmul";
         } else if (op == "/") { operand = "fdiv";
         } else if (op == "%") { operand = "frem";
         } else {
            throw JologCompileException("", "INTERNAL COMPILER ERROR: invalid arithmetic operator (" + op + ")");
         }
         return "   " + operand + endl();
      }

      static string doubleArithmeticOperation(string op, StackDepth& stack) {
         string operand;
         stack.pop();
         stack.pop();
         stack.pop();
         stack.pop();
         stack.push();
         stack.push();

         if (op == "+") { operand = "dadd";
         } else if (op == "-") { operand = "dsub";
         } else if (op == "*") { operand = "dmul";
         } else if (op == "/") { operand = "ddiv";
         } else if (op == "%") { operand = "drem";
         } else {
            throw JologCompileException("", "INTERNAL COMPILER ERROR: invalid arithmetic operator (" + op + ")");
         }
         return "   " + operand + endl();
      }

      static string longArithmeticOperation(string op, StackDepth& stack) {
         string operand;
         stack.pop();
         stack.pop();
         stack.pop();
         stack.pop();
         stack.push();
         stack.push();

         if (op == "+") { operand = "ladd";
         } else if (op == "-") { operand = "lsub";
         } else if (op == "*") { operand = "lmul";
         } else if (op == "/") { operand = "ldiv";
         } else if (op == "%") { operand = "lrem";
         } else {
            throw JologCompileException("", "INTERNAL COMPILER ERROR: invalid arithmetic operator (" + op + ")");
         }
         return "   " + operand + endl();
      }

      static string convertIntToByte(StackDepth& stack) {
         stack.pop();
         stack.push();
         return "   i2b" + endl();
      }

      static string convertIntToChar(StackDepth& stack) {
         stack.pop();
         stack.push();
         return "   i2c" + endl();
      }

      static string convertIntToDouble(StackDepth& stack) {
         stack.pop();
         stack.push();
         stack.push();
         return "   i2d" + endl();
      }

      static string convertIntToFloat(StackDepth& stack) {
         stack.pop();
         stack.push();
         return "   i2f" + endl();
      }

      static string convertIntToLong(StackDepth& stack) {
         stack.pop();
         stack.push();
         stack.push();
         return "   i2l" + endl();
      }

      static string convertIntToShort(StackDepth& stack) {
         stack.pop();
         stack.push();
         return "   i2s" + endl();
      }

      static string convertFloatToInt(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   f2i" + endl();
      }

      static string convertFloatToDouble(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   f2d" + endl();
      }

      static string convertFloatToLong(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         stack.push();
         return "   f2l" + endl();
      }

      static string convertDoubleToInt(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   d2i" + endl();
      }

      static string convertDoubleToFloat(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   d2f" + endl();
      }

      static string convertDoubleToLong(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         stack.push();
         return "   d2l" + endl();
      }

      static string convertLongToInt(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   l2i" + endl();
      }

      static string convertLongToDouble(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         stack.push();
         return "   l2d" + endl();
      }

      static string convertLongToFloat(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   l2f" + endl();
      }

      //cannot add booleans or references
      static string add(JologType type, string location, StackDepth& stack) {
         if (type == JologType::Int() || type == JologType::Short() ||
             type == JologType::Boolean() || type == JologType::Character()) {
            return integerArithmeticOperation("+", stack);
         } else if (type == JologType::Float()) {
            return floatArithmeticOperation("+", stack);
         } else if (type == JologType::Double()) {
            return doubleArithmeticOperation("+", stack);
         } else if (type == JologType::Long()) {
            return longArithmeticOperation("+", stack);
         } else {
            throw JologCompileException(location, "addition not defined on \""+type.getClassName()+"\"");
         }
      }

      //cannot sub booleans or references
      static string sub(JologType type, string location, StackDepth& stack) {
         if (type == JologType::Int() || type == JologType::Short() ||
             type == JologType::Boolean() || type == JologType::Character()) {
            return integerArithmeticOperation("-", stack);
         } else if (type == JologType::Float()) {
            return floatArithmeticOperation("-", stack);
         } else if (type == JologType::Double()) {
            return doubleArithmeticOperation("-", stack);
         } else if (type == JologType::Long()) {
            return longArithmeticOperation("-", stack);
         } else {
            throw JologCompileException(location, "subtraction not defined on \""+type.getClassName()+"\"");
         }
      }

      //can't negate a character, boolean or reference
      static string negate(JologType type, string location, StackDepth& stack) {
         if (type == JologType::Int() || type == JologType::Byte() ||
             type == JologType::Short()) {
            stack.pop();
            stack.push();
            return "   ineg" + endl();
         } else if (type == JologType::Float()) {
            stack.pop();
            stack.push();
            return "   fneg" + endl();
         } else if (type == JologType::Double()) {
            stack.pop();
            stack.pop();
            stack.push();
            stack.push();
            return "   dneg" + endl();
         } else if (type == JologType::Long()) {
            stack.pop();
            stack.pop();
            stack.push();
            stack.push();
            return "   lneg" + endl();
         } else {
            throw JologCompileException(location, "negation only defined on numeric types (got \""+type.getClassName()+"\")");
         }
      }

      /*
       * this is only used INTERNALLY and only on integers
       * so this is all we need ...
       * note to those who follow: fix this to do other types if you care!
       */
      static string exclusiveOr(StackDepth& stack) {
         stack.pop();
         stack.pop();
         stack.push();
         return "   ixor" + endl();
      }

      static string duplicate(StackDepth& stack) {
         stack.pop(); //grab something
         stack.push(); //put it back
         stack.push(); //clone!
         return "   dup" + endl();
      }

      static string pop(StackDepth& stack) {
         stack.pop();
         return "   pop" + endl();
      }

      static string getField(string fieldName, const JologType fieldType, StackDepth& stack) {
         stack.pop(); //class
         stack.push(); //field
         if (fieldType == JologType::Long() || fieldType == JologType::Double()) {
            stack.push();
         }
         return "   getfield " + fieldName + " " + fieldType.getTypeName() + endl();
      }

      static string getStaticField(string fieldName, const JologType fieldType, StackDepth& stack) {
         stack.push(); //field
         if (fieldType == JologType::Long() || fieldType == JologType::Double()) {
            stack.push();
         }
         return "   getstatic " + fieldName + " " + fieldType.getTypeName() + endl();
      }

      static string putField(string fieldName, const JologType fieldType, StackDepth& stack) {
         stack.pop(); //class
         stack.pop(); //field
         if (fieldType == JologType::Long() || fieldType == JologType::Double()) {
            stack.pop();
         }
         return "   putfield " + fieldName + " " + fieldType.getTypeName() + endl();
      }

      static string putStaticField(string fieldName, const JologType fieldType, StackDepth& stack) {
         stack.pop(); //field
         if (fieldType == JologType::Long() || fieldType == JologType::Double()) {
            stack.pop();
         }
         return "   putstatic " + fieldName + " " + fieldType.getTypeName() + endl();
      }

      static string gotoLabel(string target) {
         return "   goto " + target + endl(); //TODO: goto_w? jsr = jump to subroutine + jsr_w; only used for try-catch-finally?
      }

      static string label(string name) {
         return name + ":" + endl();
      }

      static string throws(string name) {
         return ".throws " + name + endl();
      }

      static string nop() {
         return "   nop" + endl();
      }

      static string getArrayLength(StackDepth& stack) {
         stack.pop(); //array
         stack.push(); //size
         return "   arraylength" + endl();
      }

      static string swap(StackDepth& stack) {
         stack.pop(); //grab the top two items
         stack.pop();
         stack.push(); //and put them back in the reverse order
         stack.push();
         return "   swap" + endl();
      }

      static string newArray(const JologType type, StackDepth& stack) {
         string s;
         stack.pop(); //size
         stack.push(); //new array

         //for some bizarre reason, java seems to want arrays of primitive
         //types to have non-standard type names ...
         if (type == JologType::Int()) {
            s = "   newarray int" + endl();
         } else if (type == JologType::Double()) {
            s = "   newarray double" + endl();
         } else if (type == JologType::Character()) {
            s = "   newarray char" + endl();
         } else if (type == JologType::Boolean()) {
            s = "   newarray boolean" + endl();
         } else if (type == JologType::Byte()) {
            s = "   newarray byte" + endl();
         } else if (type == JologType::Long()) {
            s = "   newarray long" + endl();
         } else if (type == JologType::Float()) {
            s = "   newarray float" + endl();
         } else if (type == JologType::Short()) {
            s = "   newarray short" + endl();
         } else {
            s = "   anewarray " + type.getClassName() + endl();
         }
         return s;
      }

      static string newObject(const JologType type, StackDepth& stack) {
         stack.push();
         return "   new " + type.getClassName() + endl();
      }

      static string typecast(const JologType type, StackDepth& stack) {
         stack.pop(); //load object
         stack.push(); //put it back
         return "   checkcast " + type.getClassName() + endl();
      }

      static string throwFailedPreconditionException(string errorMessage, StackDepth& stack) {
         return throwJologException(JologType::FailedPreconditionException(), errorMessage, stack);
      }

      static string throwProgramFailureException(string errorMessage, StackDepth& stack) {
         return throwJologException(JologType::ProgramFailureException(), errorMessage, stack);
      }

      static string throwJologException(JologType type, string errorMessage, StackDepth& stack) {
         int initial = stack.final();
         string s = opcodes::newObject(type, stack) +
                    opcodes::duplicate(stack) +
                    opcodes::loadStringLiteral(errorMessage, stack);

         stack.pop(); //arg
         stack.pop(); //parent
         s += opcodes::invokespecial(type.getClassName(),
                                     "<init>("+JologType::String().getTypeName()+")"+JologType::Void().getTypeName());
         s += opcodes::throwReference(stack);

         if (initial != stack.final()) { throw JologCompileException("INTERNAL COMPILER ERROR", "throwing "+type.getClassName()+" should not affect final stack depth"); }
         return s;
      }

      static string catchException(string exceptionType, string start, string end, string handler) {
         return ".catch " + exceptionType + " from " + start + " to " + end + " using " + handler + endl();
      }

      static string loadNull(StackDepth& stack) {
         stack.push();
         return "   aconst_null" + endl();
      }

      static string replaceInString(string s, string find, string replace) {
         string escaped;
         size_t i = s.find(find);
         while (i != string::npos) {
            if (i == 0) {
               escaped += replace;
               s = s.substr(1);
            } else if (i > 0) {
               escaped += s.substr(0, i) + replace;
               s = s.substr(i+1);
            }
            i = s.find(find);
         }
         escaped += s; //finally, append whatever is left
         return escaped;
      }

      static string loadEscapedStringLiteral(string escaped, StackDepth& stack) {
         stack.push();
         return "   ldc \"" + escaped + "\"" + endl();
      }

      static string loadStringLiteral(string s, StackDepth& stack) {
         string escaped = replaceInString(replaceInString(s, "\\", "\\\\"), "\"", "\\\"");
         return loadEscapedStringLiteral(escaped, stack);
      }

      static string loadCharacterLiteral(char c, StackDepth& stack) {
         stack.push();
         return "   ldc '" + c + string("'") + endl();
      }

      static string loadIntegerLiteral(int i, StackDepth& stack) {
         stack.push();
         string s;
         if (i == -1) {
            s = "   iconst_m1";
         } else if (i >= 0 && i <= 5) {
            s = "   iconst_" + intToString(i);
         } else if (i >= -128 && i <= 127) {
            s = "   bipush " + intToString(i);
         } else {
            s = "   sipush " + intToString(i);
         }
         return s + endl();
      }

      static string loadFloatLiteral(float f, StackDepth& stack) {
         stack.push();
         std::stringstream ss;
         if (f == 0) {
            ss << "   fconst_0" << endl();
         } else if (f == 1) {
            ss << "   fconst_1" << endl();
         } else if (f == 2) {
            ss << "   fconst_2" << endl();
         } else {
            ss << "   ldc " << f << endl();
         }
         return ss.str();
      }

      static string loadLongLiteral(long l, StackDepth& stack) {
         stack.push();
         stack.push();
         std::stringstream ss;
         if (l == 0) {
            ss << "   lconst_0" << endl();
         } else if (l == 1) {
            ss << "   lconst_1" << endl();
         } else {
            ss << "   ldc2_w " << l << endl();
         }
         return ss.str();
      }

      static string loadDoubleLiteral(double d, StackDepth& stack) {
         stack.push();
         stack.push();
         std::stringstream ss;
         if (d == 0) {
            ss << "   dconst_0" << endl();
         } else if (d == 1) {
            ss << "   dconst_1" << endl();
         } else {
            ss << "   ldc2_w " << d << endl();
         }
         return ss.str();
      }

      static string loadBooleanLiteral(bool b, StackDepth& stack) {
         return loadIntegerLiteral(b ? 1 : 0, stack);
      }

      static string throwReference(StackDepth& stack) {
         stack.pop();
         return "   athrow" + endl();
      }

      static string intToString(int i) {
         std::stringstream ss;
         ss << i;
         return ss.str();
      }

      static string endl() {
         return "\n";
      }
};

#endif
