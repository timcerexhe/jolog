//header guards so C++ does what we want!
#ifndef COMPONENT_COMPILER_TEM
#define COMPONENT_COMPILER_TEM

#include "ComponentCompiler.h"

template <typename iter_t>
void ComponentCompiler<iter_t>::showTree(int depth, const iter_t& begin, const iter_t& end) {
   std::string indent(std::max(1, 1+depth*3), ' ');
   for (iter_t i = begin; i != end; ++i) {
      std::cout << indent;
      std::cout << "ID: " << iteratorToName(i) << ", ";

      string value(getValue(i));
      std::cout << "Value: <" << ErrorMessage::truncate(value) << ">, ";
      std::cout << "Children (" << i->children.size() << ")" << std::endl;
      showTree(depth+1, i->children.begin(), i->children.end());
   }
}

template <typename iter_t>
Agent* ComponentCompiler<iter_t>::compileFile(const iter_t& begin, const iter_t& end) {
   Agent* agent = 0;
   iter_t i = begin;
   try {
      while (i != end) {
         if (checkTag(i, file_tag, end)) {
            ensure(agent == 0, i, "only one agent definition is allowed per file");
            agent = compileFile(i->children.begin(), i->children.end());
         } else if (checkTag(i, agent_tag, end)) {
            ensure(agent == 0, i, "only one agent definition is allowed per file");
            agent = compileAgent(i->children.begin(), i->children.end());
         } else if (checkTag(i, import_tag, end)) {
            compileImport(i);
         } else if (checkTag(i, package_tag, end)) {
            compilePackage(i);
         } else {
            ensure(false, i, "unknown '"+iteratorToName(i)+"' token at file scope");
         }
         ++i;
      }
   } catch (JologCompileException e) {
      delete agent; //clean up after ourselves
      throw e;
   }
   return agent;
}

template <typename iter_t>
Agent* ComponentCompiler<iter_t>::compileAgent(const iter_t& begin, const iter_t& end) {
   vector<JologType*> implements;
   bool abstractModifier = false;
   bool finalModifier = false;

   iter_t i = begin;
   ensure(i != end, i, "premature end of agent definition");
   while (checkTag(i, prefix_tag, end)) {
      string bit(getValue(i));
      if (bit == "abstract") {
         if (abstractModifier) { warn(i, "agent already abstract"); }
         abstractModifier = true;
      } else if (bit == "final") {
         if (finalModifier) { warn(i, "agent already final"); }
         finalModifier = true;
      } else {
         warn(i, "ignoring unknown agent prefix \""+bit+"\"");
      }
      ++i;
   }

   assertTag(i, file_tag, end); //ugh
   ensure(getValue(i) == "agent", i, "agent is not a \""+getValue(i)+"\"");
   string sourceFile = i->value.begin().get_position().file; //we ensure that i exists first, so get_position() should always work
   ++i;

   assertTag(i, type_tag, end);
   JologType* className = compileType(i, packageName, true); //add classname onto package (if provided)
   packageName = "DEFINED"; //class name is set, so package cannot change; attempts to redefine it will throw errors
   ++i;

   JologType* super = 0;

   int inheritanceCount = 0;
   while (i != end && (i->value.id() == implements_tag || i->value.id() == extends_tag)) {
      if (i->value.id() == implements_tag) {
         ensure(i->children.size() == 1, i, "no class to implement");
         implements.push_back(compileType(i));
      } else {
         ensure(i->children.size() == 1, i, "no class to extend");
         ++inheritanceCount;
         super = compileType(i);
         ensure(inheritanceCount <= 1, i, "multiple inheritance is not permitted");
      }
      ++i;
   }

   if (super == 0) { //still no super? default to Object
      super = new JologType(resolver->lookupType("Object", location(begin)));
   }

   //create the agent and add all its interfaces
   Agent* agent = new Agent(sourceFile, className, super, abstractModifier, finalModifier, resolver, location(begin));
   for (vector<JologType*>::const_iterator it = implements.begin(); it != implements.end(); ++it) {
      agent->addInterface(*it);
   }

   bool failure = false;
   string failurePoint;
   map<Function*, pair<iter_t, iter_t> > definitions;
   while (i != end) {
      try {
         if (checkTag(i, function_definition_tag, end)) {
            agent->addFunction(compileFunction(definitions, i->children.begin(), i->children.end()));
         } else if (checkTag(i, action_definition_tag, end)) {
            agent->addAction(compileAction(agent, definitions, i->children.begin(), i->children.end()));
         } else if (checkTag(i, fluent_definition_tag, end)) {
            allowFluentAssignment = true;
            agent->addFluent(compileFluent(i->children.begin(), i->children.end()));
            allowFluentAssignment = false;
         } else {
            ensure(false, i, "unknown '" + iteratorToName(i) + "' token at agent scope");
         }
      } catch (JologCompileException e) {
         std::cout << "AGENT EXCEPTION: " << e.where() << ": " << e.what() << std::endl;
         failure = true;
         failurePoint = e.where();
      }
      ++i;
   }

   if (failure) { //propagate compile errors to top level
      throw JologCompileException(failurePoint, "error compiling agent \""+className->getClassName()+"\"");
   }

   compileBodies(definitions);

   for (vector<SomeAll*>::const_iterator it = someAlls.begin(); it != someAlls.end(); ++it) {
      agent->addSomeAll(*it);
   }

   return agent;
}

template <typename iter_t>
void ComponentCompiler<iter_t>::compileBodies(map<Function*, pair<iter_t, iter_t> >& agentComponents) {
   typename map<Function*, pair<iter_t, iter_t> >::const_iterator i = agentComponents.begin();
   while (i != agentComponents.end()) {
      Function* function = (*i).first;

      //save old scope and replace with our own version
      NamespaceResolver* saveResolver = resolver;
      resolver = function->getResolver();

      returnCount = 0;
      if (dynamic_cast<Action*>(function) != 0) { //its an action!
         allowNondeterminism = false;
         Action* action = (Action*) function;
         function->addBody(compileActionBody(action->getName(), action->getActionType(), (*i).second.first, (*i).second.second));
      } else { //compile function scope
         allowNondeterminism = !function->isStatic() && function->isPrivate();
         function->addBody(compileScope((*i).second.first, (*i).second.second));
      }

      //restore to previous scope
      resolver = saveResolver;

      ++i; //get the next function
   }
}

template <typename iter_t>
Scope* ComponentCompiler<iter_t>::compileActionBody(string name, Action::ActionType actionType, const iter_t begin, const iter_t end) {
   iter_t i = begin;
   allowFluentAssignment = true;
   allowNondeterminism = false;

   endScopeLabel = JologNode::newLabel();
   Scope* actionBody = new Scope(location(begin), endScopeLabel, resolver);
   NamespaceResolver* saveResolver = resolver;
   resolver = actionBody->getResolver();

   //if we have an exogenous action then we need to match an incoming message
   FunctionCall* match = 0;
   Variable* matcher = 0;
   if (actionType == Action::EXOGENOUS_ACTION) {
      Fluent* pattern = new Fluent(name+"_pattern", resolver, location(begin), false);
      FunctionCall* patternMatch = new FunctionCall(pattern, "matcher", resolver, location(begin), endScopeLabel, false);

      Identifier* message = new Identifier(0, "message", resolver, location(begin));
      FunctionCall* messageContent = new FunctionCall(message, "getContent", resolver, location(begin), endScopeLabel, false);
      patternMatch->addArgument(new JologCast(JologType("java/lang/CharSequence"), messageContent, location(begin)));

      //store the matcher result for user convenience
      matcher = new Variable(location(begin), new JologType(JologType::Matcher()), "matcher",
                             patternMatch,
                             endScopeLabel, resolver, true); //TODO should Matcher be non-transient? is it serializable?

      //use the matcher local when checking preconditions
      match = new FunctionCall(new Identifier(0, "matcher", resolver, location(begin)),
                               "matches", resolver, location(begin), endScopeLabel, false);
      actionBody->addStatement(matcher); //give easy access to regex matcher results
   }

   Scope* sets = new Scope(location(begin), JologNode::newLabel(), resolver);

   JologNode* precondition = 0;
   JologNode* returnExpr = 0;

   while (i != end) {
      string bit(getValue(i));
      ensure((bit == "possible") || (bit == "set") || (bit == "send") || (bit == "return"), i,
             "action body can only have \"possible\", \"set\", \"send\" or \"return\" statements, found \""+bit+"\"");
      ++i;
      ensure(i != end, i, "premature end of action body (unfinished " + bit + ")");

      if (bit == "possible") {
         ensure(actionType != Action::EXOGENOUS_ACTION, i-1, "exogenous actions cannot have preconditions");
         JologNode* condition = compileExpression(i);
         if (precondition == 0) {
            precondition = condition;
         } else {
            precondition = new Or(precondition, condition, location(i));
         }
         ++i;

      } else if (bit == "set") {
         if (!checkTag(i, assignment_operator_tag, end)) {
            assertTag(i, expression_tag, end); //expression tag = jolog io!?
         }
         JologNode* setExpression = compileExpression(i);
         ensure(setExpression->getType() == JologType::Void(), i, "action sets must be a void-statement");
         ++i;
         if ((i != end) && (getValue(i) == "if")) {
            ++i;
            std::cout << "CONDITIONAL SUCCESSOR STATE AXIOM" << std::endl;
            ensure(i != end, i-1, "premature end of action 'sets' precondition");
            If* conditionalSet = new If(location(i));
            conditionalSet->addCondition(compileExpression(i));
            conditionalSet->addThen(setExpression);
            setExpression = conditionalSet;
            ++i;
         }
         sets->addStatement(setExpression);

      } else if (bit == "send") {
         ensure(actionType == Action::SENSING_ACTION, i-1, "only sensing actions can have \"send\" statments");
         assertTag(i, expression_tag, end); //expression tag = jolog io!?
         JologNode* send = compileExpression(i);
         //ensure(dynamic_cast<JologIO*>(send) != 0, i, "\"send\" statements must use the << operator");
         sets->addStatement(send);
         i++;

      } else if (bit == "return") {
         ensure(returnExpr == 0, i, "actions may not have multiple returns");
         returnExpr = compileExpression(i);
         if (returnExpr->getType() != JologType::Message()) {
            returnExpr = new JologCast(JologType::Message(), returnExpr, location(i));
         }
         returnExpr = new Return(returnExpr, 0); //actions cannot backtrack so don't save transients
         ++i;

      } else if (checkTag(i, local_tag, end)) {
         sets->addStatement(compileVariable(i->children.begin(), i->children.end()));
         ++i;
      } else {
         ensure(false, i, "unknown statement \""+bit+"\" in action");
      }

   }

   if (precondition != 0 && match != 0) {
      precondition = new And(match, precondition, location(begin));
   } else if (precondition == 0 && match == 0) { //default precondition is *true*
      precondition = new BooleanLiteral(true);
   } else if (precondition == 0 && match != 0) {
      precondition = match;
   } //else we have a precondition and no match, so leave things as they are

   //prim_actions don't return anything (just void) ...
   if (actionType != Action::PRIMITIVE_ACTION) {
      if (returnExpr == 0) { //default return is *null*
         returnExpr = new Return(new Identifier(0, "null", resolver, location(begin)), 0); //don't save transients
      }
      //sets->addStatement(new Cut(location(begin), endScopeLabel, resolver));
      sets->addStatement(returnExpr);
   }

   If* ifStatement = new If(location(begin));
   ifStatement->addCondition(precondition);
   ifStatement->addThen(sets);

   if (actionType != Action::PRIMITIVE_ACTION) { //primitive actions allow individual failures, provided at least one subaction matches
      ifStatement->addElse(new FailedPreconditionException("precondition unsatisfied in action \""+name+"\""));
   }

   actionBody->addStatement(ifStatement);
   //actionBody->addStatement(new Cut(location(begin), endScopeLabel, resolver));

   //if (actionType == Action::PRIMITIVE_ACTION) {
   actionBody->addStatement(new Return(0, 0)); //do not save transients
   //}

   resolver = saveResolver; //restore old resolver
   allowFluentAssignment = false;
   return actionBody;
}


template <typename iter_t>
Action* ComponentCompiler<iter_t>::compileAction(Agent* parent,
                                                 map<Function*, pair<iter_t, iter_t> >& agentComponents,
                                                 const iter_t& begin, const iter_t& end) {
   bool finalModifier = false;
   bool abstractModifier = false;

   iter_t i = begin;
   ensure(i != end, i, "premature end of action definition");
   string bit(getValue(i));
   while (i != end && (bit == "static" || bit == "final" || bit == "abstract")) {
      ensure(bit != "static", i, "static actions not allowed");
      if (bit == "final") {
         if (finalModifier) { warn(i, "action already final"); }
         finalModifier = true;
      } else if (bit == "abstract") {
         if (abstractModifier) { warn(i, "action already abstract"); }
         abstractModifier = true;
      } else {
         ensure(false, i, "unknown action prefix \""+bit+"\"");
      }
      ++i;
      bit = getValue(i);
   }

   assertTag(i, file_tag, end);
   string actionType(getValue(i));
   Action::ActionType at = Action::PRIMITIVE_ACTION;
   if (actionType == "exog_action") {
      at = Action::EXOGENOUS_ACTION;
   } else if (actionType == "sensing_action") {
      at = Action::SENSING_ACTION;
   }
  ++i;


   string actionName;
   Action* action = 0;
   if (at == Action::PRIMITIVE_ACTION || at == Action::SENSING_ACTION) {
      assertTag(i, identifier_tag, end);
      actionName = getValue(i);
      ++i;

      //ok to give resolver to action -> function -> doesn't own a resolver
      action = new Action(at, abstractModifier, finalModifier, actionName, resolver, location(begin));
      while (checkTag(i, parameter_definition_tag, end)) {
         action->addArgument(compileParameter(i->children.begin(), i->children.end(), action->getResolver()));
         ++i;
      }

      //primitive actions are just functions, so lets register with the authorities!
      action->importPrototype(resolver);

   } else if (at == Action::EXOGENOUS_ACTION) {
      assertTag(i, string_tag, end);
      string regex(getValue(i));
      ++i;

      std::stringstream ss;
      ss << "jolog_action_" << actionId;
      ++actionId;
      ss >> actionName;

      //create fluent: Pattern jolog_action_id_PATTERN = Pattern.compile(regex);
      FunctionCall* init = new FunctionCall(new Identifier(0, JologType::Pattern().getClassName(), resolver, location(begin)),
                                            "compile", resolver, location(begin), endScopeLabel, false);
      init->addArgument(new StringLiteral(regex));
      parent->addFluent(new Fluent(new JologType(JologType::Pattern()), actionName+"_pattern", init, resolver, location(begin), true));

      //ok to give resolver to action -> function -> doesn't own a resolver
      action = new Action(at, abstractModifier, finalModifier, actionName, resolver, location(begin));

      //resolver->addLocal("message", JologType::Message(), location(i-1));
      action->addArgument(new Variable(location(begin), new JologType(JologType::Message()), "message", 0, endScopeLabel, action->getResolver(), false));
   }

   ensure(action != 0, begin, "could not compile action (type = " + actionType + ")");
   agentComponents[action] = make_pair(i, end);

   return action;
}

template <typename iter_t>
Function* ComponentCompiler<iter_t>::compileFunction(map<Function*, pair<iter_t, iter_t> >& agentComponents,
                                                     const iter_t& begin, const iter_t& end) {
   bool staticModifier = false;
   bool finalModifier = false;
   bool abstractModifier = false;

   iter_t i = begin;

   string privacy = "private";
   if (checkTag(i, privacy_tag, end)) {
      privacy = getValue(i);
      ++i;
   }

   ensure(i != end, i, "premature end of function definition");
   string bit(getValue(i));
   while (checkTag(i, prefix_tag, end)) { // && (bit == "static" || bit == "final" || bit == "abstract")) {
      bit = getValue(i);
      if (bit == "static") {
         if (staticModifier) { warn(i, "function already static"); }
         staticModifier = true;
      } else if (bit == "final") {
         if (finalModifier) { warn(i, "function already final"); }
         finalModifier = true;
      } else if (bit == "abstract") {
         if (abstractModifier) { warn(i, "function already abstract"); }
         abstractModifier = true;
      } else {
         ensure(false, i, "unknown function prefix \""+bit+"\"");
      }
      ++i;
   }

   assertTag(i, type_tag, end);
   JologType* returnType = compileType(i);
   ++i;

   assertTag(i, identifier_tag, end);
   string name(getValue(i));
   ++i;

   Function* function = new Function(returnType, privacy, staticModifier, abstractModifier, finalModifier, name, resolver, location(i-1));
   while (checkTag(i, parameter_definition_tag, end)) {
      function->addArgument(compileParameter(i->children.begin(), i->children.end(), function->getResolver()));
      ++i;
   }
   function->importPrototype(resolver);
   agentComponents[function] = make_pair(i, end);

   return function;
}

template <typename iter_t>
Variable* ComponentCompiler<iter_t>::compileParameter(const iter_t& begin, const iter_t& end, NamespaceResolver* resolver) {
   iter_t i = begin;
   bool transient = false;
   while (checkTag(i, file_tag, end)) {
      string prefix(getValue(i));
      ensure(prefix == "transient", i, "unknown parameter prefix \""+prefix+"\"");
      transient = true;
   }

   assertTag(i, type_tag, end);
   JologType* type = compileType(i);
   ++i;

   assertTag(i, identifier_tag, end);
   string name = getValue(i);
   ++i;

   ensure(i == end, i, "trailing tokens in parameter");
   Variable* v = new Variable(location(i-1), type, name, 0, endScopeLabel, resolver, transient);
   //std::cout << "FINISHED COMPILING PARAMETER (" << name << " : " << type->getTypeName() << ")" << std::endl;
   return v;
}

template <typename iter_t>
Fluent* ComponentCompiler<iter_t>::compileFluent(const iter_t& begin, const iter_t& end) {
   //bool staticModifier = false;
   bool finalModifier = false;

   iter_t i = begin;
   /*string privacy;
   if (checkTag(i, privacy_tag, end)) {
      privacy = getValue(i);
      ++i;
   }*/

   ensure(i != end, i, "premature end of fluent definition");
   string bit(getValue(i));
   while (i != end && (bit == "static" || bit == "final")) {
      throw JologCompileException(location(i), "fluents cannot be prefixed");
      if (bit == "static") {
         //if (staticModifier) { warn(i, "variable already static"); }
         //staticModifier = true;
         throw JologCompileException(location(i), "static fluents not allowed");
      } else if (bit == "final") {
         if (finalModifier) { warn(i, "variable already final"); }
         finalModifier = true;
      }
      ++i;
      bit = getValue(i);
   }

   assertTag(i, type_tag, end);
   JologType* type = compileType(i);
   ++i;

   assertTag(i, identifier_tag, end);
   string name(getValue(i));
   ++i;

   //std::cout << "COMPILING FLUENT " << name << std::endl;
   JologNode* init = 0;
   vector<JologNode*> args;
   while ((i != end) && (!checkTag(i, file_tag, end))) {
      args.push_back(compileExpression(i));
      //std::cout << "FLUENT '" << name << "' gets argument at " << args.back() << std::endl;
      //std::cout << "TYPE = " << args.back()->getType().getClassName() << std::endl;
      ++i;
   }
   if (i != end) {
      ensure(getValue(i) == "=", i, "expected '=' before initialisation");
      init = compileInitialiser(i+1, end);
   }

   Fluent* f = new Fluent(type, name, init, resolver, location(begin), allowFluentAssignment);
   for (vector<JologNode*>::const_iterator it = args.begin(); it != args.end(); ++it) {
      f->addArgument(*it);
   }
   //std::cout << "FINISHED COMPILING FLUENT" << std::endl;
   return f;
}

template <typename iter_t>
Variable* ComponentCompiler<iter_t>::compileVariable(const iter_t& begin, const iter_t& end) {
   iter_t i = begin;
   bool transient = false;
   while (checkTag(i, file_tag, end)) {
      string prefix(getValue(i));
      ensure(prefix == "transient", i, "unknown variable prefix \""+prefix+"\"");
      transient = true;
      ++i;
   }

   assertTag(i, type_tag, end);
   JologType* type = compileType(i);
   ++i;

   assertTag(i, identifier_tag, end);
   string name(getValue(i));
   ++i;

   JologNode* init = 0;
   if (i != end) {
      assertTag(i, file_tag, end);
      ensure(getValue(i) == "=", i, "expected '=' before initialisation");
      init = compileInitialiser(i+1, end);
   }

   return new Variable(location(begin), type, name, init, endScopeLabel, resolver, transient);
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileInitialiser(const iter_t& begin, const iter_t& end) {
   JologNode* init = 0;
   iter_t i = begin;
   while (i != end) {
      if (i->value.id() == initialiser_tag) {
         ensure(false, i, "initialiser lists not yet supported"); //TODO
//         std::cout << " {";
//         init = compileInitialiser(i->children.begin(), i->children.end());
//         std::cout << " }";
      } else {
         init = compileExpression(i);
      }
      ++i;
   }
   return init;
}

template <typename iter_t>
JologType* ComponentCompiler<iter_t>::compileType(const iter_t& expr, string package, bool create) {
   string s(package);
   size_t arrayDimensions = 0;

   iter_t i;
   iter_t end;

   if (expr->children.size() > 0) {
      i = expr->children.begin();
      end = expr->children.end();
   } else {
      i = expr;
      end = expr + 1;
   }

   string name = getValue(i);

   string fqcn = package;
   if (package.size() > 0) {
      fqcn += "/";
   }
   fqcn += name;

   if (create) {
      resolver->addType(fqcn, fqcn); //do we ever want addType(name, fqcn)?
   }

   s = resolver->lookupType(fqcn, location(i)).getClassName();
   ++i;

   while (checkTag(i, file_tag, end)) { //arrays!
      string suffix(getValue(i));
      ensure(suffix == "[]", i, "invalid type suffix \""+suffix+"\"");
      ++arrayDimensions;
      ++i;
   }
   ensure(i == end, i, "trailing data in type declaration");

   return new JologType(s, arrayDimensions);
}

template <typename iter_t>
void ComponentCompiler<iter_t>::compileImport(const iter_t& expr) {

   string fqcn, name;
   iter_t i = expr->children.begin();
   while (i != expr->children.end()) {
      name = getValue(i);
      fqcn += name;
      if (i != expr->children.end() - 1) {
         fqcn += "/";
      }
      ++i;
   }
   ensure(expr->value.id() == import_tag, expr, "expected import, but found \"" + name + "\"");

   string command("\""+getSignatureScript+"\" "+fqcn);
   std::cout << "GET SCRIPT: " << command << std::endl;
   FILE* signatures = popen(command.c_str(), "r");
   ensure(signatures != 0, expr, "error loading class file \""+fqcn+"\"");

   int maxLineSize = 256;
   char line[maxLineSize];
   while (fgets(line, maxLineSize, signatures) != 0) {
      string s(line);
      if (s.size() > 0) {
         s = s.substr(0, s.size() - 1);
         std::cout << "importing \"" << s << "\"" << std::endl;
      }
   }

   resolver->addType(name, fqcn);
}

template <typename iter_t>
void ComponentCompiler<iter_t>::compilePackage(const iter_t& expr) {

   ensure(packageName.size() == 0, expr, "cannot redefine package from \"" + packageName + "\"");

   iter_t i = expr->children.begin();
   while (i != expr->children.end()) {
      packageName += getValue(i);

      if (i != expr->children.end() - 1) {
         packageName += "/";
      }

      ++i;
   }

   iter_t end;
   assertTag(expr, package_tag, end);


   /*string fqcn, name;
	iter_t i = expr->children.begin();
	while (i != expr->children.end()) {
		name = getValue(i);
		fqcn += name;
		if (i != expr->children.end() - 1) {
			fqcn += "/";
		}
		++i;
	}
	ensure(expr->value.id() == package_tag, expr, "expected package, but found \"" + name + "\"");
	resolver.add("this", fqcn);*/
}

template <typename iter_t>
Scope* ComponentCompiler<iter_t>::compileScope(const iter_t& begin, const iter_t& end, string endLabel) {
   string oldLabel = endScopeLabel;
   endScopeLabel = (endLabel.size() > 0) ? endLabel : JologNode::newLabel();
   string loc = "unknown";
   if (begin != end) {
      loc = location(begin);
   }

   Scope* s;
   if (endLabel.size() > 0) { //we were given a label (by ndet) so someone else will emit it for us
      s = new Scope(loc, resolver);
   } else { //otherwise we have to do everything around here!
      s = new Scope(loc, endScopeLabel, resolver);
   }
   NamespaceResolver* saveResolver = resolver;
   resolver = s->getResolver();
   iter_t i = begin;
   while (i != end) {
      s->addStatement(compileStatement(i));
      ++i;
   }

   endScopeLabel = oldLabel; //restore old label for old scope
   resolver = saveResolver; //restore old resolver
   return s;
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileIf(const iter_t& begin, const iter_t& end) {

   ensure(begin != end && begin+1 != end, begin, "if statement requires at least a condition and a body");

   if (!allowNondeterminism) {
      iter_t i = begin;
      If* statement = new If(location(i));
      statement->addCondition(compileExpression(i));
      ++i;
      statement->addThen(compileExpression(i));
      ++i;
      if (i != end) {
         statement->addElse(compileExpression(i));
         ++i;
      }
      ensure(i == end, i, "trailing data in if statement");
      return statement;
   } else {
      Ndet* ndetIf = new Ndet(location(begin), endScopeLabel, resolver);

      string endThenBody = JologType::newLabel();
      Scope* thenBody = new Scope(location(begin), resolver);
      thenBody->addStatement(new Holds(compileExpression(begin), location(begin))); //if condition
      thenBody->addStatement(compileScope(begin+1, begin+2, endThenBody));
      ndetIf->addProgram(thenBody, endThenBody);

      string endElseBody = JologType::newLabel();
      Scope* elseBody = new Scope(location(begin), resolver);
      elseBody->addStatement(new Holds(new Unary("!", compileExpression(begin)), location(begin))); //else (not) condition
      if (begin+2 != end) {
         elseBody->addStatement(compileScope(begin+2, end, endElseBody));
      }
      ndetIf->addProgram(elseBody, endElseBody);
      return ndetIf;
   }
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileFor(const iter_t& expr) {
   JologNode* initialiser = 0;
   JologNode* condition = 0;
   JologNode* postCondition = 0;
   JologNode* update = 0;
   JologNode* body = 0;
   string conditionLocation;

   ensure(expr->children.size() >= 3, expr, "incomplete for loop");
   iter_t i = expr->children.begin();

   //initialiser
   if (getValue(i) != ";") {
      initialiser = compileExpression(i);
      ++i;
      string bit(getValue(i));
      ensure(bit == ";", i, "expected ';' after for loop initialiser but found \""+bit +"\"");
   }
   ++i;

   //condition
   if (getValue(i) != ";") {
      conditionLocation = location(i);
      condition = compileExpression(i);
      if (allowNondeterminism) {
         postCondition = new Unary("!", compileExpression(i));
      }
      ++i;
      string bit(getValue(i));
      ensure(bit == ";", i, "expected ';' after for loop condition but found \""+bit +"\"");
   } else {
      warn(i, "for loop without a condition will run forever");
   }
   ++i;

   //update
   if (getValue(i) != ")") {
      update = compileExpression(i);
      ++i;
      string bit(getValue(i));
      ensure(bit == ")", i, "expected ')' after for loop update but found \""+bit +"\"");
   }
   ++i;

   //body
   ensure(i != expr->children.end(), i, "empty body in for loop");
   body = compileScope(i, i+1);
   ++i;
   ensure(i == expr->children.end(), i, "trailing data after for loop body");

   if (!allowNondeterminism) {
      For* statement = new For(location(expr));
      statement->addInitialiser(initialiser);
      statement->addCondition(condition);
      statement->addUpdate(update);
      statement->addBody(body);
      return statement;
   } else {
      Scope* s = new Scope(location(expr), resolver); //give me a new scope for the whole for loop
      Scope* kleeneScope = new Scope(location(expr), resolver); //and a scope for the kleene bit too
      s->addStatement(initialiser);
      s->addStatement(new Kleene(location(expr), kleeneScope, endScopeLabel, resolver));
      kleeneScope->addStatement(new Holds(condition, conditionLocation));
      kleeneScope->addStatement(body);

      if (update != 0) {
         kleeneScope->addStatement(update);
      }

      s->addStatement(new Holds(postCondition, conditionLocation));
      return s;
   }
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileWhile(const iter_t& expr) {
   ensure(expr->children.size() == 2, expr, "while statement needs a condition and a body");
   iter_t i = expr->children.begin();

   if (!allowNondeterminism) {
      return new While(compileExpression(i), compileExpression(i+1));
   } else {
      Scope* s = new Scope(location(i), resolver); //give me a new scope for the whole while loop

      Scope* kleeneScope = new Scope(location(i), resolver); //and a scope for the kleene bit too
      kleeneScope->addStatement(new Holds(compileExpression(i), location(i))); //condition
      kleeneScope->addStatement(compileExpression(i+1)); //body

      //while loop = kleene(?(condition) >> body) >> holds(not(condition))
      s->addStatement(new Kleene(location(i), kleeneScope, endScopeLabel, resolver));
      s->addStatement(new Holds(new Unary("!", compileExpression(i)), location(i)));
      return s;
   }
}

template <typename iter_t>
Ndet* ComponentCompiler<iter_t>::compileNdet(const iter_t& begin, const iter_t& end) {
   iter_t i = begin;
   ensure(allowNondeterminism, begin, "cannot use nondeterministic branch (ndet) in a static or non-private function");
   ensure(i != end, i, "nondeterministic branch requires at least two subprograms");
   Ndet* ndet = new Ndet(location(i), endScopeLabel, resolver);
   while (i != end) {
      string endProgram = JologNode::newLabel();
      ndet->addProgram(compileScope(i, i+1, endProgram), endProgram);
      ++i;
   }
   return ndet;
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileStatement(const iter_t& expr) {
   JologNode* s = 0;
   if (expr->value.id() == scope_tag) {
      s = compileScope(expr->children.begin(), expr->children.end());
   } else if (expr->value.id() == if_tag) {
      s = compileIf(expr->children.begin(), expr->children.end());
   } else if (expr->value.id() == while_tag) {
      s = compileWhile(expr);
   } else if (expr->value.id() == for_tag) {
      s = compileFor(expr);
   } else if (expr->value.id() == local_tag) {
      s = compileVariable(expr->children.begin(), expr->children.end());
   } else if (expr->value.id() == ndet_tag) {
      ensure(allowNondeterminism, expr, "cannot use nondeterministic branch (ndet) in a static function");
      s = compileNdet(expr->children.begin(), expr->children.end());
   } else if (expr->value.id() == kleene_tag) {
      ensure(allowNondeterminism, expr, "cannot use nondeterministic iteration (kleene star) in a static function");
      s = new Kleene(location(expr),
                     compileScope(expr->children.begin(), expr->children.end()),
                     endScopeLabel,
                     resolver);
   } else if (expr->value.id() == pick_tag) {
      string pickType = getValue(expr);
      ensure(pickType == "pick" || pickType == "some" || pickType == "all", expr, "unknown pick type \""+pickType+"\"");
      ensure(allowNondeterminism, expr, "cannot use nondeterministic arguments ("+pickType+") in a static function");
      //ensure(expr->children.size() % 2 == 1, expr, "pick requires at least one parameter (type and variable), followed by exactly one scope body");
      ensure(expr->children.size() == 3, expr, pickType+" requires three arguments: a type, a variable name and a scope body");
      iter_t i = expr->children.begin();
      iter_t body = i+2;
      JologType* type(JologCast::primitiveToReference(compileType(i)));
      string argumentName(getValue(i+1));

//      JologNode* v;
//      if (resolver->getSlot(argumentName) != NamespaceResolver::NO_SLOT) { //we've already created this variable
//         v = new Identifier(0, argumentName, resolver, location(expr));
//      } else {
//         v = new Variable(location(i), type, getValue(i+1), 0, endScopeLabel, resolver, false); //TODO: transient? need prefix?
//      }

//      std::cout << "COMPONENT COMPILER: PICK VARIABLE (" << v << ")" << std::endl;
//      std::cout << " - slot = " << v->getSlot() << std::endl;
//      std::cout << " - name = " << v->getName() << std::endl;

      if (pickType == "pick") {
         Variable* v = new Variable(location(i), type, argumentName, 0, endScopeLabel, resolver, false); //TODO: transient? need prefix?
         s = new Pick(location(expr), v, endScopeLabel, //name,
                      compileScope(body->children.begin(), body->children.end()), resolver);
      } else {
         int newSlots = resolver->nextSlot() - 1; //TODO: fucking terrible hack to get past backtrackId flag in resolver
         NamespaceResolver* newResolver = new NamespaceResolver(resolver, &newSlots);
         Variable* v = new Variable(location(i), type, argumentName, 0, END_SOME_LABEL, newResolver, false);

         NamespaceResolver* saveResolver = resolver;
         resolver = newResolver;
         JologNode* condition = compileExpression(body);
         resolver = saveResolver;

         ensure(condition->getType() == JologType::Boolean(), i, "'some' and 'all' constucts need a Boolean condition");

         SomeAll* sa = new SomeAll(location(expr), v, condition, resolver, newResolver, pickType, someAlls.size());
         someAlls.push_back(sa);
         s = sa->getCall();
      }
   } else if (expr->value.id() == holds_tag) {
      ensure(expr->children.size() == 1, expr, "holds check requires exactly one condition");
      s = new Holds(compileExpression(expr->children.begin()), location(expr));
   } else {
      s = compileExpression(expr);
   }
   return s;
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileExpression(const iter_t& expr) {
   JologNode* node = 0;
   iter_t i = expr;
   if (i->value.id() == string_tag) {
      node = new StringLiteral(getValue(i));

   } else if (i->value.id() == char_tag) {
      string c(getValue(i));
      ensure(c.size() == 1, i, "invalid multichar \""+c+"\"");
      node = new CharLiteral(c[0]);

   } else if (i->value.id() == double_tag) {
      double r;
      std::stringstream ss;
      ss << getValue(i);
      ss >> r;
      node = new DoubleLiteral(r);

   } else if (i->value.id() == integer_tag) {
      int integer;
      std::stringstream ss;
      ss << getValue(i);
      ss >> integer;
      node = new IntegerLiteral(integer);

   } else if (i->value.id() == boolean_tag) {
      node = new BooleanLiteral(getValue(i));

   } else if (i->value.id() == identifier_tag) { //identifier by itself = this.fluent | this.action() | this.function()
      node = compileIdentifier(i, 0);

   } else if (i->value.id() == logical_or_expression_tag) {
      ensure(i->children.size() == 2, i, "logical or expression requires two arguments");
      iter_t c = i->children.begin();
      node = new Or(compileExpression(c), compileExpression(c+1), location(i));
   } else if (i->value.id() == logical_and_expression_tag) {
      ensure(i->children.size() == 2, i, "logical and expression requires two arguments");
      iter_t c = i->children.begin();
      node = new And(compileExpression(c), compileExpression(c+1), location(i));
   } else if (i->value.id() == additive_expression_tag ||
         i->value.id() == multiplicative_expression_tag) {
      string op(getValue(i));
      ensure(i->children.size() == 2, i, op+" requires two arguments");
      node = new Arithmetic(op,
                            compileExpression(i->children.begin()),
                            compileExpression(i->children.begin()+1),
                            location(i));

   } else if (i->value.id() == jump_tag) {
      ensure(returnCount == 0, i, "multiple returns not permitted");
      ++returnCount;
      NamespaceResolver* retResolver = allowNondeterminism ? resolver : 0;
      if (i->children.size() == 0) {
         node = new Return(0, retResolver);
      } else if (i->children.size() == 1) {
         node = new Return(compileExpression(i->children.begin()), retResolver);
      } else {
         ensure(false, i, "can return at most one value");
      }

   } else if (i->value.id() == local_tag) {
      node = compileVariable(i->children.begin(), i->children.end());
   } else if (i->value.id() == assignment_operator_tag) {
      string op(getValue(i));
      ensure(i->children.size() == 2, i, op+" requires two arguments");
      node = new Assignment(compilePostfix(i->children.begin(), 0), op, compileExpression(i->children.begin()+1), resolver, location(i));

   } else if (i->value.id() == conditional_expression_tag) {
      node = compileTernary(i);
   } else if (i->value.id() == relational_expression_tag) {
      iter_t it = i->children.begin();
      int children = i->children.size();
      ensure(children >= 3 && children % 2 == 1, it, "unbalanced relational expression");
      children = (children - 1) / 2; //number of individual binary expressions

      JologNode* left = compileExpression(it);
      ++it;
      while (it != i->children.end()) {
         assertTag(it, file_tag, i->children.end());
         string op(getValue(it));
         ensure(opcodes::isRelationalOperator(op), it, "expected relational or equality operator but found \""+op+"\"");
         ++it;

         JologNode* right = compileExpression(it);
         ++it;

         if (node == 0) {
            node = new Relational(left, op, right, location(i));
         } else {
            node = new And(node, new Relational(left, op, right, location(i)), location(i));
         }
         left = right;
      }

   } else if (i->value.id() == cast_expression_tag) {
      ensure(i->children.size() == 2, i, "typecast needs a type and an expression");
      node = new JologCast(compileType(i->children.begin()), compileExpression(i->children.begin()+1), location(i));

   } else if (i->value.id() == unary_expression_tag) {
      ensure(i->children.size() == 1, i, "unary expression requires one operator and one expression");
      string op(getValue(i));
      node = new Unary(op, compileExpression(i->children.begin()));
   } else if (i->value.id() == postfix_expression_tag || i->value.id() == tuple_tag) {
      node = compilePostfix(i, 0);

   } else if (i->value.id() == atomic_expression_tag) {
      node = compileAtomicExpression(i);

   } else if (i->value.id() == expression_tag) {
      iter_t child = i->children.begin();
      JologIO* io = new JologIO(compileExpression(child), resolver, location(i));
      ++child;

      string op;
      while (child != i->children.end()) {
         assertTag(child, file_tag, i->children.end());
         op = getValue(child);
         ensure((op == "<<" || op == ">>"), child, "invalid communication operator \""+op+"\"");
         ++child;

         if (op == "<<") {
            io->addOutput(compileExpression(child));
         } else {
            io->addInput(compileExpression(child));
         }
         ++child;
      }
      node = io;

   } else if (i->value.id() == scope_tag) {
      node = compileScope(i->children.begin(), i->children.end());
   } else if (i->value.id() == pick_tag) {
      ensure(getValue(i) == "some" || getValue(i) == "all", i, "only nondeterministic 'some' and 'all' may be used in expressions");
      node = compileStatement(i);
   } else {
      ensure(false, i, "unexpected tag '" + ErrorMessage::tagToName(i->value.id().to_long()) + "' in expression");
   }
   return node;
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileTernary(const iter_t& expr) {

   ensure(expr->children.size() == 5, expr, "expected ternary of form (condition ? expression : expression)");
   iter_t i = expr->children.begin();
   string bit;

   JologNode* condition = compileExpression(i);
   ++i;

   bit = getValue(i);
   ensure(bit == "?", i, "expected '?' after ternary condition but found \""+bit+"\"");
   ++i;

   JologNode* p1 = compileExpression(i);
   ++i;

   bit = getValue(i);
   ensure(bit == ":", i, "expected ':' between ternary expressions but found \""+bit+"\"");
   ++i;

   JologNode* p2 = compileExpression(i);
   ++i;
   ensure(i == expr->children.end(), i, "trailing data in ternary expression");

   return new Ternary(condition, p1, p2, location(i-1));
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compilePostfix(const iter_t& expr, JologNode* parent) {
   JologNode* node = 0;
   string root(getValue(expr));


   if (expr->value.id() == tuple_tag) {
      node = compileTuple(expr);

   } else if (expr->value.id() == identifier_tag) {
      node = compileIdentifier(expr, parent);

   } else if (root == ".") { //member
      ensure(expr->children.size() == 2, expr, "dot (member) operator requires two operands");
      iter_t child = expr->children.begin();
      node = compilePostfix(child+1, compilePostfix(child, parent));

   } else if (root == "(") { //function call (or fluent if function not defined)
      ensure(expr->children.size() > 0, expr, "function call requires at least a name");
      iter_t child = expr->children.begin();
      string name = getValue(child);
      ++child;
      iter_t firstArg = child;

      FunctionCall* fc = new FunctionCall(parent, name, resolver, location(expr), endScopeLabel, allowNondeterminism);
      try {
         while (child != expr->children.end()) {
            fc->addArgument(compileExpression(child));
            ++child;
         }
         fc->getType();
         node = fc;
      } catch (JologCompileException e) {
         delete fc;
         Fluent* fluent = new Fluent(name, resolver, location(expr), allowFluentAssignment);
         child = firstArg;
         while (child != expr->children.end()) {
            fluent->addArgument(compileExpression(child));
            ++child;
         }
         node = fluent;
      }

   } else if (root == "[") { //array reference
      ensure(expr->children.size() > 0, expr, "array reference requires an array name and an index");
		iter_t child = expr->children.begin();
		node = new ArrayReference(location(child), compilePostfix(child, parent), compileExpression(child+1));

   } else if (expr->value.id() == atomic_expression_tag) {
      node = compileAtomicExpression(expr);

   } else if (expr->value.id() == cast_expression_tag) {
      node = compileExpression(expr);
   } else {
      ensure(root == "++" || root == "--", expr, "unknown postfix operator \""+root+"\"");
      ensure(expr->children.size() > 0, expr, "premature end of postfix operator \""+root+"\"");

      //NOTE: we are doing PREFIX ++/-- but so far neither actually returns a value, so it is ok
      node = new Unary(root, compilePostfix(expr->children.begin(), parent));
   }
   return node;
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileTuple(const iter_t& expr) {
   ensure(getValue(expr) == "(", expr, "invalid tuple prefix");
   Tuple* tuple = new Tuple(location(expr), resolver);

   iter_t child = expr->children.begin();
   while (child != expr->children.end()) {
      tuple->addElement(compileExpression(child));
      ++child;
   }
   return tuple;
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileIdentifier(const iter_t& expr, JologNode* parent) {
   string id(getValue(expr));
   int slot = resolver->getSlot(id);
   JologNode* node = 0;

   if (id == "fail") { //this *looks* like an identifier, but we give it special treatment because it does more
      node = new Fail(location(expr));

   } else if (id == "cut") {
      node = new Cut(location(expr), endScopeLabel, resolver);

   //fluents do not have a variable slot and cannot be static
   } else if ((slot != NamespaceResolver::NO_SLOT) ||
              (Identifier::isBuiltin(id)) ||
              (resolver->isStatic(id))) {

      node = new Identifier(parent, id, resolver, location(expr));
   } else { //fluent!
      node = new Fluent(id, resolver, location(expr), allowFluentAssignment);
   }
   return node;
}

template <typename iter_t>
JologNode* ComponentCompiler<iter_t>::compileAtomicExpression(const iter_t& i) {
   string symbol(getValue(i));
   ensure(symbol == "new", i, "unexpected value \""+symbol+"\" in expression");
   iter_t c = i->children.begin();
   ensure(c != i->children.end(), c, "new operator requires a type");

   JologType* typePointer = compileType(c);
   JologType type(*typePointer);
   delete typePointer; //Constructors copy type, so we don't need a pointer anymore

   ++c;
   ensure(c != i->children.end(), c, "expected function call or array size");
   string initialiserType(getValue(c));
   ++c;

   JologNode* constructor = 0;
   if (initialiserType == "(") { //constructor function call
      Constructor* functionConstructor = new Constructor(type, resolver, location(i));

      while (c != i->children.end()) {
         functionConstructor->addArgument(compileExpression(c));
         ++c;
      }
      constructor = functionConstructor;
   } else if (initialiserType == "[") { //array size
      //check that the type is a pure type, not an array: we need to create the first array first ...
      ensure(type.getClassName().size() > 0 && type.getClassName()[0] != '[',
             c, "multidimensional arrays must be constructed in order");

      ensure(c != i->children.end(), c, "expected array size after '['");
      JologNode* size = compileExpression(c);
      ++c;

      size_t dimensions = 0;
      while (c != i->children.end()) {
         ++dimensions;
         ++c;
      }

      constructor = new ArrayConstructor(JologType(type.getClassName(), dimensions), size, resolver, location(i));

   } else {
      ensure(false, i, "invalid constructor suffix \""+initialiserType+"\"");
   }
   return constructor;
}

#endif

