/*
 * JologCompileException.h
 *
 *  Created on: Aug 12, 2009
 *      Author: timothyc
 */

#ifndef JOLOG_COMPILE_EXCEPTION
#define JOLOG_COMPILE_EXCEPTION

#include <exception>
#include <string>

using std::string;
using std::exception;

class JologCompileException : public exception {
	public:
		JologCompileException(string location, string message) :
		   location(location), message(message) {}

		virtual const string where() const throw() {
		   return location;
		}

		virtual const char* what() const throw() {
			return message.c_str();
		}

		~JologCompileException() throw() {}

	private:
	   string location;
		string message;
};

#endif
