/*
 * ComponentCompiler.h
 *
 *  Created on: 28/06/2009
 *      Author: timothyc
 */

#ifndef COMPONENT_COMPILER
#define COMPONENT_COMPILER

#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <exception>
#include "../grammars/JologGrammar.h"
#include "../grammars/GrammarUtility.h"
#include "JologCompileException.h"

#include "../ast/JologNode.h"
#include "../ast/JologType.h"
#include "../ast/Agent.h"
#include "../ast/Function.h"
#include "../ast/Action.h"
#include "../ast/Fluent.h"
#include "../ast/While.h"
#include "../ast/For.h"
#include "../ast/If.h"
#include "../ast/And.h"
#include "../ast/Or.h"
#include "../ast/ArrayReference.h"
#include "../ast/Arithmetic.h"
#include "../ast/Assignment.h"
#include "../ast/Tuple.h"
#include "../ast/Scope.h"
#include "../ast/Ternary.h"
#include "../ast/opcodes.h"
#include "../ast/Variable.h"
#include "../ast/IntegerLiteral.h"
#include "../ast/DoubleLiteral.h"
#include "../ast/StringLiteral.h"
#include "../ast/CharLiteral.h"
#include "../ast/BooleanLiteral.h"
#include "../ast/Ndet.h"
#include "../ast/Kleene.h"
#include "../ast/Pick.h"
#include "../ast/SomeAll.h"
#include "../ast/Cut.h"
#include "../ast/Fail.h"
#include "../ast/Holds.h"
#include "../ast/Identifier.h"
#include "../ast/NamespaceResolver.h"
#include "../ast/Return.h"
#include "../ast/JologCast.h"
#include "../ast/Unary.h"
#include "../ast/FunctionCall.h"
#include "../ast/Constructor.h"
#include "../ast/ArrayConstructor.h"
#include "../ast/Relational.h"
#include "../ast/JologIO.h"
#include "../ast/FailedPreconditionException.h"

using std::ostream;
using std::vector;
using std::map;
using std::pair;
using std::make_pair;

template <typename iter_t>
class ComponentCompiler {
	public:

		/**
		 * debug method: pretty (ugly) prints out all tokens in the AST
		 */
		static void showTree(int depth, const iter_t& begin, const iter_t& end);

		/**
		 * create a (private) ComponentCompiler instance and tell it to compile
		 * the provided AST
		 */
		static void compile(const iter_t& begin, const iter_t& end, ostream& out, ostream& stderr, string getSignatureScript) {
			ComponentCompiler<iter_t> cc(stderr, getSignatureScript);
			JologNode* program = cc.compileFile(begin, end);
			if (program != 0) {
   			StackDepth junk; //so im not proud of this, but at least it is kinda hidden away ...
	   		program->emit(out, junk);
         } else {
            stderr << "compilation error; assembly cannot be generated" << std::endl;
         }
		}

	private:
		ostream& stderr;
		string packageName;
		size_t actionId;
		NamespaceResolver* resolver;
		string endScopeLabel;
      string getSignatureScript;
      bool allowFluentAssignment;
      int returnCount;
      bool allowNondeterminism; //use normal if/while/for + prevent backtracking in general if in a static function
      vector<SomeAll*> someAlls;

		/**
		 * construct me! NB. private; can only be created by static compile() method
		 */
		ComponentCompiler<iter_t>(ostream& stderr, string getSignatureScript) :
				stderr(stderr), actionId(0),
            resolver(new NamespaceResolver()), getSignatureScript(getSignatureScript),
            allowFluentAssignment(false) {}

		Agent* compileFile(const iter_t& begin, const iter_t& end);
		Agent* compileAgent(const iter_t& begin, const iter_t& end);
		void compileBodies(map<Function*, pair<iter_t, iter_t> >& agentComponents);
      Scope* compileActionBody(string name, Action::ActionType actionType, const iter_t begin, const iter_t end);
		Function* compileFunction(map<Function*, pair<iter_t, iter_t> >& agentComponents,
                                const iter_t& begin, const iter_t& end);
		Variable* compileParameter(const iter_t& begin, const iter_t& end, NamespaceResolver* resolver);
		Fluent* compileFluent(const iter_t& begin, const iter_t& end);
		Variable* compileVariable(const iter_t& begin, const iter_t& end);
		Action* compileAction(Agent* parent,
                            map<Function*, pair<iter_t, iter_t> >& agentComponents,
                            const iter_t& begin, const iter_t& end);
		JologNode* compileInitialiser(const iter_t& begin, const iter_t& end);
		JologType* compileType(const iter_t& expr, string package = "", bool create = false);
		void compileImport(const iter_t& expr);
		void compilePackage(const iter_t& expr);
		Scope* compileScope(const iter_t& begin, const iter_t& end, string endLabel="");
		JologNode* compileIf(const iter_t& begin, const iter_t& end);
		JologNode* compileFor(const iter_t& expr);
		JologNode* compileWhile(const iter_t& expr);
		Ndet* compileNdet(const iter_t& begin, const iter_t& end);
		JologNode* compileStatement(const iter_t& expr);
		JologNode* compileExpression(const iter_t& expr);
		JologNode* compileTernary(const iter_t& expr);
		JologNode* compilePostfix(const iter_t& expr, JologNode* parent);
		JologNode* compileTuple(const iter_t& expr);
      JologNode* compileIdentifier(const iter_t& expr, JologNode* parent);
      JologNode* compileAtomicExpression(const iter_t& i);

		static inline bool checkTag(const iter_t& tag, const ParserTags expected, const iter_t& end) {
			bool ok = true;
			if (tag == end) {
				ok = false;
			} else if (tag->value.id() != expected) {
				ok = false;
			}
			return ok;
		}

		static inline string getValue(const iter_t& i) {
			string s;
			if (i->value.begin() != i->value.end()) {
				s = string(i->value.begin(), i->value.end());
			}
			return s;
		}

		static inline bool assertTag(const iter_t& tag, const ParserTags expected, const iter_t& end) {
			ensure(tag != end, tag, "unexpected end of input");
			ensure(tag->value.id() == expected, tag, "expected " +
                                                  ErrorMessage::tagToName(expected) +
                                                  " but found " +
                                                  ErrorMessage::tagToName(tag->value.id().to_long()));
			return true; //if we made it this far (without an exception) then we are ok!
		}

		static inline void ensure(bool result, const iter_t& i, string errorMessage) {
			if (!result) {
				throw JologCompileException(location(i), "ERROR: " + errorMessage);
			}
		}

		void inline warn(const iter_t& i, string warning) {
			//throw JologCompileException("(" + location(i) + ") WARNING: " + warning);
			stderr << "(" << location(i) << ") WARNING: " << warning << std::endl;
		}

		static inline std::string iteratorToName(const iter_t& i) {
			return ErrorMessage::tagToName(i->value.id().to_long());
		}

		/**
		 * return a pretty string representation of the provided token's
		 * file and line number
		 */
		static inline string location(const iter_t& i) {
			string s("unknown");
			const iter_t end;

			if ((i != end) && //(i->value.is_root()) &&
             (i->value.begin() != i->value.end())) {
            std::stringstream ss;
            ss << i->value.begin().get_position().file
               << ": "
               << i->value.begin().get_position().line;
            s = ss.str();
			}
			return s;
		}
};

//force template instantiation
#include "ComponentCompiler.cpp"

#endif
