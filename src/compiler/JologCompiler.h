/*
 * JologCompiler.h
 *
 *  Created on: 23/06/2009
 *      Author: timothyc
 */

#ifndef JOLOG_COMPILER
#define JOLOG_COMPILER

#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_ast.hpp>
#include <boost/spirit/include/classic_tree_to_xml.hpp>
#include "../grammars/JologGrammar.h"
#include "ComponentCompiler.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using namespace boost::spirit::classic;

class JologCompiler {
	public:
		static bool parse(string s, string filename, std::ostream& stderr) {
			std::stringstream output;
			return parseAndCompile(s.c_str(), filename, output, stderr, false, "");
		}

		static bool parse(const char* s, string filename, std::ostream& stderr) {
			std::stringstream output;
			return parseAndCompile(s, filename, output, stderr, false, "");
		}

		static int compile(string& s, string filename,
   						    std::ostream& output, std::ostream& stderr,
                         string getSignatureScript) {
			return parseAndCompile(s.c_str(), filename, output, stderr, true, getSignatureScript);
		}

		static int compile(const char* s, string filename,
   						    std::ostream& output, std::ostream& stderr,
                         string getSignatureScript) {
			return parseAndCompile(s, filename, output, stderr, true, getSignatureScript);
		}

	private:
		static bool parseAndCompile(const char* s, string filename,
									       std::ostream& output, std::ostream& stderr,
                                  bool compile, string getSignatureScript) {
			bool errorFlag = false;
			JologGrammar g(stderr, errorFlag);

			tree_parse_info<JologGrammar::iterator_t, JologGrammar::factory_t> info = g.parseString(s, filename);

			bool success = false;
			if ((!errorFlag) && (info.full) && (info.trees.size() > 0)) {
				success = true;
				showAST(info.trees.begin(), info.trees.end());

				if (compile) {
					try {
						//showAST(info.trees.begin(), info.trees.end());
						compileAST(info.trees.begin(), info.trees.end(), output, stderr, getSignatureScript);
					} catch (JologCompileException e) {
						stderr << e.where() << ": " << e.what() << std::endl;
						success = false;
					}
				}

			} else {
//            const char* stop = *(info.stop);
				stderr << "("
					   << info.stop.get_position().file
					   << ": "
					   << info.stop.get_position().line
					   << ") ERROR: compilation halting (full = "
					   << std::boolalpha
					   << info.full
					   << "), trees = "
					   << info.trees.size()
                  //<< string(stop, s+strlen(s)) //, s+strlen(s))
					   << std::endl;
			}

			return success;
		}

		/**
		 * helper function to resolve ComponentCompiler's parameterisation
		 * from its argument types (makes ComponentCompiler nicer to look at!)
		 */
		template <typename iter_t>
		static void compileAST(const iter_t& begin, const iter_t& end,
                             std::ostream& output, std::ostream& stderr,
                             string getSignatureScript) {
			ComponentCompiler<iter_t>::compile(begin, end, output, stderr, getSignatureScript);
		}

		/**
		 * helper function to resolve ComponentCompiler's parameterisation
		 * from its argument types (makes ComponentCompiler nicer to look at!)
		 */
		template <typename iter_t>
		static void showAST(const iter_t& begin, const iter_t& end) {
			ComponentCompiler<iter_t>::showTree(0, begin, end);
		}

};

#endif
