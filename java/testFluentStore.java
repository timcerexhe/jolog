import jolog.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class testFluentStore {
   public static void main(String[] args) {
      testLoadAndStore();
      testArgumentMatch();
   }

   private static void testLoadAndStore() {
      FluentStore f = new FluentStore();
      Predicate p1 = new Predicate("a");
      Predicate p2 = new Predicate("a");
      f.store(p1, new Object());
      System.out.println("loading " + f.load(p2));

      Pattern p = Pattern.compile("^\\w+\\d\\s*$");
      Matcher m1 = p.matcher("hello7");
      System.out.println("hello7 matches? " + m1.matches());

      Matcher m2 = p.matcher("hello9   \t");
      System.out.println("hello9 matches? " + m2.matches());

      Matcher m3 = p.matcher("77");
      System.out.println("77 matches? " + m3.matches());

      Matcher m4 = p.matcher("3");
      System.out.println("3 does not match? " + !m4.matches());
   }

   private static void testArgumentMatch() {
      FluentStore f = new FluentStore();
      f.store(new Predicate("f"), "hello");
      Predicate p1 = new Predicate("f");
      p1.addArgument(new Integer(3));
      f.store(p1, "goodbye");

      Predicate p2 = new Predicate("f");
      p2.addArgument(new Integer(10));
      f.store(p2, new Float(3.14));

      Predicate p3 = new Predicate("g");
      p3.addArgument(new Integer(1));
      f.store(p3, new Integer(6));
      try {
         int index;
         try {
            System.out.println("Integers:");
            index = 0;
            Integer i;
            while ((i = (Integer) f.getNext(Class.forName("java.lang.Integer"), index)) != null) {
               System.out.println(" - "+ i);
               index++;
            }
         } catch (jolog.FailedPreconditionException e) {
            System.out.println("DONE! ("+e.getMessage()+")");
         }

         try {
            System.out.println("Strings:");
            index = 0;
            String s;
            while ((s = (String) f.getNext(Class.forName("java.lang.String"), index)) != null) {
               System.out.println(" - "+ s);
               index++;
            }
         } catch (jolog.FailedPreconditionException e) {
            System.out.println("DONE! ("+e.getMessage()+")");
         }

         try {
            System.out.println("TimeUnits (not in fluent store ...):");
            index = 0;
            Object o;
            while ((o = f.getNext(Class.forName("java.util.concurrent.TimeUnit"), index)) != null) {
               System.out.println(" - " + o);
               index++;
            }
         } catch (jolog.FailedPreconditionException e) {
            System.out.println("DONE! ("+e.getMessage()+")");
         }

      } catch (ClassNotFoundException e) {
         System.out.println("FAILURE ("+e.getMessage()+")");
      }
   }
}

