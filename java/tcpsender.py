#! /usr/bin/python

import socket, sys

HOST = ""
PORT = 8080

try:
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
   s.bind((HOST, PORT))
   s.listen(1)
   (conn, addr) = s.accept()
   print "Connection from ", addr
   while 1:
      #data = conn.recv(1024)
      data = sys.stdin.readline()
      if not data: break
      conn.send(data)
   conn.close()
except:
   print "all over!"
