import jolog.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class testChoicePoint {
   public static void main(String[] args) {
      try {
         Integer i = new Integer(0);
         String s = "my initial string";

         System.out.println("state: i = " + i + ", s = " + s);

         System.out.println("SAVING");
         Object[] state = {i, s};
         ChoicePointState cps = new ChoicePointState(state, "the start");
         state = null; //all gone!

         System.out.println("MODIFYING");
         i = new Integer(1000000);
         s = "not the 9 o'clock news ...";

         System.out.println("state: i = " + i + ", s = " + s);

         System.out.println("RESTORING");
         state = cps.restoreState("the end");
         i = (Integer) state[0];
         s = (String) state[1];

         System.out.println("state: i = " + i + ", s = " + s);

         ChoicePointState cps2 = new ChoicePointState(null, "try 2");
         Object[] junk = cps2.restoreState("end try 2");
         
      } catch (ProgramFailureException e) {
         System.out.println("something broke :(");
         System.out.println("error: " + e.getMessage());
      }
   }
}


