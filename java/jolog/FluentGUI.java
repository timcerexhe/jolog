package jolog;

import java.util.Map;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

//public?
final class FluentGUI extends JFrame {
   public FluentGUI(Map<Predicate, Object> fluents) {
      super("Fluent Store");
      this.fluents = fluents;

      this.pane = new JScrollPane();
      this.add(pane);
      update();
      this.pack();
      this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      this.setVisible(true);
      this.setSize(300, 500);
   }

   public void update() {
      Vector<String> data = new Vector<String>();
      for (Object o : fluents.keySet()) {
         data.add(o + ": " + fluents.get(o));
      }

      JList list = new JList(data.toArray());
      pane.getViewport().setView(list);
   }

   private JScrollPane pane;
   private Map<Predicate, Object> fluents;
}

