package jolog;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import java.util.Vector;
import java.io.InputStream;
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
import java.net.Socket;
import java.net.InetAddress;
import java.io.IOException;

public class JologMonitor implements Runnable, Environment {
   private final static int SLEEP_DURATION = 100;

   private final ExecutorService threadPool;
   private final Vector<JologAgent> agents;
   private final List<StreamChannelPair> inputs;


   public JologMonitor(int maxThreads) {
      threadPool = Executors.newCachedThreadPool();
      agents = new Vector<JologAgent>();
      inputs = new CopyOnWriteArrayList<StreamChannelPair>();

      //do router stuff here too!
   }

   public void start() {
      new Thread(this).start();
   }

   public void send(String s) {
      System.out.println("send not implemented");
   }

   public String sense() throws IOException {
      System.out.println("sense not implemented");
      return "";
   }

   public void addChannel(InputStream stream, JologChannel channel) {
      inputs.add(new StreamChannelPair(stream, channel));
   }

   public Runnable asynchronousSend(final JologAgent a, final Message message) {
      return new Runnable() {
         public void run() {
            try {
               a.receive(message);
               System.out.println("asynchronous send complete!");
            } catch (FailedPreconditionException e) {
               System.out.println("Asynchronous send failed on precondition: " + e.getMessage());
            } catch (InterruptedException e) {
               System.out.println("Asynchronous send interrupted!? " + e.getMessage());
               Thread.currentThread().interrupt();
            }
         }
      };
   }

   public Runnable asynchronousSend(final JologAgent a, final Message message,
                                    final JologAgent callbackAgent,
                                    final JologAgent errorAgent) {
      return new Runnable() {
         public void run() {
            try {
               try {
                  a.receive(message);
                  System.out.println("asynchronous send complete!");
               } catch (FailedPreconditionException e) {
                  errorAgent.receive(new Message("Asynchronous send failed on precondition: " + e.getMessage()));
               } catch (InterruptedException e) {
                  errorAgent.receive(new Message("Asynchronous send interrupted!? " + e.getMessage()));
                  Thread.currentThread().interrupt();
               }
            } catch (FailedPreconditionException e) {
               System.out.println("ERROR: Asynchronous send error callback failed on precondition " + e.getMessage());
            } catch (InterruptedException e) {
               System.out.println("ERROR: Asynchronous send error callback interrupted!? " + e.getMessage());
               Thread.currentThread().interrupt();
            }
         }
      };
   }

   private void jolog_broadcast(Message m) {
      synchronized (agents) {
         for (JologAgent a : agents) {
            threadPool.execute(asynchronousSend(a, m));
         }
      }
   }

   public void run() {
      try {
//         BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         boolean done = false;

         while (!done) {
            for (StreamChannelPair pair : inputs) {
               int available = pair.stream.available();
               if (available > 0) {
                  byte[] data = new byte[available];
                  int read = pair.stream.read(data);
                  jolog_broadcast(new Message(new String(data, 0, read), pair.channel));
               }
            }
            Thread.sleep(SLEEP_DURATION);
         }

/*            String line = reader.readLine();
            if (line.equals("quit")) {
               done = true;
            } else {
               System.out.println("JologMonitor received: \"" + line + "\"");
//               threadPool.invokeAll(new ArrayList().add(new Callable<void>() {
//                                                           public void call() {}
//                                                        }), 60, TimeUnit.SECONDS);
            }*/

         threadPool.shutdownNow();
         if (!threadPool.awaitTermination(1000, TimeUnit.MILLISECONDS)) {
            System.out.println("Rogue agents! JologMonitor may be unable to self-terminate");
         }
      } catch (java.io.IOException e) {
         System.out.println("MONITOR IO EXCEPTION: "+e.getMessage());
      } catch (java.lang.InterruptedException e) {
         System.out.println("MONITOR FORCED EXIT: "+e.getMessage());
         Thread.currentThread().interrupt();
      }
   }

   public void addAgent(JologAgent a) {
      synchronized (agents) {
         threadPool.execute(a);
         agents.add(a);
      }
   }

   public static void main(String[] args) {
      try {
         JologMonitor m = new JologMonitor(1);
         m.addChannel(System.in, JologChannel.STDIN);

         JologAgent a = new PrintAgent(m);
         m.addAgent(a);
         m.start();

         Thread.sleep(3000);

         m.addAgent(a);

         Socket sock = new Socket(InetAddress.getLocalHost(), 8080);
         m.addChannel(sock.getInputStream(), JologChannel.TCP);

         a.receive(new Message("good luck from main!", JologChannel.FUNCTION));
      } catch (FailedPreconditionException e) {
         System.out.println("huh, precondition failed? "+e.getMessage());
      } catch (InterruptedException e) {
         System.out.println("Interrupted, even in main! "+e.getMessage());
      } catch (java.net.UnknownHostException e) {
         System.out.println("Cannot find localhost? "+e.getMessage());
      } catch (java.io.IOException e) {
         System.out.println("Error with IO: "+e.getMessage());
      }
   }

   private class StreamChannelPair {
      public InputStream stream;
      public JologChannel channel;

      public StreamChannelPair(InputStream in, JologChannel c) {
         stream = in;
         channel = c;
      }
   }


}

