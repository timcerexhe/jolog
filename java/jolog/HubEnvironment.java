package jolog;

import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.IOException;

public class HubEnvironment implements Environment {
   public void addAgent(JologAgent a) {
      agents.add(a);
   }

   public void readFromStream(InputStream input) {
      try {
         BufferedReader br = new BufferedReader(new InputStreamReader(input));
//         ObjectInputStream ois = new ObjectInputStream(input);
//         Object o = ois.readObject();
//         ois.close();

         Object o;
         while ((o = br.readLine()) != null) {
            if (o instanceof Message) {
               sendToAll((Message) o);
            } else if (o instanceof String) {
               sendToAll(new Message((String) o));
            } else {
               System.out.println("ERROR: unhandled message type");
            }
         }

      } catch (IOException e) {
         System.out.println("IO Error: " + e.getMessage());
//      } catch (ClassNotFoundException e) {
//         System.out.println("Read error: " + e.getMessage());
//      } catch (NotSerializableException e) {
      }
   }

   private void sendToAll(Message m) {
      int i = 0;
      for (JologAgent a : agents) {
         try {
            Message ret = a.receive(m);
            System.out.println("agent "+i+" response: "+ret);
         } catch (FailedPreconditionException e) {
            System.out.println("Agent got angry: " + e.getMessage());
         } catch (InterruptedException e) {
            System.out.println("Interruption error: " + e.getMessage());
         }
         i++;
      }
   }

   private void sendToAll(Message m, int sender) {
      int i = 0;
      for (JologAgent a : agents) {
         if (i != sender) {
            try {
               Message ret = a.receive(m);
               System.out.println("agent "+i+" response: "+ret);
            } catch (FailedPreconditionException e) {
               System.out.println("Agent got angry: " + e.getMessage());
            } catch (InterruptedException e) {
               System.out.println("Interruption error: " + e.getMessage());
            }
         }
         i++;
      }
  }

   public void send(String s) {
      System.out.println("agent 0 sent: " + s);
      sendToAll(new Message(s), 0);
   }

   public String sense() throws IOException {
      String s = reader.readLine();
      return s;
   }

   private List<JologAgent> agents = new ArrayList<JologAgent>();
   private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

}

