package jolog;

import java.io.IOException;

public interface Environment {
   public void addAgent(JologAgent a);
   public void send(String s);
   public String sense() throws IOException;
}


