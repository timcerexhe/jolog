package jolog;

public class FailedPreconditionException extends Exception {

	private static final long serialVersionUID = -4447931686209387573L;

   public FailedPreconditionException() {
      super("anonymous failed precondition");
   }

   public FailedPreconditionException(String message) {
      super(message);
   }

}

