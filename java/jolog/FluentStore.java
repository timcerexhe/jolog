package jolog;

import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public final class FluentStore {

   public FluentStore() {
      //this.gui = new FluentGUI(fluents);
   }

   public Object getNext(Class type, int index) throws FailedPreconditionException {
      if (type.isEnum()) {
         for (Object c : type.getEnumConstants()) {
            if (index == 0) {
               //System.out.println("PICK "+index+": enumeration = "+c);
               return c;
            }
            index--;
         }
      }
      for (Predicate p : fluents.keySet()) {
         for (Object a : p.getArguments()) {
            if (a.getClass().equals(type)) {
               if (index == 0) { //found!
                  //System.out.println("PICK "+index+": predicate = "+a);
                  return a;
               }
               index--;
            }
         }
         if (fluents.get(p).getClass().equals(type)) {
            if (index == 0) { //found!
               //System.out.println("PICK "+index+": predicate argument = "+fluents.get(p));
               return fluents.get(p);
            }
            index--;
         }
      }
      //we went all the way through, there is nothing left that matches ...
      throw new FailedPreconditionException("pick enumeration has been exhausted");
   }

   public void store(Predicate name, Object value) {
      //System.out.println("storing \"" + name + "\" = <" + value + ">");
      fluents.put(name, value);

      if (gui != null) {
         gui.update();
      }
   }

   public Object load(Predicate name) {
      Object o = fluents.get(name);
      //System.out.println("loading \"" + name + "\" = <" + o + ">");
      return o;
   }

   private FluentGUI gui;
   private Map<Predicate, Object> fluents = new ConcurrentHashMap<Predicate, Object>();
}

