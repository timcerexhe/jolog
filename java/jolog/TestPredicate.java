package jolog;

import static org.junit.Assert.*;

import jolog.Predicate;
import jolog.Variable;

public class TestPredicate {

	@org.junit.Test
	public void testZero() { //A = B
		Object a = new Variable();
		Object b = new Variable();

		assertTrue("two variables should be bound", a.equals(b));
	}

	@org.junit.Test
	public void testOne() { //A = abc
		Object abc = new Predicate("abc");
		Object v = new Variable();

		assertTrue("variable not equal to \"abc\"", v.equals(abc));
		assertTrue("variable incorrectly bound", v.equals("abc"));

	} 

	@org.junit.Test
	public void testTwo() { //abc = A
		Object abc = new Predicate("abc");
		Object v = new Variable();

		assertTrue("variable not equal to \"abc\"", abc.equals(v));
		assertTrue("variable incorrectly bound", v.equals("abc"));

	} 

	@org.junit.Test
	public void testThree() { //A = B and B = abc
		Object a = new Variable();
		Object b = new Variable();

		assertTrue("two variables should be bound", a.equals(b));

		Object c = new Predicate("abc");
		assertTrue("b not bound to actual value", b.equals(c));
		assertTrue("a has not inherited b's binding", c.equals(a));
		assertTrue("a has not inherited b's binding", a.equals(c));
		assertTrue("a different from itself?", a.equals(a));
		assertTrue("a no longer equal to b?", b.equals(a));
		assertTrue("a no longer equal to b?", a.equals(b));

	}

	@org.junit.Test
	public void testFour() { //A = B and B = C and C = abc 
		Object a = new Variable();
		Object b = new Variable();
		Object c = new Variable();
		
		assertTrue(a.equals(b));
		assertTrue(b.equals(c));

		Object abc = new Predicate("abc");
		assertTrue(c.equals(abc));
		
		assertTrue(a.equals(b));
		assertTrue(b.equals(b));
		assertTrue(c.equals(b));
		assertTrue(a.equals(c));
		assertTrue(b.equals(c));
		assertTrue(c.equals(c));
		assertTrue(a.equals(abc));
		assertTrue(b.equals(abc));
		assertTrue(c.equals(abc));
		assertTrue(abc.equals(a));
		assertTrue(abc.equals(b));
		assertTrue(abc.equals(c));
		
	}
	
	@org.junit.Test
	public void testFive() { //A = abc and abc = D
		Object a = new Variable();
		Object b = new Predicate("abc");
		assertTrue(a.equals(b));
		
		Object c = new Predicate("abc");
		Object d = new Variable();
		assertTrue(c.equals(d));
		
		assertTrue(a.equals(c));
		assertTrue(a.equals(d));
		assertTrue(c.equals(a));
		assertTrue(d.equals(a));
	}
	
	@org.junit.Test
	public void testSix() { //A = abc and XYZ = D
		Object a = new Variable();
		Object b = new Predicate("abc");
		assertTrue(a.equals(b));
		
		Object c = new Predicate("XYZ");
		Object d = new Variable();
		assertTrue(c.equals(d));
		
		assertFalse(a.equals(c));
		assertFalse(a.equals(d));
		assertFalse(c.equals(a));
		assertFalse(d.equals(a));
	}
	
	//Introducing arguments!
	@org.junit.Test
	public void testSeven() { //A = abc(xyz)
		Predicate abc = new Predicate("abc");
		abc.addArgument(new Predicate("xyz"));
		Object v = new Variable();

		assertTrue("variable not equal to \"abc\"", v.equals(abc));
		assertTrue("variable incorrectly bound", v.equals("abc(xyz)"));
		assertTrue(v.equals("abc(xyz)"));
		assertTrue(abc.equals("abc(xyz)"));
	} 
	
	@org.junit.Test
	public void testEight() { //abc(xyz) = A
		Predicate abc = new Predicate("abc");
		abc.addArgument(new Predicate("xyz"));
		Object v = new Variable();

		assertTrue("variable not equal to \"abc\"", abc.equals(v));
		assertTrue("variable incorrectly bound", v.equals("abc(xyz)"));
		assertTrue(abc.equals("abc(xyz)"));

	} 

	@org.junit.Test
	public void testNine() { //A = B and B = abc(xyz)
		Object a = new Variable();
		Object b = new Variable();

		assertTrue("two variables should be bound", a.equals(b));

		Predicate c = new Predicate("abc");
		c.addArgument(new Predicate("xyz"));
		assertTrue("b not bound to actual value", b.equals(c));
		assertTrue("a has not inherited b's binding", c.equals(a));
		assertTrue("a has not inherited b's binding", a.equals(c));
		assertTrue("a different from itself?", a.equals(a));
		assertTrue("a no longer equal to b?", b.equals(a));
		assertTrue("a no longer equal to b?", a.equals(b));
		
		assertTrue(a.equals("abc(xyz)"));
		assertTrue(b.equals("abc(xyz)"));
		assertTrue(c.equals("abc(xyz)"));

	}

	@org.junit.Test
	public void testTen() { //A = B and B = C and C = abc(xyz) 
		Object a = new Variable();
		Object b = new Variable();
		Object c = new Variable();
		
		assertTrue(a.equals(b));
		assertTrue(b.equals(c));

		Predicate abc = new Predicate("abc");
		abc.addArgument(new Predicate("xyz"));
		assertTrue(c.equals(abc));
		
		assertTrue(a.equals(b));
		assertTrue(b.equals(b));
		assertTrue(c.equals(b));
		assertTrue(a.equals(c));
		assertTrue(b.equals(c));
		assertTrue(c.equals(c));
		assertTrue(a.equals(abc));
		assertTrue(b.equals(abc));
		assertTrue(c.equals(abc));
		assertTrue(abc.equals(a));
		assertTrue(abc.equals(b));
		assertTrue(abc.equals(c));
		
		assertTrue(a.equals("abc(xyz)"));
		assertTrue(b.equals("abc(xyz)"));
		assertTrue(c.equals("abc(xyz)"));
		assertTrue(abc.equals("abc(xyz)"));
	}
	
	@org.junit.Test
	public void testEleven() { //A = abc(xyz) and abc(xyz) = D
		Object a = new Variable();
		Predicate b = new Predicate("abc");
		b.addArgument(new Predicate("xyz"));
		assertTrue(a.equals(b));
		
		Predicate c = new Predicate("abc");
		c.addArgument(new Predicate("xyz"));
		Object d = new Variable();
		assertTrue(c.equals(d));
		
		//check that both sides HAPPEN to be equal
		assertTrue(a.equals(c));
		assertTrue(a.equals(d));
		assertTrue(b.equals(c));
		assertTrue(b.equals(d));
		assertTrue(c.equals(a));
		assertTrue(d.equals(a));
		assertTrue(c.equals(b));
		assertTrue(d.equals(b));
	}
	
	@org.junit.Test
	public void testTwelve() { //A = abc(xyz) and XYZ(ABC) = D
		Object a = new Variable();
		Predicate b = new Predicate("abc");
		b.addArgument(new Predicate("xyz"));
		assertTrue(a.equals(b));

		assertTrue(a.equals("abc(xyz)"));
		assertTrue(b.equals("abc(xyz)"));
		
		Predicate c = new Predicate("XYZ");
		c.addArgument(new Predicate("ABC"));
		Object d = new Variable();
		assertTrue(c.equals(d));

		assertTrue(c.equals("XYZ(ABC)"));
		assertTrue(d.equals("XYZ(ABC)"));
		
		assertFalse(a.equals(c));
		assertFalse(a.equals(d));
		assertFalse(c.equals(a));
		assertFalse(d.equals(a));
	}
	
	
	//now we try unbinding!
	@org.junit.Test
	public void testThirteen() { //A = abc, unbind A, A = xyz
		Predicate abc = new Predicate("abc");
		Variable v = new Variable();
		Predicate xyz = new Predicate("xyz");

		assertTrue("variable not equal to \"abc\"", v.equals(abc));
		assertTrue(abc.equals(v));
		assertTrue("variable incorrectly bound", v.equals("abc"));
		assertFalse(v.equals(xyz));
		assertFalse(xyz.equals(v));
		
		v.unbind();
		
		assertTrue(v.equals(xyz));
		assertTrue(xyz.equals(v));
		assertTrue(v.equals("xyz"));
		assertFalse(v.equals(abc));
		assertFalse(abc.equals(v));

	} 

	@org.junit.Test
	public void testFourteen() { //A = abc(xyz), unbind A, A = 123
		Predicate abc = new Predicate("abc");
		abc.addArgument(new Predicate("xyz"));
		Variable v = new Variable();
		Predicate oneTwoThree = new Predicate("123");

		assertTrue("variable not equal to \"abc\"", v.equals(abc));
		assertTrue(abc.equals(v));
		assertTrue(abc.equals("abc(xyz)"));
		
		assertFalse(v.equals(oneTwoThree));
		assertFalse(v.equals("123"));
		
		v.unbind();

		assertTrue(v.equals(oneTwoThree));
		assertTrue(v.equals("123"));
		assertTrue(abc.equals("abc(xyz)"));
		
		assertFalse(v.equals(abc));
		assertFalse(v.equals("abc(xyz)"));
	}
	
	@org.junit.Test
	public void testFifteen() { //A = abc(xyz), unbind A, A = xyz
		Predicate abc = new Predicate("abc");
		Predicate xyz = new Predicate("xyz");
		abc.addArgument(xyz);
		Variable v = new Variable();

		assertTrue("variable not equal to \"abc\"", v.equals(abc));
		assertTrue(abc.equals(v));
		assertTrue(abc.equals("abc(xyz)"));
		
		v.unbind();
		
		assertTrue(abc.equals("abc(xyz)"));
		assertTrue(v.equals(xyz));
		assertTrue(v.equals("xyz"));
		assertTrue(abc.equals("abc(xyz)")); //this hasn't changed
	}
	
	@org.junit.Test
	public void testSixteen() { //loc(x,y) != pos(3,5)
		Predicate loc = new Predicate("loc");
		Object x = new Variable();
		Object y = new Variable();
		loc.addArgument(x);
		loc.addArgument(y);
		
		Predicate pos = new Predicate("pos");
		pos.addArgument(new Predicate("3"));
		pos.addArgument(new Predicate("5"));
		
		assertTrue(pos.equals("pos(3, 5)"));
		assertFalse(loc.equals(pos));
	}
	
	@org.junit.Test
	public void testSeventeen() { //pos(3,5) == pos(3,5)
		Predicate loc = new Predicate("pos");
		Variable x = new Variable();
		Object y = new Variable();
		loc.addArgument(x);
		loc.addArgument(y);
		
		Predicate pos = new Predicate("pos");
		pos.addArgument(new Predicate("3"));
		pos.addArgument(new Predicate("5"));
		
		assertTrue(pos.equals("pos(3, 5)"));
		assertTrue(loc.equals(pos));
		assertTrue(loc.equals("pos(3, 5)"));
		
		x.unbind();
		assertTrue(pos.equals("pos(3, 5)"));
	}
	
	@org.junit.Test
	public void testEighteen() { //A = B and B = C and C = abc(xyz), unbind C, C = 123
		Object a = new Variable();
		Object b = new Variable();
		Variable c = new Variable();
		
		assertTrue(a.equals(b));
		assertTrue(b.equals(c));

		Predicate abc = new Predicate("abc");
		abc.addArgument(new Predicate("xyz"));
		assertTrue(c.equals(abc));
		
		assertTrue(a.equals(b));
		assertTrue(b.equals(b));
		assertTrue(c.equals(b));
		assertTrue(a.equals(c));
		assertTrue(b.equals(c));
		assertTrue(c.equals(c));
		assertTrue(a.equals(abc));
		assertTrue(b.equals(abc));
		assertTrue(c.equals(abc));
		assertTrue(abc.equals(a));
		assertTrue(abc.equals(b));
		assertTrue(abc.equals(c));
		
		assertTrue(a.equals("abc(xyz)"));
		assertTrue(b.equals("abc(xyz)"));
		assertTrue(c.equals("abc(xyz)"));
		assertTrue(abc.equals("abc(xyz)"));
		
		c.unbind();
		
		Predicate oneTwoThree = new Predicate("123");
		assertTrue(c.equals(oneTwoThree));

		assertFalse(a.equals("123"));
		assertFalse(b.equals("123"));
		assertTrue(a.equals("abc(xyz)"));
		assertTrue(b.equals("abc(xyz)"));
		assertTrue(c.equals("123"));

		assertTrue(a.equals(b));
		assertTrue(b.equals(b));
		assertFalse(c.equals(b));
		assertFalse(a.equals(c));
		assertFalse(b.equals(c));
		assertTrue(c.equals(c));
		assertTrue(a.equals(abc));
		assertTrue(b.equals(abc));
		assertFalse(c.equals(abc));
		assertTrue(abc.equals(a));
		assertTrue(abc.equals(b));
		assertFalse(abc.equals(c));
		
		assertTrue(abc.equals("abc(xyz)"));
	}
	
}
