package jolog;

import jolog.Message;

public interface JologAgent extends Runnable {
   public Message receive(Message message) throws FailedPreconditionException, InterruptedException;
}

