package jolog;

import java.lang.ClassNotFoundException;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ChoicePointState {
   public ChoicePointState(Object[] stateTuple, int backtrackId, String location) throws ProgramFailureException {
      saveState(stateTuple, backtrackId, location);
   }

   public ChoicePointState(Object[] stateTuple, String location) throws ProgramFailureException {
      saveState(stateTuple, 0, location);
   }

   private void saveState(Object[] stateTuple, int backtrackId, String location) throws ProgramFailureException {
//      System.out.println("SAVING STATE");
      try {
         id = backtrackId;
         ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
         ObjectOutputStream oos = new ObjectOutputStream(dataStream);
         oos.writeObject((Object) stateTuple);
         oos.flush();
         oos.close();
         data = dataStream.toByteArray();
      } catch (IOException e) {
         throw new ProgramFailureException("could not save state at "+location);
      }
   }

   public Object[] restoreState(String location) throws ProgramFailureException {
//      System.out.println("RESTORING STATE");
      try {
         ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
         Object[] restoration = (Object[]) ois.readObject();
         ois.close();
         return restoration;
      } catch (Exception e) {
         throw new ProgramFailureException("could not restore state at "+location);
      }
   }

   public int getBacktrackId() {
      return id;
   }

   public void setBacktrackId(int newId) {
      id = newId;
   }

   public void increaseBacktrackId() {
      id++;
   }

   private int id;
   private byte[] data;
   
}

