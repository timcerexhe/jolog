package jolog;

public class Message implements java.io.Serializable {
   private final String content;
   private final JologChannel channel;

   public Message(String message) {
      content = message;
      channel = JologChannel.FUNCTION;
   }

   public Message(String message, JologChannel c) {
      content = message;
      channel = c;
   }

   public String getContent() {
      return content;
   }

   public JologChannel getChannel() {
      return channel;
   }

   public String toString() {
      return content;
   }
}

