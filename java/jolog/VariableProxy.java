package jolog;

public class VariableProxy {
	public VariableProxy() {
		proxy = null;
	}

	public VariableProxy(Object o) {
		proxy = o;
	}

	public boolean equals(Object other) {
		boolean same;
		if (other instanceof VariableProxy) {
			same = proxy.equals(((VariableProxy) other).proxy);
		} else if (proxy == null) {
			proxy = other;
			same = true;
		} else {
			same = proxy.equals(other);
		}
		return same;
	}

	public String toString() {
		return (proxy != null) ? proxy.toString() : "null";
	}

   protected Object getBinding() {
      if (proxy instanceof VariableProxy) {
         return ((VariableProxy) proxy).getBinding();
      } else if (proxy instanceof Variable) {
         return ((Variable) proxy).getBinding();
      } else {
         return proxy;
      }
   }

	private Object proxy;
}

