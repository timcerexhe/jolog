package jolog;

import java.lang.ClassNotFoundException;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class TransientState extends ChoicePointState {
   public TransientState(Object[] stateTuple, String location) throws ProgramFailureException {
      super(null, location);
      saveState(stateTuple, location);
   }

   private void saveState(Object[] stateTuple, String location) {
//      System.out.println("SAVING TRANSIENT STATE");
      data = stateTuple;
   }

   public Object[] restoreState(String location) {
//      System.out.println("RESTORING TRANSIENT STATE");
      return data;
   }

   private Object[] data;
   
}

