package jolog;

import java.util.List;
import java.util.ArrayList;

public final class Predicate {
	public Predicate(String head) {
		this.head = head;
	}

	public void addArgument(Object o) {
		arguments.add(o);
	}

   public void addArgument(int i) {
      arguments.add(new Integer(i));
   }

   public void addArgument(double d) {
      arguments.add(new Double(d));
   }

   public void addArgument(char c) {
      arguments.add(new Character(c));
   }

   public void addArgument(boolean b) {
      arguments.add(new Boolean(b));
   }

	public boolean equals(Object other) {
      //System.out.println("PREDICATE == OBJECT?");
		if (other instanceof Predicate) {
			return this.equals((Predicate) other);
		} else if (other instanceof Variable) {
			return ((Variable) other).equals(this);
		} else if (other instanceof VariableProxy) {
			return ((VariableProxy) other).equals(this);
		} else {
			return toString().equals(other);
		}
	}

   public int hashCode() {
      //System.out.println("DOING PREDICATE HASHCODE");
      return head.hashCode() + arguments.hashCode();
   }

	public boolean equals(Predicate other) {
      //System.out.println("comparing predicate " + head + "/" + arguments.size() + " to " + other.head + "/" + other.arguments.size() + " ...");
		boolean same = (head.equals(other.head));
		same = (same && (arguments.size() == other.arguments.size()));
		int i=0;
		while (same && i < arguments.size()) {
			same = (same && (arguments.get(i).equals(other.arguments.get(i))));
			++i;
		}
      //System.out.println("same = " + same);
		return same;
	}

	public String toString() {
		String s = head;
		if (arguments.size() > 0) { //only print brackets if we have arguments
			s += "(";
			for (int i=0; i < arguments.size(); i++) {
				if (i != 0) {
					s += ", ";
				}
				s += arguments.get(i).toString();
			}
			s += ")";
		}
		return s;
	}

   protected List<Object> getArguments() {
      return arguments;
   }

	private String head;
	private List<Object> arguments = new ArrayList<Object>();

}
