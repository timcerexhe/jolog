package jolog;

import jolog.FailedPreconditionException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class PrintAgent implements JologAgent {
   //private final JologMonitor environment;
   private final Pattern jolog_action1;

   public PrintAgent(JologMonitor environment) {
      //this.environment = environment;
      this.jolog_action1 = Pattern.compile("(\\w+)\\(.*\\)");
   }

   public void run() {
      try {
         System.out.println("countdown:");
         int i = 5;
         while (i > 0) {
            System.out.println("i = "+i);
            --i;
            Thread.sleep(1000);
         }
         System.out.println("all done!");

         if (Thread.interrupted()) {
            Thread.currentThread().interrupt();
            throw new InterruptedException();
         }
      } catch (java.lang.InterruptedException e) {}
   }

   public final Message receive(Message message) throws FailedPreconditionException, InterruptedException {
      if (message != null) {
         Matcher jolog_matcher1 = jolog_action1.matcher(message.getContent());
         if (jolog_matcher1.matches()) { jolog_doPredicate(jolog_matcher1.group(1)); }

         if (true) { System.out.println("AGENT: "+message); }
      }
      return null;
   }

   private void jolog_doPredicate(String predicate) throws FailedPreconditionException, InterruptedException {
      System.out.println("AGENT: predicate type \"" + predicate + "\"");
      Thread.sleep(10000);
   }

}


