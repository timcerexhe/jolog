package jolog;

import jolog.VariableProxy;

public class Variable {
	public Variable() {
		binding = null;
	}

	public void unbind() {
		binding = null;
	}

	public boolean equals(Object other) {
		boolean same;
		if (other instanceof Variable) {
			same = this.equals((Variable) other);
		} else if (binding == null) {
			binding = new VariableProxy(other);
			same = true;
		} else {
			same = binding.equals(other);
		}
		return same;
	}

	public boolean equals(Variable other) {
		boolean same = true;

		//both variables are unbound
		if ((binding == null) && (other.binding == null)) {
			binding = new VariableProxy(); //so create a new proxy
			other.binding = binding; //share the love

		//one of the proxies has been initialised, so share it
		} else if ((binding == null) && (other.binding != null)) {
			binding = other.binding;
		} else if ((binding != null) && (other.binding == null)) {
			other.binding = binding;

		//both variables have DIFFERENT proxies
		} else if ((binding != null) && (other.binding != null) && (binding != other.binding)) {
			same = binding.equals(other.binding);
		}
		return same;
	}

   public Object getBinding() {
      return (binding != null) ? binding.getBinding() : null;
   }

	public String toString() {
		return (binding != null) ? binding.toString() : "null";
	}

	private VariableProxy binding = null;

}


