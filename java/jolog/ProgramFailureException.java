package jolog;

public class ProgramFailureException extends Exception {

   public ProgramFailureException() {
      super("anonymous failed precondition");
   }

   public ProgramFailureException(String message) {
      super(message);
   }

}

